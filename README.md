This is a personal project I've been working on for learning purposes, using OpenGL.

https://www.youtube.com/watch?v=BrTalYoAVNU


![Screenshot](http://i.imgur.com/prdJL3d.png)

![Screenshot](http://i.imgur.com/06upRXE.png)

![Screenshot](http://i.imgur.com/dYJz9GL.png)

[more screenshots](http://imgur.com/a/iU0Wz)

Dependencies :

[Bullet Physics](https://github.com/bulletphysics/bullet3/releases)

[GLFW](https://github.com/glfw/glfw)

[GLEW](https://github.com/nigels-com/glew)

[GLM](http://glm.g-truc.net/0.9.8/index.html)

[NanoVG](https://github.com/memononen/nanovg)

[FreeImage](http://freeimage.sourceforge.net/download.html)
