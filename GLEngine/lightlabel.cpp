#include "lightlabel.h"

LightLabel::LightLabel(GUI& gui, Light &light, LightWidget& lightWidget)
    :Label(gui),
      m_light(light),
      m_lightWidget(lightWidget),
      m_color(light.color())
{
    m_type = Type::LIGHTLABEL;

    m_text = "Light n°" + std::to_string(light.id());
    if(!light.isLocal())
        m_text += " (Directional)";
    else if(light.isSpot())
        m_text += " (Spot)";
    else
        m_text += " (Point)";
}

void LightLabel::draw(NVGcontext* vg)
{
    float x = m_bounds.x()             + m_offsetX;
    float y = m_bounds.y()             + m_offsetY;
    float h = m_bounds.height()        - (2*m_offsetX);
    float w = m_bounds.width()         - (2*m_offsetY);
    NVG_NOTUSED(w);

    nvgFontSize(vg, 18.0f);
    nvgFontFace(vg, "sans");

    glm::vec3 col = glm::normalize(m_color) * glm::vec3(255, 255, 255);


    m_bounds.contains(m_mouse.x(), m_mouse.y()) ? m_isSelectedLight = true : m_isSelectedLight = false;

    m_isSelectedLight ?
        nvgFillColor(vg, nvgRGBA(col.r, col.g, col.b, 160)):
        nvgFillColor(vg, nvgRGBA(col.r, col.g, col.b, 96));

    nvgTextAlign(vg,NVG_ALIGN_MIDDLE);
    nvgText(vg, x, y+h*0.5f, m_text.data(), NULL);
}

void LightLabel::mouseCallback(GLint button, GLint action)
{
    if(action == GLFW_PRESS && button == GLFW_MOUSE_BUTTON_LEFT)
        m_lightWidget.setLight(&m_light);
}
