#include "uifactory.h"
#include "lightpickwidget.h"
#include "modelwidget.h"
#include "console.h"

void UIFactory::createLightWidget(GUI &gui)
{
    Rectangle r(Point(15, 330), Point(385, HEIGHT - 15));
    std::shared_ptr<LightWidget> lightWidget = std::make_shared<LightWidget>(gui, r);

    gui.getElements().insert(std::make_pair("lightWidget", lightWidget));

}

void UIFactory::createModelWidget(GUI &gui)
{
    Rectangle r(Point(400, 15), Point(685, 265));
    std::shared_ptr<ModelWidget> modelWidget = std::make_shared<ModelWidget>(gui, r);

    gui.getElements().insert(std::make_pair("modelWidget", modelWidget));
}

void UIFactory::createConsoleWidget(GUI &gui)
{
    Rectangle r(Point(WIDTH - 400 , 415), Point(WIDTH - 15, HEIGHT - 100));
    std::shared_ptr<Widget> consoleWidget = std::make_shared<Widget>(gui, "Console", r);

    std::shared_ptr<Container> c1 = std::make_shared<Container>(gui);
    std::shared_ptr<Container> c2 = std::make_shared<Container>(gui);

    consoleWidget->addChildren({c1, c2});

    std::shared_ptr<EditBoxBase> consolePrintBox = std::make_shared<EditBoxBase>(gui);
    std::shared_ptr<Paragraph> consolePrint = std::make_shared<Paragraph>(gui, "");


    std::shared_ptr<EditBoxBase> consoleInputBox = std::make_shared<EditBoxBase>(gui);
    std::shared_ptr<Paragraph> consoleInput = std::make_shared<Paragraph>(gui, "input");

    c1->setVertical(true);
    c1->addChild(consolePrintBox);
    c2->addChild(consoleInputBox);

    consolePrint->setOffset(10, 10);
    consolePrintBox->addChild(consolePrint);

    consoleInput->setOffset(10, 10);
    consoleInputBox->addChild(consoleInput);

    gui.console().setOutput(consolePrint.get());
    gui.getElements().insert(std::make_pair("consolePrintBox", consolePrintBox));
    gui.getElements().insert(std::make_pair("consoleParagraph", consolePrint));
    gui.getElements().insert(std::make_pair("consoleInputBox", consoleInputBox));
    gui.getElements().insert(std::make_pair("consoleInput", consoleInput));

    gui.getElements().insert(std::make_pair("consoleWidget", consoleWidget));
    gui.getWidgets().push_back(*consoleWidget);
}

void UIFactory::createPPWidget(GUI &gui)
{
    Rectangle r3(Point(WIDTH - 400 , 15), Point(WIDTH - 15, 400));
    std::shared_ptr<Widget> infoWidget = std::make_shared<Widget>(gui, "PostProcess", r3);

    std::shared_ptr<Button> ppButton1 = std::make_shared<Button>(gui, "Invert");
    std::shared_ptr<Button> ppButton2 = std::make_shared<Button>(gui, "Grayscale");
    std::shared_ptr<Button> ppButton3 = std::make_shared<Button>(gui, "Kernel");
    std::shared_ptr<Button> ppButton4 = std::make_shared<Button>(gui, "Blur");
    std::shared_ptr<Button> ppButton5 = std::make_shared<Button>(gui, "Edge");
    std::shared_ptr<Button> ppButton6 = std::make_shared<Button>(gui, "Cartoon");
    std::shared_ptr<Button> ppButton7 = std::make_shared<Button>(gui, "FXAA");
    ppButton7->press();
    std::shared_ptr<Button> ppButton8 = std::make_shared<Button>(gui, "Gamma");
    ppButton8->press();
    std::shared_ptr<Button> ppButton9 = std::make_shared<Button>(gui, "Other");

    std::shared_ptr<CheckBox> cb1 = std::make_shared<CheckBox>(gui, "Wireframe");
    std::shared_ptr<CheckBox> cb2 = std::make_shared<CheckBox>(gui, "Normals");
    std::shared_ptr<CheckBox> cb3 = std::make_shared<CheckBox>(gui, "Lights");

    std::shared_ptr<Label> label = std::make_shared<Label>(gui, "Deferred Debug");
    std::shared_ptr<CheckBox> cb4 = std::make_shared<CheckBox>(gui, "Normals");
    std::shared_ptr<CheckBox> cb5 = std::make_shared<CheckBox>(gui, "Ambient");
    std::shared_ptr<CheckBox> cb6 = std::make_shared<CheckBox>(gui, "Reflected");
    std::shared_ptr<CheckBox> cb7 = std::make_shared<CheckBox>(gui, "Positions");
    std::shared_ptr<CheckBox> cb8 = std::make_shared<CheckBox>(gui, "Color");
    std::shared_ptr<CheckBox> cb9 = std::make_shared<CheckBox>(gui, "Shadows");
    std::shared_ptr<CheckBox> cb10 = std::make_shared<CheckBox>(gui, "Depth");
    std::shared_ptr<CheckBox> cb11 = std::make_shared<CheckBox>(gui, "Bloom");
    std::shared_ptr<CheckBox> cb12 = std::make_shared<CheckBox>(gui, "Scaterred");



    infoWidget->addChildren({
                                (std::make_shared<Container>(gui))->insertChildren({cb1, cb2, cb3}),
                                (std::make_shared<Container>(gui))->insertChildren({ppButton1, ppButton2, ppButton3}),
                                (std::make_shared<Container>(gui))->insertChildren({ppButton4, ppButton5, ppButton6}),
                                (std::make_shared<Container>(gui))->insertChildren({ppButton7, ppButton8, ppButton9}),
                                (std::make_shared<Container>(gui))->insertChild(label),
                                (std::make_shared<Container>(gui))->insertChildren({cb4, cb5, cb6}),
                                (std::make_shared<Container>(gui))->insertChildren({cb7, cb8, cb9}),
                                (std::make_shared<Container>(gui))->insertChildren({cb10, cb11, cb12})
                            });

    gui.getElements().insert(std::make_pair("invertButton", ppButton1));
    gui.getElements().insert(std::make_pair("grayscaleButton", ppButton2));
    gui.getElements().insert(std::make_pair("kernelButton", ppButton3));
    gui.getElements().insert(std::make_pair("blurButton", ppButton4));
    gui.getElements().insert(std::make_pair("edgeButton", ppButton5));
    gui.getElements().insert(std::make_pair("cartoonButton", ppButton6));
    gui.getElements().insert(std::make_pair("fxaaButton", ppButton7));
    gui.getElements().insert(std::make_pair("gammaButton", ppButton8));
    gui.getElements().insert(std::make_pair("otherButton2", ppButton9));
    gui.getElements().insert(std::make_pair("wireMode", cb1));
    gui.getElements().insert(std::make_pair("normalMode", cb2));
    gui.getElements().insert(std::make_pair("lightsMode", cb3));
    gui.getElements().insert(std::make_pair("deferredLabel", label));
    gui.getElements().insert(std::make_pair("deferredNormals", cb4));
    gui.getElements().insert(std::make_pair("deferredAmbient", cb5));
    gui.getElements().insert(std::make_pair("deferredReflected", cb6));
    gui.getElements().insert(std::make_pair("deferredPositions", cb7));
    gui.getElements().insert(std::make_pair("deferredColor", cb8));
    gui.getElements().insert(std::make_pair("deferredShadows", cb9));
    gui.getElements().insert(std::make_pair("deferredDepth", cb10));
    gui.getElements().insert(std::make_pair("deferredBloom", cb11));
    gui.getElements().insert(std::make_pair("deferredScaterred", cb12));


    gui.getElements().insert(std::make_pair("ppWidget", infoWidget));
    gui.getWidgets().push_back(*infoWidget);
}

void UIFactory::createInfoWidget(GUI &gui)
{
    Scene& sce = gui.scene();

    Point p1(15, 15);
    int width = 370;
    int height = 300;
    Point p2(p1.x() + width, p1.y() + height);
    Rectangle r(p1, p2);
    std::shared_ptr<Widget> widg = std::make_shared<Widget>(gui, "Info", r);

    std::shared_ptr<Label> label4 = std::make_shared<Label>(gui, "Camera Position :");
    std::shared_ptr<EditBoxNum> eBox1 = std::make_shared<EditBoxNum>(gui, "Editbox 1", "x", &(sce.camera().getPos().x));
    std::shared_ptr<EditBoxNum> eBox2 = std::make_shared<EditBoxNum>(gui, "Editbox 2", "y", &(sce.camera().getPos().y));
    std::shared_ptr<EditBoxNum> eBox3 = std::make_shared<EditBoxNum>(gui, "Editbox 3", "z", &(sce.camera().getPos().z));

    eBox1->setCanEdit(true);
    eBox2->setCanEdit(true);
    eBox3->setCanEdit(true);

    std::shared_ptr<Label> label5 = std::make_shared<Label>(gui, "Camera Direction :");
    std::shared_ptr<EditBoxNum> eBox4 = std::make_shared<EditBoxNum>(gui, "Editbox 4", "x", &(sce.camera().getFront().x));
    std::shared_ptr<EditBoxNum> eBox5 = std::make_shared<EditBoxNum>(gui, "Editbox 5", "y", &(sce.camera().getFront().y));
    std::shared_ptr<EditBoxNum> eBox6 = std::make_shared<EditBoxNum>(gui, "Editbox 6", "z", &(sce.camera().getFront().z));

    std::shared_ptr<Label> label6 = std::make_shared<Label>(gui, "Mouse Hit :");
    std::shared_ptr<EditBoxNum> editBox1 = std::make_shared<EditBoxNum>(gui, "Editbox 1", "x", &(sce.mousePicker().hitPos().m_floats[0]));
    std::shared_ptr<EditBoxNum> editBox2 = std::make_shared<EditBoxNum>(gui, "Editbox 2", "y", &(sce.mousePicker().hitPos().m_floats[1]));
    std::shared_ptr<EditBoxNum> editBox3 = std::make_shared<EditBoxNum>(gui, "Editbox 3", "z", &(sce.mousePicker().hitPos().m_floats[2]));

    widg->addChildren({
                          (std::make_shared<Container>(gui))->insertChild(label4),
                          (std::make_shared<Container>(gui))->insertChildren({eBox1, eBox2, eBox3}),
                          (std::make_shared<Container>(gui))->insertChild(label5),
                          (std::make_shared<Container>(gui))->insertChildren({eBox4, eBox5, eBox6}),
                          (std::make_shared<Container>(gui))->insertChild(label6),
                          (std::make_shared<Container>(gui))->insertChildren({editBox1, editBox2, editBox3})
                      });

    gui.getElements().insert(std::make_pair("label4", label4));
    gui.getElements().insert(std::make_pair("eBox1", eBox1));
    gui.getElements().insert(std::make_pair("eBox2", eBox2));
    gui.getElements().insert(std::make_pair("eBox3", eBox3));
    gui.getElements().insert(std::make_pair("label5", label5));
    gui.getElements().insert(std::make_pair("eBox4", eBox4));
    gui.getElements().insert(std::make_pair("eBox5", eBox5));
    gui.getElements().insert(std::make_pair("eBox6", eBox6));
    gui.getElements().insert(std::make_pair("label5", label6));
    gui.getElements().insert(std::make_pair("editBox1", editBox1));
    gui.getElements().insert(std::make_pair("editBox2", editBox2));
    gui.getElements().insert(std::make_pair("editBox3", editBox3));


    gui.getWidgets().push_back(*widg);
    gui.getElements().insert(std::make_pair("widget", widg));
}

void UIFactory::createMenuWidget(GUI &gui)
{
    std::shared_ptr<Menu> tmpMenu = std::make_shared<Menu>(gui, Rectangle(Point(0, 0), Point(WIDTH, Widget::HEADERHEIGHT)));
    gui.getElements().insert(std::make_pair("menu", tmpMenu));

}

void UIFactory::createGUIWidget(GUI &gui)
{
    Point p1(WIDTH - 310, HEIGHT - 85);
    Point p2(p1.x() + 100, p1.y() + 70);
    Rectangle r(p1, p2);

    std::shared_ptr<Widget> widg = std::make_shared<Widget>(gui, "GUI", r);

    std::shared_ptr<CheckBox> cb = std::make_shared<CheckBox>(gui, "GUI");
    cb->check();

    widg->addChild({
                      (std::make_shared<Container>(gui))->insertChild(cb)
                      });

    gui.getWidgets().push_back(*widg);
    gui.getElements().insert(std::make_pair("guiCheckBox", cb));
    gui.getElements().insert(std::make_pair("guiWidget", widg));
}
