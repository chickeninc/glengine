#ifndef SEARCHBOX_H
#define SEARCHBOX_H

#include "uielement.h"
#include <string>

class SearchBox : public UIElement
{

public:

    SearchBox(GUI& gui, Rectangle r, std::string text);

    inline std::string text() const
    {
        return m_text;
    }

    virtual void draw(NVGcontext *vg);

private:

    std::string m_text;
};

#endif // SEARCHBOX_H
