#ifndef SLIDER_H
#define SLIDER_H

#include "widget.h"

class Slider : public UIElement
{

public:

    Slider(GUI& gui, float min, float max);
    Slider(GUI& gui, float position, float min, float max);

    void draw(NVGcontext* vg) override;
    void cursorCallback(double xpos, double ypos) override;

    inline float position() const
    {
        return m_position;
    }

    inline float& position()
    {
        return m_position;
    }

    inline float maxValue() const
    {
        return m_maxValue;
    }

    inline float minValue() const
    {
        return m_minValue;
    }

    inline void setPosition(const float position)
    {
        if(position >= 0 && position <= 1)
        {
            m_position = position;
            m_value = m_position * m_maxValue;
        }
    }

    inline void setMaxValue(const float max)
    {
        m_maxValue = max;
    }

    inline void setMinValue(const float min)
    {
        m_minValue = min;
    }

    inline float getValue() const
    {
        return m_position * m_value;
    }

    inline float& getValue()
    {
        return m_value;
    }

    inline void setAffectedValue(float* value)
    {
        m_affectedValue = value;
    }

private:
    float* m_affectedValue = nullptr;
    float m_position = 0.5f;
    float m_maxValue = 1.0f;
    float m_minValue = 0.0f;
    float m_value = 0.0f;
};

#endif // SLIDER_H
