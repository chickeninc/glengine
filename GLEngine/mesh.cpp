#include "mesh.h"
#define STB_PERLIN_IMPLEMENTATION
#include "stb_perlin.h"
Mesh::Mesh()
{

}

Mesh::Mesh(btCollisionShape* shape)
{

}

bool Mesh::loadOBJ(const char * path){
    std::cout << "Loading OBJ file "<< path << "..." << std::endl;

    std::vector<unsigned int> vertexIndices, uvIndices, normalIndices;
    std::vector<glm::vec3> temp_vertices;
    std::vector<glm::vec2> temp_uvs;
    std::vector<glm::vec3> temp_normals;


    FILE * file = fopen(path, "r");
    if( file == NULL ){
        std::cout << "Can't open file !" << std::endl;
        getchar();
        return false;
    }

    while( 1 ){

        char lineHeader[128];
        // read the first word of the line
        int res = fscanf(file, "%s", lineHeader);
        if (res == EOF)
            break; // EOF = End Of File. Quit the loop.

        // else : parse lineHeader
        if ( strcmp( lineHeader, "v" ) == 0 ){
            glm::vec3 vertex = glm::vec3();
            fscanf(file, "%f %f %f\n", &vertex.x, &vertex.y, &vertex.z);
            temp_vertices.push_back(vertex);
        }else if ( strcmp( lineHeader, "vt" ) == 0 ){
            glm::vec2 uv = glm::vec2();
            fscanf(file, "%f %f\n", &uv.x, &uv.y);
            temp_uvs.push_back(uv);
        }else if ( strcmp( lineHeader, "vn" ) == 0 ){
            glm::vec3 normal = glm::vec3();
            fscanf(file, "%f %f %f\n", &normal.x, &normal.y, &normal.z);
            temp_normals.push_back(normal);
        }else if ( strcmp( lineHeader, "f" ) == 0 ){
            std::string vertex1, vertex2, vertex3;
            unsigned int vertexIndex[3], uvIndex[3], normalIndex[3];
            int matches = fscanf(file, "%d/%d/%d %d/%d/%d %d/%d/%d\n", &vertexIndex[0], &uvIndex[0], &normalIndex[0], &vertexIndex[1], &uvIndex[1], &normalIndex[1], &vertexIndex[2], &uvIndex[2], &normalIndex[2] );
            if (matches != 9){
                printf("File can't be read : Try exporting with other options\n");
                return false;
            }
            vertexIndices.push_back(vertexIndex[0]);
            vertexIndices.push_back(vertexIndex[1]);
            vertexIndices.push_back(vertexIndex[2]);
            uvIndices    .push_back(uvIndex[0]);
            uvIndices    .push_back(uvIndex[1]);
            uvIndices    .push_back(uvIndex[2]);
            normalIndices.push_back(normalIndex[0]);
            normalIndices.push_back(normalIndex[1]);
            normalIndices.push_back(normalIndex[2]);

        }else{
            // Probably a comment, eat up the rest of the line
            char junkBuffer[1000];
            fgets(junkBuffer, 1000, file);
        }

    }

    size = vertexIndices.size();


    for(int i = 0; i < vertexIndices.size(); i++){

        // Get the indices of its attributes
        unsigned int vertexIndex = vertexIndices[i];
        unsigned int uvIndex = uvIndices[i];
        unsigned int normalIndex = normalIndices[i];

        Vertex v;
        v.position = temp_vertices[ vertexIndex-1 ];
        v.normals = temp_normals[ normalIndex-1 ];
        v.uvs = temp_uvs[ uvIndex-1 ];


        vertices.push_back(v);
        indices.push_back(i);
    }


    for(Vertex v : vertices) {
        btScalar x  = btScalar(v.position.x);
        btScalar y  = btScalar(v.position.y);
        btScalar z  = btScalar(v.position.z);
        shape.push_back(btVector3(x, y ,z));
    }
    std::cout << "shape size : " << sizeof(shape) << std::endl;

    for(int i = 0; i < (int)indices.size()/3; i++) {
        Vertex* vertex0 = &vertices[(3*i)];
        Vertex* vertex1 = &vertices[(3*i) + 1];
        Vertex* vertex2 = &vertices[(3*i) + 2];

        glm::vec3 deltaPos1 = vertex1->position - vertex0->position;
        glm::vec3 deltaPos2 = vertex2->position - vertex0->position;

        glm::vec2 deltaUv1 = vertex1->uvs - vertex0->uvs;
        glm::vec2 deltaUv2 = vertex2->uvs - vertex0->uvs;

        float f = 1 / ((deltaUv1.x * deltaUv2.y) - (deltaUv1.y * deltaUv2.x));

        glm::vec3 tangent;
        tangent.x = f * (deltaUv2.y * deltaPos1.x - deltaUv1.y * deltaPos2.x);
        tangent.y = f * (deltaUv2.y * deltaPos1.y - deltaUv1.y * deltaPos2.y);
        tangent.z = f * (deltaUv2.y * deltaPos1.z - deltaUv1.y * deltaPos2.z);

        glm::vec3 bitangent;
        bitangent.x = f * (-deltaUv2.x * deltaPos1.x - deltaUv1.x * deltaPos2.x);
        bitangent.y = f * (-deltaUv2.x * deltaPos1.y - deltaUv1.x * deltaPos2.y);
        bitangent.z = f * (-deltaUv2.x * deltaPos1.z - deltaUv1.x * deltaPos2.z);

        vertex0->tangent = tangent;
        vertex0->bitangent = bitangent;

        vertex1->tangent = tangent;
        vertex1->bitangent = bitangent;

        vertex2->tangent = tangent;
        vertex2->bitangent = bitangent;
    }

    std::cout << "Indice count : " << indices.size() << std::endl;
    return true;
}

void Mesh::createTerrain()
{
    int imgw, imgh, n;

    const char* path = "/home/lebasith/Git/cpp/GLEngine/assets/textures/heightmap.png";
    unsigned char* img = stbi_load(path ,
                                   &imgw, &imgh, &n, 0);

    if(img != NULL){
        std::cout << path << " loaded \n" << "width : " << imgw << " height : " << imgh << " n :" << n << std::endl;

    } else {
        std::cout << "Couldnt load image" << path << std::endl;
    }

    for(int i = 0; i < imgw; i++) {
        for(int j = 0; j < imgh; j++){
            std::cout << (uint) img[i*j] << std::endl;
        }
    }

}

void Mesh::createPlane(float sideSize)
{
    Vertex v1,v2,v3,v4,v5,v6;

    float half = sideSize/2.0f;
    for(float j = 0; j < sideSize; j++){
        for(float i = 0; i < sideSize; i++){
            v1 = Vertex();
            v2 = Vertex();
            v3 = Vertex();
            v4 = Vertex();
            v5 = Vertex();
            v6 = Vertex();

            v1.position = glm::vec3(i + 1.0f-half , 0.0, j+1.0f-half);
            v2.position = glm::vec3(i + 1.0f-half , 0.0f, j-half)     ;
            v3.position = glm::vec3(i - half, 0.0f, j+1.0f-half)      ;
            v4.position = glm::vec3(i - half, 0.0f, j+1.0f-half)      ;
            v5.position = glm::vec3(i + 1.0f-half , 0.0f, j-half)     ;
            v6.position = glm::vec3(i - half      , 0.0f, j-half)     ;

            v1.normals = glm::vec3(0.0f, 1.0f, 0.0f);
            v2.normals = glm::vec3(0.0f, 1.0f, 0.0f);
            v3.normals = glm::vec3(0.0f, 1.0f, 0.0f);
            v4.normals = glm::vec3(0.0f, 1.0f, 0.0f);
            v5.normals = glm::vec3(0.0f, 1.0f, 0.0f);
            v6.normals = glm::vec3(0.0f, 1.0f, 0.0f);

            v1.uvs = glm::vec2( 0.0f, 0.0f);
            v2.uvs = glm::vec2( 1.0f, 0.0f);
            v3.uvs = glm::vec2( 0.0f, 1.0f);
            v4.uvs = glm::vec2( 0.0f, 1.0f);
            v5.uvs = glm::vec2( 1.0f, 0.0f);
            v6.uvs = glm::vec2( 1.0f, 1.0f);

            vertices.push_back(v1);
            vertices.push_back(v2);
            vertices.push_back(v3);
            vertices.push_back(v4);
            vertices.push_back(v5);
            vertices.push_back(v6);

        }
    }

    for(int i = 0; i < vertices.size(); i++){
        indices.push_back(i);
    }

    for(int i = 0; i < (int)indices.size()/3; i++) {
        Vertex* vertex0 = &vertices[(3*i)];
        Vertex* vertex1 = &vertices[(3*i) + 1];
        Vertex* vertex2 = &vertices[(3*i) + 2];

        glm::vec3 deltaPos1 = vertex1->position - vertex0->position;
        glm::vec3 deltaPos2 = vertex2->position - vertex0->position;

        glm::vec2 deltaUv1 = vertex1->uvs - vertex0->uvs;
        glm::vec2 deltaUv2 = vertex2->uvs - vertex0->uvs;

        float f = 1 / ((deltaUv1.x * deltaUv2.y) - (deltaUv1.y * deltaUv2.x));

        glm::vec3 tangent;
        tangent.x = f * (deltaUv2.y * deltaPos1.x - deltaUv1.y * deltaPos2.x);
        tangent.y = f * (deltaUv2.y * deltaPos1.y - deltaUv1.y * deltaPos2.y);
        tangent.z = f * (deltaUv2.y * deltaPos1.z - deltaUv1.y * deltaPos2.z);

        glm::vec3 bitangent;
        bitangent.x = f * (-deltaUv2.x * deltaPos1.x - deltaUv1.x * deltaPos2.x);
        bitangent.y = f * (-deltaUv2.x * deltaPos1.y - deltaUv1.x * deltaPos2.y);
        bitangent.z = f * (-deltaUv2.x * deltaPos1.z - deltaUv1.x * deltaPos2.z);

        vertex0->tangent = tangent;
        vertex0->bitangent = bitangent;

        vertex1->tangent = tangent;
        vertex1->bitangent = bitangent;

        vertex2->tangent = tangent;
        vertex2->bitangent = bitangent;
    }

}
