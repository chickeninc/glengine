#ifndef GUI_H
#define GUI_H


#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include "nanovg.h"
#include "widget.h"
#include "slider.h"
#include "lightlabel.h"
#include "button.h"
#include "rectangle.h"
#include "checkbox.h"
#include "colorwheel.h"
#include "editbox.h"
#include "dropdown.h"
#include "searchbox.h"
#include "paragraph.h"
#include "menu.h"
#include "mouse.h"
#include <iostream>
#include <memory>
#include <map>
#include "callbacks.h"
#include "scene.h"
#include "uifactory.h"
#include "lightwidget.h"
#include "modelwidget.h"
#include <stdio.h>
#include <string.h>
#include <math.h>
#include "nanovg.h"
#include <memory>
#include <iostream>
#include "stb_image_write.h"
#include "console.h"


static constexpr GLuint WIDTH = Scene::WIDTH;
static constexpr GLuint HEIGHT = Scene::HEIGHT;

class Scene;

class GUI : public Callbacks
{
    using Child = Container::Child;
    using Children = Container::Children;

public:

    GUI(NVGcontext* vg, Rectangle& screen, Mouse& mouse, Scene& scene);

    void mouseCallback(int button, int action) override;
    void cursorCallback(double xpos, double ypos) override;
    void framebufferSizeCallback() override;
    void scrollCallback(double xoffset, double yoffset) override;

    void dispatchCursorCallback(std::map<std::string, std::shared_ptr<UIElement>> elems);
    void drawElements(std::vector<std::shared_ptr<UIElement>> elements);

    void drawThumbnails(NVGcontext* vg, float x, float y, float w, float h, const int* images, int nimages, float t);
    void drawSpinner(NVGcontext* vg, float cx, float cy, float r, float t);
    void drawGraph(NVGcontext* vg, float x, float y, float w, float h, float t);

    int loadDemoData();
    void freeDemoData();
    void renderDemo(float t);

    inline std::map<std::string, std::shared_ptr<UIElement>>& getElements()
    {
        return m_elements;
    }

    inline std::vector<std::reference_wrapper<Widget>>& getWidgets()
    {
        return m_widgets;
    }

    inline Rectangle& screen() const
    {
        return m_screen;
    }

    inline Mouse& mouse() const
    {
        return m_mouse;
    }

    inline Scene& scene() const
    {
        return m_scene;
    }

    inline void setTime(const float t)
    {
        m_t = t;
    }

    inline NVGcontext* nvgContext()
    {
        return m_vg;
    }

    inline Console& console()
    {
        return m_console;
    }

private:

    std::map<std::string, std::shared_ptr<UIElement>> m_elements;
    std::vector<std::reference_wrapper<Widget>> m_widgets;

    Mouse& m_mouse;
    Rectangle& m_screen;
    NVGcontext* m_vg = nullptr;
    GLfloat m_t;
    Scene& m_scene;
    MousePicker& m_mousePicker;
    Console m_console;

    DemoData m_data;

    Widget* m_lockedWidget = nullptr;
    UIElement* m_lockedElement = nullptr;

};


#endif // GUI_H
