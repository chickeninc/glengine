#ifndef INDEXBUFFER_H
#define INDEXBUFFER_H

#include "GL/glew.h"

class IndexBuffer
{
public:
    IndexBuffer();

    inline GLuint id() const
    {
        return m_id;
    }

    inline void bind() const
    {
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_id);
    }

private:

    GLuint m_id = 0;
};

#endif // INDEXBUFFER_H
