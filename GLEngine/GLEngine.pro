TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle


SOURCES += main.cpp \
    shader.cpp \
    camera.cpp \
    model.cpp \
    scene.cpp \
    light.cpp \
    material.cpp \
    nanovg/example/perf.c \
    nanovg/example/demo.c \
    gui.cpp \
    rectangle.cpp \
    uielement.cpp \
    widget.cpp \
    slider.cpp \
    label.cpp \
    point.cpp \
    button.cpp \
    colorwheel.cpp \
    checkbox.cpp \
    searchbox.cpp \
    dropdown.cpp \
    editbox.cpp \
    paragraph.cpp \
    container.cpp \
    mouse.cpp \
    controller.cpp \
    texture.cpp \
    cubemaptexture.cpp \
    framebuffer.cpp \
    physics.cpp \
    instance.cpp \
    bullet/examples/ThirdPartyLibs/Wavefront/tiny_obj_loader.cpp \
    mousepicker.cpp \
    menu.cpp \
    callbackmanager.cpp \
    mesh.cpp \
    meshinfo.cpp \
    renderer.cpp \
    gbuffer.cpp \
    shadowmap.cpp \
    lightwidget.cpp \
    uifactory.cpp \
    lightlabel.cpp \
    lightpickwidget.cpp \
    assetmanager.cpp \
    bullet/Extras/Serialize/BulletWorldImporter/btBulletWorldImporter.cpp \
    bullet/Extras/Serialize/BulletWorldImporter/btWorldImporter.cpp \
    modelwidget.cpp \
    modelloader.cpp \
    console.cpp \
    gameobject.cpp \
    gamecomponent.cpp \
    transform.cpp \
    shaderprogram.cpp \
    indexbuffer.cpp \
    vertexbuffer.cpp \
    shaderloader.cpp \
    gldebugdrawer.cpp

INCLUDEPATH += \
    glew/include/ \
    glfw/include/ \
    $$PWD/nanovg/src/ \
    $$PWD/nanovg/example/ \
    glm/glm/ \
    /home/lebasith/Programmation/Cpp/BulletPhysics/Extras/Serialize/BulletWorldImporter/ \

DEPENDPATH += $$PWD/nanovg/build

LIBS += -lGLEW \
    -lglfw \
    -L$$PWD/nanovg/build/ -lnanovg \
    -lfreeimage \
    -lassimp \

DISTFILES += \
    shaders/skybox.frag \
    shaders/skybox.vert \
    assets/negx.jpg \
    shaders/postprocess.frag \
    shaders/postprocess.vert \
    shaders/ppinvert.frag \
    shaders/ppgrayscale.frag \
    shaders/ppkernel.frag \
    shaders/ppblur.frag \
    shaders/ppedge.frag \
    shaders/passthrough.geom \
    shaders/passthrough.frag \
    shaders/passthrough.vert \
    shaders/ppfractal.frag \
    assets/testlaplacien.png \
    shaders/shadow.frag \
    shaders/shadow.vert \
    shaders/gbuffer.frag \
    shaders/gbuffer.vert \
    shaders/lightpass.frag \
    shaders/lightpass.vert \
    shaders/shadowcube.frag \
    shaders/shadowcube.vert \
    shaders/shadowcube.geom \
    shaders/lightgeometry.frag \
    shaders/lightgeometry.geom \
    shaders/lightgeometry.vert \
    shaders/ppcartoon.frag \
    shaders/ppfxaa.frag \
    shaders/ppgaussianblur.frag \
    shaders/pptonemapping.frag \
    shaders/ppscatter.frag \
    shaders/pbrlightpass.frag \
    assets/textures/coper_normal.png \
    shaders/lightsspheres.vert \
    shaders/lightsspheres.frag \
    shaders/lightquad.frag \
    shaders/lightquad.vert \
    shaders/stencilpass.vert \
    shaders/stencilpass.frag \
    shaders/bloomfilter.frag \
    shaders/convertskybox.frag \
    shaders/convertskybox.vert \
    shaders/lightambient.frag \
    shaders/lightambient.vert \
    shaders/lighting.glsl \
    shaders/cubemapconvolution.frag \
    shaders/cubemapconvolution.vert \
    shaders/prefiltercubemap.frag \
    shaders/prefiltercubemap.vert \
    shaders/brdf.frag \
    shaders/gbuffer.tcs \
    shaders/gbuffer.tes \
    shaders/geomtess.tcs \
    shaders/geomtess.tes \
    shaders/geomtess2.tcs \
    shaders/geomtess2.tes \
    shaders/debug.frag \
    shaders/debug.vert

HEADERS += \
    shader.h \
    camera.h \
    model.h \
    scene.h \
    stb_image.h \
    light.h \
    material.h \
    nanovg/example/perf.h \
    nanovg/example/demo.h \
    gui.h \
    rectangle.h \
    uielement.h \
    widget.h \
    slider.h \
    label.h \
    point.h \
    button.h \
    colorwheel.h \
    checkbox.h \
    searchbox.h \
    dropdown.h \
    editbox.h \
    paragraph.h \
    container.h \
    mouse.h \
    controller.h \
    texture.h \
    callbacks.h \
    cubemaptexture.h \
    framebuffer.h \
    physics.h \
    instance.h \
    bullet/examples/ThirdPartyLibs/Wavefront/tiny_obj_loader.h \
    bullet/examples/Importers/ImportObjDemo/Wavefront2GLInstanceGraphicsShape.h \
    mousepicker.h \
    menu.h \
    stb_perlin.h \
    callbackmanager.h \
    postprocess.h \
    mesh.h \
    meshinfo.h \
    renderable.h \
    renderer.h \
    gbuffer.h \
    listenable.h \
    listener.h \
    uifactory.h \
    shadowmap.h \
    lightwidget.h \
    lightlabel.h \
    lightpickwidget.h \
    assetmanager.h \
    bullet/Extras/Serialize/BulletWorldImporter/btBulletWorldImporter.h \
    bullet/Extras/Serialize/BulletWorldImporter/btWorldImporter.h \
    modelwidget.h \
    modelloader.h \
    console.h \
    gameobject.h \
    gamecomponent.h \
    transform.h \
    shaderprogram.h \
    indexbuffer.h \
    vertexbuffer.h \
    debugger.h \
    shaderloader.h \
    gldebugdrawer.h

unix: CONFIG += link_pkgconfig
unix: PKGCONFIG += bullet

unix: LIBS += -L$$PWD/bullet/Extras/Serialize/BulletWorldImporter/ -lBulletWorldImporter

INCLUDEPATH += $$PWD/bullet/Extras/Serialize/BulletWorldImporter
DEPENDPATH += $$PWD/bullet/Extras/Serialize/BulletWorldImporter

unix: PRE_TARGETDEPS += $$PWD/bullet/Extras/Serialize/BulletWorldImporter/libBulletWorldImporter.a
unix: LIBS += -L$$PWD/bullet/Extras/Serialize/BulletFileLoader/ -lBulletFileLoader

INCLUDEPATH += $$PWD/bullet/Extras/Serialize/BulletFileLoader
DEPENDPATH += $$PWD/bullet/Extras/Serialize/BulletFileLoader
unix: PRE_TARGETDEPS += $$PWD/bullet/Extras/Serialize/BulletFileLoader/libBulletFileLoader.a


