#ifndef CUBEMAPTEXTURE_H
#define CUBEMAPTEXTURE_H


#include "texture.h"
#include <vector>



class CubeMapTexture : public Texture
{
public:
    CubeMapTexture();
    CubeMapTexture(std::vector<const char*> pathfaces);

    void Use() const override;
};

#endif // CUBEMAPTEXTURE_H
