#ifndef GAMECOMPONENT_H
#define GAMECOMPONENT_H

#include "shaderprogram.h"
#include "GL/gl.h"

class GameComponent
{
public:
    GameComponent();

    virtual void render(ShaderProgram program, GLenum mode) = 0;
};

#endif // GAMECOMPONENT_H
