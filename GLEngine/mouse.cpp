#include "mouse.h"

Mouse::Mouse()
{

}

void Mouse::setX(float x)
{
    m_deltaX = x - m_x;
    m_lastX = m_x;
    m_x = x;
}

void Mouse::setY(float y)
{
    m_deltaY = y - m_y;
    m_lastY = m_y;
    m_y = y;
}

void Mouse::update(float x, float y)
{
    m_deltaX = x - m_x;
    m_deltaY = y - m_y;

    m_lastX = m_x;
    m_lastY = m_y;

    setX(x);
    setY(y);
}
