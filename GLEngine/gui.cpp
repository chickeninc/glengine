#include "gui.h"

#define ICON_SEARCH 0x1F50D
#define ICON_CIRCLED_CROSS 0x2716
#define ICON_CHEVRON_RIGHT 0xE75E
#define ICON_CHECK 0x2713
#define ICON_LOGIN 0xE740
#define ICON_TRASH 0xE729


//static float minf(float a, float b) { return a < b ? a : b; }
static float maxf(float a, float b) { return a > b ? a : b; }
//static float absf(float a) { return a >= 0.0f ? a : -a; }
static float clampf(float a, float mn, float mx) { return a < mn ? mn : (a > mx ? mx : a); }

// Returns 1 if col.rgba is 0.0f,0.0f,0.0f,0.0f, 0 otherwise
int isBlack(NVGcolor col)
{
    if( col.r == 0.0f && col.g == 0.0f && col.b == 0.0f && col.a == 0.0f )
    {
        return 1;
    }
    return 0;
}

static char* cpToUTF8(int cp, char* str)
{

    int n = 0;
    if (cp < 0x80) n = 1;
    else if (cp < 0x800) n = 2;
    else if (cp < 0x10000) n = 3;
    else if (cp < 0x200000) n = 4;
    else if (cp < 0x4000000) n = 5;
    else if (cp <= 0x7fffffff) n = 6;
    str[n] = '\0';
    switch (n) {
    case 6: str[5] = 0x80 | (cp & 0x3f); cp = cp >> 6; cp |= 0x4000000;
    case 5: str[4] = 0x80 | (cp & 0x3f); cp = cp >> 6; cp |= 0x200000;
    case 4: str[3] = 0x80 | (cp & 0x3f); cp = cp >> 6; cp |= 0x10000;
    case 3: str[2] = 0x80 | (cp & 0x3f); cp = cp >> 6; cp |= 0x800;
    case 2: str[1] = 0x80 | (cp & 0x3f); cp = cp >> 6; cp |= 0xc0;
    case 1: str[0] = cp;
    }
    return str;
}


GUI::GUI(NVGcontext* vg, Rectangle& screen, Mouse& mouse, Scene& scene)
    :m_vg(vg),
     m_screen(screen),
     m_mouse(mouse),
     m_scene(scene),
     m_mousePicker(scene.mousePicker())
{
    if(!loadDemoData() == -1)
    {
        std::cout << "Couldnt load demo data." << std::endl;
    }

    UIFactory::createPPWidget(*this);
    UIFactory::createLightWidget(*this);
    UIFactory::createInfoWidget(*this);
    UIFactory::createConsoleWidget(*this);
    UIFactory::createGUIWidget(*this);
    UIFactory::createModelWidget(*this);

    framebufferSizeCallback();
}

void GUI::drawElements(std::vector<std::shared_ptr<UIElement>> elements)
{
    for(auto & e : elements){
        if(e->isVisible())
        {
            switch (e->type()) {
            case EDITBOXBASE:
                std::static_pointer_cast<EditBoxBase>(e)->draw(m_vg);
                drawElements(std::static_pointer_cast<EditBoxBase>(e)->children());
                break;
            case CONTAINER:
                drawElements(std::static_pointer_cast<Container>(e)->children());
            default:
                e->draw(m_vg);
                break;
            }
        }
    }
}

void GUI::renderDemo(float t)
{
    m_t = t;

    nvgSave(m_vg);
    if(std::static_pointer_cast<CheckBox>(m_elements.at("guiCheckBox"))->isChecked())
    {
    for(Widget& w : m_widgets) {
        w.draw(m_vg);
        drawElements(w.children());
    }
    } else
    {
        std::shared_ptr<Widget> w = std::static_pointer_cast<Widget>(m_elements.at("guiWidget"));
        w->draw(m_vg);
        drawElements(w->children());
    }



    //drawThumbnails(m_vg, 365, 30, 160, 300, m_data.images, 12, t);
    //drawGraph(m_vg, 500, 500, 100, 100, m_t);
    nvgRestore(m_vg);

}

void GUI::mouseCallback(int button, int action)
{
    if (button == GLFW_MOUSE_BUTTON_MIDDLE)
        std::static_pointer_cast<Paragraph>(m_elements.at("consoleParagraph"))->addText("test");

    for(Widget& widget : m_widgets)
    {
        if(widget.bounds().contains(m_mouse.x(), m_mouse.y()))
        {
            widget.mouseCallback(button, action);
        }

        if(widget.header().contains(m_mouse.x(), m_mouse.y()) && action == GLFW_PRESS && button == GLFW_MOUSE_BUTTON_LEFT)
            m_lockedWidget = &widget;

    }

    for(auto & e : m_elements){
        if(e.second->type() != Type::WIDGET){
            if(e.second->bounds().contains(m_mouse.x(), m_mouse.y()) && action == GLFW_PRESS) {
                m_lockedElement = e.second.get();
                e.second->lockMouse();
            }

        }
    }
/*
    if(m_lockedWidget) {
        if(action == GLFW_RELEASE) {
            if(m_lockedWidget->overlaps(*std::static_pointer_cast<Menu>(m_elements.at("menu")))) {
                if(m_lockedWidget->header().overlapArea(m_elements.at("menu")->bounds()) >= (m_lockedWidget->header().getArea()/2))
                    std::static_pointer_cast<Menu>(m_elements.at("menu"))->addChild(*m_lockedWidget);
            }
        }
    }
*/
    if (action == GLFW_RELEASE) {
        m_lockedWidget = 0;
        if(m_lockedElement)
            m_lockedElement->unlockMouse();
        m_lockedElement = 0;
    }

}

void GUI::scrollCallback(double xoffset, double yoffset)
{
    for(auto & e : m_elements){
        if(e.second->bounds().contains(m_mouse.x(), m_mouse.y())){
            e.second->scrollCallback(xoffset, yoffset);
        }
    }
}

void GUI::cursorCallback(double xpos, double ypos)
{

    ModelWidget* modelWidget = (ModelWidget*) m_elements.at("modelWidget").get();
    if(modelWidget)
        modelWidget->setInstance(m_scene.getPickedInstance());

    dispatchCursorCallback(m_elements);

}

void GUI::dispatchCursorCallback(std::map<std::string, std::shared_ptr<UIElement>> elements)
{

    for(auto & e : elements){
        if(e.second->bounds().contains(m_mouse.x(), m_mouse.y())){
            e.second->cursorCallback(m_mouse.x(), m_mouse.y());
        }
    }

    if(m_lockedElement)
        m_lockedElement->cursorCallback(m_mouse.x(), m_mouse.y());

    // if a widget is locked
    bool widgetOverlaps = false;
    if(m_lockedWidget != nullptr && m_lockedWidget->isMouseMovable() && !m_lockedWidget->isLocked()) {
        //m_lockedWidget->translate(m_mouse.deltaX(), m_mouse.deltaY());
        for(Widget& w : m_widgets) {

                if(w.overlaps(*m_lockedWidget) && (m_lockedWidget != &w))
                {
                    widgetOverlaps = true;
                    break;
                }

        }
        if(widgetOverlaps)
            m_lockedWidget->translate(-m_mouse.deltaX(), -m_mouse.deltaY());

    }

}

int lastX, lastY;
void GUI::framebufferSizeCallback()
{
    m_elements.at("consoleWidget")->translate(m_screen.x2() - lastX, m_screen.y2() - lastX);
    m_elements.at("ppWidget")->translate(m_screen.x2() - lastX, m_screen.y2() - lastX);
    Point p1(m_screen.width() - 310, m_screen.height() - 85);
    Point p2(p1.x() + 100, p1.y() + 70);
    Rectangle r(p1, p2);
    m_elements.at("guiWidget")->setBounds(r);


    lastX = m_screen.x2();
    lastY = m_screen.y2();
/*
    for(auto & e : m_elements) {
        // for each widget
        if(e.second->type() == Type::WIDGET) {
            std::shared_ptr<Widget> widg = std::static_pointer_cast<Widget>(e.second);
            // if widget if outside window
            if (!m_screen.contains(widg->bounds())) {
                // check offset and translate back
                if (widg->bounds().x() <= 0)
                    widg->translate((-widg->bounds().x()) + 1, 0);
                if (widg->bounds().y() <= 0)
                    widg->translate(0, -widg->bounds().y() + 1);
                if (m_screen.x2() <= widg->bounds().x2())
                    widg->translate(m_screen.x2() - widg->bounds().x2() -1, 0);
                if (m_screen.y2() <= widg->bounds().y2())
                    widg->translate(0, m_screen.y2() - widg->bounds().y2() -1);
            }
        }
    }
*/

}


int GUI::loadDemoData()
{
    int i;

    if (m_vg == NULL)
        return -1;

    for (i = 0; i < 12; i++) {
        char file[128];
        snprintf(file, 128, "/home/lebasith/Programmation/Cpp/nanovg-master/example/images/image%d.jpg", i+1);
        m_data.images[i] = nvgCreateImage(m_vg, file, 0);
        if (m_data.images[i] == 0) {
            printf("Could not load %s.\n", file);
            return -1;
        }
    }

    m_data.fontIcons = nvgCreateFont(m_vg, "icons", "/home/lebasith/Programmation/Cpp/nanovg-master/example/entypo.ttf");
    if (m_data.fontIcons == -1) {
        printf("Could not add font icons.\n");
        return -1;
    }
    m_data.fontNormal = nvgCreateFont(m_vg, "sans", "/home/lebasith/Programmation/Cpp/nanovg-master/example/Roboto-Regular.ttf");
    if (m_data.fontNormal == -1) {
        printf("Could not add font italic.\n");
        return -1;
    }
    m_data.fontBold = nvgCreateFont(m_vg, "sans-bold", "/home/lebasith/Programmation/Cpp/nanovg-master/example/Roboto-Bold.ttf");
    if (m_data.fontBold == -1) {
        printf("Could not add font bold.\n");
        return -1;
    }
    m_data.fontEmoji = nvgCreateFont(m_vg, "emoji", "/home/lebasith/Programmation/Cpp/nanovg-master/example/NotoEmoji-Regular.ttf");
    if (m_data.fontEmoji == -1) {
        printf("Could not add font emoji.\n");
        return -1;
    }
    nvgAddFallbackFontId(m_vg, m_data.fontNormal, m_data.fontEmoji);
    nvgAddFallbackFontId(m_vg, m_data.fontBold, m_data.fontEmoji);

    return 0;
}

void GUI::freeDemoData()
{
    int i;

    if (m_vg == NULL)
        return;

    for (i = 0; i < 12; i++)
        nvgDeleteImage(m_vg, m_data.images[i]);
}

void GUI::drawGraph(NVGcontext* vg, float x, float y, float w, float h, float t)
{
    NVGpaint bg;
    float samples[6];
    float sx[6], sy[6];
    float dx = w/5.0f;
    int i;

    samples[0] = (1+sinf(t*1.2345f+cosf(t*0.33457f)*0.44f))*0.5f;
    samples[1] = (1+sinf(t*0.68363f+cosf(t*1.3f)*1.55f))*0.5f;
    samples[2] = (1+sinf(t*1.1642f+cosf(t*0.33457)*1.24f))*0.5f;
    samples[3] = (1+sinf(t*0.56345f+cosf(t*1.63f)*0.14f))*0.5f;
    samples[4] = (1+sinf(t*1.6245f+cosf(t*0.254f)*0.3f))*0.5f;
    samples[5] = (1+sinf(t*0.345f+cosf(t*0.03f)*0.6f))*0.5f;

    for (i = 0; i < 6; i++) {
        sx[i] = x+i*dx;
        sy[i] = y+h*samples[i]*0.8f;
    }

    // Graph background
    bg = nvgLinearGradient(vg, x,y,x,y+h, nvgRGBA(0,160,192,0), nvgRGBA(0,160,192,64));
    nvgBeginPath(vg);
    nvgMoveTo(vg, sx[0], sy[0]);
    for (i = 1; i < 6; i++)
        nvgBezierTo(vg, sx[i-1]+dx*0.5f,sy[i-1], sx[i]-dx*0.5f,sy[i], sx[i],sy[i]);
    nvgLineTo(vg, x+w, y+h);
    nvgLineTo(vg, x, y+h);
    nvgFillPaint(vg, bg);
    nvgFill(vg);

    // Graph line
    nvgBeginPath(vg);
    nvgMoveTo(vg, sx[0], sy[0]+2);
    for (i = 1; i < 6; i++)
        nvgBezierTo(vg, sx[i-1]+dx*0.5f,sy[i-1]+2, sx[i]-dx*0.5f,sy[i]+2, sx[i],sy[i]+2);
    nvgStrokeColor(vg, nvgRGBA(0,0,0,32));
    nvgStrokeWidth(vg, 3.0f);
    nvgStroke(vg);

    nvgBeginPath(vg);
    nvgMoveTo(vg, sx[0], sy[0]);
    for (i = 1; i < 6; i++)
        nvgBezierTo(vg, sx[i-1]+dx*0.5f,sy[i-1], sx[i]-dx*0.5f,sy[i], sx[i],sy[i]);
    nvgStrokeColor(vg, nvgRGBA(0,160,192,255));
    nvgStrokeWidth(vg, 3.0f);
    nvgStroke(vg);

    // Graph sample pos
    for (i = 0; i < 6; i++) {
        bg = nvgRadialGradient(vg, sx[i],sy[i]+2, 3.0f,8.0f, nvgRGBA(0,0,0,32), nvgRGBA(0,0,0,0));
        nvgBeginPath(vg);
        nvgRect(vg, sx[i]-10, sy[i]-10+2, 20,20);
        nvgFillPaint(vg, bg);
        nvgFill(vg);
    }

    nvgBeginPath(vg);
    for (i = 0; i < 6; i++)
        nvgCircle(vg, sx[i], sy[i], 4.0f);
    nvgFillColor(vg, nvgRGBA(0,160,192,255));
    nvgFill(vg);
    nvgBeginPath(vg);
    for (i = 0; i < 6; i++)
        nvgCircle(vg, sx[i], sy[i], 2.0f);
    nvgFillColor(vg, nvgRGBA(220,220,220,255));
    nvgFill(vg);

    nvgStrokeWidth(vg, 1.0f);
}

void GUI::drawSpinner(NVGcontext* vg, float cx, float cy, float r, float t)
{
    float a0 = 0.0f + t*6;
    float a1 = NVG_PI + t*6;
    float r0 = r;
    float r1 = r * 0.75f;
    float ax,ay, bx,by;
    NVGpaint paint;

    nvgSave(vg);

    nvgBeginPath(vg);
    nvgArc(vg, cx,cy, r0, a0, a1, NVG_CW);
    nvgArc(vg, cx,cy, r1, a1, a0, NVG_CCW);
    nvgClosePath(vg);
    ax = cx + cosf(a0) * (r0+r1)*0.5f;
    ay = cy + sinf(a0) * (r0+r1)*0.5f;
    bx = cx + cosf(a1) * (r0+r1)*0.5f;
    by = cy + sinf(a1) * (r0+r1)*0.5f;
    paint = nvgLinearGradient(vg, ax,ay, bx,by, nvgRGBA(0,0,0,0), nvgRGBA(0,0,0,128));
    nvgFillPaint(vg, paint);
    nvgFill(vg);

    nvgRestore(vg);
}

void GUI::drawThumbnails(NVGcontext* vg, float x, float y, float w, float h, const int* images, int nimages, float t)
{
    float cornerRadius = 3.0f;
    NVGpaint shadowPaint, imgPaint, fadePaint;
    float ix,iy,iw,ih;
    float thumb = 60.0f;
    float arry = 30.5f;
    int imgw, imgh;
    float stackh = (nimages/2) * (thumb+10) + 10;
    int i;
    float u = (1+cosf(t*0.5f))*0.5f;
    float u2 = (1-cosf(t*0.2f))*0.5f;
    float scrollh, dv;

    nvgSave(vg);
    //	nvgClearState(vg);

    // Drop shadow
    shadowPaint = nvgBoxGradient(vg, x,y+4, w,h, cornerRadius*2, 20, nvgRGBA(0,0,0,128), nvgRGBA(0,0,0,0));
    nvgBeginPath(vg);
    nvgRect(vg, x-10,y-10, w+20,h+30);
    nvgRoundedRect(vg, x,y, w,h, cornerRadius);
    nvgPathWinding(vg, NVG_HOLE);
    nvgFillPaint(vg, shadowPaint);
    nvgFill(vg);

    // Window
    nvgBeginPath(vg);
    nvgRoundedRect(vg, x,y, w,h, cornerRadius);
    nvgMoveTo(vg, x-10,y+arry);
    nvgLineTo(vg, x+1,y+arry-11);
    nvgLineTo(vg, x+1,y+arry+11);
    nvgFillColor(vg, nvgRGBA(200,200,200,255));
    nvgFill(vg);

    nvgSave(vg);
    nvgScissor(vg, x,y,w,h);
    nvgTranslate(vg, 0, -(stackh - h)*u);

    dv = 1.0f / (float)(nimages-1);

    for (i = 0; i < nimages; i++) {
        float tx, ty, v, a;
        tx = x+10;
        ty = y+10;
        tx += (i%2) * (thumb+10);
        ty += (i/2) * (thumb+10);
        nvgImageSize(vg, images[i], &imgw, &imgh);
        if (imgw < imgh) {
            iw = thumb;
            ih = iw * (float)imgh/(float)imgw;
            ix = 0;
            iy = -(ih-thumb)*0.5f;
        } else {
            ih = thumb;
            iw = ih * (float)imgw/(float)imgh;
            ix = -(iw-thumb)*0.5f;
            iy = 0;
        }

        v = i * dv;
        a = clampf((u2-v) / dv, 0, 1);

        if (a < 1.0f)
            drawSpinner(vg, tx+thumb/2,ty+thumb/2, thumb*0.25f, t);

        imgPaint = nvgImagePattern(vg, tx+ix, ty+iy, iw,ih, 0.0f/180.0f*NVG_PI, images[i], a);
        nvgBeginPath(vg);
        nvgRoundedRect(vg, tx,ty, thumb,thumb, 5);
        nvgFillPaint(vg, imgPaint);
        nvgFill(vg);

        shadowPaint = nvgBoxGradient(vg, tx-1,ty, thumb+2,thumb+2, 5, 3, nvgRGBA(0,0,0,128), nvgRGBA(0,0,0,0));
        nvgBeginPath(vg);
        nvgRect(vg, tx-5,ty-5, thumb+10,thumb+10);
        nvgRoundedRect(vg, tx,ty, thumb,thumb, 6);
        nvgPathWinding(vg, NVG_HOLE);
        nvgFillPaint(vg, shadowPaint);
        nvgFill(vg);

        nvgBeginPath(vg);
        nvgRoundedRect(vg, tx+0.5f,ty+0.5f, thumb-1,thumb-1, 4-0.5f);
        nvgStrokeWidth(vg,1.0f);
        nvgStrokeColor(vg, nvgRGBA(255,255,255,192));
        nvgStroke(vg);
    }
    nvgRestore(vg);

    // Hide fades
    fadePaint = nvgLinearGradient(vg, x,y,x,y+6, nvgRGBA(200,200,200,255), nvgRGBA(200,200,200,0));
    nvgBeginPath(vg);
    nvgRect(vg, x+4,y,w-8,6);
    nvgFillPaint(vg, fadePaint);
    nvgFill(vg);

    fadePaint = nvgLinearGradient(vg, x,y+h,x,y+h-6, nvgRGBA(200,200,200,255), nvgRGBA(200,200,200,0));
    nvgBeginPath(vg);
    nvgRect(vg, x+4,y+h-6,w-8,6);
    nvgFillPaint(vg, fadePaint);
    nvgFill(vg);

    // Scroll bar
    shadowPaint = nvgBoxGradient(vg, x+w-12+1,y+4+1, 8,h-8, 3,4, nvgRGBA(0,0,0,32), nvgRGBA(0,0,0,92));
    nvgBeginPath(vg);
    nvgRoundedRect(vg, x+w-12,y+4, 8,h-8, 3);
    nvgFillPaint(vg, shadowPaint);
    //	nvgFillColor(vg, nvgRGBA(255,0,0,128));
    nvgFill(vg);

    scrollh = (h/stackh) * (h-8);
    shadowPaint = nvgBoxGradient(vg, x+w-12-1,y+4+(h-8-scrollh)*u-1, 8,scrollh, 3,4, nvgRGBA(220,220,220,255), nvgRGBA(128,128,128,255));
    nvgBeginPath(vg);
    nvgRoundedRect(vg, x+w-12+1,y+4+1 + (h-8-scrollh)*u, 8-2,scrollh-2, 2);
    nvgFillPaint(vg, shadowPaint);
    //	nvgFillColor(vg, nvgRGBA(0,0,0,128));
    nvgFill(vg);

    nvgRestore(vg);
}

void drawLines(NVGcontext* vg, float x, float y, float w, float h, float t)
{
    int i, j;
    float pad = 5.0f, s = w/9.0f - pad*2;
    float pts[4*2], fx, fy;
    int joins[3] = {NVG_MITER, NVG_ROUND, NVG_BEVEL};
    int caps[3] = {NVG_BUTT, NVG_ROUND, NVG_SQUARE};
    NVG_NOTUSED(h);

    nvgSave(vg);
    pts[0] = -s*0.25f + cosf(t*0.3f) * s*0.5f;
    pts[1] = sinf(t*0.3f) * s*0.5f;
    pts[2] = -s*0.25;
    pts[3] = 0;
    pts[4] = s*0.25f;
    pts[5] = 0;
    pts[6] = s*0.25f + cosf(-t*0.3f) * s*0.5f;
    pts[7] = sinf(-t*0.3f) * s*0.5f;

    for (i = 0; i < 3; i++) {
        for (j = 0; j < 3; j++) {
            fx = x + s*0.5f + (i*3+j)/9.0f*w + pad;
            fy = y - s*0.5f + pad;

            nvgLineCap(vg, caps[i]);
            nvgLineJoin(vg, joins[j]);

            nvgStrokeWidth(vg, s*0.3f);
            nvgStrokeColor(vg, nvgRGBA(0,0,0,160));
            nvgBeginPath(vg);
            nvgMoveTo(vg, fx+pts[0], fy+pts[1]);
            nvgLineTo(vg, fx+pts[2], fy+pts[3]);
            nvgLineTo(vg, fx+pts[4], fy+pts[5]);
            nvgLineTo(vg, fx+pts[6], fy+pts[7]);
            nvgStroke(vg);

            nvgLineCap(vg, NVG_BUTT);
            nvgLineJoin(vg, NVG_BEVEL);

            nvgStrokeWidth(vg, 1.0f);
            nvgStrokeColor(vg, nvgRGBA(0,192,255,255));
            nvgBeginPath(vg);
            nvgMoveTo(vg, fx+pts[0], fy+pts[1]);
            nvgLineTo(vg, fx+pts[2], fy+pts[3]);
            nvgLineTo(vg, fx+pts[4], fy+pts[5]);
            nvgLineTo(vg, fx+pts[6], fy+pts[7]);
            nvgStroke(vg);
        }
    }


    nvgRestore(vg);
}

void drawWidths(NVGcontext* vg, float x, float y, float width)
{
    int i;

    nvgSave(vg);

    nvgStrokeColor(vg, nvgRGBA(0,0,0,255));

    for (i = 0; i < 20; i++) {
        float w = (i+0.5f)*0.1f;
        nvgStrokeWidth(vg, w);
        nvgBeginPath(vg);
        nvgMoveTo(vg, x,y);
        nvgLineTo(vg, x+width,y+width*0.3f);
        nvgStroke(vg);
        y += 10;
    }

    nvgRestore(vg);
}

void drawCaps(NVGcontext* vg, float x, float y, float width)
{
    int i;
    int caps[3] = {NVG_BUTT, NVG_ROUND, NVG_SQUARE};
    float lineWidth = 8.0f;

    nvgSave(vg);

    nvgBeginPath(vg);
    nvgRect(vg, x-lineWidth/2, y, width+lineWidth, 40);
    nvgFillColor(vg, nvgRGBA(255,255,255,32));
    nvgFill(vg);

    nvgBeginPath(vg);
    nvgRect(vg, x, y, width, 40);
    nvgFillColor(vg, nvgRGBA(255,255,255,32));
    nvgFill(vg);

    nvgStrokeWidth(vg, lineWidth);
    for (i = 0; i < 3; i++) {
        nvgLineCap(vg, caps[i]);
        nvgStrokeColor(vg, nvgRGBA(0,0,0,255));
        nvgBeginPath(vg);
        nvgMoveTo(vg, x, y + i*10 + 5);
        nvgLineTo(vg, x+width, y + i*10 + 5);
        nvgStroke(vg);
    }

    nvgRestore(vg);
}

void drawScissor(NVGcontext* vg, float x, float y, float t)
{
    nvgSave(vg);

    // Draw first rect and set scissor to it's area.
    nvgTranslate(vg, x, y);
    nvgRotate(vg, nvgDegToRad(5));
    nvgBeginPath(vg);
    nvgRect(vg, -20,-20,60,40);
    nvgFillColor(vg, nvgRGBA(255,0,0,255));
    nvgFill(vg);
    nvgScissor(vg, -20,-20,60,40);

    // Draw second rectangle with offset and rotation.
    nvgTranslate(vg, 40,0);
    nvgRotate(vg, t);

    // Draw the intended second rectangle without any scissoring.
    nvgSave(vg);
    nvgResetScissor(vg);
    nvgBeginPath(vg);
    nvgRect(vg, -20,-10,60,30);
    nvgFillColor(vg, nvgRGBA(255,128,0,64));
    nvgFill(vg);
    nvgRestore(vg);

    // Draw second rectangle with combined scissoring.
    nvgIntersectScissor(vg, -20,-10,60,30);
    nvgBeginPath(vg);
    nvgRect(vg, -20,-10,60,30);
    nvgFillColor(vg, nvgRGBA(255,128,0,255));
    nvgFill(vg);

    nvgRestore(vg);
}

static int mini(int a, int b) { return a < b ? a : b; }

static void unpremultiplyAlpha(unsigned char* image, int w, int h, int stride)
{
    int x,y;

    // Unpremultiply
    for (y = 0; y < h; y++) {
        unsigned char *row = &image[y*stride];
        for (x = 0; x < w; x++) {
            int r = row[0], g = row[1], b = row[2], a = row[3];
            if (a != 0) {
                row[0] = (int)mini(r*255/a, 255);
                row[1] = (int)mini(g*255/a, 255);
                row[2] = (int)mini(b*255/a, 255);
            }
            row += 4;
        }
    }

    // Defringe
    for (y = 0; y < h; y++) {
        unsigned char *row = &image[y*stride];
        for (x = 0; x < w; x++) {
            int r = 0, g = 0, b = 0, a = row[3], n = 0;
            if (a == 0) {
                if (x-1 > 0 && row[-1] != 0) {
                    r += row[-4];
                    g += row[-3];
                    b += row[-2];
                    n++;
                }
                if (x+1 < w && row[7] != 0) {
                    r += row[4];
                    g += row[5];
                    b += row[6];
                    n++;
                }
                if (y-1 > 0 && row[-stride+3] != 0) {
                    r += row[-stride];
                    g += row[-stride+1];
                    b += row[-stride+2];
                    n++;
                }
                if (y+1 < h && row[stride+3] != 0) {
                    r += row[stride];
                    g += row[stride+1];
                    b += row[stride+2];
                    n++;
                }
                if (n > 0) {
                    row[0] = r/n;
                    row[1] = g/n;
                    row[2] = b/n;
                }
            }
            row += 4;
        }
    }
}

static void setAlpha(unsigned char* image, int w, int h, int stride, unsigned char a)
{
    int x, y;
    for (y = 0; y < h; y++) {
        unsigned char* row = &image[y*stride];
        for (x = 0; x < w; x++)
            row[x*4+3] = a;
    }
}

static void flipHorizontal(unsigned char* image, int w, int h, int stride)
{
    int i = 0, j = h-1, k;
    while (i < j) {
        unsigned char* ri = &image[i * stride];
        unsigned char* rj = &image[j * stride];
        for (k = 0; k < w*4; k++) {
            unsigned char t = ri[k];
            ri[k] = rj[k];
            rj[k] = t;
        }
        i++;
        j--;
    }
}
