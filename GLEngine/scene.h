#ifndef SCENE_H
#define SCENE_H

#include <GL/glew.h>
#include <algorithm>
#include <GLFW/glfw3.h>
#include "camera.h"
#include "shader.h"
#include <map>
#include <string>
#include <vector>
#include <model.h>
#include <light.h>
#include <material.h>
#include "mouse.h"
#include "texture.h"
#include "cubemaptexture.h"
#include "framebuffer.h"
#include "instance.h"
#include "physics.h"
#include "rectangle.h"
#include "mousepicker.h"
#include "callbacks.h"
#include "postprocess.h"
#include "gbuffer.h"
#include "shadowmap.h"
#include "renderer.h"
#include "assetmanager.h"
#include "gameobject.h"


class Scene : public Callbacks
{
public:    
    static constexpr GLuint WIDTH = 1600;
    static constexpr GLuint HEIGHT = 900;
    Scene(GLFWwindow& w, Rectangle& screen, Mouse& mouse, AssetManager& assetManager);
    ~Scene();

    void addBox();
    void addPlayer();
    void addLights();
    void update();
    void processInputs();


    btVector3 getRayToMousePos();

    void mouseCallback(int button, int action) override;
    void cursorCallback(double xpos, double ypos) override;
    void framebufferSizeCallback() override;

    Instance* getPickedInstance();

    inline std::vector<Light>& lights()
    {
        return m_lights;
    }

    inline std::vector<Instance>& instances()
    {
        return m_instances;
    }

    inline  std::map<std::string, Model>& models()
    {
        return m_models;
    }


    inline Camera& camera()
    {
        return m_camera;
    }

    inline Camera camera() const
    {
        return m_camera;
    }

    inline btVector3& rayTo()
    {
        return m_rayTo;
    }

    inline btVector3 rayTo() const
    {
        return m_rayTo;
    }

    inline MousePicker& mousePicker()
    {
        return m_mousePicker;
    }

    inline Rectangle& screen() const
    {
        return m_screen;
    }

    inline GLfloat exposure() const
    {
        return m_exposure;
    }

    inline GLfloat& exposure()
    {
        return m_exposure;
    }

    inline GLfloat turbidity() const
    {
        return m_turbidity;
    }

    inline GLfloat& turbidity()
    {
        return m_turbidity;
    }

    inline Physics& physics()
    {
        return m_physics;
    }

    inline std::map<std::string, Model> models() const
    {
        return m_models;
    }

    inline AssetManager& assetManager()
    {
        return m_assetManager;
    }

    inline std::map<std::string, btCollisionShape*>& shapes()
    {
        return m_shapes;
    }

    inline DirectionalLight& directionalLight()
    {
        return m_directionalLight;
    }

    inline GameObject root() const
    {
        return m_root;
    }

    inline GameObject& root()
    {
        return m_root;
    }

private:

    std::map<std::string, Model> m_models;

    GameObject m_root;

    std::map<std::string, btCollisionShape*> m_shapes;
    std::vector<Instance> m_instances;
    std::vector<Light> m_lights;
    DirectionalLight m_directionalLight;

    Camera m_camera;

    GLfloat m_exposure = 1.0f;
    GLfloat m_turbidity = 2.0f;
    GLFWwindow& m_window;

    AssetManager& m_assetManager;
    GLfloat m_mouseSensitivity = 0.1f;

    btVector3 m_rayFrom;
    btVector3 m_rayTo;

    Physics m_physics;
    MousePicker m_mousePicker;    

    Rectangle& m_screen;
    Mouse& m_mouse;

};

#endif // SCENE_H
