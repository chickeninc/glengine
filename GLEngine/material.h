#ifndef MATERIAL_H
#define MATERIAL_H

#include "texture.h"
#include <vector>

class Material
{

public:

    Material(const float roughness = 1.0f, const float metallic = 1.0f);

    inline float roughness() const
    {
        return m_roughness;
    }

    inline float& roughness()
    {
        return m_roughness;
    }

    inline float metallic() const
    {
        return m_metallic;
    }

    inline float& metallic()
    {
        return m_metallic;
    }

    inline void setRoughness(const float r)
    {
        m_roughness = r;
    }

    inline void setMetallic(const float m)
    {
        m_metallic = m;
    }

    inline Texture* albedoMap()
    {
        return m_albedoMap;
    }

    inline Texture* normalMap()
    {
        return m_normalMap;
    }

    inline Texture* metallicMap()
    {
        return m_metallicMap;
    }

    inline Texture* heightMap()
    {
        return m_heightMap;
    }

    inline Texture* aoMap()
    {
        return m_aoMap;
    }

    inline Texture* roughnessMap()
    {
        return m_roughnessMap;
    }

    inline std::vector<Texture> textures() const
    {
        return m_textures;
    }

    inline std::vector<Texture>& textures()
    {
        return m_textures;
    }



    void addTexture(Texture& tex);

private:

    std::vector<Texture> m_textures;
    Texture* m_albedoMap = nullptr;
    Texture* m_normalMap = nullptr;
    Texture* m_metallicMap = nullptr;
    Texture* m_aoMap = nullptr;
    Texture* m_heightMap = nullptr;
    Texture* m_roughnessMap = nullptr;
    float m_roughness = 0.0f;
    float m_metallic = 0.0f;

};


#endif // MATERIAL_H
