#ifndef TEXTURE_H
#define TEXTURE_H

#include "stb_image.h"
#include <iostream>
#include <GL/glew.h>
#include <GL/gl.h>
#include <string>
#include <FreeImage.h>

enum class TextureType
{
    ALBEDO, NORMAL, METALLIC, ROUGHNESS, AO, HEIGHT, HDR, TEXTURE, FRAMETEXTURE
};

class Texture
{

public:

    Texture();
    Texture(const GLuint width,
            const GLuint height,
            const GLenum internalFormat = GL_RGBA16F,
            const GLenum format = GL_RGBA,
            const GLenum dataType = GL_FLOAT);

    Texture(const char* path, const TextureType type = TextureType::TEXTURE);
    //Texture(const char* path, const TextureType type);

    inline GLuint ID() const
    {
        return m_id;
    }

    inline GLuint& ID()
    {
        return m_id;
    }

    inline void clean() const
    {
        glDeleteTextures(1, &m_id);
    }

    inline TextureType type() const
    {
        return m_type;
    }

    virtual void Use() const;
    virtual void resize(const GLuint width, const GLuint height);

protected:
    TextureType m_type = TextureType::TEXTURE;
    GLuint m_id;

    GLenum m_internalFormat = GL_RGBA16F;
    GLenum m_format = GL_RGBA;
    GLenum m_dataType = GL_FLOAT;

    GLuint m_width;
    GLuint m_height;
};

#endif // TEXTURE_H
