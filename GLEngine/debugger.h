#ifndef DEBUGGER_H
#define DEBUGGER_H

#include <iostream>
#include <string>



namespace Debugger {

static bool m_debug = true;

static void setDebug(bool d) { m_debug = d; }
static void debug(const char* debugMessage)
{
    if(m_debug)
        std::cout << debugMessage << std::endl;
}

static void debug(std::string debugMessage)
{
    if(m_debug)
        std::cout << debugMessage << std::endl;
}

}


#endif // DEBUGGER_H
