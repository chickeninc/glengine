#ifndef GAMEOBJECT_H
#define GAMEOBJECT_H

#include <vector>
#include "gamecomponent.h"
#include "btBulletCollisionCommon.h"

#include <btBulletDynamicsCommon.h>

class GameObject
{
public:
    GameObject():m_children{} {}
    GameObject(btTransform t, float mass, btCollisionShape* shape, bool isKinematic);

    inline void addChild(GameObject* child)
    {
        m_children.push_back(child);
        child->setParent(this);
    }

    inline void setParent(GameObject* parent)
    {
        m_parent = parent;
    }

    inline void update()
    {
        for(GameObject* child : m_children)
            child->update();
    }

    virtual void render(ShaderProgram program, GLenum mode)
    {
        for(GameObject* c : m_children)
            c->render(program, mode);
    }

    inline btRigidBody* physicsBody() const
    {
        return m_physicsBody;
    }


    btScalar* getWorldTransform();
    void transform(btTransform t);

protected:
    btRigidBody* m_physicsBody = nullptr;
    std::vector<GameObject*> m_children;
    GameObject* m_parent = nullptr;
    GameComponent* m_component = nullptr;

    btScalar m_worldTransform[16];
};

#endif // GAMEOBJECT_H
