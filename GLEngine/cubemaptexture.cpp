#include "cubemaptexture.h"

CubeMapTexture::CubeMapTexture()
{

}

CubeMapTexture::CubeMapTexture(std::vector<const GLchar*> pathfaces)
{
    glActiveTexture(GL_TEXTURE0);

    glGenTextures(1, &m_id);

    int width,height,n;
    unsigned char* image;

    glBindTexture(GL_TEXTURE_CUBE_MAP, m_id);

    for(GLuint i = 0; i < pathfaces.size(); i++)
    {
        image = stbi_load(pathfaces[i], &width, &height, &n, 0);
        if(image != NULL){
            std::cout << pathfaces[i] << " loaded \n" << "width : " << width << " height : " << height << std::endl;

        } else {
            std::cout << "Couldnt load image " << pathfaces[i] << std::endl;
        }

        glTexImage2D(
                    GL_TEXTURE_CUBE_MAP_POSITIVE_X + i, 0,
                    GL_RGB16F, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, image
                    );
        stbi_image_free(image);
    }
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);
    glBindTexture(GL_TEXTURE_CUBE_MAP, 0);
}

void CubeMapTexture::Use() const

{
    glBindTexture(GL_TEXTURE_CUBE_MAP, m_id);
}
