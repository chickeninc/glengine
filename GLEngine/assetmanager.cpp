#include "assetmanager.h"


const std::string PATH = "/home/lebasith/Git/cpp/GLEngine";


AssetManager::AssetManager()
{
    FreeImage_Initialise();
    // load all the textures and store it in the textures map
    if(!loadTextures()) std::cout << "Couldnt load textures" << std::endl;
}


bool AssetManager::loadTextures()
{
    Texture greasy_albedo =  Texture((PATH + "/assets/textures/greasy_albedo.png").data(), TextureType::ALBEDO);
    m_textures.insert(std::make_pair("greasy_albedo", greasy_albedo));
    Texture greasy_normal =  Texture((PATH + "/assets/textures/greasy_normal.png").data(), TextureType::NORMAL);
    m_textures.insert(std::make_pair("greasy_normal", greasy_normal));
    Texture greasy_metallic =  Texture((PATH + "/assets/textures/greasy_metallic.png").data(), TextureType::METALLIC);
    m_textures.insert(std::make_pair("greasy_metallic", greasy_metallic));
    Texture greasy_roughness =  Texture((PATH + "/assets/textures/greasy_roughness.png").data(), TextureType::ROUGHNESS);
    m_textures.insert(std::make_pair("greasy_roughness", greasy_roughness));


    Texture gold_albedo =  Texture((PATH + "/assets/textures/gold_albedo-boosted.png").data(), TextureType::ALBEDO);
    m_textures.insert(std::make_pair("gold_albedo", gold_albedo));
    Texture gold_normal =  Texture((PATH + "/assets/textures/gold_normal.png").data(), TextureType::NORMAL);
    m_textures.insert(std::make_pair("gold_normal", gold_normal));
    Texture gold_metallic =  Texture((PATH + "/assets/textures/gold_metallic.png").data(), TextureType::METALLIC);
    m_textures.insert(std::make_pair("gold_metallic", gold_metallic));
    Texture gold_roughness =  Texture((PATH + "/assets/textures/gold_roughness.png").data(), TextureType::ROUGHNESS);
    m_textures.insert(std::make_pair("gold_roughness", gold_roughness));

    Texture gun_albedo =  Texture((PATH + "/assets/textures/gun_albedo.jpg").data(), TextureType::ALBEDO);
    m_textures.insert(std::make_pair("gun_albedo", gun_albedo));
    Texture gun_normal =  Texture((PATH + "/assets/textures/gun_normal.jpg").data(), TextureType::NORMAL);
    m_textures.insert(std::make_pair("gun_normal", gun_normal));
    Texture gun_metallic =  Texture((PATH + "/assets/textures/gun_metallic.jpg").data(), TextureType::METALLIC);
    m_textures.insert(std::make_pair("gun_metallic", gun_metallic));
    Texture gun_roughness =  Texture((PATH + "/assets/textures/gun_roughness.jpg").data(), TextureType::ROUGHNESS);
    m_textures.insert(std::make_pair("gun_roughness", gun_roughness));
    Texture gun_ao =  Texture((PATH + "/assets/textures/gun_ao.tga").data(), TextureType::AO);
    m_textures.insert(std::make_pair("gun_ao", gun_ao));

    Texture asphalt_albedo =  Texture((PATH + "/assets/textures/RockyGround_basecolor.tga").data(), TextureType::ALBEDO);
    m_textures.insert(std::make_pair("asphalt_albedo", asphalt_albedo));
    Texture asphalt_normal =  Texture((PATH + "/assets/textures/RockyGround_normal.tga").data(), TextureType::NORMAL);
    m_textures.insert(std::make_pair("asphalt_normal", asphalt_normal));
    Texture asphalt_metallic =  Texture((PATH + "/assets/textures/RockyGround_metallic.tga").data(), TextureType::METALLIC);
    m_textures.insert(std::make_pair("asphalt_metallic", asphalt_metallic));
    Texture asphalt_roughness =  Texture((PATH + "/assets/textures/RockyGround_roughness.tga").data(), TextureType::ROUGHNESS);
    m_textures.insert(std::make_pair("asphalt_roughness", asphalt_roughness));
    Texture asphalt_ao =  Texture((PATH + "/assets/textures/RockyGround_ao.tga").data(), TextureType::AO);
    m_textures.insert(std::make_pair("asphalt_ao", asphalt_ao));
    Texture asphalt_height =  Texture((PATH + "/assets/textures/RockyGround_height.tga").data(), TextureType::HEIGHT);
    m_textures.insert(std::make_pair("asphalt_height", asphalt_height));


    Texture cement_albedo =  Texture((PATH + "/assets/textures/cement_albedo.png").data(), TextureType::ALBEDO);
    m_textures.insert(std::make_pair("cement_albedo", cement_albedo));
    Texture cement_normal =  Texture((PATH + "/assets/textures/cement_normal.png").data(), TextureType::NORMAL);
    m_textures.insert(std::make_pair("cement_normal", cement_normal));
    Texture cement_metallic =  Texture((PATH + "/assets/textures/cement_metallic.png").data(), TextureType::METALLIC);
    m_textures.insert(std::make_pair("cement_metallic", cement_metallic));
    Texture cement_roughness =  Texture((PATH + "/assets/textures/cement_roughness.png").data(), TextureType::ROUGHNESS);
    m_textures.insert(std::make_pair("cement_roughness", cement_roughness));
    Texture cement_ao =  Texture((PATH + "/assets/textures/cement_ao.png").data(), TextureType::AO);
    m_textures.insert(std::make_pair("cement_ao", cement_ao));

    Texture copper_albedo =  Texture((PATH + "/assets/textures/copper_albedo.png").data(), TextureType::ALBEDO);
    m_textures.insert(std::make_pair("copper_albedo", copper_albedo));
    Texture copper_normal =  Texture((PATH + "/assets/textures/copper_normal.png").data(), TextureType::NORMAL);
    m_textures.insert(std::make_pair("copper_normal", copper_normal));
    Texture copper_metallic =  Texture((PATH + "/assets/textures/copper_metallic.png").data(), TextureType::METALLIC);
    m_textures.insert(std::make_pair("copper_metallic", copper_metallic));
    Texture copper_roughness =  Texture((PATH + "/assets/textures/copper_roughness.png").data(), TextureType::ROUGHNESS);
    m_textures.insert(std::make_pair("copper_roughness", copper_roughness));
    Texture copper_height =  Texture((PATH + "/assets/textures/copper_height.tga").data(), TextureType::HEIGHT);
    m_textures.insert(std::make_pair("copper_height", copper_height));
    Texture copper_ao =  Texture((PATH + "/assets/textures/copper_ao.tga").data(), TextureType::AO);
    m_textures.insert(std::make_pair("copper_ao", copper_ao));

    Texture planks_albedo =  Texture((PATH + "/assets/textures/planks_albedo.png").data(), TextureType::ALBEDO);
    m_textures.insert(std::make_pair("planks_albedo", planks_albedo));
    Texture planks_normal =  Texture((PATH + "/assets/textures/planks_normal.png").data(), TextureType::NORMAL);
    m_textures.insert(std::make_pair("planks_normal", planks_normal));
    Texture planks_ao =  Texture((PATH + "/assets/textures/planks_ao.png").data(), TextureType::AO);
    m_textures.insert(std::make_pair("planks_ao", planks_ao));
    Texture planks_roughness =  Texture((PATH + "/assets/textures/planks_roughness.png").data(), TextureType::ROUGHNESS);
    m_textures.insert(std::make_pair("planks_roughness", planks_roughness));
    Texture planks_height =  Texture((PATH + "/assets/textures/planks_height.png").data(), TextureType::HEIGHT);
    m_textures.insert(std::make_pair("planks_height", planks_height));

    Texture bricks_albedo =  Texture((PATH + "/assets/textures/bricks_albedo.jpg").data(), TextureType::ALBEDO);
    m_textures.insert(std::make_pair("bricks_albedo", bricks_albedo));
    Texture bricks_normal =  Texture((PATH + "/assets/textures/bricks_normal.jpg").data(), TextureType::NORMAL);
    m_textures.insert(std::make_pair("bricks_normal", bricks_normal));
    Texture bricks_height =  Texture((PATH + "/assets/textures/bricks_height.jpg").data(), TextureType::HEIGHT);
    m_textures.insert(std::make_pair("bricks_height", bricks_height));

    Texture black_metallic =  Texture((PATH + "/assets/textures/black_Metallic.png").data(), TextureType::METALLIC);
    m_textures.insert(std::make_pair("black_metallic", black_metallic));

    Texture white_metallic =  Texture((PATH + "/assets/textures/white_Metallic.png").data(), TextureType::METALLIC);
    m_textures.insert(std::make_pair("white_metallic", white_metallic));

    Texture white_ao =  Texture((PATH + "/assets/textures/white_Metallic.png").data(), TextureType::AO);
    m_textures.insert(std::make_pair("white_ao", white_ao));

    Texture irradiance = Texture((PATH + "/assets/textures/parkingLot_env.hdr").data(), TextureType::HDR);
    m_textures.insert(std::make_pair("irradianceMap", irradiance));

    Texture radiance = Texture((PATH + "/assets/textures/parkingLot_ref.hdr").data(), TextureType::HDR);
    m_textures.insert(std::make_pair("environmentMap", radiance));

    Texture heightMap = Texture((PATH + "/assets/textures/black_metallic.png").data(), TextureType::HEIGHT);
    m_textures.insert(std::make_pair("heightMap", heightMap));


    std::vector<const GLchar*> faces;
    const std::string pathposx = PATH + "/assets/stormydays_rt.tga";
    const std::string pathnegx = PATH + "/assets/stormydays_lf.tga";
    const std::string pathposy = PATH + "/assets/stormydays_up.tga";
    const std::string pathnegy = PATH + "/assets/stormydays_dn.tga";
    const std::string pathposz = PATH + "/assets/stormydays_bk.tga";
    const std::string pathnegz = PATH + "/assets/stormydays_ft.tga";
    faces.push_back(pathposx.data());
    faces.push_back(pathnegx.data());
    faces.push_back(pathposy.data());
    faces.push_back(pathnegy.data());
    faces.push_back(pathposz.data());
    faces.push_back(pathnegz.data());

    m_skyboxTexture = CubeMapTexture(faces);

    return true;
}

AssetManager::~AssetManager()
{
    for(auto text : m_textures)
    {
        text.second.clean();
    }
}
