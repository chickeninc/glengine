//#define STB_IMAGE_IMPLEMENTATION


#include "scene.h"
#include "model.h"
#include "stb_image.h"
#include <string>
#include <cmath>
#include <bullet/BulletCollision/CollisionShapes/btHeightfieldTerrainShape.h>
#include "modelloader.h"


const std::string PATH = "/home/lebasith/Git/cpp/GLEngine";

float zoom;


Scene::Scene(GLFWwindow& w, Rectangle& screen, Mouse& mouse, AssetManager& assetManager)
     :m_window(w),
      m_screen(screen),
      m_mouse(mouse),
      m_mousePicker(m_physics.world()),
      m_assetManager(assetManager),
      m_directionalLight(glm::vec3(0.1f, 0.1f, 0.1f),
                         glm::vec3(0.0f, 1.0f, -1.0f))
{

    //m_physics.loadBulletWorld(*this);

    addLights();

    m_camera.update(WIDTH, HEIGHT);
    zoom = 2.0f;


    ModelLoader modelLoader = ModelLoader();
    modelLoader.Import3DFromFile(PATH + "/assets/test.dae", m_models);

    /************************************************************************************
                                    MODEL VAO/VBO
    ************************************************************************************/
    addBox();


    Mesh groundMesh;
    groundMesh.createPlane(4);
    //groundMesh.loadOBJ((PATH + "/assets/terrain.obj").data());
    //groundMesh.createTerrain((PATH + "/assets/textures/heightmap.png").data());
    Model ground = Model(groundMesh);
    ground.material().addTexture(m_assetManager.textures().at("asphalt_albedo"));
    ground.material().addTexture(m_assetManager.textures().at("asphalt_metallic"));
    ground.material().addTexture(m_assetManager.textures().at("asphalt_normal"));
    ground.material().addTexture(m_assetManager.textures().at("asphalt_roughness"));
    ground.material().addTexture(m_assetManager.textures().at("asphalt_ao"));
    ground.material().addTexture(m_assetManager.textures().at("asphalt_height"));
    m_models.insert(std::make_pair("ground", ground));
/*
    btTriangleMesh *mTriMesh = new btTriangleMesh();
    for(int i = 0; i < (float)groundMesh.indices.size(); i+=3)
    {
        mTriMesh->addTriangle(groundMesh.shape[groundMesh.indices[i]],
                groundMesh.shape[groundMesh.indices[i+1]],
                groundMesh.shape[groundMesh.indices[i+2]]);
    }

    btCollisionShape* triMeshShape = new btBvhTriangleMeshShape(mTriMesh, true);
*/
    btTransform t;
    btMatrix3x3 matrix = btMatrix3x3();
    matrix.setIdentity();
    matrix = matrix.scaled(btVector3(25, 25, 25));

    t.setBasis(matrix);
    t.setOrigin(btVector3(0, 0, 0));

    btStaticPlaneShape* plane = new btStaticPlaneShape(btVector3(0,1,0), btScalar(0));
    m_shapes.insert(std::make_pair("ground", plane));
    Instance inst(&m_models.at("ground"), t, 0.0f, m_shapes.at("ground"), false);
    m_physics.addBody(inst.physicsBody());

    m_instances.push_back(inst);


    btSphereShape* sphere = new btSphereShape(1);
    m_shapes.insert(std::make_pair("sphere", sphere));

    Mesh cubeMesh;
    cubeMesh.loadOBJ((PATH + "/assets/cube.obj").data());
    Model cube = Model(cubeMesh);
    cube.material().addTexture(m_assetManager.textures().at("copper_albedo"));
    cube.material().addTexture(m_assetManager.textures().at("copper_metallic"));
    cube.material().addTexture(m_assetManager.textures().at("copper_normal"));
    cube.material().addTexture(m_assetManager.textures().at("copper_roughness"));
    cube.material().addTexture(m_assetManager.textures().at("copper_ao"));
    cube.material().addTexture(m_assetManager.textures().at("copper_height"));
    m_models.insert(std::make_pair("cube", cube));
    std::cout << "nb indices : " << cube.mesh().indices.size() << std::endl;

    Mesh sphereMesh;
    sphereMesh.loadOBJ((PATH + "/assets/uvsphere.obj").data());
    Model sphereModel = Model(sphereMesh);
    sphereModel.material().addTexture(m_assetManager.textures().at("copper_albedo"));
    sphereModel.material().addTexture(m_assetManager.textures().at("copper_metallic"));
    sphereModel.material().addTexture(m_assetManager.textures().at("copper_normal"));
    sphereModel.material().addTexture(m_assetManager.textures().at("copper_roughness"));
    sphereModel.material().addTexture(m_assetManager.textures().at("copper_ao"));
    sphereModel.material().addTexture(m_assetManager.textures().at("copper_height"));
    m_models.insert(std::make_pair("sphere", sphereModel));

    btConvexHullShape* convexShape = new btConvexHullShape();
    for(btVector3 v : cubeMesh.shape)
    {
        convexShape->addPoint(v);
    }
    convexShape->optimizeConvexHull();

    m_shapes.insert(std::make_pair("convex", convexShape));
    std::cout << convexShape->getNumPoints() << std::endl;

    btBoxShape* cubeShape = new btBoxShape(btVector3(1, 1, 1));
    m_shapes.insert(std::make_pair("cube", cubeShape));

    for(int i = 0; i < 10; i ++){
        btTransform t;
        t.setIdentity();
        t.setOrigin(btVector3(0.2*i,3*i+3,0));

        Instance instance;
        if(i%2 == 0)
        {
            instance = Instance(&m_models.at("cube"), t, 3.0f, m_shapes.at("cube"));
        }
        else
        {
            instance = Instance(&m_models.at("sphere"), t, 3.0f, m_shapes.at("sphere"));
        }


        m_physics.addBody(instance.physicsBody());

        m_instances.push_back(instance);
    }

    //addPlayer();

    Mesh mitsubaMesh;
    mitsubaMesh.loadOBJ((PATH + "/assets/gun.obj").data());
    Model mitsubaModel = Model(mitsubaMesh);
    mitsubaModel.material().addTexture(m_assetManager.textures().at("gun_albedo"));
    mitsubaModel.material().addTexture(m_assetManager.textures().at("gun_metallic"));
    mitsubaModel.material().addTexture(m_assetManager.textures().at("gun_normal"));
    mitsubaModel.material().addTexture(m_assetManager.textures().at("gun_roughness"));
    mitsubaModel.material().addTexture(m_assetManager.textures().at("gun_ao"));
    mitsubaModel.material().addTexture(m_assetManager.textures().at("heightMap"));
    m_models.insert(std::make_pair("mitsuba", mitsubaModel));
    for(int i = 0; i < 2; i++)
    {
        t.setIdentity();
        t.setOrigin(btVector3(i * 10.0f, 5.01f, -45.0f));
        t.setBasis(t.getBasis().scaled(btVector3(5.0, 5.0, 5.0)));

        Instance instance;
/*
        if(i == 0)
        {
            t.setIdentity();
            t.setRotation(btQuaternion(90.0, 0.0, 0.0));
            t.setOrigin(btVector3(0.0, 1.0, 0.0));
        }
*/
        instance = Instance(&m_models.at("mitsuba"), t, 0.0f, m_shapes.at("cube"));




        std::cout << "instance id : " << m_instances.size() << std::endl;
        m_physics.addBody(instance.physicsBody());
        if(i == 1)
        {
            instance.addChild(&m_instances.back());
        }
        m_instances.push_back(instance);

    }

    int i = 0;
    for(Instance & inst : m_instances){
        inst.physicsBody()->setUserIndex(i);
        m_root.addChild(&inst);
        i++;
    }

}

float t = 0.5;
void Scene::update()
{

    // Check if any key is pressed to move the camera
    processInputs();
    m_physics.update();
    m_camera.update();

//    t += 0.001;
    glm::vec3 v = glm::vec3(cos(t), sin(t), 0.0);
    if(v.x < 0)
        m_directionalLight.setColor(glm::vec3(0.0, 0.0, 0.0));
    else
        m_directionalLight.setColor(glm::vec3(0.5, 0.5, 0.5));

    //m_exposure = 1.2f + sin(t);
    m_directionalLight.setPosition(v);
    glm::mat4 lightProjection;
    glm::mat4 lightView;
    lightProjection = glm::ortho(-50.0f, 50.0f, -50.0f, 50.0f, 0.1f, 142.0f);
    lightView = glm::lookAt(m_directionalLight.position()* glm::vec3(50, 50, 50),
                                     glm::vec3( 0.0f, 0.0f,  0.0f),
                                     glm::vec3( 0.0f, 1.0f,  0.0f));
//    m_lightSpaceMatrix = lightProjection * lightView;
    m_directionalLight.setLightSpaceMatrix(lightProjection * lightView);

}

void Scene::processInputs()
{
    bool cameraMoved = false;

    if(glfwGetKey(&m_window, GLFW_KEY_W) == GLFW_PRESS) {
        m_camera.moveForward();
        cameraMoved = true;
    }
    if(glfwGetKey(&m_window, GLFW_KEY_S) == GLFW_PRESS) {
        m_camera.moveBackward();
        cameraMoved = true;
    }
    if(glfwGetKey(&m_window, GLFW_KEY_A) == GLFW_PRESS) {
        m_camera.moveLeft();
        cameraMoved = true;
    }
    if(glfwGetKey(&m_window, GLFW_KEY_D) == GLFW_PRESS) {
        m_camera.moveRight();
        cameraMoved = true;
    }
    if(glfwGetKey(&m_window, GLFW_KEY_Q) == GLFW_PRESS) {
        m_camera.moveUp();
        cameraMoved = true;
    }
    if(glfwGetKey(&m_window, GLFW_KEY_E) == GLFW_PRESS) {
        m_camera.moveDown();
        cameraMoved = true;
    }
    if(glfwGetKey(&m_window, GLFW_KEY_B) == GLFW_PRESS) {
        btTransform t;
        t.setIdentity();
        t.setRotation(btQuaternion(0.01, 0.01, 0.01));
        m_instances.at(12).transform(t);
    }

}

void Scene::addPlayer()
{
    btTransform t;
    btMatrix3x3 matrix;


    t.setIdentity();
    t.setOrigin(btVector3(-20, 9.4, 0));
    matrix.setIdentity();
    matrix = matrix.scaled(btVector3(0.6, 0.6, 0.6));
    t.setBasis(matrix);
    Instance playerHead = Instance(&m_models.at("cube"), t , 0.0f, m_shapes.at("cube"));
    m_physics.addBody(playerHead.physicsBody());
    m_instances.push_back(playerHead);

    t.setIdentity();
    t.setOrigin(btVector3(-20, 7.1, 0));
    matrix.setIdentity();
    matrix = matrix.scaled(btVector3(1.5, 1.5, 0.8));
    t.setBasis(matrix);
    Instance playerTorso = Instance(&m_models.at("cube"), t, 0.0f, m_shapes.at("cube"));
    m_physics.addBody(playerTorso.physicsBody());
    m_instances.push_back(playerTorso);

    t.setIdentity();
    t.setOrigin(btVector3(-22.6, 7.8, 0));
    matrix.setIdentity();
    matrix.setEulerZYX(0, 0, -45);
    matrix = matrix.scaled(btVector3(0.3, 1, 0.3));
    t.setBasis(matrix);
    Instance playerLeftUpArm = Instance(&m_models.at("cube"), t, 0.0f, m_shapes.at("cube"));
    m_physics.addBody(playerLeftUpArm.physicsBody());
    m_instances.push_back(playerLeftUpArm);

    t.setOrigin(btVector3(-24.4, 6.7, 0));
    Instance playerLeftDownArm = Instance(&m_models.at("cube"), t, 0.0f, m_shapes.at("cube"));
    m_physics.addBody(playerLeftDownArm.physicsBody());
    m_instances.push_back(playerLeftDownArm);

    t.setIdentity();
    t.setOrigin(btVector3(-17.4, 7.8, 0));
    matrix.setIdentity();
    matrix.setEulerZYX(0, 0, 45);
    matrix = matrix.scaled(btVector3(0.3, 1, 0.3));
    t.setBasis(matrix);
    Instance playerRightUpArm = Instance(&m_models.at("cube"), t, 0.0f, m_shapes.at("cube"));
    m_physics.addBody(playerRightUpArm.physicsBody());
    m_instances.push_back(playerRightUpArm);

    t.setOrigin(btVector3(-15.6, 6.7, 0));
    Instance playerRightDownArm = Instance(&m_models.at("cube"), t, 0.0f, m_shapes.at("cube"));
    m_physics.addBody(playerRightDownArm.physicsBody());
    m_instances.push_back(playerRightDownArm);


    t.setIdentity();
    t.setOrigin(btVector3(-20, 4.7, 0));
    matrix.setIdentity();
    matrix = matrix.scaled(btVector3(1.4, 0.8, 0.7));
    t.setBasis(matrix);
    Instance playerPelvis = Instance(&m_models.at("cube"), t, 0.0f, m_shapes.at("cube"));
    m_physics.addBody(playerPelvis.physicsBody());
    m_instances.push_back(playerPelvis);

    t.setIdentity();
    t.setOrigin(btVector3(-21, 3.0, 0));
    matrix.setIdentity();
    matrix = matrix.scaled(btVector3(0.5, 0.8, 0.4));
    t.setBasis(matrix);
    Instance playerRightUpLeg = Instance(&m_models.at("cube"), t, 0.0f, m_shapes.at("cube"));
    m_physics.addBody(playerRightUpLeg.physicsBody());
    m_instances.push_back(playerRightUpLeg);

    t.setIdentity();
    t.setOrigin(btVector3(-21, 1.3, 0));
    matrix.setIdentity();
    matrix = matrix.scaled(btVector3(0.5, 0.8, 0.4));
    t.setBasis(matrix);
    Instance playerRightDownLeg = Instance(&m_models.at("cube"), t, 0.0f, m_shapes.at("cube"));
    m_physics.addBody(playerRightDownLeg.physicsBody());
    m_instances.push_back(playerRightDownLeg);

    t.setIdentity();
    t.setOrigin(btVector3(-19, 3.0, 0));
    matrix.setIdentity();
    matrix = matrix.scaled(btVector3(0.5, 0.8, 0.4));
    t.setBasis(matrix);
    Instance playerLeftUpLeg = Instance(&m_models.at("cube"), t, 0.0f, m_shapes.at("cube"));
    m_physics.addBody(playerLeftUpLeg.physicsBody());
    m_instances.push_back(playerLeftUpLeg);

    t.setIdentity();
    t.setOrigin(btVector3(-19, 1.3, 0));
    matrix.setIdentity();
    matrix = matrix.scaled(btVector3(0.5, 0.8, 0.4));
    t.setBasis(matrix);
    Instance playerLeftDownLeg = Instance(&m_models.at("cube"), t, 0.0f, m_shapes.at("cube"));
    m_physics.addBody(playerLeftDownLeg.physicsBody());
    m_instances.push_back(playerLeftDownLeg);

    t.setIdentity();
    t.setOrigin(btVector3(-21, 0.2, 0.5));
    matrix.setIdentity();
    matrix = matrix.scaled(btVector3(0.4, 0.2, 1));
    t.setBasis(matrix);
    Instance playerRightFoot = Instance(&m_models.at("cube"), t, 0.0f, m_shapes.at("cube"));
    m_physics.addBody(playerRightFoot.physicsBody());
    m_instances.push_back(playerRightFoot);

    t.setIdentity();
    t.setOrigin(btVector3(-19, 0.2, 0.5));
    matrix.setIdentity();
    matrix = matrix.scaled(btVector3(0.4, 0.2, 1));
    t.setBasis(matrix);
    Instance playerLeftFoot = Instance(&m_models.at("cube"), t, 0.0f, m_shapes.at("cube"));
    m_physics.addBody(playerLeftFoot.physicsBody());
    m_instances.push_back(playerLeftFoot);


}

void Scene::addBox()
{
    btTransform t;
    btQuaternion quat;

    btMotionState* motion;
    btStaticPlaneShape* plane;
    btRigidBody* body;

    t.setIdentity();
    t.setOrigin(btVector3(0, 0, 0));

    motion = new btDefaultMotionState(t);
    plane = new btStaticPlaneShape(btVector3(0,1,0), btScalar(0));
    body = new btRigidBody(btRigidBody::btRigidBodyConstructionInfo(0.0f, motion, plane));
    body->setRestitution(0.1f);
    body->setFriction(0.5f);
    m_physics.addBody(body);

    t.setIdentity();
    t.setOrigin(btVector3(-50, 0, 0));

    motion = new btDefaultMotionState(t);
    plane = new btStaticPlaneShape(btVector3(1,0,0), btScalar(0));
    body = new btRigidBody(btRigidBody::btRigidBodyConstructionInfo(0.0f, motion, plane));
    body->setRestitution(0.1f);
    body->setFriction(0.5f);
    m_physics.addBody(body);

    t.setIdentity();
    t.setOrigin(btVector3(50, 0, 0));

    motion = new btDefaultMotionState(t);
    plane = new btStaticPlaneShape(btVector3(-1,0,0), btScalar(0));
    body = new btRigidBody(btRigidBody::btRigidBodyConstructionInfo(0.0f, motion, plane));
    body->setRestitution(0.1f);
    body->setFriction(0.5f);
    m_physics.addBody(body);

    t.setIdentity();
    t.setOrigin(btVector3(0, 0, -50));

    motion = new btDefaultMotionState(t);
    plane = new btStaticPlaneShape(btVector3(0,0,1), btScalar(0));
    body = new btRigidBody(btRigidBody::btRigidBodyConstructionInfo(0.0f, motion, plane));
    body->setRestitution(0.1f);
    body->setFriction(0.5f);
    m_physics.addBody(body);

    t.setIdentity();
    t.setOrigin(btVector3(0, 0, 50));

    motion = new btDefaultMotionState(t);
    plane = new btStaticPlaneShape(btVector3(0,0, -1), btScalar(0));
    body = new btRigidBody(btRigidBody::btRigidBodyConstructionInfo(0.0f, motion, plane));
    body->setRestitution(0.1f);
    body->setFriction(0.5f);
    m_physics.addBody(body);

    t.setIdentity();
    t.setOrigin(btVector3(0, 50, 0));

    motion = new btDefaultMotionState(t);
    plane = new btStaticPlaneShape(btVector3(0,-1,0), btScalar(0));
    body = new btRigidBody(btRigidBody::btRigidBodyConstructionInfo(0.0f, motion, plane));
    body->setRestitution(0.1f);
    body->setFriction(0.5f);
    m_physics.addBody(body);
}

Scene::~Scene()
{
    // Properly de-allocate all resources once they've outlived their purpose
    for(auto model : m_models){
        MeshInfo info = model.second.meshInfo();
        info.clean();
    }

}

Instance* Scene::getPickedInstance()
{
    if(m_mousePicker.pickedBody())
    {
        btRigidBody* body = m_mousePicker.pickedBody();
        for(int i = 0; i < m_instances.size(); i++)
        {
            if(body->getUserIndex() == m_instances[i].physicsBody()->getUserIndex())
            {
                return &m_instances[i];
            }
        }
    }
    else return nullptr;
}

void Scene::addLights()
{

    m_directionalLight = DirectionalLight( /*color */  glm::vec3(0.5f, 0.5f, 0.5f),
                                           /*position*/  glm::vec3(0.0f, 1.0f, -1.0f));
    m_directionalLight.setStrength(3.0f);

    m_lights.push_back(PointLight(     /*color */  glm::vec3(0.01f, 0.01f, 0.81f),
                                         /*position*/  glm::vec3(-20.0f, 5.0f, 0.0f),
                                    /* radius */ 10.0f));

    m_lights.push_back(PointLight(     /*color */  glm::vec3(0.01f, 0.81f, 0.01f),
                                         /*position*/  glm::vec3(0.0f, 5.0f, 0.0f),
                                   5.0f));

    m_lights.push_back(PointLight(      /*color */  glm::vec3(0.81f, 0.01f, 0.01f),
                                         /*position*/  glm::vec3(20.0f, 5.0f, 0.0f),
                                   15.0f));



}

void Scene::mouseCallback(int button, int action)
{    
    if(action == GLFW_RELEASE && m_mousePicker.pickedConstraint())
        m_mousePicker.removePickingConstraint();

    if(button == GLFW_MOUSE_BUTTON_LEFT && action == GLFW_PRESS) {
        m_mousePicker.pickBody(m_rayFrom, m_rayTo);
    }
}

void Scene::cursorCallback(double xpos, double ypos)
{

    if(m_mouse.rbDown())
        m_camera.rotateBy(-m_mouse.deltaY()*m_mouseSensitivity, m_mouse.deltaX()*m_mouseSensitivity);

    m_rayFrom = btVector3(m_camera.getPos().x, m_camera.getPos().y, m_camera.getPos().z);
    m_rayTo = getRayToMousePos();

    m_mousePicker.movePickedBody(m_rayFrom, m_rayTo);

}

btVector3 Scene::getRayToMousePos()
{
    float x = ((float)(2 * m_mouse.x()) / (float)m_screen.width()) - 1;
    float y = ((float)(2 * m_mouse.y()) / (float)m_screen.height()) - 1;

    glm::vec4 clipCoords(x, -y, -1.0f, 1.0f);

    glm::mat4 invProj = glm::inverse(m_camera.getProjectionMatrix());

    glm::vec4 tmp = invProj * clipCoords;

    glm::vec4 eyeCoords(tmp.x, tmp.y, -1.0f, 0.0f);

    glm::mat4 invView = glm::inverse(m_camera.getViewMatrix());


    glm::vec4 tmpRay = invView * eyeCoords;

    glm::vec3 ray(tmpRay.x, tmpRay.y, tmpRay.z);
    glm::normalize(ray);

    btVector3 rayTo = btVector3(ray.x * m_camera.far(), ray.y * m_camera.far(), ray.z * m_camera.far());

    return rayTo;
}

void Scene::framebufferSizeCallback()
{
    m_camera.update(m_screen.x2(), m_screen.y2());
}
