#include "lightpickwidget.h"
#include "lightlabel.h"

LightPickWidget::LightPickWidget(GUI& gui, Rectangle r)
    :Widget(gui, "LightPicker", r),
      m_container(gui)
{
    m_children.push_back(std::shared_ptr<Container>(&m_container));


}

LightPickWidget::LightPickWidget(GUI& gui, std::vector<Light>& lights, Rectangle r)
    :Widget(gui, "LightPicker", r),
      m_container(gui)
{
    m_children.push_back(std::shared_ptr<Container>(&m_container));

    m_container.setVertical(true);


}
