#ifndef LIGHTPICKWIDGET_H
#define LIGHTPICKWIDGET_H

#include "widget.h"
#include "light.h"

class LightLabel;

class LightPickWidget : public Widget
{
public:
    LightPickWidget(GUI& gui, Rectangle r);
    LightPickWidget(GUI& gui, std::vector<Light>& lights, Rectangle r);

    inline Container& getContainer()
    {
        return m_container;
    }

private:
    Container m_container;

};

#endif // LIGHTPICKWIDGET_H
