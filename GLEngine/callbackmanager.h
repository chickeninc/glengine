#ifndef CALLBACKMANAGER_H
#define CALLBACKMANAGER_H

// GLFW
#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <iostream>
#include <memory>
#include "scene.h"

#include "gui.h"

class CallbackManagerBase
{

public:
    virtual void cursor_position_callback(GLFWwindow* window, double xpos, double ypos) = 0;
    virtual void key_callback(GLFWwindow* window, int key, int scancode, int action, int mode) = 0;
    virtual void mouse_callback(GLFWwindow* window, int button, int action, int mods) = 0;
    virtual void scroll_callback(GLFWwindow* window, double xoffset, double yoffset) = 0;
    virtual void framebuffer_size_callback(GLFWwindow* window, int width, int height) = 0;
    virtual void error_callback(int error, const char* desc) = 0;

    static CallbackManagerBase *event_handling_instance;

    void setEventHandling();

    static void cursor_position_callback_dispatch(GLFWwindow* window, double xpos, double ypos)
    {
        if(event_handling_instance)
            event_handling_instance->cursor_position_callback(window, xpos, ypos);
    }
    static void key_callback_dispatch(GLFWwindow *window, int key, int scancode, int action, int mods)
    {
        if(event_handling_instance)
            event_handling_instance->key_callback(window, key, scancode, action, mods);
    }

    static void mouse_callback_dispatch(GLFWwindow* window, int button, int action, int mods)
    {
        if(event_handling_instance)
            event_handling_instance->mouse_callback(window, button, action, mods);
    }

    static void scroll_callback_dispatch(GLFWwindow* window, double xoffset, double yoffset)
    {
        if(event_handling_instance)
            event_handling_instance->scroll_callback(window, xoffset, yoffset);
    }

    static void framebuffer_size_callback_dispatch(GLFWwindow* window, int width, int height)
    {
        if(event_handling_instance)
            event_handling_instance->framebuffer_size_callback(window, width, height);
    }

    static void error_callback_dispatch(int error, const char* desc)
    {
        if(event_handling_instance)
            event_handling_instance->error_callback(error, desc);
    }
};

class CallbackManager : CallbackManagerBase
{  

public:
    CallbackManager(Rectangle& screen);

    void cursor_position_callback(GLFWwindow* window, double xpos, double ypos) override;
    void key_callback(GLFWwindow* window, int key, int scancode, int action, int mode) override;
    void mouse_callback(GLFWwindow*, int button, int action, int mods) override;
    void scroll_callback(GLFWwindow* window, double xoffset, double yoffset) override;
    void framebuffer_size_callback(GLFWwindow* window, int width, int height) override;
    void error_callback(int error, const char* desc) override;

    inline Mouse& mouse()
    {
        return m_mouse;
    }

    inline void setGUI(GUI* gui)
    {
        m_gui = gui;
    }

    inline void setScene(Scene* scene)
    {
        m_scene = scene;
    }

    inline void setRenderer(Renderer* r)
    {
        m_renderer = r;
    }

private:
    GUI* m_gui = nullptr;
    Scene* m_scene = nullptr;
    Renderer* m_renderer = nullptr;
    Rectangle& m_screen;
    Mouse m_mouse;
};

#endif // CALLBACKMANAGER_H
