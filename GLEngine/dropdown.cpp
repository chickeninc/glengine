#include "dropdown.h"
#include <GLFW/glfw3.h>

DropDown::DropDown(GUI& gui, std::string text)
    :UIElement(gui),
      m_text(text)
{
    m_type = Type::DROPDOWN;
}

DropDown::DropDown(GUI& gui, Rectangle r, std::string text)
    :UIElement(gui, r),
      m_text(text)
{
    m_type = Type::DROPDOWN;
}

void DropDown::updateChild()
{
    if(m_child)
    {
        int i = 0;

        int x = m_bounds.x2() + m_offsetX;
        int y = m_bounds.y() + m_offsetY;
        int x2 = x + m_child->bounds().width();
        int y2 = y + m_child->bounds().height();

        Rectangle r(x, y, x2, y2);
        m_child->setBounds(r);

    }

}

void DropDown::setBounds(Rectangle r)
{
    m_bounds = r;
    updateChild();    
}

void DropDown::translate(int x, int y)
{

    m_bounds.translate(x, y);
    m_child->translate(x, y);

}

void DropDown::draw(NVGcontext* vg)
{
    float x = m_bounds.x()             + m_offsetX;
    float y = m_bounds.y()             + m_offsetY;
    float h = m_bounds.height()        - (2*m_offsetX);
    float w = m_bounds.width()         - (2*m_offsetY);

    NVGpaint bg;
    char icon[8];
    float cornerRadius = 4.0f;

    bg = nvgLinearGradient(vg, x,y,x,y+h, nvgRGBA(255,255,255,16), nvgRGBA(0,0,0,16));
    nvgBeginPath(vg);
    nvgRoundedRect(vg, x+1,y+1, w-2,h-2, cornerRadius-1);
    nvgFillPaint(vg, bg);
    nvgFill(vg);

    nvgBeginPath(vg);
    nvgRoundedRect(vg, x+0.5f,y+0.5f, w-1,h-1, cornerRadius-0.5f);
    nvgStrokeColor(vg, nvgRGBA(0,0,0,48));
    nvgStroke(vg);

    nvgFontSize(vg, 20.0f);
    nvgFontFace(vg, "sans");
    nvgFillColor(vg, nvgRGBA(255,255,255,160));
    nvgTextAlign(vg,NVG_ALIGN_LEFT|NVG_ALIGN_MIDDLE);
    nvgText(vg, x+h*0.3f,y+h*0.5f, m_text.data(), NULL);

    nvgFontSize(vg, h*1.3f);
    nvgFontFace(vg, "icons");
    nvgFillColor(vg, nvgRGBA(255,255,255,64));
    nvgTextAlign(vg,NVG_ALIGN_CENTER|NVG_ALIGN_MIDDLE);
    nvgText(vg, x+w-h*0.5f, y+h*0.5f, cpToUTF8(ICON_CHEVRON_RIGHT,icon), NULL);
}

void DropDown::mouseCallback(int button, int action)
{
    if(action == GLFW_PRESS)
    {
        if(m_child)
            m_child->setVisible(!m_child->isVisible());
    }    
}

void DropDown::setChild(Widget* widg)
{
    if(m_child)
        m_child->setHasParent(false);

    m_child = widg;
    m_child->setHasParent(true);
    m_child->setVisible(false);
    updateChild();
}

void DropDown::setVisible(bool b)
{
    m_isVisible = b;
    m_child->setVisible(b);
}
