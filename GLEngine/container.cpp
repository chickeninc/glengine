#include "container.h"

#include <iostream>

using Child = Container::Child;
using Children = Container::Children;


Container::Container(GUI& gui)
    :UIElement(gui)
{
    m_type = Type::CONTAINER;
}

Container::Container(GUI& gui, Rectangle r)
    : UIElement(gui, r)
{
    m_type = Type::CONTAINER;
}

void Container::addChild(Child e)
{
    m_children.push_back(e);
    updateChildren();
}

Child Container::insertChild(Child e)
{
    addChild(e);
    return shared_from_this();
}

Child Container::insertChildren(Children e)
{
    addChildren(e);
    return shared_from_this();
}

void Container::addChildren(Children e)
{
    for(Child c : e)
        addChild(c);
}

void Container::translate(int x, int y)
{

    m_bounds.translate(x, y);

    for (Child c : m_children) {
        c->translate(x, y);
    }
}

void Container::setVisible(bool b)
{
    UIElement::setVisible(b);
}

void Container::updateChildren()
{
    if(m_isVertical) {
        int i = 0;
        for(Child c : m_children){
            int height = (m_bounds.height() / m_children.size());
            int x = m_bounds.x();
            int y = m_bounds.y() + (i * height);
            int x2 = m_bounds.x2();
            int y2 = y + (height);
            Rectangle r(x,y,x2,y2);
            c->setBounds(r);
            i++;
        }
    } else {
        int i = 0;
        for(Child c : m_children){
            int width = (m_bounds.width()/ m_children.size());
            int x = m_bounds.x() + (i * width);
            int y = m_bounds.y();
            int x2 = x + width;
            int y2 = m_bounds.y2();
            Rectangle r(x,y,x2,y2);
            c->setBounds(r);
            i++;
        }
    }
}

void Container::setBounds(Rectangle r)
{
    m_bounds = r;
    updateChildren();
}

void Container::cursorCallback(double xpos, double ypos)
{
    for(Child c : m_children) {
        if(c->bounds().contains(xpos, ypos)){
            c->cursorCallback(xpos, ypos);
        }
    }
}

void Container::mouseCallback(int button, int action)
{
    for(Child c : m_children) {
        if(c->bounds().contains(m_mouse.x(), m_mouse.y()) && c->isVisible()){
            c->mouseCallback(button, action);
        }
    }
}

void Container::scrollCallback(double xoffset, double yoffset)
{
    for(Child c : m_children) {
        if(c->bounds().contains(m_mouse.x(), m_mouse.y()) && c->isVisible()){
            c->scrollCallback(xoffset, yoffset);
        }
    }
}


