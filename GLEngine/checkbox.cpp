#include "checkbox.h"
#include <GLFW/glfw3.h>

CheckBox::CheckBox(GUI& gui, std::string text)
    :UIElement(gui),
      m_text(text)
{
    m_type = Type::CHECKBOX;
}

CheckBox::CheckBox(GUI& gui, Rectangle r, std::string text)
    :UIElement(gui, r),
      m_text(text)
{
    m_type = Type::CHECKBOX;
}


void CheckBox::translate(int x, int y)
{
    m_bounds.translate(x, y);
    m_box.translate(x, y);
}

void CheckBox::mouseCallback(int button, int action)
{
    if(action == GLFW_PRESS && button == GLFW_MOUSE_BUTTON_LEFT && m_mouseOverBox)
        flip();

    const bool b = m_checked;
    if(m_affectedBoolean)
        (*m_affectedBoolean) = b;
}

void CheckBox::setBounds(Rectangle r)
{
    m_bounds = r;
    float x1 = r.x() + m_offsetX + 1;
    float y1 = r.y() + m_offsetY + (int)(r.height()/2) -9;
    float x2 = x1 + 18;
    float y2 = y1 + 18;
    m_box = Rectangle(x1,y1,x2,y2);
}

void CheckBox::cursorCallback(double xpos, double ypos)
{
    m_box.contains(m_mouse.x(), m_mouse.y()) ?
                m_mouseOverBox = true : m_mouseOverBox = false;
}

void CheckBox::draw(NVGcontext* vg)
{
    float x =  m_bounds.x()             + m_offsetX;
    float y =  m_bounds.y()             + m_offsetY;
    float w =  m_bounds.width()         - (2*m_offsetX);
    float h =  m_bounds.height()         - (2*m_offsetY);
    NVGpaint bg;
    char icon[8];
    NVG_NOTUSED(w);

    nvgFontSize(vg, 18.0f);
    nvgFontFace(vg, "sans");
    nvgFillColor(vg, nvgRGBA(255,255,255,160));

    nvgTextAlign(vg,NVG_ALIGN_LEFT|NVG_ALIGN_MIDDLE);
    nvgText(vg, x+28,y+h*0.5f, m_text.data(), NULL);

    bg = nvgBoxGradient(vg, x+1,y+(int)(h*0.5f)-9+1, 18,18, 3,3, nvgRGBA(0,0,0,32), nvgRGBA(0,0,0,92));
    nvgBeginPath(vg);
    nvgRoundedRect(vg, x+1,y+(int)(h*0.5f)-9, 18,18, 3);
    nvgFillPaint(vg, bg);
    nvgFill(vg);

    nvgFontSize(vg, 40);
    nvgFontFace(vg, "icons");
    if(m_checked)
        nvgFillColor(vg, nvgRGBA(255,255,255,128));
    nvgTextAlign(vg,NVG_ALIGN_CENTER|NVG_ALIGN_MIDDLE);
    nvgText(vg, x+9+2, y+h*0.5f, cpToUTF8(ICON_CHECK,icon), NULL);
}
