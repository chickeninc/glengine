#include "mousepicker.h"

MousePicker::MousePicker()
{

}

MousePicker::MousePicker(btDynamicsWorld* world)
    :m_world(world)
{

}

void MousePicker::update(Camera& camera, Mouse mouse, Rectangle screen)
{
    float x = ((float)(2 * mouse.x()) / screen.width()) - 1;
    float y = ((float)(2 * mouse.y()) / screen.height()) - 1;

    glm::vec4 clipCoords(x, -y, -1.0f, 1.0f);

    glm::mat4 invProj = glm::inverse(camera.getProjectionMatrix());

    glm::vec4 tmp = invProj * clipCoords;

    glm::vec4 eyeCoords(tmp.x, tmp.y, -1.0f, 0.0f);

    glm::mat4 invView = glm::inverse(camera.getViewMatrix());

    glm::vec4 tmpRay = invView * eyeCoords;

    glm::vec3 ray(tmpRay.x, tmpRay.y, tmpRay.z);
    glm::normalize(ray);

    btVector3 rayFrom(camera.getPos().x, camera.getPos().y, camera.getPos().z);

    btVector3 rayTo(ray.x * camera.far(), ray.y * camera.far(), ray.z * camera.far());

    btCollisionWorld::ClosestRayResultCallback rayCallback(rayFrom, rayTo);

    m_world->rayTest(rayFrom, rayTo, rayCallback);
    if(rayCallback.hasHit()) {
        std::cout << "hit" << std::endl;
        glm::vec3 v(rayCallback.m_hitPointWorld.x(),rayCallback.m_hitPointWorld.y(),rayCallback.m_hitPointWorld.z());

        std::cout << "user index:" << rayCallback.m_collisionObject->getUserIndex() << std::endl;

        btVector3 pickPos = rayCallback.m_hitPointWorld;
        btRigidBody* body = (btRigidBody*)btRigidBody::upcast(rayCallback.m_collisionObject);
        if (body) {
            if (!(body->isStaticObject() || body->isKinematicObject())) {
                m_pickedBody = body;
                m_savedState = m_pickedBody->getActivationState();
                m_pickedBody->setActivationState(DISABLE_DEACTIVATION);

                btVector3 localPivot = body->getCenterOfMassTransform().inverse() * pickPos;
                btPoint2PointConstraint* p2p = new btPoint2PointConstraint(*body, localPivot);

                m_world->addConstraint(p2p, true);
                m_pickedConstraint = p2p;

                btScalar mousePickClamping = 30.f;
                p2p->m_setting.m_impulseClamp = mousePickClamping;
                p2p->m_setting.m_tau = 0.001f;

            }
        }
        m_oldPickingPos = rayTo;
        m_hitPos = pickPos;
        m_oldPickingDist = (pickPos - rayFrom).length();
    }
    else {
        std::cout << "no hit" << std::endl;
    }

}

bool MousePicker::pickBody(const btVector3& rayFromWorld, const btVector3& rayToWorld)
    {
        if (m_world==0)
            return false;

        btCollisionWorld::ClosestRayResultCallback rayCallback(rayFromWorld, rayToWorld);

        m_world->rayTest(rayFromWorld, rayToWorld, rayCallback);
        if (rayCallback.hasHit())
        {

            btVector3 pickPos = rayCallback.m_hitPointWorld;
            btRigidBody* body = (btRigidBody*)btRigidBody::upcast(rayCallback.m_collisionObject);
            if (body)
            {                
                m_pickedBody = body;

                //other exclusions?
                if (!(body->isStaticObject() || body->isKinematicObject()))
                {
                    m_savedState = m_pickedBody->getActivationState();
                    m_pickedBody->setActivationState(DISABLE_DEACTIVATION);
                    //printf("pickPos=%f,%f,%f\n",pickPos.getX(),pickPos.getY(),pickPos.getZ());
                    btVector3 localPivot = body->getCenterOfMassTransform().inverse() * pickPos;
                    btPoint2PointConstraint* p2p = new btPoint2PointConstraint(*body, localPivot);
                    m_world->addConstraint(p2p, true);
                    m_pickedConstraint = p2p;
                    btScalar mousePickClamping = 30.f;
                    p2p->m_setting.m_impulseClamp = mousePickClamping;
                    //very weak constraint for picking
                    p2p->m_setting.m_tau = 0.001f;
                }
            }


            m_oldPickingPos = rayToWorld;
            m_hitPos = pickPos;
            m_oldPickingDist = (pickPos - rayFromWorld).length();
            //					printf("hit !\n");
        }
        return false;
    }

bool MousePicker::movePickedBody(const btVector3& rayFromWorld, const btVector3& rayToWorld)
{
    if (m_pickedBody  && m_pickedConstraint)
            {
                btPoint2PointConstraint* pickCon = static_cast<btPoint2PointConstraint*>(m_pickedConstraint);
                if (pickCon)
                {
                    //keep it at the same picking distance

                    btVector3 newPivotB;

                    btVector3 dir = rayToWorld - rayFromWorld;
                    dir.normalize();
                    dir *= m_oldPickingDist;

                    newPivotB = rayFromWorld + dir;
                    pickCon->setPivotB(newPivotB);
                    return true;
                }
            }
            return false;
}

void MousePicker::removePickingConstraint()
{
    if(m_pickedConstraint){
        m_pickedBody->forceActivationState(m_savedState);
        m_pickedBody->activate();
        m_world->removeConstraint(m_pickedConstraint);
        delete m_pickedConstraint;
        m_pickedConstraint = 0;
        m_pickedBody = 0;
    }
}
