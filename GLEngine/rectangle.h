#ifndef RECTANGLE_H
#define RECTANGLE_H

#include "point.h"
#include <cmath>
#include <glm/vec2.hpp>

class Rectangle
{
public:

    Rectangle();
    Rectangle(const Rectangle& r);
    Rectangle(int x, int y, int x2, int y2);
    Rectangle(Point p1, Point p2);

    bool contains(const int x, const int y) const;
    bool contains(const int x, const int y, const float amplitude) const;

    int overlapArea(const Rectangle& rect) const;

    void translate(const int x, const int y);

    void description() const;

    inline bool contains(const Point point) const
    {
        return contains(point.x(), point.y());
    }
    inline bool contains(const Rectangle& rect) const
    {
        return (contains(rect.x(), rect.y()) && contains(rect.x2(), rect.y2()));
    }

    inline bool overlaps(const Rectangle& r) const
    {
        return m_x < r.x() + r.width() && m_x + width() > r.x() && m_y < r.y() + r.height() && m_y + height() > r.y();
    }

    inline int getHCenter() const
    {
        return (int)(m_x + m_x2) / 2;
    }

    inline int getVCenter() const
    {
        return (int)(m_y + m_y2) / 2;
    }

    inline Point getCenter() const
    {
        return Point(getHCenter(), getVCenter());
    }

    inline uint width() const
    {
        return std::abs(m_x2 - m_x);
    }

    inline uint height() const
    {
        return std::abs(m_y2 - m_y);
    }

    inline int getArea() const
    {
        return width() * height();
    }

    inline int x() const
    {
        return m_x;
    }

    inline int y() const
    {
        return m_y;
    }

    inline int x2() const
    {
        return m_x2;
    }

    inline int y2() const
    {
        return m_y2;
    }

    inline void addX(const int x)
    {
        m_x += x;
    }

    inline void addY(const int y)
    {
        m_y += y;
    }

    inline void addX2(const int x2)
    {
        m_x2 += x2;
    }

    inline void addY2(const int y2)
    {
        m_y2 += y2;
    }

    inline void setX(const int x)
    {
        m_x = x;
    }

    inline void setY(const int y)
    {
        m_y = y;
    }

    inline void setX2(const int x2)
    {
        m_x2 = x2;
    }

    inline void setY2(const int y2)
    {
        m_y2 = y2;
    }

    inline void translate(const glm::vec2 v)
    {
        translate(v.x, v.y);
    }

private:
    int m_x = 0;
    int m_y = 0;
    int m_x2 = 0;
    int m_y2 = 0;
};

#endif // RECTANGLE_H
