#include "controller.h"
#include "postprocess.h"


Controller::Controller(Scene& scene, GUI& ui, Renderer& renderer)
    :scene(scene),
      ui(ui),
      renderer(renderer)
{
    std::static_pointer_cast<CheckBox>(ui.getElements().at("wireMode"))->
                                       setAffectedBoolean(&renderer.displayWireFrame());
    std::static_pointer_cast<CheckBox>(ui.getElements().at("normalMode"))->
                                       setAffectedBoolean(&renderer.displayNormals());
    std::static_pointer_cast<CheckBox>(ui.getElements().at("lightsMode"))->
                                       setAffectedBoolean(&renderer.displayLights());

    std::static_pointer_cast<CheckBox>(ui.getElements().at("deferredNormals"))->
                                       setAffectedBoolean(&renderer.displayDeferredNormalsOnly());
    std::static_pointer_cast<CheckBox>(ui.getElements().at("deferredAmbient"))->
                                       setAffectedBoolean(&renderer.displayDeferredAmbientOnly());
    std::static_pointer_cast<CheckBox>(ui.getElements().at("deferredReflected"))->
                                       setAffectedBoolean(&renderer.displayDeferredReflectedOnly());

    std::static_pointer_cast<CheckBox>(ui.getElements().at("deferredPositions"))->
                                       setAffectedBoolean(&renderer.displayDeferredPositionOnly());
    std::static_pointer_cast<CheckBox>(ui.getElements().at("deferredColor"))->
                                       setAffectedBoolean(&renderer.displayDeferredColorOnly());
    std::static_pointer_cast<CheckBox>(ui.getElements().at("deferredShadows"))->
                                       setAffectedBoolean(&renderer.displayDeferredShadowsOnly());

    std::static_pointer_cast<CheckBox>(ui.getElements().at("deferredDepth"))->
                                       setAffectedBoolean(&renderer.displayDeferredDepthOnly());

    std::static_pointer_cast<CheckBox>(ui.getElements().at("deferredBloom"))->
                                       setAffectedBoolean(&renderer.displayDeferredBloomOnly());

    std::static_pointer_cast<CheckBox>(ui.getElements().at("deferredScaterred"))->
                                       setAffectedBoolean(&renderer.displayDeferredScaterredOnly());
}

void Controller::control()
{

    PostProcessMode ppMode;
    if (std::static_pointer_cast<Button>(ui.getElements().at("invertButton"))->isPressed())
        ppMode = ppMode | PostProcessMode::INVERT;
    if (std::static_pointer_cast<Button>(ui.getElements().at("grayscaleButton"))->isPressed())
        ppMode = ppMode | PostProcessMode::GRAYSCALE;
    if (std::static_pointer_cast<Button>(ui.getElements().at("kernelButton"))->isPressed())
        ppMode = ppMode | PostProcessMode::KERNEL;
    if (std::static_pointer_cast<Button>(ui.getElements().at("blurButton"))->isPressed())
        ppMode = ppMode | PostProcessMode::BLUR;
    if (std::static_pointer_cast<Button>(ui.getElements().at("edgeButton"))->isPressed())
        ppMode = ppMode | PostProcessMode::EDGE;
    if (std::static_pointer_cast<Button>(ui.getElements().at("cartoonButton"))->isPressed())
        ppMode = ppMode | PostProcessMode::CARTOON;
    if (std::static_pointer_cast<Button>(ui.getElements().at("fxaaButton"))->isPressed())
        ppMode = ppMode | PostProcessMode::FXAA;

    if (std::static_pointer_cast<Button>(ui.getElements().at("gammaButton"))->isPressed())
        renderer.setGammaCorrection(true);
    else renderer.setGammaCorrection(false);

    renderer.setPostProcessMode(ppMode);

    std::ostringstream ss;
    glm::vec3 camPos = scene.camera().getPos();
    ss << camPos.x << " " << camPos.y << " " << camPos.z;
    std::string cameraString(ss.str());

    //std::static_pointer_cast<Paragraph>(ui.elems.at("paragraph"))->addText(cameraString);

}
