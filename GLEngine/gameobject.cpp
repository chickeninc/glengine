#include "gameobject.h"

GameObject::GameObject(btTransform t, float mass, btCollisionShape *shape, bool isKinematic)
{
    btMotionState* ms = new btDefaultMotionState(t);

    if(mass == 0) {
        m_physicsBody = new btRigidBody(btRigidBody::btRigidBodyConstructionInfo(0.0f, ms, shape));
        m_physicsBody->setRestitution(0.1f);
        if(isKinematic)
            m_physicsBody->setCollisionFlags(btCollisionObject::CF_KINEMATIC_OBJECT);
        m_physicsBody->setFriction(0.5f);
    } else {
        btVector3 localInertia(0, 0, 0);
        shape->calculateLocalInertia(mass, localInertia);

        m_physicsBody = new btRigidBody(btRigidBody::btRigidBodyConstructionInfo(mass, ms, shape, localInertia));
        m_physicsBody->setRestitution(0.5f);
        m_physicsBody->setFriction(0.5f);
    }

    m_physicsBody->setCollisionFlags(m_physicsBody->getCollisionFlags() | btCollisionObject::CF_CUSTOM_MATERIAL_CALLBACK);


}

btScalar* GameObject::getWorldTransform()
{
    btTransform t = btTransform::getIdentity();
    if(m_physicsBody)
    {
        m_physicsBody->getMotionState()->getWorldTransform(t);
    }
    t.getOpenGLMatrix(m_worldTransform);
    return m_worldTransform;
}

void GameObject::transform(btTransform t)
{


    btTransform transform = btTransform();
    m_physicsBody->getMotionState()->getWorldTransform(transform);
/*
    if(m_parent)
    {
        btTransform pt;
        pt.setIdentity();
        pt.setFromOpenGLMatrix(m_parent->getWorldTransform());
        transform = transform * pt;
    }
  */



    transform = transform * t;
    m_physicsBody->getMotionState()->setWorldTransform(transform);


    for(GameObject* c : m_children)
    {

        c->transform(t);
    }



}
