#include "renderer.h"
#include "gui.h"
#include "GLFW/glfw3.h"
#include <random>
#include "debugger.h"

#define DEBUG_DRAW_IMPLEMENTATION
#include "debug_draw.h"

const std::string PATH = "/home/lebasith/Git/cpp/GLEngine";
using namespace Debugger;

void CheckForGLError()
{
    GLenum error;
    while ((error = glGetError()) != GL_NO_ERROR)
    {
        std::cout << "ERROR:  (Renderer)  \t";
        if (error == GL_INVALID_ENUM)
            std::cout << "GL_INVALID_ENUM";
        if (error == GL_INVALID_VALUE)
            std::cout << "GL_INVALID_VALUE";
        if (error == GL_INVALID_OPERATION)
            std::cout << "GL_INVALID_OPERATION";
        if (error == GL_INVALID_FRAMEBUFFER_OPERATION)
            std::cout << "GL_INVALID_FRAMEBUFFER_OPERATION";
        if (error == GL_OUT_OF_MEMORY)
            std::cout << "GL_OUT_OF_MEMORY";
        if (error == GL_STACK_UNDERFLOW)
            std::cout << "GL_STACK_UNDERFLOW";
        if (error == GL_STACK_OVERFLOW)
            std::cout << "GL_STACK_OVERFLOW";
        std::cout << std::endl;
    }
}

GLContextInfo Renderer::getContextInfos()
{
    GLContextInfo infos;

    glGetIntegerv(GL_MAJOR_VERSION, &infos.Version.Major);
    glGetIntegerv(GL_MINOR_VERSION, &infos.Version.Minor);
    infos.Version.Driver = (const char*)glGetString(GL_VERSION);
    infos.Vendor = (const char*)glGetString(GL_VENDOR);
    infos.Renderer = (const char*)glGetString(GL_RENDERER);
    infos.Version.ShadingLanguage = (const char*)glGetString(GL_SHADING_LANGUAGE_VERSION);

    GLint numberofextensions = 0;
    glGetIntegerv(GL_NUM_EXTENSIONS, &numberofextensions);
    for (int i = 0; i < numberofextensions; i++)
        infos.SupportedExtensions.push_back((const char*)glGetStringi(GL_EXTENSIONS, i));

    GLint numberofsupportedglslversions = 0;
    glGetIntegerv(GL_NUM_SHADING_LANGUAGE_VERSIONS, &numberofsupportedglslversions);
    for (int i = 0; i < numberofsupportedglslversions; i++)
        infos.SupportedGLSLVersions.push_back((const char*)glGetStringi(GL_SHADING_LANGUAGE_VERSION, i));

    return infos;
}

float lerp(const float a, const float b, const float f)
{
    return a + f * (b -a);
}

void Renderer::printContextInfo()
{
    GLContextInfo ci = m_glContextInfo;

    m_gui.console().addText("OpenGL Context Initialized");
    m_gui.console().addText("Version : " + std::to_string(ci.Version.Major) + "." + std::to_string(ci.Version.Minor));
    m_gui.console().addText("Driver : " + ci.Version.Driver);
    m_gui.console().addText("GLSL Version: " + ci.Version.ShadingLanguage);
    m_gui.console().addText("Renderer : " + ci.Renderer);
    m_gui.console().addText("Vendor : " + ci.Vendor);
}

Renderer::Renderer(GLFWwindow& window, Scene& scene, GUI& gui, Rectangle& screen, AssetManager& assetManager)
    :m_window(window),
      m_scene(scene),
      m_gui(gui),
      m_screen(screen),
      m_assetManager(assetManager),
      m_glContextInfo(getContextInfos()),
      m_gBuffer(WIDTH, HEIGHT),
      m_primaryFBO(WIDTH, HEIGHT),
      m_secondaryFBO(WIDTH, HEIGHT),
      m_bloomFBO(WIDTH, HEIGHT),
      m_bloomFBObis(WIDTH, HEIGHT),
      m_renderFBO(WIDTH, HEIGHT),
      m_scatterFBO(WIDTH, HEIGHT),
      m_bloomTexture(WIDTH, HEIGHT),
      m_shadowMap(SHADOW_WIDTH, SHADOW_HEIGHT),
      m_shadowMapCube(SHADOW_WIDTH, SHADOW_HEIGHT),
      m_skyboxInfo(MeshType::SKYBOX),
      m_ppInfo(MeshType::POSTPROCESSQUAD),
      m_glDebugDrawer(m_scene.camera())
{
    m_captureProjection = glm::perspective(glm::radians(90.0f), 1.0f, 0.1f, 10.0f);

    m_captureViews[0] = glm::lookAt(glm::vec3(0.0f, 0.0f, 0.0f), glm::vec3( 1.0f,  0.0f,  0.0f), glm::vec3(0.0f, -1.0f,  0.0f));
    m_captureViews[1] = glm::lookAt(glm::vec3(0.0f, 0.0f, 0.0f), glm::vec3(-1.0f,  0.0f,  0.0f), glm::vec3(0.0f, -1.0f,  0.0f));
    m_captureViews[2] = glm::lookAt(glm::vec3(0.0f, 0.0f, 0.0f), glm::vec3( 0.0f,  1.0f,  0.0f), glm::vec3(0.0f,  0.0f,  1.0f));
    m_captureViews[3] = glm::lookAt(glm::vec3(0.0f, 0.0f, 0.0f), glm::vec3( 0.0f, -1.0f,  0.0f), glm::vec3(0.0f,  0.0f, -1.0f));
    m_captureViews[4] = glm::lookAt(glm::vec3(0.0f, 0.0f, 0.0f), glm::vec3( 0.0f,  0.0f,  1.0f), glm::vec3(0.0f, -1.0f,  0.0f));
    m_captureViews[5] = glm::lookAt(glm::vec3(0.0f, 0.0f, 0.0f), glm::vec3( 0.0f,  0.0f, -1.0f), glm::vec3(0.0f, -1.0f,  0.0f));


    //m_scene.physics().world()->setDebugDrawer(&m_glDebugDrawer);

    //m_scene.physics().world()->debugDrawWorld();

    printContextInfo();

    initGraph(&m_fpsGraph, GRAPH_RENDER_FPS, "Frame Time");
    initGraph(&m_cpuGraph, GRAPH_RENDER_MS, "CPU Time");

    // load all the shaders and store it in the shaders map
    if(!loadShaders()) std::cout << "Couldnt compile shaders" << std::endl;

    glGenQueries(11, m_queries);

    glEnable(GL_TEXTURE_CUBE_MAP_SEAMLESS);
    m_previousTime = glfwGetTime();
    m_time = glfwGetTime();
    m_deltaTime = m_time - m_previousTime;

    m_renderFBO.use();
    m_renderFBO.attach(m_bloomTexture, GL_COLOR_ATTACHMENT1);
    GLuint attachments[2] = {GL_COLOR_ATTACHMENT0, GL_COLOR_ATTACHMENT1};
    glDrawBuffers(2, attachments);
    glBindFramebuffer(GL_FRAMEBUFFER, 0);

    glGenFramebuffers(1, &m_captureFBO);
    glGenRenderbuffers(1, &m_captureRBO);

    glBindFramebuffer(GL_FRAMEBUFFER, m_captureFBO);
    glBindRenderbuffer(GL_RENDERBUFFER, m_captureRBO);
    glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT24, 512, 512);
    glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, m_captureRBO);
    m_envCubeMap = convertEquirectanguralToCube(m_assetManager.textures().at("environmentMap"));
//    m_irradianceMap = convertEquirectanguralToCube(m_assetManager.textures().at("irradianceMap"), 32, 32);
    m_irradianceMap = cubeMapConvolution(m_assetManager.textures().at("environmentMap"));
    m_prefilteredMap = prefilterEnvMap(m_envCubeMap);

    glGenTextures(1, &m_brdfLUT);
    glBindTexture(GL_TEXTURE_2D, m_brdfLUT);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RG16F, 512, 512, 0, GL_RG, GL_FLOAT, 0);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glBindFramebuffer(GL_FRAMEBUFFER, m_captureFBO);
    glBindRenderbuffer(GL_RENDERBUFFER, m_captureRBO);
    glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT24, 512, 512);
    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, m_brdfLUT, 0);

    glViewport(0, 0, 512, 512);
    ShaderProgram shader = m_shaders.at(ShaderMode::BRDF);
    shader.Use();
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glBindVertexArray(m_ppInfo.VAO());
    glDrawArrays(GL_TRIANGLES, 0, 6);
    glBindVertexArray(0);
    glViewport(0, 0, m_screen.width(), m_screen.height());
    glBindFramebuffer(GL_FRAMEBUFFER, 0);

    GLint MaxPatchVertices = 0;
    glGetIntegerv(GL_MAX_PATCH_VERTICES, &MaxPatchVertices);
    std::cout << "Max supported patch vertices" << MaxPatchVertices << std::endl;
    glPatchParameteri(GL_PATCH_VERTICES, 3);
}

bool firstFrame = true;
void Renderer::render()
{


    //Get window current size
    glfwGetWindowSize(&m_window, &m_width, &m_height);
    glfwGetFramebufferSize(&m_window, &m_fwidth, &m_fheight);

    m_time = glfwGetTime();
    m_deltaTime = m_time - m_previousTime;
    m_previousTime = m_time;

    m_primaryFBO.use();
    // First render Geometry to Gbuffer
    glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LESS);
    glEnable(GL_CULL_FACE);
    glDepthMask(GL_TRUE);

    if(!firstFrame)
        glGetQueryObjectuiv(m_queries[0], GL_QUERY_RESULT, &m_queryResults[0]);

    glBeginQuery(GL_TIME_ELAPSED, m_queries[0]);
    renderToGBuffer();


    renderStencilPass();

    // Copy depth to hdrBuffer
    glBindFramebuffer(GL_READ_FRAMEBUFFER, m_gBuffer.fbo().id());
    glBindFramebuffer(GL_DRAW_FRAMEBUFFER, m_renderFBO.id());
    glBlitFramebuffer(0, 0, m_screen.width(), m_screen.height(), 0, 0, m_screen.width(), m_screen.height(),
                      GL_DEPTH_BUFFER_BIT, GL_NEAREST);    

    glEndQuery(GL_TIME_ELAPSED);

    if(!firstFrame)
        glGetQueryObjectuiv(m_queries[1], GL_QUERY_RESULT, &m_queryResults[1]);

    glBeginQuery(GL_TIME_ELAPSED, m_queries[1]);
    renderShadows();
    glEndQuery(GL_TIME_ELAPSED);

    glDepthMask(GL_FALSE);

    // render to render buffer
    m_renderFBO.use();

    glDisable(GL_DEPTH_TEST);

    if(!firstFrame)
        glGetQueryObjectuiv(m_queries[2], GL_QUERY_RESULT, &m_queryResults[2]);

    glBeginQuery(GL_TIME_ELAPSED, m_queries[2]);
    renderLightQuad(ShaderMode::LIGHTDIRECTIONAL);
    glEndQuery(GL_TIME_ELAPSED);


    if(!firstFrame)
        glGetQueryObjectuiv(m_queries[3], GL_QUERY_RESULT, &m_queryResults[3]);

    glBeginQuery(GL_TIME_ELAPSED, m_queries[3]);
    glEnable(GL_BLEND);
    glBlendEquation(GL_FUNC_ADD);
    glBlendFunc(GL_ONE, GL_ONE);
//    renderLightQuad(ShaderMode::LIGHTDIRECTIONAL);
    for(Light& l : m_scene.lights())
    {
        glDepthMask(GL_TRUE);
        glEnable(GL_DEPTH_TEST);
        renderShadowsCube(l);
        glDisable(GL_DEPTH_TEST);
        glDepthMask(GL_FALSE);
        m_renderFBO.use();
        renderLightsSpheres(l);
    }
    glDisable(GL_BLEND);
    glEnable(GL_DEPTH_TEST);
    glEndQuery(GL_TIME_ELAPSED);

    if(!firstFrame)
        glGetQueryObjectuiv(m_queries[10], GL_QUERY_RESULT, &m_queryResults[10]);

    glBeginQuery(GL_TIME_ELAPSED, m_queries[10]);
    //renderLightScatter();
    glEndQuery(GL_TIME_ELAPSED);

    // render the rest of the scene which doesnt need bloom
    // (geometry and skybox)
    m_renderFBO.use();
    if(!firstFrame)
        glGetQueryObjectuiv(m_queries[4], GL_QUERY_RESULT, &m_queryResults[4]);

    glBeginQuery(GL_TIME_ELAPSED, m_queries[4]);
    renderSkybox();
    glEndQuery(GL_TIME_ELAPSED);

    if(!firstFrame)
        glGetQueryObjectuiv(m_queries[5], GL_QUERY_RESULT, &m_queryResults[5]);

    glBeginQuery(GL_TIME_ELAPSED, m_queries[5]);
    if(m_displayNormals || m_displayWireframe)
        renderGeometryShader();
    if(m_displayLights)
        renderLightsGeometry();
    glEndQuery(GL_TIME_ELAPSED);


    if(!firstFrame)
        glGetQueryObjectuiv(m_queries[6], GL_QUERY_RESULT, &m_queryResults[6]);

    glBeginQuery(GL_TIME_ELAPSED, m_queries[6]);
    m_secondaryFBO.use();
    glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    renderPostProcess(m_bloomTexture, PostProcessMode::BLOOMFILTER);
    renderBloom();
    glEndQuery(GL_TIME_ELAPSED);

    if(!firstFrame)
        glGetQueryObjectuiv(m_queries[7], GL_QUERY_RESULT, &m_queryResults[7]);

    glBeginQuery(GL_TIME_ELAPSED, m_queries[7]);
    m_primaryFBO.use();
    renderPostProcess(m_renderFBO.texColorBuffer(), PostProcessMode::PASSTHROUGH);

    m_secondaryFBO.use();
    renderPostProcess();
    glEndQuery(GL_TIME_ELAPSED);

    if(!firstFrame)
        glGetQueryObjectuiv(m_queries[8], GL_QUERY_RESULT, &m_queryResults[8]);

    glBeginQuery(GL_TIME_ELAPSED, m_queries[8]);
    // unbind fbo so we draw on screen
    glBindFramebuffer(GL_FRAMEBUFFER, 0);
    glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    //GLint id;
    glActiveTexture(GL_TEXTURE1);
    glBindTexture(GL_TEXTURE_2D, m_bloomFBO.texColorBuffer().ID());
    glActiveTexture(GL_TEXTURE2);
    glBindTexture(GL_TEXTURE_2D, m_scatterFBO.texColorBuffer().ID());

    GLint id;
    glActiveTexture(GL_TEXTURE0);
    glGetIntegerv(GL_TEXTURE_BINDING_2D, &id);

    id == m_primaryFBO.texColorBuffer().ID() ?
                renderPostProcess(m_secondaryFBO.texColorBuffer(), PostProcessMode::TONEMAPPING):
                renderPostProcess(m_primaryFBO.texColorBuffer(), PostProcessMode::TONEMAPPING);

    glEnable(GL_BLEND);
    glBlendEquation(GL_MAX);
    glBlendFunc(GL_ONE, GL_ONE);
    renderPostProcess(m_bloomFBO.texColorBuffer(), PostProcessMode::TONEMAPPING);

    glBlendEquation(GL_FUNC_ADD);
    glDisable(GL_BLEND);
    glEndQuery(GL_TIME_ELAPSED);

    glEnable(GL_DEPTH_TEST);

    glClear(GL_STENCIL_BUFFER_BIT);

    if(!firstFrame)
        glGetQueryObjectuiv(m_queries[9], GL_QUERY_RESULT, &m_queryResults[9]);

    glBeginQuery(GL_TIME_ELAPSED, m_queries[9]);


    nvgBeginFrame(m_gui.nvgContext(), m_width, m_height, (float)m_fwidth/m_width);
    m_gui.renderDemo(m_time);

    renderGraph(m_gui.nvgContext(), m_width - 5 - 200, m_height - 45, &m_fpsGraph);
    renderGraph(m_gui.nvgContext(), m_width - 5 - 200, m_height - (45) * 2, &m_cpuGraph);
    nvgEndFrame(m_gui.nvgContext());
    glEndQuery(GL_TIME_ELAPSED);

    // Measure the CPU time taken excluding swap buffers (as the swap may wait for GPU)
    double cpuTime = glfwGetTime() - m_time;

    updateGraph(&m_fpsGraph, m_deltaTime);
    updateGraph(&m_cpuGraph, cpuTime);

    std::cout.setf(std::ios_base::fixed, std::ios_base::floatfield);
    std::cout.precision(2);

   debug("GBuffer : " + std::to_string(m_queryResults[0] * 0.000001) +
                 "Shadows : " + std::to_string(m_queryResults[1] * 0.000001) +
                 "Ambient : " + std::to_string(m_queryResults[2] * 0.000001) +
                 "Lights : " + std::to_string(m_queryResults[3] * 0.000001) +
                 "Light Scatter : " + std::to_string(m_queryResults[10] * 0.000001) +
                 "Skybox : " + std::to_string(m_queryResults[4] * 0.000001) +
                 "Geometry : " + std::to_string(m_queryResults[5] * 0.000001) +
                 "Bloom : " + std::to_string(m_queryResults[6] * 0.000001) +
                 "PostProcess : " + std::to_string(m_queryResults[7] * 0.000001) +
                 "Final : " + std::to_string(m_queryResults[8] * 0.000001) +
                 "Gui : " + std::to_string(m_queryResults[9] * 0.000001));


    // Swap the screen buffers
    glfwSwapBuffers(&m_window);

    CheckForGLError();
    firstFrame = false;




}

void Renderer::renderStencilPass()
{
    ShaderProgram shader = m_shaders.at(ShaderMode::STENCILPASS);
    shader.Use();
    glClear(GL_STENCIL_BUFFER_BIT);

    glStencilFunc(GL_ALWAYS, 0, 0);

    glStencilOpSeparate(GL_BACK, GL_KEEP, GL_INCR_WRAP, GL_KEEP);
    glStencilOpSeparate(GL_FRONT, GL_KEEP, GL_DECR_WRAP, GL_KEEP);

    glUniformMatrix4fv(glGetUniformLocation(shader.program(), "projection"), 1, GL_FALSE, m_scene.camera().getProjection());
    glUniformMatrix4fv(glGetUniformLocation(shader.program(), "view"), 1, GL_FALSE, m_scene.camera().getView());


    Model model = m_scene.models().at("sphere");

    for(int i = 1; i < m_scene.lights().size(); i++)
    {
        if(m_scene.lights()[i].isLocal())
        {
            glUniform3f(glGetUniformLocation(shader.program(),
                                             ("light.position")),
                        m_scene.lights()[i].position().x, m_scene.lights()[i].position().y, m_scene.lights()[i].position().z);


            model.render(shader);
        }

    }

}

uint Renderer::cubeMapConvolution(Texture texture, const int width, const int height)
{
    glBindFramebuffer(GL_FRAMEBUFFER, m_captureFBO);
    glBindRenderbuffer(GL_RENDERBUFFER, m_captureRBO);
    glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT24, width, height);

    GLuint cubeMap;
    // Generate cubeMap texture which is gonna hold the data
    glGenTextures(1, &cubeMap);
    glBindTexture(GL_TEXTURE_CUBE_MAP, cubeMap);
    for (unsigned int i = 0; i < 6; ++i)
    {
        // note that we store each face with 16 bit floating point values
        glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X + i, 0, GL_RGB16F,
                     width, height, 0, GL_RGB, GL_FLOAT, nullptr);
    }
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

    GLuint id;
    ShaderProgram shader = m_shaders.at(ShaderMode::CUBEMAPCONVOLUTION);
    shader.Use();

    glActiveTexture(GL_TEXTURE0);
    texture.Use();

    glUniformMatrix4fv(glGetUniformLocation(shader.program(), "projection"),
                       1, GL_FALSE, glm::value_ptr(m_captureProjection));


    glBindFramebuffer(GL_FRAMEBUFFER, m_captureFBO);
    glViewport(0, 0, width, height);
    for (unsigned int i = 0; i < 6; ++i)
    {
        glUniformMatrix4fv(glGetUniformLocation(shader.program(), "view"),
                           1, GL_FALSE, glm::value_ptr(m_captureViews[i]));

        glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0,
                                   GL_TEXTURE_CUBE_MAP_POSITIVE_X + i, cubeMap, 0);

        glBindVertexArray(m_skyboxInfo.VAO());
        glDrawArrays(GL_TRIANGLES, 0, 36);
        glBindVertexArray(0);
    }

    glViewport(0, 0, m_screen.width(), m_screen.height());
    glBindFramebuffer(GL_FRAMEBUFFER, 0);

    return cubeMap;

}

uint Renderer::prefilterEnvMap(uint texture, const int width, const int height)
{
    glBindTexture(GL_TEXTURE_CUBE_MAP, texture);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
    glGenerateMipmap(GL_TEXTURE_CUBE_MAP);

    GLuint cubeMap;
    // Generate cubeMap texture which is gonna hold the data
    glGenTextures(1, &cubeMap);
    glBindTexture(GL_TEXTURE_CUBE_MAP, cubeMap);
    for (unsigned int i = 0; i < 6; ++i)
    {
        // note that we store each face with 16 bit floating point values
        glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X + i, 0, GL_RGB16F,
                     width, height, 0, GL_RGB, GL_FLOAT, nullptr);
    }

    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

    glGenerateMipmap(GL_TEXTURE_CUBE_MAP);

    GLuint id;
    ShaderProgram shader = m_shaders.at(ShaderMode::PREFILTERCUBEMAP);
    shader.Use();


    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_CUBE_MAP, texture);
    glUniformMatrix4fv(glGetUniformLocation(shader.program(), "projection"),
                       1, GL_FALSE, glm::value_ptr(m_captureProjection));


    glBindFramebuffer(GL_FRAMEBUFFER, m_captureFBO);
    unsigned int maxMipLevels = 5;
    for (unsigned int mip = 0; mip < maxMipLevels; ++mip)
    {
        // reisze framebuffer according to mip-level size.
        unsigned int mipWidth  = 128 * std::pow(0.5, mip);
        unsigned int mipHeight = 128 * std::pow(0.5, mip);
        glBindRenderbuffer(GL_RENDERBUFFER, m_captureRBO);
        glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT24, mipWidth, mipHeight);
        glViewport(0, 0, mipWidth, mipHeight);

        float roughness = (float)mip / (float)(maxMipLevels - 1);
        glUniform1f(glGetUniformLocation(shader.program(), "roughness"), roughness);
        for (unsigned int i = 0; i < 6; ++i)
        {
            glUniformMatrix4fv(glGetUniformLocation(shader.program(), "view"),
                               1, GL_FALSE, glm::value_ptr(m_captureViews[i]));

            glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0,
                                   GL_TEXTURE_CUBE_MAP_POSITIVE_X + i, cubeMap, mip);

            glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
            glBindVertexArray(m_skyboxInfo.VAO());
            glDrawArrays(GL_TRIANGLES, 0, 36);
            glBindVertexArray(0);
        }
    }

    glViewport(0, 0, m_screen.width(), m_screen.height());
    glBindFramebuffer(GL_FRAMEBUFFER, 0);

    return cubeMap;
}
uint Renderer::convertEquirectanguralToCube(Texture texture, const int width, const int height)
{
    glBindFramebuffer(GL_FRAMEBUFFER, m_captureFBO);
    glBindRenderbuffer(GL_RENDERBUFFER, m_captureRBO);
    glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT24, width, height);

    GLuint cubeMap;
    // Generate cubeMap texture which is gonna hold the data
    glGenTextures(1, &cubeMap);
    glBindTexture(GL_TEXTURE_CUBE_MAP, cubeMap);
    for (unsigned int i = 0; i < 6; ++i)
    {
        // note that we store each face with 16 bit floating point values
        glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X + i, 0, GL_RGB16F,
                     width, height, 0, GL_RGB, GL_FLOAT, nullptr);
    }
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

    GLuint id;
    ShaderProgram shader = m_shaders.at(ShaderMode::CONVERTSKYBOX);
    shader.Use();

    glActiveTexture(GL_TEXTURE0);
    texture.Use();

    glUniformMatrix4fv(glGetUniformLocation(shader.program(), "projection"),
                       1, GL_FALSE, glm::value_ptr(m_captureProjection));


    glBindFramebuffer(GL_FRAMEBUFFER, m_captureFBO);
    glViewport(0, 0, width, height);
    for (unsigned int i = 0; i < 6; ++i)
    {
        glUniformMatrix4fv(glGetUniformLocation(shader.program(), "view"),
                           1, GL_FALSE, glm::value_ptr(m_captureViews[i]));

        glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0,
                                   GL_TEXTURE_CUBE_MAP_POSITIVE_X + i, cubeMap, 0);

        glBindVertexArray(m_skyboxInfo.VAO());
        glDrawArrays(GL_TRIANGLES, 0, 36);
        glBindVertexArray(0);
    }

    glBindTexture(GL_TEXTURE_CUBE_MAP, cubeMap);
    glGenerateMipmap(GL_TEXTURE_CUBE_MAP);
    glViewport(0, 0, m_screen.width(), m_screen.height());
    glBindFramebuffer(GL_FRAMEBUFFER, 0);

    return cubeMap;
}

void Renderer::renderSkybox()
{
    glDepthFunc(GL_LEQUAL); // Change depth function so depth test passes when values are equal to depth buffer's content

    GLuint id;
    ShaderProgram shader = m_shaders.at(ShaderMode::SKYBOX);
    shader.Use();

    glUniformMatrix4fv(glGetUniformLocation(shader.program(), "projection"),
                       1, GL_FALSE, m_scene.camera().getProjection());
    glUniformMatrix4fv(glGetUniformLocation(shader.program(), "view"),
                       1, GL_FALSE, glm::value_ptr(glm::mat4(glm::mat3(m_scene.camera().getViewMatrix()))));

    glUniform1i(glGetUniformLocation(shader.program(), "gammaCorrection"), m_gammaCorrection);
    glUniform3f(glGetUniformLocation(shader.program(), "sunDir"),
                m_scene.directionalLight().position().x,
                m_scene.directionalLight().position().y,
                m_scene.directionalLight().position().z
            );
    glUniform1f(glGetUniformLocation(shader.program(), "turbidity"), m_scene.turbidity());

    glUniform3fv(glGetUniformLocation(shader.program(), "eyePos"), 1,
                m_scene.camera().getPosition());

    // skybox cube
    glBindVertexArray(m_skyboxInfo.VAO());

    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_CUBE_MAP, m_envCubeMap);
    //m_assetManager.skyboxTexture().Use();
    glActiveTexture(GL_TEXTURE1);
    m_assetManager.textures().at("environmentMap").Use();

    glDrawArrays(GL_TRIANGLES, 0, 36);
    glBindVertexArray(0);
    glBindTexture(GL_TEXTURE_CUBE_MAP, 0);
    glDepthFunc(GL_LESS); // Set depth function back to default
}

void Renderer::renderToGBuffer()
{
    glDisable(GL_BLEND);
    GLuint id;
    ShaderProgram shader = m_shaders.at(ShaderMode::GBUFFERPASS);
    m_gBuffer.use();
    shader.Use();
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    glUniform3fv(glGetUniformLocation(shader.program(), "cameraPos"), 1, m_scene.camera().getPosition());
    glUniform3f(glGetUniformLocation(shader.program(), "lightPos"),
                m_scene.directionalLight().position().x, m_scene.directionalLight().position().y, m_scene.directionalLight().position().z);
    glUniformMatrix4fv(glGetUniformLocation(shader.program(), "projection"), 1, GL_FALSE, m_scene.camera().getProjection());
    glUniformMatrix4fv(glGetUniformLocation(shader.program(), "view"), 1, GL_FALSE, m_scene.camera().getView());

    glUniform1i(glGetUniformLocation(shader.program(), "gammaCorrection"), m_gammaCorrection);

/*
    Camera& cam = m_scene.camera();
    float Hnear = 2 * tan(cam.fov() / 2) * cam.near();
    float Wnear = Hnear * cam.aspectRatio();

    float Hfar = 2 * tan(cam.fov() / 2) * cam.far();
    float Wfar = Hfar * cam.aspectRatio();

    glm::vec3 p = cam.getPos();
    glm::vec3 d = cam.getDir();

    // far plane points
    glm::vec3 fc = p + d * cam.far();
    glm::vec3 ftl = fc + (cam.getUp() * (Hfar/2)) - (cam.getRight() * (Wfar/2));
    glm::vec3 ftr = fc + (cam.getUp() * (Hfar/2)) + (cam.getRight() * (Wfar/2));
    glm::vec3 fbl = fc - (cam.getUp() * (Hfar/2)) - (cam.getRight() * (Wfar/2));
    glm::vec3 fbr = fc - (cam.getUp() * (Hfar/2)) + (cam.getRight() * (Wfar/2));

    // near plane points
    glm::vec3 nc = p + d * cam.near();
    glm::vec3 ntl = nc + (cam.getUp() * (Hnear/2)) - (cam.getRight() * (Wnear/2));
    glm::vec3 ntr = nc + (cam.getUp() * (Hnear/2)) + (cam.getRight() * (Wnear/2));
    glm::vec3 nbl = nc - (cam.getUp() * (Hnear/2)) - (cam.getRight() * (Wnear/2));
    glm::vec3 nbr = nc - (cam.getUp() * (Hnear/2)) + (cam.getRight() * (Wnear/2));

    btConvexHullShape frustum = btConvexHullShape();
    frustum.addPoint(btVector3(ftl.x, ftl.y, ftl.z));
    frustum.addPoint(btVector3(ftr.x, ftr.y, ftr.z));
    frustum.addPoint(btVector3(fbl.x, fbl.y, fbl.z));
    frustum.addPoint(btVector3(fbr.x, fbr.y, fbr.z));

    frustum.addPoint(btVector3(ntl.x, ntl.y, ntl.z));
    frustum.addPoint(btVector3(ntr.x, ntr.y, ntr.z));
    frustum.addPoint(btVector3(nbl.x, nbl.y, nbl.z));
    frustum.addPoint(btVector3(nbr.x, nbr.y, nbr.z));
*/

    m_scene.root().render(shader, GL_TRIANGLES);
    glBindFramebuffer(GL_FRAMEBUFFER, 0);
    glEnable(GL_BLEND);

}

void Renderer::renderShadows()
{
    m_shadowMap.use();
    glViewport(0, 0, SHADOW_WIDTH, SHADOW_HEIGHT);

    GLuint id;
    ShaderProgram shader = m_shaders.at(ShaderMode::SHADOW);
    glCullFace(GL_FRONT);
    shader.Use();

    glUniformMatrix4fv(glGetUniformLocation(shader.program(), "lightSpaceMatrix"), 1, GL_FALSE, glm::value_ptr(m_scene.directionalLight().lightSpaceMatrix()));

    glClear(GL_DEPTH_BUFFER_BIT);
    for(Instance& i : m_scene.instances()){
        i.render(shader);
    }

    glCullFace(GL_BACK);

    glViewport(0, 0, m_screen.width(), m_screen.height());
    glBindFramebuffer(GL_FRAMEBUFFER, 0);


}

void Renderer::renderGeometryShader()
{
    ShaderProgram shader = m_shaders.at(ShaderMode::GEOMETRY);
    GLint id;
    shader.Use();

    btRigidBody* body = m_scene.mousePicker().pickedBody();
    int bodyIndex = -1;
    if(body != nullptr) {
        bodyIndex = body->getUserIndex();
    }

    glUniformMatrix4fv(glGetUniformLocation(shader.program(), "projection"), 1, GL_FALSE, m_scene.camera().getProjection());
    glUniformMatrix4fv(glGetUniformLocation(shader.program(), "view"), 1, GL_FALSE, m_scene.camera().getView());
    glUniform3f(glGetUniformLocation(shader.program(), "lightPos"), m_scene.directionalLight().position().x, m_scene.directionalLight().position().y, m_scene.directionalLight().position().z);

    for(int i = 0; i < m_scene.lights().size(); i++)
    {
        glUniform3f(glGetUniformLocation(shader.program(),
                                         ("lights[" + std::to_string(i) + "].color").data()),
                    m_scene.lights()[i].color().x, m_scene.lights()[i].color().y, m_scene.lights()[i].color().z);
        glUniform3f(glGetUniformLocation(shader.program(),
                                         ("lights[" + std::to_string(i) + "].position").data()), m_scene.lights()[i].position().x, m_scene.lights()[i].position().y, m_scene.lights()[i].position().z);
        glUniform1ui(glGetUniformLocation(shader.program(),
                                          ("lights[" + std::to_string(i) + "].isLocal").data()), m_scene.lights()[i].isLocal());
        glUniform1ui(glGetUniformLocation(shader.program(),
                                          ("lights[" + std::to_string(i) + "].isSpot").data()), m_scene.lights()[i].isSpot());
        if(m_scene.lights()[i].isSpot())
            glUniform3f(glGetUniformLocation(shader.program(),
                                             ("lights[" + std::to_string(i) + "].spotDirection").data()), m_scene.lights()[i].direction().x, m_scene.lights()[i].direction().y, m_scene.lights()[i].direction().z);
    }


    for(Instance& i : m_scene.instances()){
        if(i.physicsBody()->getUserIndex() == bodyIndex) {

        }

        glUniform1ui(glGetUniformLocation(shader.program(), "displayWireframe"), m_displayWireframe);
        glUniform1ui(glGetUniformLocation(shader.program(), "displayNormals"), m_displayNormals);

        i.render(shader);
    }
}

void Renderer::renderLightQuad(ShaderMode mode)
{
    ShaderProgram shader = m_shaders.at(mode);
    shader.Use();

    glActiveTexture(GL_TEXTURE0);
    m_gBuffer.gPosition().Use();
    glUniform1i(glGetUniformLocation(shader.program(), "gPosition"), 0);

    glActiveTexture(GL_TEXTURE1);
    m_gBuffer.gNormal().Use();
    glUniform1i(glGetUniformLocation(shader.program(), "gNormal"), 1);

    glActiveTexture(GL_TEXTURE2);
    m_gBuffer.gColorSpec().Use();
    glUniform1i(glGetUniformLocation(shader.program(), "gColorSpec"), 2);

    glActiveTexture(GL_TEXTURE3);
    glBindTexture(GL_TEXTURE_CUBE_MAP, m_irradianceMap);
    glUniform1i(glGetUniformLocation(shader.program(), "irradianceMap"), 3);

    glActiveTexture(GL_TEXTURE4);
    glBindTexture(GL_TEXTURE_CUBE_MAP, m_envCubeMap);
    glUniform1i(glGetUniformLocation(shader.program(), "envMap"), 4);

    glActiveTexture(GL_TEXTURE5);
    glBindTexture(GL_TEXTURE_2D, m_shadowMap.depthmap());
    glUniform1i(glGetUniformLocation(shader.program(), "shadowMap"), 5);

    glActiveTexture(GL_TEXTURE6);
    glBindTexture(GL_TEXTURE_CUBE_MAP, m_prefilteredMap);
    glUniform1i(glGetUniformLocation(shader.program(), "prefilteredMap"), 6);

    glActiveTexture(GL_TEXTURE7);
    glBindTexture(GL_TEXTURE_2D, m_brdfLUT);
    glUniform1i(glGetUniformLocation(shader.program(), "brdfLUT"), 7);

    glUniformMatrix4fv(glGetUniformLocation(shader.program(), "lightSpaceMatrix"), 1, GL_FALSE, glm::value_ptr(m_scene.directionalLight().lightSpaceMatrix()));

    glUniformMatrix4fv(glGetUniformLocation(shader.program(), "projection"), 1, GL_FALSE, m_scene.camera().getProjection());
    glUniformMatrix4fv(glGetUniformLocation(shader.program(), "view"), 1, GL_FALSE, m_scene.camera().getView());
    glUniform3fv(glGetUniformLocation(shader.program(), "cameraPos"), 1, m_scene.camera().getPosition());

    glUniform2f(glGetUniformLocation(shader.program(), "resolution"), m_screen.width(), m_screen.height());
    glUniform2f(glGetUniformLocation(shader.program(), "halfSizeNearPlane"),
                tan(m_scene.camera().fov()/2.0) * m_scene.camera().aspectRatio(),
                tan(m_scene.camera().fov()/2.0));


    glUniform3f(glGetUniformLocation(shader.program(),
                                     ("light.color")),
                m_scene.directionalLight().color().x, m_scene.directionalLight().color().y, m_scene.directionalLight().color().z);
    glUniform3f(glGetUniformLocation(shader.program(),
                                     ("light.position")),
                m_scene.directionalLight().position().x, m_scene.directionalLight().position().y, m_scene.directionalLight().position().z);

    glUniform1i(glGetUniformLocation(shader.program(),
                                     ("light.isLocal")),
                m_scene.directionalLight().isLocal());
    glUniform1i(glGetUniformLocation(shader.program(),
                                     ("light.isSpot")),
                m_scene.directionalLight().isSpot());

    glUniform1f(glGetUniformLocation(shader.program(),
                                     "light.strength"),
                m_scene.directionalLight().strength());

    glUniform1i(glGetUniformLocation(shader.program(), "normalsOnly"), m_displayDeferredNormalsOnly);
    glUniform1i(glGetUniformLocation(shader.program(), "positionsOnly"), m_displayDeferredPositionsOnly);
    glUniform1i(glGetUniformLocation(shader.program(), "colorOnly"), m_displayDeferredColorOnly);
    glUniform1i(glGetUniformLocation(shader.program(), "shadowsOnly"), m_displayDeferredShadowsOnly);
    glUniform1i(glGetUniformLocation(shader.program(), "ambientOnly"), m_displayDeferredAmbientOnly);
    glUniform1i(glGetUniformLocation(shader.program(), "depthOnly"), m_displayDeferredDepthOnly);
    glUniform1i(glGetUniformLocation(shader.program(), "reflectedOnly"), m_displayDeferredReflectedOnly);
    glUniform1i(glGetUniformLocation(shader.program(), "scaterredOnly"), m_displayDeferredScaterredOnly);

    glBindVertexArray(m_ppInfo.VAO());
    glDrawArrays(GL_TRIANGLES, 0, 6);
    glBindVertexArray(0);

}

void Renderer::renderLightsSpheres(Light& light)
{
    ShaderProgram shader = m_shaders.at(ShaderMode::LIGHTSSPHERES);
    GLint id;
    shader.Use();
    glStencilFunc(GL_NOTEQUAL, 0, 0xFF);
    glEnable(GL_CULL_FACE);
    glCullFace(GL_FRONT);

    glActiveTexture(GL_TEXTURE0);
    m_gBuffer.gPosition().Use();
    glUniform1i(glGetUniformLocation(shader.program(), "gPosition"), 0);

    glActiveTexture(GL_TEXTURE1);
    m_gBuffer.gNormal().Use();
    glUniform1i(glGetUniformLocation(shader.program(), "gNormal"), 1);

    glActiveTexture(GL_TEXTURE2);
    m_gBuffer.gColorSpec().Use();
    glUniform1i(glGetUniformLocation(shader.program(), "gColorSpec"), 2);

    glActiveTexture(GL_TEXTURE3);
    glBindTexture(GL_TEXTURE_CUBE_MAP, m_irradianceMap);
    glUniform1i(glGetUniformLocation(shader.program(), "irradianceMap"), 3);

    glActiveTexture(GL_TEXTURE4);
    glBindTexture(GL_TEXTURE_CUBE_MAP, m_envCubeMap);
    glUniform1i(glGetUniformLocation(shader.program(), "envMap"), 4);

    glActiveTexture(GL_TEXTURE5);
    glBindTexture(GL_TEXTURE_CUBE_MAP, m_shadowMapCube.depthmap());
    glUniform1i(glGetUniformLocation(shader.program(), "shadowMapCube"), 5);

    glUniformMatrix4fv(glGetUniformLocation(shader.program(), "projection"), 1, GL_FALSE, m_scene.camera().getProjection());
    glUniformMatrix4fv(glGetUniformLocation(shader.program(), "view"), 1, GL_FALSE, m_scene.camera().getView());
    glUniform3fv(glGetUniformLocation(shader.program(), "cameraPos"), 1, m_scene.camera().getPosition());

    glUniform2f(glGetUniformLocation(shader.program(), "resolution"), m_screen.width(), m_screen.height());

    Model model = m_scene.models().at("sphere");

    glUniform3f(glGetUniformLocation(shader.program(),
                                     ("light.color")),
                light.color().x, light.color().y, light.color().z);
    glUniform3f(glGetUniformLocation(shader.program(),
                                     ("light.position")),
                light.position().x, light.position().y, light.position().z);

    glUniform1i(glGetUniformLocation(shader.program(),
                                     ("light.isLocal")),
                light.isLocal());
    glUniform1i(glGetUniformLocation(shader.program(),
                                     ("light.isSpot")),
                light.isSpot());

    glUniform1f(glGetUniformLocation(shader.program(),
                                     "light.radius"),
                light.radius());

    glUniform1f(glGetUniformLocation(shader.program(),
                                     "light.strength"),
                light.strength());

    if(light.isSpot())
    {
        glUniform3f(glGetUniformLocation(shader.program(),
                                         ("light.spotDirection")),
                    light.direction().x, light.direction().y, light.direction().z);

        glUniform1f(glGetUniformLocation(shader.program(),
                                         ("lights.spotCosCutOff")),
                    light.spotCosCutOff());
        glUniform1f(glGetUniformLocation(shader.program(),
                                         ("lights.spotExponent")),
                    light.spotExponent());
    }


    model.render(shader, GL_TRIANGLES);


    glCullFace(GL_BACK);
    glBindTexture(GL_TEXTURE_2D, 0);

}

void Renderer::renderLightsGeometry()
{
    ShaderProgram shader = m_shaders.at(ShaderMode::LIGHTSGEOMETRY);
    GLint id;
    shader.Use();

    glUniformMatrix4fv(glGetUniformLocation(shader.program(), "projection"), 1, GL_FALSE, m_scene.camera().getProjection());
    glUniformMatrix4fv(glGetUniformLocation(shader.program(), "view"), 1, GL_FALSE, m_scene.camera().getView());

    Model model = m_scene.models().at("sphere");
    for(int i = 0; i < m_scene.lights().size(); i++)
    {
        if(m_scene.lights()[i].isLocal())
        {
            glUniform3f(glGetUniformLocation(shader.program(),
                                             "light.color"),
                        m_scene.lights()[i].color().x, m_scene.lights()[i].color().y, m_scene.lights()[i].color().z);

            glUniform3f(glGetUniformLocation(shader.program(),
                                             "light.position"),
                        m_scene.lights()[i].position().x, m_scene.lights()[i].position().y, m_scene.lights()[i].position().z);

            glUniform1ui(glGetUniformLocation(shader.program(),
                                              "light.isLocal"),
                         m_scene.lights()[i].isLocal());

            glUniform1ui(glGetUniformLocation(shader.program(),
                                              "light.isSpot"),
                         m_scene.lights()[i].isSpot());

            glUniform1f(glGetUniformLocation(shader.program(),
                                              "light.radius"),
                         m_scene.lights()[i].radius());

            if(m_scene.lights()[i].isSpot())
                glUniform3f(glGetUniformLocation(shader.program(), "light.spotDirection"),
                            m_scene.lights()[i].direction().x, m_scene.lights()[i].direction().y, m_scene.lights()[i].direction().z);


            model.render(shader, GL_TRIANGLES);
        }
    }
}

void Renderer::renderLightScatter()
{
    m_scatterFBO.use();
    glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    GLint id;
    ShaderProgram shader = m_lightScatterShader;
    shader.Use();

    glUniform3f(glGetUniformLocation(shader.program(), "lightPos"),
                m_scene.directionalLight().position().x, m_scene.directionalLight().position().y, m_scene.directionalLight().position().z);
    glm::mat4 ortho = glm::ortho(-50.0f, 50.0f, -50.0f, 50.0f, 0.1f, 142.0f);
    glUniformMatrix4fv(glGetUniformLocation(shader.program(), "projection"), 1, GL_FALSE, glm::value_ptr(ortho));
    glUniformMatrix4fv(glGetUniformLocation(shader.program(), "view"), 1, GL_FALSE, m_scene.camera().getView());

    glUniform2f(glGetUniformLocation(shader.program(), "resolution"), m_screen.width(), m_screen.height());

    glUniform3fv(glGetUniformLocation(shader.program(), "camPos"), 1,
                m_scene.camera().getPosition());

    glUniform1f(glGetUniformLocation(shader.program(), "exposure"), m_scene.exposure());


    glDisable(GL_DEPTH_TEST);
    glBindVertexArray(m_ppInfo.VAO());
    // draw FBO on a quad using texColorBuffer previously filled
    glActiveTexture(GL_TEXTURE0);
    m_renderFBO.texColorBuffer().Use();
    glDrawArrays(GL_TRIANGLES, 0, 6);
    glBindVertexArray(0);
    glBindTexture(GL_TEXTURE_2D, 0);
    glEnable(GL_DEPTH_TEST);

    glBindFramebuffer(GL_FRAMEBUFFER, 0);

}

void Renderer::renderShadowsCube(Light light)
{
    std::vector<glm::mat4> shadowTransforms;
    GLfloat aspect = 1.0f;
    GLfloat near = 0.0001f;
    GLfloat far = light.radius();
    glm::vec3 pos = light.position();
    glm::mat4 shadowProj = glm::perspective(glm::radians(90.0f), aspect, near, far);
    shadowTransforms.push_back(shadowProj *
                     glm::lookAt(pos, pos + glm::vec3(1.0,0.0,0.0), glm::vec3(0.0,-1.0,0.0)));
    shadowTransforms.push_back(shadowProj *
                     glm::lookAt(pos, pos + glm::vec3(-1.0,0.0,0.0), glm::vec3(0.0,-1.0,0.0)));
    shadowTransforms.push_back(shadowProj *
                     glm::lookAt(pos, pos + glm::vec3(0.0,1.0,0.0), glm::vec3(0.0,0.0,1.0)));
    shadowTransforms.push_back(shadowProj *
                     glm::lookAt(pos, pos + glm::vec3(0.0,-1.0,0.0), glm::vec3(0.0,0.0,-1.0)));
    shadowTransforms.push_back(shadowProj *
                     glm::lookAt(pos, pos + glm::vec3(0.0,0.0,1.0), glm::vec3(0.0,-1.0,0.0)));
    shadowTransforms.push_back(shadowProj *
                     glm::lookAt(pos, pos + glm::vec3(0.0,0.0,-1.0), glm::vec3(0.0,-1.0,0.0)));

    glBindFramebuffer(GL_FRAMEBUFFER, m_shadowMapCube.id());
    GLuint id;
    ShaderProgram shader = m_shaders.at(ShaderMode::SHADOWCUBE);
    //glCullFace(GL_FRONT);
    shader.Use();
    for(int i = 0; i < 6; i++) {
       glUniformMatrix4fv(glGetUniformLocation(shader.program(),
                                               ("lightSpaceMatrix["+ std::to_string(i) + "]").data()),
                          1, GL_FALSE, glm::value_ptr(shadowTransforms[i]));
    }
    glUniform1f(glGetUniformLocation(shader.program(), "far_plane"), light.radius());
    glUniform3f(glGetUniformLocation(shader.program(), "lightPos"),
                light.position().x, light.position().y, light.position().z);


    glViewport(0, 0, SHADOW_WIDTH, SHADOW_HEIGHT);
    glClear(GL_DEPTH_BUFFER_BIT);
    for(Instance& i : m_scene.instances()){
       btVector3 iPos = i.physicsBody()->getWorldTransform().getOrigin();
       glm::vec3 ipos(iPos.x(), iPos.y(), iPos.z());
       glm::vec3 diff = pos - ipos;
       float length = glm::length(diff);
       // if instance is in light's radius
       if(length <= far)
        i.render(shader);
    }

    //glCullFace(GL_BACK);

    glViewport(0, 0, m_screen.width(), m_screen.height());
    glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

void Renderer::renderBloom()
{

    m_bloomFBO.use();
    glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    GLint id;
    ShaderProgram shader = m_ppShaders.at(PostProcessMode::GAUSSIANBLUR);
    shader.Use();

    glUniform2f(glGetUniformLocation(shader.program(), "resolution"), m_screen.width(), m_screen.height());
//    glViewport(0, 0, m_screen.width(), m_screen.height());
    glDisable(GL_DEPTH_TEST);
    glBindVertexArray(m_ppInfo.VAO());
    // draw FBO on a quad using texColorBuffer previously filled
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, m_secondaryFBO.texColorBuffer().ID());
    glDrawArrays(GL_TRIANGLES, 0, 6);
    glBindVertexArray(0);
    glBindTexture(GL_TEXTURE_2D, 0);
    glEnable(GL_DEPTH_TEST);

    for(int i = 0 ; i < 1; i++)
    {
        m_bloomFBObis.use();
        glUniform1i(glGetUniformLocation(shader.program(), "horizontal"), true);
        renderPostProcess(m_bloomFBO.texColorBuffer(), PostProcessMode::GAUSSIANBLUR);


        m_bloomFBO.use();
        glUniform1i(glGetUniformLocation(shader.program(), "horizontal"), false);
        renderPostProcess(m_bloomFBObis.texColorBuffer(), PostProcessMode::GAUSSIANBLUR);
    }
    glBindFramebuffer(GL_FRAMEBUFFER, 0);
//    glViewport(0, 0, m_screen.width(), m_screen.height());
}

void Renderer::renderPostProcess()
{
    GLint id;

    // for each PostProcessMode
    for(PostProcessMode modes = PostProcessMode::INVERT; modes != PostProcessMode::PASSTHROUGH; modes++)
    {
        // if mode is activated
        if(m_modes.ppMode & modes)
        {
            glGetIntegerv(GL_TEXTURE_BINDING_2D, &id);
            if(id == m_primaryFBO.texColorBuffer().ID())
            {
                renderPostProcess(m_secondaryFBO.texColorBuffer(), modes);
                m_secondaryFBO.use();
            }
            else
            {
                renderPostProcess(m_primaryFBO.texColorBuffer(), modes);
                m_primaryFBO.use();
            }
        }
    }
}

void Renderer::renderPostProcess(Texture& texture, PostProcessMode mode)
{
    glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
    //glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    GLuint id;
    ShaderProgram shader = m_ppShaders.at(mode);
    shader.Use();
    float zoom = 2.0f;
    glUniform1f(glGetUniformLocation(shader.program(), "time"), (GLfloat)glfwGetTime());
    glUniform1f(glGetUniformLocation(shader.program(), "zoom"), (GLfloat)zoom);
    glUniform2f(glGetUniformLocation(shader.program(), "resolution"), m_screen.width(), m_screen.height());

    glUniform1i(glGetUniformLocation(shader.program(), "gammaCorrection"), m_gammaCorrection);
    glUniform1f(glGetUniformLocation(shader.program(), "exposure"), m_scene.exposure());

    glUniform1i(glGetUniformLocation(shader.program(), "bloomOnly"), m_displayDeferredBloomOnly);

    glBindVertexArray(m_ppInfo.VAO());
    // draw FBO on a quad using texColorBuffer previously filled
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, texture.ID());
    glDrawArrays(GL_TRIANGLES, 0, 6);
    glBindVertexArray(0);
    glBindTexture(GL_TEXTURE_2D, 0);
}


bool Renderer::loadShaders()
{

    ShaderProgram skyboxShader({
                Shader((PATH + "/shaders/skybox.vert").data(), GL_VERTEX_SHADER),
                Shader((PATH + "/shaders/skybox.frag").data(), GL_FRAGMENT_SHADER)
                });


    ShaderProgram convertskyboxShader({
                Shader((PATH + "/shaders/convertskybox.vert").data(), GL_VERTEX_SHADER),
                Shader((PATH + "/shaders/convertskybox.frag").data(), GL_FRAGMENT_SHADER)
                });

    ShaderProgram ppShader({
                Shader((PATH + "/shaders/postprocess.vert").data(), GL_VERTEX_SHADER),
                Shader((PATH + "/shaders/postprocess.frag").data(), GL_FRAGMENT_SHADER)
                });

    ShaderProgram ppInvertShader({
                Shader((PATH + "/shaders/postprocess.vert").data(), GL_VERTEX_SHADER),
                Shader((PATH + "/shaders/ppinvert.frag").data(), GL_FRAGMENT_SHADER)
                });

    ShaderProgram ppGrayScale({
                Shader((PATH + "/shaders/postprocess.vert").data(), GL_VERTEX_SHADER),
                Shader((PATH + "/shaders/ppgrayscale.frag").data(), GL_FRAGMENT_SHADER)
                });

    ShaderProgram ppLightScatter({
                Shader((PATH + "/shaders/postprocess.vert").data(), GL_VERTEX_SHADER),
                Shader((PATH + "/shaders/ppscatter.frag").data(), GL_FRAGMENT_SHADER)
                });

    ShaderProgram ppKernel({
                Shader((PATH + "/shaders/postprocess.vert").data(), GL_VERTEX_SHADER),
                Shader((PATH + "/shaders/ppkernel.frag").data(), GL_FRAGMENT_SHADER)
                });

    ShaderProgram ppBlur({
                Shader((PATH + "/shaders/postprocess.vert").data(), GL_VERTEX_SHADER),
                Shader((PATH + "/shaders/ppblur.frag").data(), GL_FRAGMENT_SHADER)
                });

    ShaderProgram ppBloomFilter({
                Shader((PATH + "/shaders/postprocess.vert").data(), GL_VERTEX_SHADER),
                Shader((PATH + "/shaders/bloomfilter.frag").data(), GL_FRAGMENT_SHADER)
                });

    ShaderProgram ppgaussianBlur({
                Shader((PATH + "/shaders/postprocess.vert").data(), GL_VERTEX_SHADER),
                Shader((PATH + "/shaders/ppgaussianblur.frag").data(), GL_FRAGMENT_SHADER)
                });

    ShaderProgram ppCartoon({
                Shader((PATH + "/shaders/postprocess.vert").data(), GL_VERTEX_SHADER),
                Shader((PATH + "/shaders/ppcartoon.frag").data(), GL_FRAGMENT_SHADER)
                });

    ShaderProgram ppEdge({
                Shader((PATH + "/shaders/postprocess.vert").data(), GL_VERTEX_SHADER),
                Shader((PATH + "/shaders/ppedge.frag").data(), GL_FRAGMENT_SHADER)
                });

    ShaderProgram ppFXAA({
                Shader((PATH + "/shaders/postprocess.vert").data(), GL_VERTEX_SHADER),
                Shader((PATH + "/shaders/ppfxaa.frag").data(), GL_FRAGMENT_SHADER)
                });

    ShaderProgram pptonemapping({
                Shader((PATH + "/shaders/postprocess.vert").data(), GL_VERTEX_SHADER),
                Shader((PATH + "/shaders/pptonemapping.frag").data(), GL_FRAGMENT_SHADER)
                });



    ShaderProgram geom({
                Shader((PATH + "/shaders/passthrough.vert").data(), GL_VERTEX_SHADER),
                Shader((PATH + "/shaders/passthrough.geom").data(), GL_GEOMETRY_SHADER),
                Shader((PATH + "/shaders/passthrough.frag").data(), GL_FRAGMENT_SHADER)
                });

    ShaderProgram geomTess({
                Shader((PATH + "/shaders/passthrough.vert").data(), GL_VERTEX_SHADER),
                Shader((PATH + "/shaders/geomtess.tcs").data(), GL_TESS_CONTROL_SHADER),
                Shader((PATH + "/shaders/geomtess.tes").data(), GL_TESS_EVALUATION_SHADER),
                Shader((PATH + "/shaders/passthrough.geom").data(), GL_GEOMETRY_SHADER),
                Shader((PATH + "/shaders/passthrough.frag").data(), GL_FRAGMENT_SHADER)
                });

    ShaderProgram geomTess2({
                Shader((PATH + "/shaders/passthrough.vert").data(), GL_VERTEX_SHADER),
                Shader((PATH + "/shaders/geomtess2.tcs").data(), GL_TESS_CONTROL_SHADER),
                Shader((PATH + "/shaders/geomtess2.tes").data(), GL_TESS_EVALUATION_SHADER),
                Shader((PATH + "/shaders/passthrough.geom").data(), GL_GEOMETRY_SHADER),
                Shader((PATH + "/shaders/passthrough.frag").data(), GL_FRAGMENT_SHADER)
                });


    ShaderProgram lightsGeom({
                Shader((PATH + "/shaders/lightgeometry.vert").data(), GL_VERTEX_SHADER),
                Shader((PATH + "/shaders/lightgeometry.geom").data(), GL_GEOMETRY_SHADER),
                Shader((PATH + "/shaders/lightgeometry.frag").data(), GL_FRAGMENT_SHADER)
                });



    ShaderProgram shadow({
                Shader((PATH + "/shaders/shadow.vert").data(), GL_VERTEX_SHADER),
                Shader((PATH + "/shaders/shadow.frag").data(), GL_FRAGMENT_SHADER)
                });


    ShaderProgram shadowCube({
                Shader((PATH + "/shaders/shadowcube.vert").data(), GL_VERTEX_SHADER),
                Shader((PATH + "/shaders/shadowcube.geom").data(), GL_GEOMETRY_SHADER),
                Shader((PATH + "/shaders/shadowcube.frag").data(), GL_FRAGMENT_SHADER)
                });

    ShaderProgram gBuffer({
                Shader((PATH + "/shaders/gbuffer.vert").data(), GL_VERTEX_SHADER),
                Shader((PATH + "/shaders/gbuffer.frag").data(), GL_FRAGMENT_SHADER)
                });

    ShaderProgram gBufferTess({
                      Shader((PATH + "/shaders/gbuffer.vert").data(), GL_VERTEX_SHADER),
                      Shader((PATH + "/shaders/gbuffer.tcs").data(), GL_TESS_CONTROL_SHADER),
                      Shader((PATH + "/shaders/gbuffer.tes").data(), GL_TESS_EVALUATION_SHADER),
                      Shader((PATH + "/shaders/gbuffer.frag").data(), GL_FRAGMENT_SHADER)
                  });

    ShaderProgram lightPass({
                Shader((PATH + "/shaders/lightpass.vert").data(), GL_VERTEX_SHADER),
                Shader((PATH + "/shaders/lightpass.frag").data(), GL_FRAGMENT_SHADER)
                });



    ShaderProgram pbrLightPass({
                Shader((PATH + "/shaders/lightpass.vert").data(), GL_VERTEX_SHADER),
                Shader((PATH + "/shaders/pbrlightpass.frag").data(), GL_FRAGMENT_SHADER)
                });

    ShaderProgram lightsspheres({
                Shader((PATH + "/shaders/lightsspheres.vert").data(), GL_VERTEX_SHADER),
                Shader((PATH + "/shaders/lightsspheres.frag").data(), GL_FRAGMENT_SHADER,
                                    (PATH + "/shaders/lighting.glsl").data())
                });



    ShaderProgram lightquad({
                Shader((PATH + "/shaders/lightquad.vert").data(), GL_VERTEX_SHADER),
                Shader((PATH + "/shaders/lightquad.frag").data(), GL_FRAGMENT_SHADER,
                                (PATH + "/shaders/lighting.glsl").data())
                });


    ShaderProgram cubemapConvolution({
                Shader((PATH + "/shaders/cubemapconvolution.vert").data(), GL_VERTEX_SHADER),
                Shader((PATH + "/shaders/cubemapconvolution.frag").data(), GL_FRAGMENT_SHADER)
                });


    ShaderProgram cubemapprefilter({
                Shader((PATH + "/shaders/prefiltercubemap.vert").data(), GL_VERTEX_SHADER),
                Shader((PATH + "/shaders/prefiltercubemap.frag").data(), GL_FRAGMENT_SHADER)
                });


    ShaderProgram stencilpass({
                Shader((PATH + "/shaders/stencilpass.vert").data(), GL_VERTEX_SHADER),
                Shader((PATH + "/shaders/stencilpass.frag").data(), GL_FRAGMENT_SHADER)
                });


    ShaderProgram brdf({
                Shader((PATH + "/shaders/postprocess.vert").data(), GL_VERTEX_SHADER),
                Shader((PATH + "/shaders/brdf.frag").data(), GL_FRAGMENT_SHADER,
                           (PATH + "/shaders/lighting.glsl").data())
                });

    m_shaders.insert(std::make_pair(ShaderMode::GBUFFERPASS, gBuffer));
    m_shaders.insert(std::make_pair(ShaderMode::BRDF, brdf));
    m_shaders.insert(std::make_pair(ShaderMode::CUBEMAPCONVOLUTION, cubemapConvolution));
    m_shaders.insert(std::make_pair(ShaderMode::PREFILTERCUBEMAP, cubemapprefilter));
    m_shaders.insert(std::make_pair(ShaderMode::LIGHTPASS, lightPass));
    m_shaders.insert(std::make_pair(ShaderMode::PBRLIGHTPASS, pbrLightPass));
    m_shaders.insert(std::make_pair(ShaderMode::SHADOW, shadow));
    m_shaders.insert(std::make_pair(ShaderMode::SHADOWCUBE, shadowCube));
    m_shaders.insert(std::make_pair(ShaderMode::GEOMETRY, geomTess));
    m_shaders.insert(std::make_pair(ShaderMode::SKYBOX, skyboxShader));
    m_shaders.insert(std::make_pair(ShaderMode::CONVERTSKYBOX, convertskyboxShader));
    m_shaders.insert(std::make_pair(ShaderMode::LIGHTSGEOMETRY, lightsGeom));
    m_shaders.insert(std::make_pair(ShaderMode::LIGHTSSPHERES, lightsspheres));
    m_shaders.insert(std::make_pair(ShaderMode::LIGHTDIRECTIONAL, lightquad));
    m_shaders.insert(std::make_pair(ShaderMode::STENCILPASS, stencilpass));

    m_ppShaders.insert(std::make_pair(PostProcessMode::PASSTHROUGH, ppShader));
    m_ppShaders.insert(std::make_pair(PostProcessMode::INVERT, ppInvertShader));
    m_ppShaders.insert(std::make_pair(PostProcessMode::GRAYSCALE, ppGrayScale));
    m_ppShaders.insert(std::make_pair(PostProcessMode::KERNEL, ppKernel));
    m_ppShaders.insert(std::make_pair(PostProcessMode::BLUR, ppBlur));
    m_ppShaders.insert(std::make_pair(PostProcessMode::GAUSSIANBLUR, ppgaussianBlur));
    m_ppShaders.insert(std::make_pair(PostProcessMode::EDGE, ppEdge));
    m_ppShaders.insert(std::make_pair(PostProcessMode::CARTOON, ppCartoon));
    m_ppShaders.insert(std::make_pair(PostProcessMode::FXAA, ppFXAA));
    m_ppShaders.insert(std::make_pair(PostProcessMode::BLOOMFILTER, ppBloomFilter));
    m_ppShaders.insert(std::make_pair(PostProcessMode::TONEMAPPING, pptonemapping));


    m_lightScatterShader = ppLightScatter;

    return true;
}

void Renderer::framebufferSizeCallback()
{
    glViewport(0, 0, m_screen.x2(), m_screen.y2());

    m_bloomTexture.resize(m_screen.x2(), m_screen.y2());
    m_gBuffer.resize(m_screen.x2(), m_screen.y2());
    m_renderFBO.resize(m_screen.x2(), m_screen.y2());
    m_bloomFBO.resize(m_screen.x2(), m_screen.y2());
    m_bloomFBObis.resize(m_screen.x2(), m_screen.y2());
    m_primaryFBO.resize(m_screen.x2(), m_screen.y2());
    m_secondaryFBO.resize(m_screen.x2(), m_screen.y2());    
    m_scatterFBO.resize(m_screen.x2(), m_screen.y2());
    m_scene.framebufferSizeCallback();
}

Renderer::~Renderer()
{


    for(auto sh : m_shaders){
        glDeleteProgram(sh.second.program());
    }

    m_primaryFBO.clean();
    m_secondaryFBO.clean();
    m_renderFBO.clean();
    m_scatterFBO.clean();
    m_bloomFBO.clean();
    m_bloomFBObis.clean();
    m_gBuffer.clean();
}
