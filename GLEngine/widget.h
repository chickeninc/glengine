#ifndef WIDGET_H
#define WIDGET_H

#include "uielement.h"
#include <string>
#include <vector>
#include "container.h"

class DropDown;

class Widget : public Container
{
    using Child = Container::Child;
    using Children = Container::Children;


public:
    Widget(GUI& gui);
    Widget(GUI& gui, std::string title, Rectangle r);

    static constexpr GLuint HEADERHEIGHT = 30;

    virtual GLvoid draw(NVGcontext* vg);
    virtual GLvoid translate(GLint x, GLint y);
    virtual GLvoid setBounds(Rectangle r);
    virtual GLvoid updateChildren();

    virtual void mouseCallback(int button, int action);

    virtual GLvoid setVisible(GLboolean b);

    void setHeader(Rectangle r);
    void expand();

    inline bool overlaps(Widget& widg)
    {
        return m_header.overlaps(widg.m_header);
    }

    inline bool overlaps(Container& container)
    {
        return m_header.overlaps(container.bounds());
    }

    inline void contract()
    {
        if(isContractable())
        {
            m_isExpanded = false;

            for(Child c : m_children)
            {
                c->setVisible(false);
            }
        }
    }

    inline void flipExpand()
    {
        m_isExpanded ? contract() : expand();
    }

    inline void lock()
    {
        m_isLocked = true;
    }

    inline void unlock()
    {
        m_isLocked = false;
    }

    inline void setMouseMovable(const GLboolean b)
    {
        m_isMouseMovable = b;
    }

    inline bool isLocked() const
    {
        return m_isLocked;
    }

    inline bool isExpanded() const
    {
        return m_isExpanded;
    }

    inline bool isMouseMovable() const
    {
        return m_isMouseMovable;
    }

    inline void setContractable(const bool b)
    {
        m_isContractable = b;
    }

    inline bool isContractable() const
    {
        return m_isContractable;
    }

    inline Rectangle header() const
    {
        return m_header;
    }

    inline Rectangle window() const
    {
        return m_window;
    }

    inline bool hasParent() const
    {
        return m_hasParent;
    }

    inline void setHasParent(const bool b)
    {
        m_hasParent = b;
    }

private:

    Rectangle m_header;
    Rectangle m_window;
    std::string m_title;

    bool m_isExpanded = true;
    bool m_isContractable = true;
    bool m_isLocked = false;
    bool m_isMouseMovable = true;
    bool m_hasParent = false;

    Rectangle m_savedBounds;
};

#endif // WIDGET_H
