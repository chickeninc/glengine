#ifndef PARAGRAPH_H
#define PARAGRAPH_H

#include "uielement.h"
#include <string>

class Paragraph : public UIElement
{

public:

    Paragraph(GUI& gui, std::string text);
    Paragraph(GUI& gui, Rectangle r, std::string text);

    virtual void draw(NVGcontext* vg);

    void addText(std::string text);

    inline std::string text() const
    {
        return m_text;
    }

    inline void setText(const std::string& text)
    {
        m_text = text;
    }

    inline int textHeight() const
    {
        return m_textHeight;
    }

private:

    int m_textHeight;
    std::string m_text;
};

#endif // PARAGRAPH_H
