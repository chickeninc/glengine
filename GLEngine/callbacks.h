#ifndef CALLBACKS_H
#define CALLBACKS_H

class Callbacks
{
public:
    virtual void mouseCallback(int button, int action) { }
    virtual void cursorCallback(double xpos, double ypos) { }
    virtual void keyCallback(int key, int scancode, int action, int mode) { }
    virtual void scrollCallback(double xoffset, double yoffset) { }
    virtual void framebufferSizeCallback() { }
};

#endif // CALLBACKS_H
