#ifndef LISTENER_H
#define LISTENER_H

#include "listenable.h"
#include <functional>

class Listenable;

class Listener
{

public:
    Listener() { }
    Listener(Listenable* listenable) : m_listenable(listenable) { }
    inline void onUpdate() { m_function(); }

    inline void listenTo(Listenable* listenable, std::function<void()> function)
    {
        m_listenable = listenable;
        m_function = function;
    }

protected:
    Listenable* m_listenable = nullptr;
    std::function<void()> m_function;

};

#endif // LISTENER_H
