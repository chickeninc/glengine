#ifndef MENU_H
#define MENU_H

#include "widget.h"

class Menu : public Container
{
public:
    Menu(GUI& gui);
    Menu(GUI& gui, Rectangle r);

    virtual void draw(NVGcontext* vg);
    virtual void addChild(Widget& w);
    virtual void setBounds(Rectangle r);
    virtual void updateChildren();

    virtual void mouseCallback(int button, int action);

    inline std::vector<std::reference_wrapper<Widget>> children() const
    {
        return m_children;
    }

private:
    std::vector<std::reference_wrapper<Widget>> m_children;

};

#endif // MENU_H
