#ifndef SHADERPROGRAM_H
#define SHADERPROGRAM_H

#include <GL/glew.h>
#include <vector>
#include "shader.h"

class ShaderProgram
{
public:
    ShaderProgram() { }
    ShaderProgram(std::vector<Shader> shaders);

    void Use();

    inline GLuint program() const
    {
        return m_program;
    }

private:

    GLuint m_program = 0;
};

#endif // SHADERPROGRAM_H
