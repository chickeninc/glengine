#include "texture.h"

GLuint loadHDRTexture(const char* path)
{

    stbi_set_flip_vertically_on_load(true);

    int imageWidth;
    int imageHeight;
    int nbComponents;


    float* textureData = stbi_loadf(path, &imageWidth, &imageHeight, &nbComponents, 0);

    GLuint tmpTextureID;

    if(textureData)
    {
        std::cout << "Loading hdr image : " << path << std::endl;
        GLint mipmapLevels = 10;

        glGenTextures(1, &tmpTextureID);
        glBindTexture(GL_TEXTURE_2D, tmpTextureID);

        glTexStorage2D(GL_TEXTURE_2D,       // Texture type
                       mipmapLevels,                  // mimap levels
                       GL_RGB32F,            // Internal Format
                       imageWidth,          // Image width
                       imageHeight);

        for(GLint i = 0; i < mipmapLevels; i++)
        {
            glTexSubImage2D(GL_TEXTURE_2D,
                            i,
                            0,
                            0,
                            imageWidth,
                            imageHeight,
                            GL_RGB,
                            GL_FLOAT,
                            textureData);
        }

        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

        stbi_image_free(textureData);

    }
    else
    {
        std::cout << "Couldnt load hdr image : " << path << std::endl;
    }

    GLenum glError = glGetError();
    if(glError)
    {
        std::cout << "Error loading texture : " << path << std::endl;

        switch (glError) {
        case GL_INVALID_ENUM:
            std::cout << "Invalid enum." << std::endl;
            break;
        case GL_INVALID_VALUE:
            std::cout << "Invalid value." << std::endl;
            break;
        case GL_INVALID_OPERATION:
            std::cout << "Invalid operation." << std::endl;

        default:
            break;
        }
    }


    return tmpTextureID;
}


GLuint loadTexture(const char* path,
                   GLenum internalFormat = GL_BGRA,
                   GLenum minificationFilter = GL_LINEAR_MIPMAP_LINEAR,
                   GLenum magnificationFilter = GL_LINEAR_MIPMAP_LINEAR)
{

    FREE_IMAGE_FORMAT format = FreeImage_GetFileType(path, 0);
    if(format == -1) {
        std::cout << "Couldnt find image" << path << std::endl;
    }

    if(format == FIF_UNKNOWN)
    {
        std::cout << "Couldnt find image format - attempting to get from file extension" << path << std::endl;

        format = FreeImage_GetFIFFromFilename(path);

        if(!FreeImage_FIFSupportsReading(format))
        {
            std::cout << "Image cannot be read" << std::endl;
        }
    }

    FIBITMAP* bitmap = FreeImage_Load(format, path);

    int bitsPerPixel = FreeImage_GetBPP(bitmap);

    FIBITMAP* bitmap32;
    if(bitsPerPixel == 32)
    {
        std::cout << "Source image has " << bitsPerPixel << " bits per pixel, skipping conversion." << std::endl;
        bitmap32 = bitmap;
    }
    else
    {
        std::cout << "Source image has " << bitsPerPixel << " bits per pixel, converting to 32-bit.";
        bitmap32 = FreeImage_ConvertTo32Bits(bitmap);
    }

    int imageWidth = FreeImage_GetWidth(bitmap32);
    int imageHeight = FreeImage_GetHeight(bitmap32);

    std::cout << "Image : " << path << " is size : " << imageWidth << "x" << imageHeight << "y" << std::endl;

    GLubyte* textureData = FreeImage_GetBits(bitmap32);

    GLuint tmpTextureID;
    glGenTextures(1, &tmpTextureID);
    glBindTexture(GL_TEXTURE_2D, tmpTextureID);

    glTexImage2D(GL_TEXTURE_2D,
                 0,
                 GL_RGBA8,
                 imageWidth,
                 imageHeight,
                 0,
                 internalFormat,
                 GL_UNSIGNED_BYTE,
                 textureData);

    glGenerateMipmap(GL_TEXTURE_2D);



    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, minificationFilter);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, magnificationFilter);



    GLenum glError = glGetError();
    if(glError)
    {
        std::cout << "Error loading texture : " << path << std::endl;

        switch (glError) {
        case GL_INVALID_ENUM:
            std::cout << "Invalid enum." << std::endl;
            break;
        case GL_INVALID_VALUE:
            std::cout << "Invalid value." << std::endl;
            break;
        case GL_INVALID_OPERATION:
            std::cout << "Invalid operation." << std::endl;

        default:
            break;
        }
    }


    FreeImage_Unload(bitmap32);

    if(bitsPerPixel != 32)
    {
        FreeImage_Unload(bitmap);
    }

    glBindTexture(GL_TEXTURE_2D, 0);
    return tmpTextureID;
}

Texture::Texture()
{
    m_type = TextureType::TEXTURE;

}

Texture::Texture(const GLuint width,
                 const GLuint height,
                 const GLenum internalFormat,
                 const GLenum format,
                 const GLenum dataType)
    :m_width(width),
      m_height(height),
      m_internalFormat(internalFormat),
      m_format(format),
      m_dataType(dataType)
{
    m_type = TextureType::FRAMETEXTURE;

    glGenTextures(1, &m_id);
    glBindTexture(GL_TEXTURE_2D, m_id);

    glTexImage2D(GL_TEXTURE_2D, 0, internalFormat, width, height, 0, format, dataType, NULL);

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);    
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_BORDER);

    glBindTexture(GL_TEXTURE_2D, 0);

}

void Texture::resize(const GLuint width, const GLuint height)
{
    m_width = width;
    m_height = height;

    glBindTexture(GL_TEXTURE_2D, m_id);
    glTexImage2D(GL_TEXTURE_2D, 0, m_internalFormat, width, height, 0, m_format, m_dataType, NULL);
    glBindTexture(GL_TEXTURE_2D, 0);
}

Texture::Texture(const char *path, const TextureType type)
    :m_type(type)
{
    if(type == TextureType::HDR)
        m_id = loadHDRTexture(path);
    else m_id = loadTexture(path);
}

void Texture::Use() const
{
    glBindTexture(GL_TEXTURE_2D, m_id);


}
