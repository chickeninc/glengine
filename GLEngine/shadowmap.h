#ifndef SHADOWMAP_H
#define SHADOWMAP_H

#include "framebuffer.h"

constexpr uint SHADOW_WIDTH = 1024, SHADOW_HEIGHT = 1024;


class ShadowMap : public FrameBuffer
{
public:    
    ShadowMap(const GLuint width, const GLuint height);

    virtual void use();
    virtual void clean();

    inline GLuint depthmap() const
    {
        return m_depthMap;
    }

private:
    GLuint m_depthMap;
};

class ShadowMapCube : public FrameBuffer
{
public:
    ShadowMapCube(const GLuint width, const GLuint height);

    virtual void use();
    virtual void clean();

    inline GLuint depthmap() const
    {
        return m_depthMap;
    }

private:
    GLuint m_depthMap;
};

#endif // SHADOWMAP_H
