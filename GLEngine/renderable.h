#ifndef RENDERABLE_H
#define RENDERABLE_H

#include <GL/glew.h>
#include "shader.h"

class Renderable
{
public:
    virtual void render(Shader shader) = 0;
};

#endif // RENDERABLE_H
