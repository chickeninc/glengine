#include "instance.h"

Instance::Instance()
{

}

Instance::Instance(Model* m)
    :m_model(m)
{

}

Instance::Instance(Model* m, btTransform t, float mass, btCollisionShape* shape, bool isKinematic)
    :GameObject(t, mass, shape, isKinematic),
      m_model(m)
{

}


void Instance::render(ShaderProgram shader, GLenum mode)
{
    GameObject::render(shader, mode);
    GLuint id;
    if(m_model)
    {
        glUniformMatrix4fv(glGetUniformLocation(shader.program(), "model"), 1, GL_FALSE, getWorldTransform());
        m_model->render(shader, mode);
    }
}
