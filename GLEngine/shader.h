#ifndef SHADER_H
#define SHADER_H

#include <string>
#include <fstream>
#include <sstream>
#include <iostream>
#include <map>
#include <vector>

#include <GL/glew.h>

enum class ShaderMode{GBUFFERPASS,
                      LIGHTPASS,
                      PBRLIGHTPASS,
                      SHADOW,
                      SHADOWCUBE,
                      GEOMETRY,
                      SKYBOX,
                      CONVERTSKYBOX,
                      LIGHTSGEOMETRY,
                      LIGHTSSPHERES,
                      LIGHTAMBIENT,
                      LIGHTDIRECTIONAL,
                      STENCILPASS,
                      EQUITOCUBE,
                      CUBEMAPCONVOLUTION,
                      PREFILTERCUBEMAP,
                      BRDF,
                      BLOOM};


class Shader
{
public:


    Shader();
    Shader(const GLchar* path, GLenum mode);
    Shader(const GLchar* path, GLenum mode, const GLchar* secondPath);

    inline GLuint id() const
    {
        return m_id;
    }

private:
    GLuint m_id;
};

#endif
