#ifndef MODELWIDGET_H
#define MODELWIDGET_H

#include "widget.h"
#include "label.h"
#include "editbox.h"
#include "gui.h"

class ModelWidget : public Widget
{
public:
    ModelWidget(GUI& gui, Rectangle r);

    void setInstance(Instance* instance);
    void mouseCallback(int button, int action) override;

private:

    Label m_roughnessLabel;
    Slider m_roughnessSlider;
    EditBoxNum m_roughnessEbox;

    Label m_metallicLabel;
    Slider m_metallicSlider;
    EditBoxNum m_metallicEbox;

    Label m_positionLabel;
    EditBoxNum m_positionXEbox;
    EditBoxNum m_positionYEbox;
    EditBoxNum m_positionZEbox;


    Instance* m_instance = nullptr;
};

#endif // MODELWIDGET_H
