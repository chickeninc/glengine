#include <iostream>

// GLEW
#define GLEW_STATIC
#include <GL/glew.h>

// GLFW
#define GLFW_EXPOSE_NATIVE_X11
#define GLFW_EXPOSE_NATIVE_GLX
#include <GLFW/glfw3.h>
#include <GLFW/glfw3native.h>

// NanoVG
#include "nanovg.h"
#define NANOVG_GL3_IMPLEMENTATION
#include "nanovg_gl.h"
#include "nanovg_gl_utils.h"

// Other includes
#include "shader.h"
#include "model.h"
#include "camera.h"
#include "scene.h"
#include "perf.h"
#include "gui.h"
#include "controller.h"
#include "callbackmanager.h"
#include <vector>

#include <btBulletDynamicsCommon.h>
#include <chrono>
// Window default dimensions

#include "debugger.h"
using namespace Debugger;


int main()
{
    GLFWwindow* window;
    NVGcontext* vg = NULL;
    DemoData data;

    Rectangle screen = Rectangle(Point(0, 0), Point(WIDTH, HEIGHT));

    double prevt, cpuTime = 0;

    // Init GLFW
    if (!glfwInit()) {
        printf("Failed to init GLFW.");
        return -1;
    }    

    // Set all the required options for GLFW
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 5);
    glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
    glfwWindowHint(GLFW_RESIZABLE, GLFW_TRUE);
    glfwWindowHint(GLFW_MAXIMIZED, GLFW_TRUE);
    glfwWindowHint(GLFW_DECORATED, GLFW_TRUE);

    //GLFWAPI::Display* x11Display = glfwGetX11Display();

    // Create GLFWwindow
    window = glfwCreateWindow(WIDTH, HEIGHT, "OpenGL Engine", nullptr, nullptr);
    glfwMakeContextCurrent(window);

    // Set this to true so GLEW knows to use a modern approach to retrieving function pointers and extensions
    glewExperimental = GL_TRUE;
    // Initialize GLEW to setup the OpenGL Function pointers
    if(glewInit() != GLEW_OK) {
        printf("Could not init glew.\n");
        return -1;
    }
    glGetError();

    vg = nvgCreateGL3(NVG_ANTIALIAS | NVG_STENCIL_STROKES);
    if (vg == NULL) {
        printf("Could not init nanovg.\n");
        return -1;
    }

    CallbackManager callbackManager(screen);
    AssetManager assetManager = AssetManager();

    Scene scene = Scene(*window, screen, callbackManager.mouse(), assetManager);

    GUI ui = GUI(vg, screen, callbackManager.mouse(), scene);

    Renderer renderer = Renderer(*window, scene, ui, screen, assetManager);

    callbackManager.setGUI(&ui);
    callbackManager.setScene(&scene);
    callbackManager.setRenderer(&renderer);

    if(!&scene || !&ui) {
        std::cout << "bug" << std::endl;
        return -1;
    }


    Controller controller(scene, ui, renderer);

    glfwSwapInterval(1);
    // Set the callback functions
    glfwSetErrorCallback(CallbackManagerBase::error_callback_dispatch);
    glfwSetKeyCallback(window, CallbackManagerBase::key_callback_dispatch);
    glfwSetScrollCallback(window, CallbackManagerBase::scroll_callback_dispatch);
    glfwSetFramebufferSizeCallback(window, CallbackManagerBase::framebuffer_size_callback_dispatch);
    glfwSetMouseButtonCallback(window, CallbackManagerBase::mouse_callback_dispatch);
    glfwSetCursorPosCallback(window, CallbackManagerBase::cursor_position_callback_dispatch);
    glfwSetTime(0);

    double dt = 0;

    // Game loop
    while (!glfwWindowShouldClose(window))
    {
        std::chrono::high_resolution_clock::time_point t1 = std::chrono::high_resolution_clock::now();
        // Check if any events have been activated and call callback functions
        glfwPollEvents();

        controller.control();        

        scene.update();


        renderer.render();

        std::chrono::high_resolution_clock::time_point t2 = std::chrono::high_resolution_clock::now();

        auto duration = std::chrono::duration_cast<std::chrono::microseconds>( t2 - t1 ).count();

        dt = duration / 1000.0;
        debug("Main loop time :" + std::to_string(duration / 1000.0));
    }

    ui.freeDemoData();

    // Terminate GLFW, clear resources allocated by GLFW.
    glfwTerminate();
    return 0;
}


