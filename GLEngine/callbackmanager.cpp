#include "callbackmanager.h"

CallbackManagerBase* CallbackManagerBase::event_handling_instance;

void CallbackManagerBase::setEventHandling()
{
     event_handling_instance = this;
}

CallbackManager::CallbackManager(Rectangle& screen)
    :m_screen(screen),
     m_mouse()
{
    this->setEventHandling();
}

void CallbackManager::cursor_position_callback(GLFWwindow* window, double xpos, double ypos)
{
    m_mouse.update(xpos, ypos);

    if(m_mouse.lastX() == m_mouse.x() && m_mouse.lastY() == m_mouse.y()){
        m_mouse.setDelta(0, 0);
    }

    if(m_scene)
        m_scene->cursorCallback(xpos, ypos);
    if(m_gui)
        m_gui->cursorCallback(xpos, ypos);
}

void CallbackManager::key_callback(GLFWwindow* window, int key, int scancode, int action, int mode)
{
    if (key == GLFW_KEY_ESCAPE && action == GLFW_RELEASE){
        glfwSetWindowShouldClose(window, GL_TRUE);
    }

    m_scene->keyCallback(key, scancode, action, mode);
}

void CallbackManager::mouse_callback(GLFWwindow*, int button, int action, int mods)
{
    switch (button) {
    case GLFW_MOUSE_BUTTON_LEFT:
        m_mouse.flipLb();
        break;
    case GLFW_MOUSE_BUTTON_RIGHT:
        m_mouse.flipRb();
    default:
        break;
    }

    if(m_scene && m_gui)
    {
        bool mouseOnGui = false;
        for(Widget& w : m_gui->getWidgets())
        {
            if(w.bounds().contains(m_mouse.x(), m_mouse.y()) && w.isVisible())
            {
                mouseOnGui = true;
                break;
            }
        }

        mouseOnGui ? m_gui->mouseCallback(button, action) :
                     m_scene->mouseCallback(button, action);
    }


}

void CallbackManager::scroll_callback(GLFWwindow* window, double xoffset, double yoffset)
{
    if(m_gui)
        m_gui->scrollCallback(xoffset, yoffset);
    if(m_scene)
        m_scene->scrollCallback(xoffset, yoffset);
}

void CallbackManager::error_callback(int error, const char* desc)
{
    std::cout << "GLFW error " << error << ": " << desc << std::endl;
}

void CallbackManager::framebuffer_size_callback(GLFWwindow* window, int width, int height)
{
    glViewport(0, 0, width, height);

    m_screen.setX2(width);
    m_screen.setY2(height);

    if(m_renderer)
        m_renderer->framebufferSizeCallback();
    if(m_gui)
        m_gui->framebufferSizeCallback();


}

