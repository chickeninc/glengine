#include "shader.h"
#include <string>

Shader::Shader(){

}

Shader::Shader(const GLchar *path, GLenum mode)
{

    std::cout << "Loading shader : " << path << std::endl;
    std::string code;
    std::ifstream shaderFile;

    shaderFile.exceptions (std::ifstream::badbit);

    try
    {
        shaderFile.open(path);
        std::stringstream shaderStream;
        shaderStream << shaderFile.rdbuf();
        shaderFile.close();
        code = shaderStream.str();
    }
    catch(std::ifstream::failure e)
    {
        std::cout << "EROOR::SHADER::FILE_NOT_SUCCESFULLY_READ" << std::endl;
    }

    const GLchar* shaderCode = code.c_str();

    m_id = glCreateShader(mode);
    glShaderSource(m_id, 1, &shaderCode, NULL);
    glCompileShader(m_id);

    GLint success;
    GLchar infoLog[512];
    glGetShaderiv(m_id, GL_COMPILE_STATUS, &success);
    if(!success)
    {
        glGetShaderInfoLog(m_id, 512, NULL, infoLog);
        std::cout << "ERROR::SHADER::COMPILATION_FAILED\n" << infoLog << std::endl;
    }
}

Shader::Shader(const GLchar *path, GLenum mode, const GLchar *secondPath)
{
    std::cout << "Loading shader : " << path << " with : " << secondPath << std::endl;
    std::string code;
    std::ifstream shaderFile;

    shaderFile.exceptions (std::ifstream::badbit);

    try
    {
        shaderFile.open(path);
        std::stringstream shaderStream;
        shaderStream << shaderFile.rdbuf();
        shaderFile.close();
        code = shaderStream.str();
    }
    catch(std::ifstream::failure e)
    {
        std::cout << "EROOR::SHADER::FILE_NOT_SUCCESFULLY_READ" << std::endl;
    }

    const GLchar* shaderCode = code.c_str();

    std::string secondCode;
    std::ifstream secondShaderFile;

    secondShaderFile.exceptions (std::ifstream::badbit);

    try
    {
        secondShaderFile.open(secondPath);
        std::stringstream secondShaderStream;
        secondShaderStream << secondShaderFile.rdbuf();
        secondShaderFile.close();
        secondCode = secondShaderStream.str();
    }
    catch(std::ifstream::failure e)
    {
        std::cout << "EROOR::SHADER::FILE_NOT_SUCCESFULLY_READ" << std::endl;
    }

    const GLchar* secondShaderCode = secondCode.c_str();

    const GLchar* shaders[2];
    shaders[0] = secondShaderCode;
    shaders[1] = shaderCode;

    m_id = glCreateShader(mode);
    glShaderSource(m_id, 2, shaders, NULL);
    glCompileShader(m_id);

    GLint success;
    GLchar infoLog[512];
    glGetShaderiv(m_id, GL_COMPILE_STATUS, &success);
    if(!success)
    {
        glGetShaderInfoLog(m_id, 512, NULL, infoLog);
        std::cout << "ERROR::SHADER::COMPILATION_FAILED\n" << infoLog << std::endl;
    }
}
