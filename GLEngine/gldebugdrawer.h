#ifndef GLDEBUGDRAWER_H
#define GLDEBUGDRAWER_H

#include <GL/glew.h>

#include "bullet/src/LinearMath/btIDebugDraw.h"
#include <iostream>
#include "camera.h"
#include "shaderprogram.h"

class GLDebugDrawer : public btIDebugDraw
{

public:
    GLDebugDrawer(Camera& cam);
    ~GLDebugDrawer() { }

    void drawLine(const btVector3&from, const btVector3& to, const btVector3 &color) override;
    void drawLine(const btVector3&from, const btVector3& to, const btVector3 &color, const btVector3& toColor) override;


    void reportErrorWarning(const char* warningString) override
    {
        std::cout << warningString << std::endl;
    }

    void draw3dText(const btVector3& location,const char* textString) override;

    void drawContactPoint(const btVector3& PointOnB, const btVector3& normalOnB, btScalar distance, int lifeTime, const btVector3& color) override;

    void setDebugMode(int debugMode) { m_debugMode = debugMode; }

    inline int getDebugMode() const { return m_debugMode;}

private:
    ShaderProgram m_shaderProgram;
    Camera& m_camera;
    int m_debugMode;
};

#endif // GLDEBUGDRAWER_H
