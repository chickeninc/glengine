#include "modelwidget.h"

ModelWidget::ModelWidget(GUI& gui, Rectangle r)
    :Widget(gui, "Model", r),
      m_roughnessLabel(gui, "Roughness : "),
      m_roughnessSlider(gui, 0.5f, 0.0f, 1.0f),
      m_roughnessEbox(gui, "", "", &m_roughnessSlider.getValue()),
      m_metallicLabel(gui, "Metallic : "),
      m_metallicSlider(gui, 0.5f, 0.0f, 1.0f),
      m_metallicEbox(gui, "", "", &m_metallicSlider.getValue()),
      m_positionLabel(gui, "Position : "),
      m_positionXEbox(gui, "", "x"),
      m_positionYEbox(gui, "", "y"),
      m_positionZEbox(gui, "", "z")
{
    m_roughnessSlider.setOffset(10, 5);
    m_metallicSlider.setOffset(10, 5);

    /*  Containers */
    for(int i = 0; i < 6; i++)
        m_children.push_back(std::make_shared<Container>(gui));

    std::static_pointer_cast<Container>(m_children[0])->
            addChild(std::shared_ptr<Label>(&m_roughnessLabel));

    std::static_pointer_cast<Container>(m_children[1])->
            addChildren({
                            std::shared_ptr<Slider>(&m_roughnessSlider),
                            std::shared_ptr<EditBoxNum>(&m_roughnessEbox)
                        });

    std::static_pointer_cast<Container>(m_children[2])->
            addChild(std::shared_ptr<Label>(&m_metallicLabel));

    std::static_pointer_cast<Container>(m_children[3])->
            addChildren({
                            std::shared_ptr<Slider>(&m_metallicSlider),
                            std::shared_ptr<EditBoxNum>(&m_metallicEbox)
                        });

    std::static_pointer_cast<Container>(m_children[4])->
            addChild(std::shared_ptr<Label>(&m_positionLabel));

    std::static_pointer_cast<Container>(m_children[5])->
            addChildren({
                            std::shared_ptr<EditBoxNum>(&m_positionXEbox),
                            std::shared_ptr<EditBoxNum>(&m_positionYEbox),
                            std::shared_ptr<EditBoxNum>(&m_positionZEbox)
                        });

    gui.getWidgets().push_back(*this);

    gui.getElements().insert(std::make_pair("roughnessLabel",
                                            std::shared_ptr<Label>(&m_roughnessLabel)));
    gui.getElements().insert(std::make_pair("roughnessEbox",
                                            std::shared_ptr<EditBoxNum>(&m_roughnessEbox)));
    gui.getElements().insert(std::make_pair("roughnessSlider",
                                            std::shared_ptr<Slider>(&m_roughnessSlider)));

    gui.getElements().insert(std::make_pair("metallicLabel",
                                            std::shared_ptr<Label>(&m_metallicLabel)));
    gui.getElements().insert(std::make_pair("metallicEbox",
                                            std::shared_ptr<EditBoxNum>(&m_metallicEbox)));
    gui.getElements().insert(std::make_pair("metallicSlider",
                                            std::shared_ptr<Slider>(&m_metallicSlider)));

    gui.getElements().insert(std::make_pair("modelPositionLabel",
                                            std::shared_ptr<Label>(&m_positionLabel)));
    gui.getElements().insert(std::make_pair("modelPositionXEbox",
                                            std::shared_ptr<EditBoxNum>(&m_positionXEbox)));
    gui.getElements().insert(std::make_pair("modelPositionYEbox",
                                            std::shared_ptr<EditBoxNum>(&m_positionYEbox)));
    gui.getElements().insert(std::make_pair("modelPositionZEbox",
                                            std::shared_ptr<EditBoxNum>(&m_positionZEbox)));

    updateChildren();
}

void ModelWidget::setInstance(Instance *instance)
{
    m_instance = instance;
    if(m_instance)
    {
        m_roughnessSlider.setPosition(m_instance->model()->material().roughness());
        m_roughnessSlider.setAffectedValue(&m_instance->model()->material().roughness());
        m_metallicSlider.setPosition(m_instance->model()->material().metallic());
        m_metallicSlider.setAffectedValue(&m_instance->model()->material().metallic());


        btVector3& v = m_instance->physicsBody()->getWorldTransform().getOrigin();
        m_positionXEbox.setListenedValue(v[0]);
        m_positionYEbox.setListenedValue(v[1]);
        m_positionZEbox.setListenedValue(v[2]);
    }
}

void ModelWidget::mouseCallback(int button, int action)
{

}
