#ifndef LIGHT_H
#define LIGHT_H

#include <GL/gl.h>
#include <glm/vec3.hpp>
#include <glm/vec4.hpp>
#include <math.h>
#include <string>
#include <glm/mat4x4.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <vector>

class Light
{

public:

    Light();
    Light(glm::vec3 color);

    virtual inline std::string description() const
    {
        return "Light";
    }

    inline glm::vec3 color() const
    {
        return m_color;
    }

    inline glm::vec3& color()
    {
        return m_color;
    }

    inline glm::vec3& position()
    {
        return m_position;
    }

    inline glm::vec3 position() const
    {
        return m_position;
    }

    inline void setPosition(const glm::vec3 position)
    {
        m_position = position;
    }

    inline void setColor(const glm::vec3 color)
    {
        m_color = color;
    }

    inline GLboolean isLocal() const
    {
        return m_isLocal;
    }

    inline GLboolean isSpot() const
    {
        return m_isSpot;
    }

    inline GLboolean isEnabled() const
    {
        return m_isEnabled;
    }

    inline GLfloat spotCosCutOff() const
    {
        return m_spotCosCutOff;
    }

    inline GLfloat spotExponent() const
    {
        return m_spotExponent;
    }

    inline glm::vec3 direction() const
    {
        return m_direction;
    }

    inline glm::vec3& direction()
    {
        return m_direction;
    }

    inline void setRadius(const GLfloat radius)
    {
        m_radius = radius;
    }

    inline GLfloat radius() const
    {
        return m_radius;
    }

    inline GLfloat& radius()
    {
        return m_radius;
    }

    inline glm::vec3 halfVector() const
    {
        return m_halfVector;
    }

    inline GLuint id() const
    {
        return m_id;
    }

    inline GLfloat& strength()
    {
        return m_strength;
    }

    inline void setStrength(const float s)
    {
        m_strength = s;
    }

    inline GLfloat strength() const
    {
        return m_strength;
    }

protected:

    glm::vec3 m_color;

    glm::vec3 m_position;
    glm::vec3 m_direction;
    glm::vec3 m_halfVector;

    GLboolean m_isLocal = false;
    GLboolean m_isSpot = false;
    GLboolean m_isEnabled = false;

    GLfloat m_lightMax = fmaxf(fmaxf(m_color.r, m_color.g), m_color.b);
    GLfloat m_radius = 1.0f;

    GLfloat m_strength = 1.0f;

    GLfloat m_spotCosCutOff = 0.6f;
    GLfloat m_spotExponent = 32.0f;

private:
    GLuint m_id;
    static GLuint s_id;

};

class DirectionalLight : public Light
{

public:

    DirectionalLight();
    DirectionalLight(glm::vec3 color, glm::vec3 direction, glm::vec3 halfVector = glm::vec3());

    inline glm::mat4 lightSpaceMatrix() const
    {
        return m_lightSpaceMatrix;
    }

    inline glm::mat4& lightSpaceMatrix()
    {
        return m_lightSpaceMatrix;
    }

    inline void setLightSpaceMatrix(const glm::mat4 m)
    {
        m_lightSpaceMatrix = m;
    }

private:
    glm::mat4 m_lightSpaceMatrix;
};

class PointLight : public Light
{

public:

    PointLight();
    PointLight(glm::vec3 color, glm::vec3 position, GLfloat radius = 1.0f, GLfloat strength = 10.0f);


};

class SpotLight : public Light
{

public :

    SpotLight();
    SpotLight(glm::vec3 color, glm::vec3 position, glm::vec3 direction, GLfloat radius = 1.0f, GLfloat strength = 10.0f);

};

#endif // LIGHT_H
