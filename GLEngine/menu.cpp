#include "menu.h"

Menu::Menu(GUI& gui)
    :Container(gui)
{
    m_type = Type::MENU;
}

Menu::Menu(GUI& gui, Rectangle r)
    :Container(gui, r)
{
    m_type = Type::MENU;
}

GLvoid Menu::setBounds(Rectangle r)
{
    m_bounds = r;
    updateChildren();
}

void Menu::draw(NVGcontext* vg)
{
    float cornerRadius = 3.0f;
    NVGpaint shadowPaint;
    NVGpaint headerPaint;

    Rectangle r = m_bounds;

    nvgSave(vg);
    //	nvgClearState(vg);

    // Window
    nvgBeginPath(vg);
    nvgRect(vg, r.x(),r.y(), r.width(),r.height());

    nvgFillColor(vg, nvgRGBA(58,60,64,168));

    nvgFill(vg);

    // Drop shadow
    shadowPaint = nvgBoxGradient(vg, r.x(),r.y()+2, r.width(),r.height(), cornerRadius*2, 10, nvgRGBA(0,0,0,128), nvgRGBA(0,0,0,0));
    nvgBeginPath(vg);
    nvgRect(vg, r.x()-10,r.y()-10, r.width()+20,r.height()+30);
    nvgRoundedRect(vg, r.x(),r.y(), r.width(),r.height(), cornerRadius);
    nvgPathWinding(vg, NVG_HOLE);
    nvgFillPaint(vg, shadowPaint);
    nvgFill(vg);


    nvgRestore(vg);
}

void Menu::addChild(Widget& w)
{
    bool flag = false;

    for(Widget& widg : m_children)
    {
        // if widget is already a child
        if(&w == &widg) flag = true;
    }

    if(!flag) {
        w.lock();
        w.contract();
        m_children.push_back(w);
        w.setHasParent(true);

        std::cout << "Menu childrens : " << m_children.size() << std::endl;

        updateChildren();
    }

}


void Menu::updateChildren()
{
    int i = 0;
    for(Widget& e : m_children){
        int width = (m_bounds.width()/ m_children.size());
        int x = m_bounds.x() + (i * width);
        int y = m_bounds.y();
        int x2 = x + width;
        int y2 = m_bounds.y2();
        Rectangle r(x,y,x2,y2);
        e.setHeader(r);
        i++;
    }

}

void Menu::mouseCallback(int button, int action)
{
    for(Widget & w : m_children)
    {
        if(w.header().contains(m_mouse.x(), m_mouse.y()))
            w.mouseCallback(button, action);
    }
}
