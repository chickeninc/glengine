#ifndef MOUSEPICKER_H
#define MOUSEPICKER_H

#include "camera.h"
#include <glm/glm.hpp>
#include <glm/mat4x4.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include "rectangle.h"
#include "mouse.h"
#include <iostream>
#include <bullet/btBulletDynamicsCommon.h>

class MousePicker
{

public:

    MousePicker();
    MousePicker(btDynamicsWorld* world);

    bool pickBody(const btVector3& rayFromWorld, const btVector3& rayToWorld);
    bool movePickedBody(const btVector3& rayFromWorld, const btVector3& rayToWorld);
    void removePickingConstraint();
    void update(Camera& camera, Mouse mouse, Rectangle Screen);


    inline btRigidBody* pickedBody() const
    {
        return m_pickedBody;
    }

    inline btPoint2PointConstraint* pickedConstraint() const
    {
        return m_pickedConstraint;
    }

    inline int savedState() const
    {
        return m_savedState;
    }

    inline btVector3 oldPickingPos() const
    {
        return m_oldPickingPos;
    }

    inline btVector3 hitPos() const
    {
        return m_hitPos;
    }

    inline btVector3& hitPos()
    {
        return m_hitPos;
    }

private:

    btDynamicsWorld* m_world = nullptr;

    btRigidBody* m_pickedBody = nullptr;
    btPoint2PointConstraint* m_pickedConstraint = nullptr;

    int m_savedState;
    btVector3 m_oldPickingPos;
    btVector3 m_hitPos;
    btScalar m_oldPickingDist;
};

#endif // MOUSEPICKER_H
