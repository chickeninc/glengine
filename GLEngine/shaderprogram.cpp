#include "shaderprogram.h"

ShaderProgram::ShaderProgram(std::vector<Shader> shaders)
{
    m_program = glCreateProgram();
    for(Shader shader : shaders)
    {
        glAttachShader(m_program, shader.id());
    }
    glLinkProgram(m_program);
    GLint success;

    glGetProgramiv(m_program, GL_LINK_STATUS, &success);
    if (!success)
    {
        GLchar infoLog[512];
        glGetProgramInfoLog(m_program, 512, NULL, infoLog);
        std::cout << "ERROR::SHADER::PROGRAM::LINKING_FAILED\n" << infoLog << std::endl;
    }
    for(Shader shader : shaders)
    {
        glDeleteShader(shader.id());
    }

}

void ShaderProgram::Use()
{
    glUseProgram(m_program);
}
