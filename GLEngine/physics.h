#ifndef PHYSICS_H
#define PHYSICS_H

#include <btBulletDynamicsCommon.h>
#include <vector>
#include <memory>
#include "gameobject.h"

class Scene;

class Physics
{
public:
    Physics();
    ~Physics();

    void update();
    void loadBulletWorld(Scene& scene);
    void addBody(btRigidBody* body);

    inline btDynamicsWorld* world() const
    {
        return m_world;
    }

private:

    btDefaultCollisionConfiguration* m_collisionConfig;
    btCollisionDispatcher* m_dispatcher;

    btBroadphaseInterface* m_broadphase;

    btDynamicsWorld* m_world;
    btSequentialImpulseConstraintSolver* m_solver;


    std::vector<btRigidBody*> m_bodies;

};

#endif // PHYSICS_H
