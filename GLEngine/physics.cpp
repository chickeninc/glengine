#include "physics.h"
#include <iostream>
#include "btBulletWorldImporter.h"

#include "scene.h"



const std::string PATH = "/home/lebasith/Git/cpp/GLEngine";

bool physics_callback(btManifoldPoint& cp,
                      const btCollisionObjectWrapper* obj1, int id1, int index1,
                      const btCollisionObjectWrapper* obj2, int id2, int index2);

Physics::Physics()
{

    m_collisionConfig =  new btDefaultCollisionConfiguration();
    m_dispatcher = new btCollisionDispatcher(m_collisionConfig);

    m_broadphase = new btDbvtBroadphase();

    m_solver = new btSequentialImpulseConstraintSolver();

    m_world = new btDiscreteDynamicsWorld(m_dispatcher, m_broadphase, m_solver, m_collisionConfig);

    m_world->setGravity(btVector3(0, -9.8, 0));

}

void Physics::update()
{
    m_world->stepSimulation(1/60.0f);
}

void Physics::loadBulletWorld(Scene& scene)
{
    btBulletWorldImporter* fileLoader = new btBulletWorldImporter(m_world);
    fileLoader->loadFile("/home/lebasith/Git/cpp/GLEngine/assets/test.bullet");

    btVector3 v = m_world->getCollisionObjectArray().at(1)->getWorldTransform().getOrigin();
    std::cout << "Bullet object 0 : vx: " << v.x() << " vy : " << v.y() << " vz : " << v.z() << std::endl;
    gContactAddedCallback = physics_callback;
    for(int i = 0; i < m_world->getNumCollisionObjects(); i++)
    {
        btCollisionObject* obj = m_world->getCollisionObjectArray()[i];
        btRigidBody* body = btRigidBody::upcast(obj);

        std::string  name = fileLoader->getNameForPointer(body);
        std::cout << "  object name = " <<  name << std::endl;
        btTransform t = obj->getWorldTransform();
        btMatrix3x3 b = t.getBasis();
        btVector3 o = t.getOrigin();
        o = btVector3(o.x(), o.z(), o.y());
        t.setOrigin(o);
        btQuaternion rot;
        b.getRotation(rot);

        std::cout << "  object origin = " <<  o.x() << o.y() << o.z() << std::endl;
        std::cout << "  object rotation = x: " <<  rot.getAxis().x() << " y : " << rot.getAxis().y() << " z : "<< rot.getAxis().z() << std::endl;
        std::cout << "  object rotation angle = " <<  (rot) << std::endl;

        std::cout << "Object scale : x : " <<
                     b.getRow(0).x() <<
                     " y : " << b.getRow(0).y() <<
                     " z : " << b.getRow(0).z() << std::endl;
        std::cout << "Object scale : x : " <<
                     b.getRow(1).x() <<
                     " y : " << b.getRow(1).y() <<
                     " z : " << b.getRow(1).z() << std::endl;

        std::cout << "Object scale : x : " <<
                     b.getRow(2).x() <<
                     " y : " << b.getRow(2).y() <<
                     " z : " << b.getRow(2).z() << std::endl;

        if(obj->isStaticOrKinematicObject())
        {

            std::cout << "l'objet est static:  " << std::endl;
        }
        else
        {

            std::cout << "l'objet n'est pas static:  " << std::endl;
        }

        if(name == "Plane")
        {
            int factor = 10;
            Mesh groundMesh;
            groundMesh.createPlane(factor);
            //groundMesh.createTerrain((PATH + "/assets/textures/heightmap.png").data());
            Model ground = Model(groundMesh);
            ground.material().addTexture(scene.assetManager().textures().at("boards_diffuse"));
            ground.material().addTexture(scene.assetManager().textures().at("boards_specular"));
            ground.material().addTexture(scene.assetManager().textures().at("boards_normal"));
            ground.material().addTexture(scene.assetManager().textures().at("boards_height"));
            scene.models().insert(std::make_pair("ground", ground));
            btStaticPlaneShape* plane = new btStaticPlaneShape(btVector3(0,1,0), btScalar(0));
            scene.shapes().insert(std::make_pair("ground", plane));
            btMatrix3x3 tr;
            tr.setIdentity();
            tr = tr.scaled(btVector3(10, 10, 10));
            t.setBasis(tr);
            t.setOrigin(btVector3(0, 0, 0));
            Instance inst(&scene.models().at("ground"), t, 0.0f, scene.shapes().at("ground"));
            addBody(inst.physicsBody());
            scene.instances().push_back(inst);
        }
        else
        {
            Mesh cubeMesh;
            cubeMesh.loadOBJ((PATH + "/assets/cube.obj").data());
            Model cube = Model(cubeMesh);
            cube.material().addTexture(scene.assetManager().textures().at("stone_diffuse"));
            cube.material().addTexture(scene.assetManager().textures().at("stone_specular"));
            cube.material().addTexture(scene.assetManager().textures().at("stone_normal"));
            cube.material().addTexture(scene.assetManager().textures().at("boards_height"));
            scene.models().insert(std::make_pair("cube", cube));
            btBoxShape* cubeShape = new btBoxShape(btVector3(1, 1, 1));
            scene.shapes().insert(std::make_pair("cube", cubeShape));



            Instance instance = Instance(&scene.models().at("cube"), t, 1.0f, scene.shapes().at("cube"));
            addBody(instance.physicsBody());
            scene.instances().push_back(instance);
        }
        m_world->removeCollisionObject(obj);
    }


    m_world->setGravity(btVector3(0, -9.8, 0));
}

Physics::~Physics()
{
    for(btRigidBody* body : m_bodies)
    {
        btMotionState* motionState = body->getMotionState();
        delete motionState;
        delete body;
    }
    delete m_collisionConfig;
    delete m_dispatcher;
    delete m_broadphase;
    delete m_solver;
}

void Physics::addBody(btRigidBody* body)
{
    m_world->addRigidBody(body);
    m_bodies.push_back(body);
}

bool physics_callback(btManifoldPoint& cp,
                      const btCollisionObjectWrapper* obj1, int id1, int index1,
                      const btCollisionObjectWrapper* obj2, int id2, int index2)
{
    std::cout << "collision" << std::endl;
    return false;
}
