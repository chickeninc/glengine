#ifndef COLORWHEEL_H
#define COLORWHEEL_H

#include "uielement.h"

class ColorWheel : public UIElement
{
public:
    ColorWheel(GUI& gui);

    void draw(NVGcontext* vg) override;
};

#endif // COLORWHEEL_H
