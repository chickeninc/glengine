#include "rectangle.h"

#include <iostream>

Rectangle::Rectangle()
{

}

Rectangle::Rectangle(const Rectangle& r)
    :m_x(r.x()),
      m_y(r.y()),
      m_x2(r.x2()),
      m_y2(r.y2())
{

}

Rectangle::Rectangle(int x, int y, int x2, int y2)
    :m_x(x),
      m_y(y),
      m_x2(x2),
      m_y2(y2)
{

}

Rectangle::Rectangle(Point p1, Point p2)
    :m_x(p1.x()),
      m_y(p1.y()),
      m_x2(p2.x()),
      m_y2(p2.y())
{

}

int Rectangle::overlapArea(const Rectangle& rect) const
{
    int x_overlap = std::max(0, std::min(m_x2, rect.x2()) - std::max(m_x, rect.x()));
    int y_overlap = std::max(0, std::min(m_y2, rect.y2()) - std::max(m_y, rect.y()));
    return x_overlap * y_overlap;
}

bool Rectangle::contains(const int x, const int y) const
{
    if(x >= m_x && x <= m_x2){
        if(y >= m_y && y <= m_y2){
            return true;
        }
    }
    return false;
}

bool Rectangle::contains(const int x, const int y, const float ampl) const
{
    if(x >= m_x - ampl && x <= m_x2 + ampl){
        if(y >= m_y - ampl && y <= m_y2 + ampl){
            return true;
        }
    }
    return false;
}

void Rectangle::translate(const int x, const int y)
{
    m_x += x;
    m_y += y;
    m_x2 += x;
    m_y2 += y;
}

void Rectangle::description() const
{
    std::cout << "Rectangle(" << m_x << ", " << m_y << ", " << m_x2 << ", " << m_y2 << ")." << std::endl;
    std::cout << "Width : " << width() << ", Height : " << height() << std::endl;
}
