#ifndef LISTENABLE_H
#define LISTENABLE_H

#include "listener.h"
#include <vector>

class Listener;

class Listenable
{

public:
    inline void addListener(Listener* listener, std::function<void()> function)
    {
        m_listeners.push_back(listener);
        listener->listenTo(this, function);
    }

    inline void notify()
    {
        for(Listener* listener : m_listeners)
            listener->onUpdate();
    }

protected:
    std::vector<Listener*> m_listeners;

};

#endif // LISTENABLE_H
