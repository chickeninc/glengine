#ifndef CONSOLE_H
#define CONSOLE_H

#include <string>
#include "paragraph.h"

class Console
{
public:
    Console(){ }

    inline void setOutput(Paragraph* p)
    {
        m_output = p;
    }
    inline void addText(std::string txt)
    {
        if(m_output)
            m_output->addText(txt);
    }

private:
    Paragraph* m_output = nullptr;
};

#endif // CONSOLE_H
