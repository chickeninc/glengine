#ifndef DROPDOWN_H
#define DROPDOWN_H

#include "container.h"
#include <string>
#include "widget.h"

class Widget;

class DropDown : public UIElement
{
public:

    DropDown(GUI& gui, std::string text);
    DropDown(GUI& gui, Rectangle r, std::string text);

    void setBounds(Rectangle r) override;
    void translate(int x, int y) override;

    inline std::string text() const
    {
        return m_text;
    }

    inline GLvoid setText(const std::string txt)
    {
        m_text = txt;
    }

    void setChild(Widget* widg);

    void setVisible(bool b) override;

    void updateChild();

    void mouseCallback(int button, int action) override;
    void draw(NVGcontext* vg) override;

protected:
    std::string m_text;
    Widget* m_child = nullptr;
};

#endif // DROPDOWN_H
