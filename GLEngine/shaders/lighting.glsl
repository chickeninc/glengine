#version 450 core
#pragma glsl

const float PI = 3.14159265359;

struct Light {
    vec3 position;
    vec3 spotDirection;
    vec3 halfVector;

    vec3 ambient;
    vec3 color;

    bool isLocal;
    bool isSpot;

    float radius;

    float spotCosCutOff;
    float spotExponent;

    float strength;
};

float saturate(in float value)
{
    return clamp(value, 0.0, 1.0);
}

vec3 saturate(in vec3 vec)
{
    return clamp(vec, 0.0, 1.0);
}

vec3 Albedo(vec3 realAlbedo)
{
    return realAlbedo / PI;
}

float GeometrySchlickGGX(float NdotV, float roughness)
{
    float r = (roughness + 1.0);
    float k = (r*r) / 8.0;

    float nom   = NdotV;
    float denom = NdotV * (1.0 - k) + k;

    return nom / denom;
}

float GeometrySmith(vec3 N, vec3 V, vec3 L, float roughness)
{
    float NdotV = max(dot(N, V), 0.0);
    float NdotL = max(dot(N, L), 0.0);
    float ggx2  = GeometrySchlickGGX(NdotV, roughness);
    float ggx1  = GeometrySchlickGGX(NdotL, roughness);

    return ggx1 * ggx2;
}

vec3 fresnelSchlick(float cosTheta, vec3 F0)
{
    return F0 + (1.0 - F0) * pow(1.0 - cosTheta, 5.0);
}

vec3 fresnelRoughness(in vec3 f0, in float f90, in float roughness) {
    return f0 + (f90-f0) * pow(1-roughness, 5.0f);
}

vec3 fresnelSchlickRoughness(float cosTheta, vec3 F0, float roughness)
{
    return F0 + (max(vec3(1.0 - roughness), F0) - F0) * pow(1.0 - cosTheta, 5.0);
}

vec3 fresnel_factor(in vec3 f0, in float product)
{
    return mix(f0, vec3(1.0), pow(1.01 - product, 5.0));
}

float Distribution_GGX(vec3 N, vec3 H, float alpha)
{
    float a2 = alpha * alpha;
    float NdotH = max(dot(N,H), 0.0);

    float tmp = (NdotH * a2 - NdotH) * NdotH + 1.0;

    return (a2 / (PI * tmp * tmp));
}

float D_GGX(in float roughness, in float NdH)
{
    float m = roughness * roughness;
    float m2 = m * m;
    float d = (NdH * m2 - NdH) * NdH + 1.0;
    return m2 / (PI * d * d);
}

float D_blinn(in float roughness, in float NdH)
{
    float m = roughness * roughness;
    float m2 = m * m;
    float n = 2.0 / m2 - 2.0;
    return (n + 2.0) / (2.0 * PI) * pow(NdH, n);
}

float D_beckmann(in float roughness, in float NdH)
{
    float m = roughness * roughness;
    float m2 = m * m;
    float NdH2 = NdH * NdH;
    return exp((NdH2 - 1.0) / (m2 * NdH2)) / (PI * m2 * NdH2 * NdH2);
}

float Diffuse_Lambert(float NdotL)
{
        return saturate(NdotL);
}

vec3 blendMaterial(vec3 Kdiff, vec3 Kspec, vec3 Kbase, float metallic)
{
    float scRange = smoothstep(0.2, 0.45, metallic);
    vec3 dielectric = Kdiff + Kspec;
    vec3 metal = Kspec * Kbase;
    return mix(dielectric, metal, scRange);
}

const float C1 = 0.429043;
const float C2 = 0.511664;
const float C3 = 0.743125;
const float C4 = 0.886227;
const float C5 = 0.247708;

const vec3 L00  = vec3( 0.871297,  0.875222,  0.864470);
const vec3 L1m1 = vec3( 0.175058,  0.245335,  0.312891);
const vec3 L10  = vec3( 0.034675,  0.036707,  0.037362);
const vec3 L11  = vec3(-0.004629, -0.029448, -0.048028);
const vec3 L2m2 = vec3(-0.120535, -0.121160, -0.117507);
const vec3 L2m1 = vec3( 0.003241,  0.003624,  0.007511);
const vec3 L20  = vec3(-0.028667, -0.024926, -0.020998);
const vec3 L21  = vec3(-0.077539, -0.086325, -0.091591);
const vec3 L22  = vec3(-0.161784, -0.191783, -0.219152);

vec3 getDiffuseFromSH(vec3 normal)
{
    return C1 * L22 * (normal.x * normal.x - normal.y * normal.y) +
            C3 * L20 * normal.z +
            C4 * L00 -
            C5 * L20 +
            2.0 * C1 * L2m2 * normal.x * normal.y +
            2.0 * C1 * L21 * normal.x * normal.z +
            2.0 * C1 * L2m1 * normal.y * normal.z +
            2.0 * C2 * L11 * normal.x +
            2.0 * C2 * L1m1 * normal.y +
            2.0 * C2 * L10 * normal.z;
}

float V_SmithGGX(float NoL, float NoV, float alphaG)
{
    float alphaG2 = alphaG * alphaG;
    float Lambda_GGXV = saturate(NoL + 1e-5f) * sqrt((-NoV * alphaG2 + NoV) * NoV + alphaG2);
    float Lambda_GGXL = NoV * sqrt((-NoL * alphaG2 + NoL) * NoL + alphaG2);

    return 0.5f / (Lambda_GGXV + Lambda_GGXL);
}

vec3 Square(vec3 x)
{
    return x*x;
}

float Square(float x)
{
    return x*x;
}

vec3 Fresnel(vec3 SpecularColor, float VoH)
{
    vec3 SpecularColorSqrt = sqrt(clamp(vec3(0.0), vec3(0.99), SpecularColor));
    vec3 n = (1 + SpecularColorSqrt) / (1 - SpecularColorSqrt);
    vec3 g = sqrt(n * n +  VoH * VoH - 1);

    return 0.5f * Square((g - VoH) / (g + VoH)) * (1 + Square(((g + VoH) * VoH - 1) / ((g - VoH) * VoH + 1)));
}

vec3 SpecularBRDF(float NoL, float NoV, float VoH, float NoH, vec3 specularColor, float roughness)
{
    return Fresnel(specularColor, VoH) * V_SmithGGX(NoL, NoV, roughness) * D_GGX(roughness, NoH);
}

float Diffuse_OrenNayar(float Roughness, float NdotV, float NdotL, float VdotH )
{

        // bias avoid divide by zero..
        NdotV = saturate(NdotV) + 0.00001f;
	NdotL = saturate(NdotL) + 0.00001f;

	float m = Roughness * Roughness;
	float m2 = m * m;

	float VdotL = 2 * VdotH - 1;
	float CosTerm = VdotL - NdotV * NdotL;

	float C1 = 1 - 0.5 * m2 * (1.0f / (m2 + 0.33));

	float C2 = 0.45 * m2 / (m2 + 0.09) * CosTerm * ( CosTerm >= 0 ? min( 1, NdotL * (1.0f / NdotV )) : NdotL );
	return NdotL * C1 + C2;
}

float bitfieldReverse(uint bits) {
     bits = (bits << 16u) | (bits >> 16u);
     bits = ((bits & 0x55555555u) << 1u) | ((bits & 0xAAAAAAAAu) >> 1u);
     bits = ((bits & 0x33333333u) << 2u) | ((bits & 0xCCCCCCCCu) >> 2u);
     bits = ((bits & 0x0F0F0F0Fu) << 4u) | ((bits & 0xF0F0F0F0u) >> 4u);
     bits = ((bits & 0x00FF00FFu) << 8u) | ((bits & 0xFF00FF00u) >> 8u);
     return float(bits) * 2.3283064365386963e-10; // / 0x100000000
 }

vec2 Hammersley(uint i, uint N)
{
  return vec2(
    float(i) / float(N),
    bitfieldReverse(i));
}

vec3 ImportanceSampleGGX(vec2 Xi, vec3 N, float roughness)
{
    float a = roughness*roughness;

    float phi = 2.0 * PI * Xi.x;
    float cosTheta = sqrt((1.0 - Xi.y) / (1.0 + (a*a - 1.0) * Xi.y));
    float sinTheta = sqrt(1.0 - cosTheta*cosTheta);

    // from spherical coordinates to cartesian coordinates - halfway vector
    vec3 H;
    H.x = cos(phi) * sinTheta;
    H.y = sin(phi) * sinTheta;
    H.z = cosTheta;

    //from tangent-space H vector to world-space sample vector
    vec3 up        = abs(N.z) < 0.999 ? vec3(0.0, 0.0, 1.0) : vec3(1.0, 0.0, 0.0);
    vec3 tangent   = normalize(cross(up, N));
    vec3 bitangent = cross(N, tangent);

    vec3 sampleVec = tangent * H.x + bitangent * H.y + N * H.z;
    return normalize(sampleVec);
}

vec2 IntegrateBRDF(float NdotV, float roughness)
{
    vec3 V;
    V.x = sqrt(1.0 - NdotV*NdotV);
    V.y = 0.0;
    V.z = NdotV;

    float A = 0.0;
    float B = 0.0;

    vec3 N = vec3(0.0, 0.0, 1.0);

    const uint SAMPLE_COUNT = 1024u;
    for(uint i = 0u; i < SAMPLE_COUNT; ++i)
    {
	vec2 Xi = Hammersley(i, SAMPLE_COUNT);
	vec3 H  = ImportanceSampleGGX(Xi, N, roughness);
	vec3 L  = normalize(2.0 * dot(V, H) * H - V);

	float NdotL = max(L.z, 0.0);
	float NdotH = max(H.z, 0.0);
	float VdotH = max(dot(V, H), 0.0);

	if(NdotL > 0.0)
	{
	    float G = GeometrySmith(N, V, L, roughness);
	    float G_Vis = (G * VdotH) / (NdotH * NdotV);
	    float Fc = pow(1.0 - VdotH, 5.0);

	    A += (1.0 - Fc) * G_Vis;
	    B += Fc * G_Vis;
	}
    }
    A /= float(SAMPLE_COUNT);
    B /= float(SAMPLE_COUNT);
    return vec2(A, B);
}
