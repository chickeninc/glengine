#version 450 core

layout (triangles) in;
layout (line_strip, max_vertices = 26) out;

in VS_OUT {
    vec4 fragPos;
    vec4 color;
} gs_in[];

out vec4 gColor;
uniform mat4 projection;
uniform mat4 view;

void wireFrame(int index1, int index2)
{
    mat4  pv = projection * view;
    gl_Position = pv * gs_in[index1].fragPos;
    EmitVertex();
    gl_Position = pv * gs_in[index2].fragPos;
    EmitVertex();
    EndPrimitive();
}

void main(void)
{
    gColor = gs_in[0].color;
    wireFrame(0, 1);
    wireFrame(1, 2);
    wireFrame(2, 0);
}
