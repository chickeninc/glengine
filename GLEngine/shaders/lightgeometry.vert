#version 450 core

struct Light {
    vec3 position;
    vec3 spotDirection;

    vec3 color;

    float radius;

    bool isLocal;
    bool isSpot;
};

layout (location = 0) in vec3 position;

out VS_OUT {
    vec4 fragPos;
    vec4 color;
} vs_out;

out vec3 lightColor;
uniform Light light;

void main()
{
    vec4 v = vec4(normalize(position) * light.radius + light.position, 1.0);

    vs_out.fragPos = v;

    vs_out.color = vec4(light.color, 1.0);

    gl_Position = v;



}
