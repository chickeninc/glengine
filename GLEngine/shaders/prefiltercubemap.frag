#version 450 core
out vec4 FragColor;
in vec3 TexCoords;

const float PI = 3.14159265359;

layout (binding = 0) uniform samplerCube environmentMap;
uniform float roughness;

float bitfieldReverse(uint bits) {
     bits = (bits << 16u) | (bits >> 16u);
     bits = ((bits & 0x55555555u) << 1u) | ((bits & 0xAAAAAAAAu) >> 1u);
     bits = ((bits & 0x33333333u) << 2u) | ((bits & 0xCCCCCCCCu) >> 2u);
     bits = ((bits & 0x0F0F0F0Fu) << 4u) | ((bits & 0xF0F0F0F0u) >> 4u);
     bits = ((bits & 0x00FF00FFu) << 8u) | ((bits & 0xFF00FF00u) >> 8u);
     return float(bits) * 2.3283064365386963e-10; // / 0x100000000
 }

vec2 Hammersley(uint i, uint N)
{
  return vec2(
    float(i) / float(N),
    bitfieldReverse(i));
}

vec3 ImportanceSampleGGX(vec2 Xi, vec3 N, float roughness)
{
    float a = roughness*roughness;

    float phi = 2.0 * PI * Xi.x;
    float cosTheta = sqrt((1.0 - Xi.y) / (1.0 + (a*a - 1.0) * Xi.y));
    float sinTheta = sqrt(1.0 - cosTheta*cosTheta);

    // from spherical coordinates to cartesian coordinates - halfway vector
    vec3 H;
    H.x = cos(phi) * sinTheta;
    H.y = sin(phi) * sinTheta;
    H.z = cosTheta;

    //from tangent-space H vector to world-space sample vector
    vec3 up        = abs(N.z) < 0.999 ? vec3(0.0, 0.0, 1.0) : vec3(1.0, 0.0, 0.0);
    vec3 tangent   = normalize(cross(up, N));
    vec3 bitangent = cross(N, tangent);

    vec3 sampleVec = tangent * H.x + bitangent * H.y + N * H.z;
    return normalize(sampleVec);
}

float D_GGX(in float roughness, in float NdH)
{
    float m = roughness * roughness;
    float m2 = m * m;
    float d = (NdH * m2 - NdH) * NdH + 1.0;
    return m2 / (PI * d * d);
}

void main()
{
    vec3 N = normalize(TexCoords);
    vec3 R = N;
    vec3 V = R;

    float NoV = max(dot(N, V), 0.0);

    const uint SAMPLE_COUNT = 1024u;
    float totalWeight = 0.0;
    vec3 prefilteredColor = vec3(0.0);





    for(uint i = 0u; i < SAMPLE_COUNT; ++i)
    {
	vec2 Xi = Hammersley(i, SAMPLE_COUNT);
	vec3 H  = ImportanceSampleGGX(Xi, N, roughness);
	vec3 L  = normalize(2.0 * dot(V, H) * H - V);

	float NoH = max(dot(N, H), 0.0);
	float VoH = max(dot(V, H), 0.0);
	float D   = D_GGX ( NoH , roughness );
	float pdf = (D * NoH / (4. * VoH)) + 0.0001;

	float resolution = 512.0; // resolution of source cubemap (per face)
	float saTexel  = 4.0 * PI / (6.0 * resolution * resolution);
	float saSample = 1.0 / (float(SAMPLE_COUNT) * pdf + 0.0001);
	float mipLevel = roughness == 0.0 ? 0.0 : 0.5 * log2(saSample / saTexel);

	float NdotL = max(dot(N, L), 0.0);
	if(NdotL > 0.0)
	{
	    prefilteredColor += textureLod(environmentMap, L, mipLevel).rgb * NdotL;
	    totalWeight      += NdotL;
	}
    }
    prefilteredColor = prefilteredColor / totalWeight;

    FragColor = vec4(prefilteredColor, 1.0);
}
