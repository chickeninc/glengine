#version 450 core
in vec2 TexCoords;

out vec4 color;

uniform sampler2D screenTexture;

void main()
{
    vec3 c = texture2D(screenTexture, TexCoords).rgb;

    float gamma = 0.6f;
    int numColors = 6;

    c = pow(c, vec3(gamma, gamma, gamma));
    c = c * numColors;
    c = floor(c);
    c = c / numColors;
    c = pow(c, vec3(1.0/gamma));
    color = vec4(c,1);
    // render depth buffer
     //float depthValue = texture2D(screenTexture, TexCoords).r;
     //color = vec4(vec3(depthValue), 1.0);

}
