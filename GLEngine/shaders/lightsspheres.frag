layout (location = 0) out vec4 FragColor;
layout (location = 1) out vec4 BloomColor;

layout (binding = 0) uniform sampler2D gPosition;
layout (binding = 1) uniform sampler2D gNormal;
layout (binding = 2) uniform sampler2D gColorSpec;
layout (binding = 3) uniform samplerCube irradianceMap;
layout (binding = 4) uniform samplerCube envMap;
layout (binding = 5) uniform samplerCube shadowMapCube;

uniform Light light;

uniform vec3 cameraPos;
uniform vec2 resolution;

in vec2 TexCoords;


// array of offset direction for sampling
vec3 gridSamplingDisk[20] = vec3[]
(
   vec3(1, 1, 1), vec3(1, -1, 1), vec3(-1, -1, 1), vec3(-1, 1, 1),
   vec3(1, 1, -1), vec3(1, -1, -1), vec3(-1, -1, -1), vec3(-1, 1, -1),
   vec3(1, 1, 0), vec3(1, -1, 0), vec3(-1, -1, 0), vec3(-1, 1, 0),
   vec3(1, 0, 1), vec3(-1, 0, 1), vec3(1, 0, -1), vec3(-1, 0, -1),
   vec3(0, 1, 1), vec3(0, -1, 1), vec3(0, -1, -1), vec3(0, 1, -1)
);

float ShadowCalculation(vec3 fragPos, Light light)
{

        vec3 fragToLight = fragPos - light.position;

	float currentDepth = length(fragToLight);

	float shadow = 0.0;
	float bias = 0.0;
	int samples = 20;
	float viewDistance = length(cameraPos - fragPos);
	float far_plane = 142.0f;
	float diskRadius = (1.0 + (viewDistance / light.radius)) / 25.0;

	for(int i = 0; i < samples; i++)
	    {
	        float closestDepth = texture(shadowMapCube, fragToLight + gridSamplingDisk[i] * diskRadius).r;
		closestDepth *= light.radius;   // Undo mapping [0;1]
		if(currentDepth - bias > closestDepth)
		    shadow += 1.0;
	    }
	    shadow /= float(samples);


	return shadow;
}

vec3 Specular(float NoL, float NoH, float NoV, float VoH, float alpha, vec3 realSpecularColor)
{
    vec3 F = realSpecularColor + (1 - realSpecularColor) * pow((saturate(1 - VoH)), 5);
    float G = GeometrySchlickGGX(NoV, alpha);

    float D = D_GGX(alpha, NoH);


    vec3 DFG = D * F * G;
    return DFG / (4 * (NoL) * (NoV));
}

vec3 computeLighting(vec3 normal, vec3 diffuse, vec3 lightDir, vec3 viewDir,
                     Light light, float roughness, float metallic, float attn)
{
    vec3 H = normalize(viewDir + lightDir);


    float NoL = max(dot(normal, lightDir), 0.0);
    float NoH = max(dot(normal, H), 0.0);
    float NoV = max(dot(normal, viewDir), 0.0);
    float VoH = max(dot(viewDir, H), 0.0);
    float alpha = max(1e-4, roughness * roughness);

    float lightStrength = 10.0f;

    vec3 realAlbedo = diffuse - diffuse * metallic;
    vec3 realSpecularColor = mix(vec3(0.03), diffuse, metallic);

    vec3 albedoDiffuse = Albedo(realAlbedo);
    vec3 specular = Specular(NoL, NoH, NoV, VoH, alpha, realSpecularColor);


    vec3 finalColor = light.color * NoL * (albedoDiffuse * (1.0 - specular) + specular);

    return (attn * (lightStrength * finalColor));
}





void main(void)
{
    vec2 uv = gl_FragCoord.xy / resolution;
    vec4 FragPosition = texture2D(gPosition, uv);
    vec3 FragPos = FragPosition.rgb;

    vec4 ND =  texture2D(gNormal, uv);
    vec3 Normal = ND.xyz;
    float Depth = FragPosition.a;

    vec4 colorMet = texture2D(gColorSpec, uv);
    vec3 Color = colorMet.rgb;

    float lightDistance = length(light.position - FragPos);
    vec3 lightDir = normalize(light.position - FragPos);

    FragColor = vec4(0.0, 0.0, 1.0, 1.0);


    vec3 viewDir = normalize(cameraPos - FragPos);
    vec3 halfVector = normalize(lightDir + viewDir);

    float metallic = colorMet.a;

    float roughness = ND.a;

    roughness *= roughness;
    metallic *= metallic;

    vec3 Lo = vec3(0.0);
    vec3 F0 = vec3(0.04);
    F0 = mix(F0, Color, metallic);
    float f90 = clamp(50 * dot(F0, vec3(0.33)), 0, 1);
    vec3 F = fresnelSchlickRoughness(dot(Normal, viewDir), F0, roughness);
    //vec3 F = fresnelRoughness(F0, f90, roughness);
    //vec3 F = fresnel_factor(F0, roughness);
    vec3 kS = F;
    vec3 kD = vec3(1.0) - kS;

    kD *= 1.0 - metallic;


    float NDF = Distribution_GGX(Normal, halfVector, roughness);
    //float G   = GeometrySmith(Normal, viewDir, lightDir, roughness);
    float G = GeometrySchlickGGX(max(dot(Normal, viewDir), 0.0), roughness);
    vec3 nominator    = NDF * G * F;
    float denominator = 4 * max(dot(viewDir, Normal), 0.0) * max(dot(lightDir, Normal), 0.0) + 0.001;
    vec3 brdf         = nominator / denominator;

    float attenuation = 1.0;

    attenuation = pow(clamp(1.0 - pow(lightDistance/light.radius, 4.0), 0.0, 1.0), 2.0) / (pow(lightDistance, 2.0) + 1.0);

    float lightStrength = 1.0f;
    lightStrength *= light.strength;


    vec3 radiance = light.color * attenuation * lightStrength;

    vec3 irradiance = texture(irradianceMap, Normal).rgb;
    vec3 diffuse = irradiance * Color;

    float shadow = ShadowCalculation(FragPos, light);
    shadow = 1 - shadow;

    float NdotL = max(dot(Normal, lightDir), 0.0);

    Lo = ((kD * diffuse / PI) + brdf) * radiance * NdotL;

    FragColor = vec4(Lo * (shadow), 1.0);
    BloomColor = vec4(Lo * (shadow), 1.0);

}
