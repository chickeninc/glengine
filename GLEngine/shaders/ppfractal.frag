#version 450 core
in vec2 TexCoords;
out vec4 color;

uniform sampler2D screenTexture;
uniform vec2 resolution;
uniform float time;
uniform float zoom;


const float AA = 2;
vec2 complex_square( vec2 v ) {
        return vec2(
	        v.x * v.x - v.y * v.y,
	        v.x * v.y * 2.0
	);
}


void main()
{
    float n = 0.0f;
    vec2 uv = gl_FragCoord.xy/resolution.y-.5;

    double offX = -1.768573656315270993;
    double offY =  0.000964296851358280;

    vec2 c = vec2(offX, offY) + (uv) * zoom;
    vec2 z = c * n;

    for( int i=0; i<2048; i++ )
    {

	z = complex_square(z) + c;
	//z = complex_square(z) + c - z;
	//z = complex_square(complex_square(z)) + c;
	//z = complex_square(z) + complex_square(complex_square(c));
	//z = complex_square(c) - complex_square(z);

	if( dot(z,z)>1e4) break;

	n++;
    }

    vec4 v = vec4((sin(-time)/2), (cos(time)/2), (sin(time)/2), 1.0f);
    vec4 col = .5 + .5*cos( v+ .05*(n - log2(log2(dot(z,z)))) );
    color = vec4(col.x, col.y, col.z, 1.0f);

}
