#version 450 core


struct Light {
    vec3 position;
    vec3 spotDirection;
    vec3 halfVector;

    vec3 ambient;
    vec3 color;

    bool isLocal;
    bool isSpot;

    float radius;

    float spotCosCutOff;
    float spotExponent;

    float strength;
};


layout (location = 0) in vec3 position;
layout (location = 1) in vec3 normal;
layout (location = 2) in vec2 texCoord;


uniform Light light;
uniform mat4 projection;
uniform mat4 view;


out vec2 TexCoords;

void main(void)
{

    vec3 fragPos = (position * light.radius) + light.position;

    vec4 pos = projection * vec4(fragPos, 1.0);


    TexCoords = pos.xy;
    gl_Position = projection * view * vec4(fragPos, 1.0);
}
