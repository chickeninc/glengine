#version 450 core

const float PI = 3.14159265359;

layout (location = 0) out vec4 FragColor;

layout (binding = 0) uniform sampler2D gPosition;
layout (binding = 1) uniform sampler2D gNormal;
layout (binding = 2) uniform sampler2D gColorSpec;
layout (binding = 3) uniform samplerCube irradianceMap;
layout (binding = 4) uniform samplerCube envMap;
layout (binding = 5) uniform sampler2D shadowMap;

struct Light {
    vec3 position;
    vec3 spotDirection;
    vec3 halfVector;

    vec3 ambient;
    vec3 color;

    bool isLocal;
    bool isSpot;

    float radius;

    float spotCosCutOff;
    float spotExponent;
};


uniform Light light;

uniform mat4 lightSpaceMatrix;

uniform vec3 cameraPos;
uniform vec2 resolution;

in vec2 TexCoords;

float ShadowCalculation(vec4 fragPosLightSpace, vec3 normal, vec3 lightDir)
{
/*
    float cosAlpha =  clamp(dot(normal, lightDir), 0.0f, 1.0f);
    float offsetScaleN = sqrt(1.0f - cosAlpha * cosAlpha);
    float offsetScaleL = offsetScaleN / cosAlpha;
    vec2 texelSize = vec2(offsetScaleN, min(2.0f, offsetScaleL));
*/




    // perspective divide
    vec3 projCoords = fragPosLightSpace.xyz / fragPosLightSpace.w;
    // Transform to [0,1] range
    projCoords = projCoords * 0.5 + 0.5;

    float closestDepth = texture2D(shadowMap, projCoords.xy).r;

    float currentDepth = projCoords.z;

    //float bias = max(0.05 * (1.0 - dot(normal, lightDir)), 0.005);
    float bias = 0.00;
    float shadow = 0.0;
        vec2 texelSize = 1.0 / vec2(1024, 1024);
	for(int x = -1; x <= 1; ++x)
	{
	    for(int y = -1; y <= 1; ++y)
	    {
		float pcfDepth = texture2D(shadowMap, projCoords.xy + vec2(x, y) * texelSize).r;
		shadow += currentDepth - bias > pcfDepth  ? 1.0 : 0.0;
	    }
	}
	shadow /= 9.0;

	// Keep the shadow at 0.0 when outside the far_plane region of the light's frustum.
	if(projCoords.z > 1.0)
	    shadow = 0.0;


	return shadow;
}

vec3 fresnelRoughness(in vec3 f0, in float f90, in float roughness) {
    return f0 + (f90-f0) * pow(1-roughness, 5.0f);
}

vec3 fresnelSchlickRoughness(float cosTheta, vec3 F0, float roughness)
{
    return F0 + (max(vec3(1.0 - roughness), F0) - F0) * pow(1.0 - cosTheta, 5.0);
}

vec3 fresnel_factor(in vec3 f0, in float product)
{
    return mix(f0, vec3(1.0), pow(1.01 - product, 5.0));
}

vec3 EnvBRDFApprox( vec3 SpecularColor, float Roughness, float NoV ) {
    const vec4 c0 = vec4(-1.0, -0.0275, -0.572, 0.022 );
    const vec4 c1 = vec4( 1.0, 0.0425, 1.04, -0.04 );
    vec4 r = Roughness * c0 + c1;
    float a004 = min( r.x * r.x, exp2( -9.28 * NoV ) ) * r.x + r.y;
    vec2 AB = vec2( -1.04, 1.04 ) * a004 + r.zw;
    return SpecularColor * AB.x + AB.y;
}

float bitfieldReverse(uint bits) {
     bits = (bits << 16u) | (bits >> 16u);
     bits = ((bits & 0x55555555u) << 1u) | ((bits & 0xAAAAAAAAu) >> 1u);
     bits = ((bits & 0x33333333u) << 2u) | ((bits & 0xCCCCCCCCu) >> 2u);
     bits = ((bits & 0x0F0F0F0Fu) << 4u) | ((bits & 0xF0F0F0F0u) >> 4u);
     bits = ((bits & 0x00FF00FFu) << 8u) | ((bits & 0xFF00FF00u) >> 8u);
     return float(bits) * 2.3283064365386963e-10; // / 0x100000000
 }

vec2 Hammersley(uint i, uint N)
{
  return vec2(
    float(i) / float(N),
    float(bitfieldReverse(i)) * 2.3283064365386963e-10);
}

vec3 getDiffuseReflection(vec3 normal)
{
   return texture(irradianceMap, vec3(normal.x, normal.y, normal.z)).rgb;
}

vec3 getSpecularReflection(vec3 reflectDir)
{
   return texture(envMap, reflectDir).rgb;
}

float GeometrySchlickGGX(float NdotV, float roughness)
{
    float r = (roughness + 1.0);
    float k = (r*r) / 8.0;

    float nom   = NdotV;
    float denom = NdotV * (1.0 - k) + k;

    return nom / denom;
}


float saturate(in float value)
{
    return clamp(value, 0.0, 1.0);
}

float D_GGX(float roughness, float NdotH )
{
        float alpha2 = roughness * roughness;
	float den = NdotH * NdotH ;
	den *= (alpha2 - 1.0f);
	den += 1.0f;
	return (alpha2)/( PI * den * den ) ;
}

void main(void)
{
    vec4 colorMet = texture2D(gColorSpec, TexCoords);
    vec3 Color = colorMet.rgb;

    vec4 FragPosition = texture2D(gPosition, TexCoords);
    vec3 FragPos = FragPosition.rgb;

    float ao = FragPosition.a;

    vec3 viewDir = normalize(cameraPos - FragPos);

    vec4 ND =  texture2D(gNormal, TexCoords);
    vec3 Normal = ND.xyz;


    float metallic = colorMet.a;
    metallic *= metallic;

    float roughness = ND.a;
    roughness *= roughness;


    float NoV = max(dot(Normal, viewDir), 0.0);


    vec3 F0 = vec3(0.04);
    F0 = mix(F0, Color, metallic);
    float f90 = clamp(50 * dot(F0, vec3(0.33)), 0, 1);

    vec3 F = fresnelSchlickRoughness(NoV, F0, roughness);
    //vec3 F = fresnelRoughness(F0, f90, roughness);
    //vec3 F = fresnel_factor(F0, roughness);
    float G = GeometrySchlickGGX(NoV, roughness);


    vec3 realSpecular = mix(vec3(0.03), Color, metallic);
    vec3 reflectance = EnvBRDFApprox(realSpecular, roughness, NoV);
    vec3 reflectDir = reflect(-viewDir, Normal);
    vec3 reflectionColor = ApproximateSpecularIBL(realSpecular, reflectDir, NoV, roughness);
    vec3 irradianceColor = getDiffuseReflection(Normal);

    vec3 kS = F;
    vec3 kD = vec3(1.0) - kS;

    vec3 irradiance = texture(irradianceMap, Normal).rgb;
    vec3 realAlbedo = Color - Color * metallic;
    vec3 diffuse = irradiance * realAlbedo;


    vec3 reflectColor = kS * reflectance * reflectionColor;
    vec3 diffuseColor = kD * irradianceColor / PI;

    vec3 result;
    result = mix(realAlbedo, diffuseColor * realAlbedo, kD);
    result = mix(result, (reflectColor) + result, kS);


    vec3 lightDir = normalize(light.position);
    float shadow = ShadowCalculation(lightSpaceMatrix * vec4(FragPos, 1.0), Normal, lightDir);
    result *= (1 - shadow);

    vec3 ambient = clamp(vec3(0.03) * (Color), 0.0, 1.0);
    FragColor = vec4(vec3(result), 1.0);
}
