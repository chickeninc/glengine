#version 450 core

layout (location = 0) in vec3 position;
layout (location = 1) in vec3 normal;
layout (location = 2) in vec2 texCoord;
layout (location = 3) in vec3 tangent;
layout (location = 4) in vec3 bitangent;

uniform vec3 cameraPos;
uniform mat4 projection;
uniform mat4 view;
uniform mat4 model;

layout (location = 0) out vec3 FragPos;
layout (location = 1) out vec3 fNormal;
layout (location = 2) out vec2 uv;
layout (location = 3) out vec3 fTangent;
layout (location = 4) out vec3 fBitangent;

void main(void)
{
    mat3 normalMatrix = transpose(inverse(mat3(model)));

    vec3 T = normalize(normalMatrix * tangent);
    vec3 N = normalize(normalMatrix * normal);
    T = normalize(T - dot(T, N) * N);
    vec3 B = cross(N, T);

    mat3 TBN = transpose(mat3(T, B, N));
    fTangent = T;
    fBitangent = B;
    fNormal = N;

    vec4 v = model * vec4(position, 1.0);
    FragPos = vec3(v);
    gl_Position = projection * view * v;

    uv = texCoord;
//    uv.x = 1 - uv.x;
//    uv.y = 1 - uv.y;

}
