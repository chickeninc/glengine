#version 450 core

struct Material {
    float roughness;
    float metallic;
};

layout (location = 0) out vec4 gPosition;
layout (location = 1) out vec4 gNormal;
layout (location = 2) out vec4 gColorSpec;

layout (location = 0) in vec3 FragPos;
layout (location = 1) in vec3 fNormal;
layout (location = 2) in vec2 uv;
layout (location = 3) in vec3 fTangent;
layout (location = 4) in vec3 fBitangent;


layout (binding = 0) uniform sampler2D albedoMap;
layout (binding = 1) uniform sampler2D metallicMap;
layout (binding = 2) uniform sampler2D normalMap;
layout (binding = 3) uniform sampler2D heightMap;
layout (binding = 4) uniform sampler2D roughnessMap;
layout (binding = 5) uniform sampler2D aoMap;

uniform bool gammaCorrection;
uniform Material material;

uniform vec3 cameraPos;

vec2 ParallaxMapping(vec2 texCoords, vec3 viewDir)
{
    float height_scale = 0.1;

    const float minLayers = 8;
    const float maxLayers = 32;
    float numLayers = mix(maxLayers, minLayers, abs(dot(vec3(0.0, 0.0, 1.0), viewDir)));

    float layerDepth = 1.0 / numLayers;
    float currentLayerDepth = 0.0;
    vec2 P = viewDir.xy * height_scale;
    vec2 deltaTexCoords = P / numLayers;
    vec2  currentTexCoords     = texCoords;
    float currentDepthMapValue = texture2D(heightMap, currentTexCoords).r;

    while(currentLayerDepth < currentDepthMapValue)
    {

	currentTexCoords -= deltaTexCoords;

	currentDepthMapValue = texture2D(heightMap, currentTexCoords).r;

	currentLayerDepth += layerDepth;
    }

    vec2 prevTexCoords = currentTexCoords + deltaTexCoords;


    float afterDepth  = currentDepthMapValue - currentLayerDepth;
    float beforeDepth = texture2D(heightMap, prevTexCoords).r - currentLayerDepth + layerDepth;


    float weight = afterDepth / (afterDepth - beforeDepth);
    vec2 finalTexCoords = prevTexCoords * weight + currentTexCoords * (1.0 - weight);

    return finalTexCoords;
}


void main()
{

    vec3 viewDir = normalize(cameraPos - FragPos);
    //vec2 texCoords = ParallaxMapping(uv, viewDir);
    vec2 texCoords = uv;

    float ao = texture2D(aoMap, texCoords).r;
    gPosition.xyz = FragPos;
    gPosition.a = ao;
//    gPosition.a = gl_FragCoord.w;



    vec3 colorTexture = texture2D(albedoMap, texCoords).rgb;
    float metallic = texture2D(metallicMap, texCoords).r;
    vec3 normalTexture = (texture2D(normalMap, texCoords).rgb * 2.0 -1.0);

    vec3 nMap = (normalTexture.x * fTangent) + (normalTexture.y * fBitangent) +
            (normalTexture.z * fNormal);


    float height = texture(heightMap, texCoords).r;

    float roughness = texture2D(roughnessMap, texCoords).r;
    gNormal.xyz = normalize(nMap);
    gNormal.a = roughness * material.roughness;

    float gamma = 2.2;
    colorTexture = pow(colorTexture, vec3(gamma));
    gColorSpec.rgb = colorTexture;
    //gColorSpec.rgb = vec3(height);

    gColorSpec.a = metallic * material.metallic;

}
