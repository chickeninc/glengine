#version 450 core
in vec2 TexCoords;

layout (binding = 0) uniform sampler2D screenTexture;

layout (location = 0) out vec4 BrightColor;

void main(void)
{
    vec4 color = texture2D(screenTexture, TexCoords);

    float brightness = dot(color.rgb, vec3(0.2126, 0.7152, 0.0722));
    if(brightness > 1.0)
	BrightColor = vec4(color.rgb, 1.0);
    else discard;

}
