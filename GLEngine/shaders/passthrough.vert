#version 450 core

layout (location = 0) in vec3 position;
layout (location = 1) in vec3 normal;
layout (location = 2) in vec2 texCoord;

out vec3 vnormal;
out vec3 fragPos;
out vec2 uv;


void main()
{
    vec4 v = vec4(position, 1.0);

    fragPos = position;
    gl_Position = v;
    uv = texCoord;
    vnormal = normalize(normal);
}
