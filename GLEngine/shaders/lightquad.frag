layout (location = 0) out vec4 FragColor;
layout (location = 1) out vec4 BloomColor;

layout (binding = 0) uniform sampler2D gPosition;
layout (binding = 1) uniform sampler2D gNormal;
layout (binding = 2) uniform sampler2D gColorSpec;
layout (binding = 3) uniform samplerCube irradianceMap;
layout (binding = 4) uniform samplerCube envMap;
layout (binding = 5) uniform sampler2D shadowMap;
layout (binding = 6) uniform samplerCube prefilteredMap;
layout (binding = 7) uniform sampler2D brdfLUT;


uniform Light light;
uniform mat4 lightSpaceMatrix;

uniform vec3 cameraPos;
uniform vec2 resolution;
uniform vec2 halfSizeNearPlane;
uniform bool normalsOnly = false;
uniform bool scaterredOnly = false;
uniform bool reflectedOnly = false;
uniform bool positionsOnly = false;
uniform bool ambientOnly = false;
uniform bool colorOnly = false;
uniform bool shadowsOnly = false;
uniform bool depthOnly = false;

in vec2 TexCoords;

uniform mat4 projection;
uniform mat4 view;


vec3 environmentBRDF(float g, float NdotV, vec3 F0)
{
    vec4 t = vec4(1.0 / 0.96, 0.475, (0.0275 - 0.25 * 0.04) / 0.96, 0.25);
    t *= vec4(g,g,g,g);
    t += vec4(0.0, 0.0, (0.015 - 0.75 * 0.04) / 0.96, 0.75);
    float a0 = t.x * min(t.y, exp2( -9.28 * NdotV)) + t.z;
    float a1 = t.w;

    return saturate(a0 + F0 * (a1 - a0));
}

vec3 EnvBRDFApprox( vec3 SpecularColor, float Roughness, float NoV ) {
    const vec4 c0 = vec4(-1.0, -0.0275, -0.572, 0.022 );
    const vec4 c1 = vec4( 1.0, 0.0425, 1.04, -0.04 );
    vec4 r = Roughness * c0 + c1;
    float a004 = min( r.x * r.x, exp2( -9.28 * NoV ) ) * r.x + r.y;
    vec2 AB = vec2( -1.04, 1.04 ) * a004 + r.zw;
    return SpecularColor * AB.x + AB.y;
}

vec3 getPrefilteredReflection(vec3 eyeDirWorld, vec3 normalWorld, float roughness) {
    float maxMipMapLevel = 10.0;
    vec3 reflectionWorld = reflect(-eyeDirWorld, normalWorld);

    vec3 R = reflectionWorld;
    float lod = roughness * maxMipMapLevel;
    float upLod = floor(lod);
    float downLod = ceil(lod);
    vec3 a = textureLod(envMap, R, upLod).rgb;
    vec3 b = textureLod(envMap, R, downLod).rgb;

    return mix(a, b, lod - upLod);
}

vec3 ApproximateSpecularIBL(vec3 specularAlbedo, vec3 reflectDir, float NoV, float roughness)
{
    float mipMapIndex = 10.0 * roughness;

    vec3 prefilteredColor = textureLod(envMap, reflectDir, mipMapIndex).rgb;
    vec3 environmentBRDF;

    return prefilteredColor;
}

vec3 Specular(float NoL, float NoH, float NoV, float VoH, float alpha, vec3 realSpecularColor)
{
    vec3 F = realSpecularColor + (1 - realSpecularColor) * pow((saturate(1 - VoH)), 5);
    float G = GeometrySchlickGGX(NoV, alpha);

    float D = D_GGX(alpha, NoH);


    vec3 DFG = D * F * G;
    return DFG / (4 * (NoL) * (NoV));
}

float ShadowCalculation(vec4 fragPosLightSpace, vec3 normal, vec3 lightDir)
{

    vec2 moments;
    moments.x = fragPosLightSpace.w;
    float dx = dFdx(fragPosLightSpace.w);
    float dy = dFdy(fragPosLightSpace.w);

    moments.y = fragPosLightSpace.w * fragPosLightSpace.w + 0.25*(dx*dx + dy*dy);

    // perspective divide
    vec3 projCoords = fragPosLightSpace.xyz / fragPosLightSpace.w;
    // Transform to [0,1] range
    projCoords = projCoords * 0.5 + 0.5;

    float closestDepth = texture2D(shadowMap, projCoords.xy).r;

    float currentDepth = projCoords.z;

    //float bias = max(0.05 * (1.0 - dot(normal, lightDir)), 0.005);
    float bias = 0.00;
    float shadow = 0.0;
        vec2 texelSize = 1.0 / vec2(1024, 1024);
	for(int x = -1; x <= 1; ++x)
	{
	    for(int y = -1; y <= 1; ++y)
	    {
		float pcfDepth = texture2D(shadowMap, projCoords.xy + vec2(x, y) * texelSize).r;
		shadow += currentDepth - bias > pcfDepth  ? 1.0 : 0.0;
	    }
	}
	shadow /= 9.0;

	// Keep the shadow at 0.0 when outside the far_plane region of the light's frustum.
	if(projCoords.z > 1.0)
	    shadow = 0.0;

	return shadow;
}

vec3 getColor(vec3 normal, vec3 reflectDir, vec3 albedo, vec3 kS)
{
    vec3 kD = vec3(1.0) - kS;
    vec3 diffuseColor = texture(irradianceMap, normal).rgb;
    vec3 specularColor = texture(envMap, reflectDir).rgb;

    vec3 color = mix(albedo, diffuseColor * albedo, kD);
    color = mix(color, specularColor + color, kS);

    return color;
}

vec3 getDiffuseReflection(vec3 normal)
{
   return texture(irradianceMap, normal).rgb;
}

vec3 getSpecularReflection(vec3 reflectDir)
{
   return texture(envMap, reflectDir).rgb;
}

void main(void)
{
    vec2 uv = TexCoords;
    vec4 FragPosition = texture2D(gPosition, uv);
    vec3 FragPos = FragPosition.rgb;

    vec4 ND =  texture2D(gNormal, uv);
    vec3 Normal = ND.xyz;
    float ao = FragPosition.a;

    vec4 colorMet = texture2D(gColorSpec, uv);
    vec3 Color = colorMet.rgb;

    vec3 lightDir = normalize(light.position);

    FragColor = vec4(0.0, 0.0, 1.0, 1.0);

    vec3 viewDir = normalize(cameraPos - FragPos);
    vec3 halfDir = normalize(lightDir + viewDir);

    float metallic = colorMet.a;

    float roughness = ND.a;

    //roughness *= roughness;
    //metallic *= metallic;


    float NoL = max(dot(Normal, lightDir), 0.0);
    float NoV = max(dot(Normal, viewDir), 0.0);
    float NoH = max(dot(Normal, halfDir), 0.0);
    float VoH = max(dot(viewDir, halfDir), 0.0);


    vec3 Lo = vec3(0.0);
    vec3 F0 = vec3(0.04);
    F0 = mix(F0, Color, metallic);
    float f90 = clamp(50 * dot(F0, vec3(0.33)), 0, 1);
    vec3 F = fresnelSchlick(VoH, F0);

    //vec3 F = fresnelRoughness(F0, f90, roughness);
    //vec3 F = fresnel_factor(F0, roughness);
    vec3 kS = F;
    vec3 kD = vec3(1.0) - kS;

    kD *= 1.0 - metallic;

    float NDF = Distribution_GGX(Normal, halfDir, roughness);
//    float NDF = V_SmithGGX(NoL, NoV, roughness);
    //float G   = GeometrySmith(Normal, viewDir, lightDir, roughness);
    float G = GeometrySchlickGGX(NoV, roughness);
    vec3 nominator    = NDF * G * F;
    float denominator = 4 * NoV * NoL + 0.001;
    vec3 brdf         = nominator / denominator;

    vec3 irradiance = texture(irradianceMap, Normal).rgb;

    vec3 reflectDir = reflect(-viewDir, Normal);

    vec3 realAlbedo = Color - Color * metallic;

    vec3 diffuse = irradiance * Color;
    vec3 realSpecular = mix(vec3(0.03), Color, metallic);
    vec3 diffuseColor = realAlbedo;
    vec3 radiance = light.color * light.strength;

    F = fresnelSchlickRoughness(NoV, F0, roughness);
    const float MAX_REFLECTION_LOD = 4.0;
    vec3 prefilteredColor = textureLod(prefilteredMap, reflectDir,  roughness * MAX_REFLECTION_LOD).rgb;
    vec2 envBRDF  = texture(brdfLUT, vec2(NoV, roughness)).rg;
    vec3 specular = prefilteredColor * (F * envBRDF.x + envBRDF.y);

    float shadow = ShadowCalculation(lightSpaceMatrix * vec4(FragPos, 1.0), Normal, lightDir);

    vec3 ambient  =  (kD * diffuse + specular);
    ambient *=  ao;

    vec3 result;
    Lo = ((kD * diffuse / PI) + brdf) * radiance * NoL;


    //result = diffuseColor * irradianceColor + reflectionColor * reflectance;
    result = Lo + ambient;
    //result = mix(realAlbedo, irradianceColor * realAlbedo, kD);
    //result = mix(result, reflectionColor + result, kS);
    result *= (1 - shadow);

    FragColor = vec4((result), 1.0);

    BloomColor = vec4(Lo, 1.0);

    if(normalsOnly) FragColor = vec4(Normal, 1.0);
    if(positionsOnly) FragColor = vec4(FragPos, 1.0);
    if(colorOnly) FragColor = vec4(Color, 1.0);
    if(shadowsOnly) FragColor = vec4(vec3(1-shadow), 1.0);
    if(ambientOnly) FragColor = vec4(ambient, 1.0);
    if(depthOnly) FragColor = vec4(vec3(ao), 1.0);
    if(reflectedOnly) FragColor = vec4(specular, 1.0);
    if(scaterredOnly) FragColor = vec4(diffuse, 1.0);
}
