#version 450 core

in vec4 gColor;

out vec4 color;
void main(void)
{
    color = gColor;
}
