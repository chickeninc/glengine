#version 450 core

const float PI = 3.14159265359;


struct Light {
    vec3 position;
    vec3 spotDirection;
    vec3 halfVector;

    vec3 ambient;
    vec3 color;

    bool isLocal;
    bool isSpot;

    float constantAttenuation;
    float linearAttenuation;
    float quadraticAttenuation;

    float spotCosCutOff;
    float spotExponent;
};

const int MAX_LIGHTS = 5;

layout (location = 0) out vec4 FragColor;
layout (location = 1) out vec4 BrightColor;
layout (location = 2) out vec4 ScatterColor;

in vec2 TexCoords;

layout (binding = 0) uniform sampler2D gPosition;
layout (binding = 1) uniform sampler2D gNormal;
layout (binding = 2) uniform sampler2D gColorSpec;
layout (binding = 3) uniform sampler2D shadowMap;
layout (binding = 4) uniform samplerCube shadowMapCube;
layout (binding = 5) uniform sampler2D ssao;

uniform mat4 lightSpaceMatrix;
uniform mat4 shadowMatrix;

uniform bool gammaCorrection;
uniform bool normalsOnly = false;
uniform bool scaterredOnly = false;
uniform bool reflectedOnly = false;
uniform bool positionsOnly = false;
uniform bool colorOnly = false;
uniform bool shadowsOnly = false;
uniform bool depthOnly = false;
uniform bool bloomOnly = false;

uniform vec3 cameraPos;
uniform vec3 cameraDir;
uniform Light[MAX_LIGHTS] lights;


float ShadowCalculation(vec4 fragPosLightSpace, vec3 normal, vec3 lightDir)
{
/*
    float cosAlpha =  clamp(dot(normal, lightDir), 0.0f, 1.0f);
    float offsetScaleN = sqrt(1.0f - cosAlpha * cosAlpha);
    float offsetScaleL = offsetScaleN / cosAlpha;
    vec2 texelSize = vec2(offsetScaleN, min(2.0f, offsetScaleL));
*/


    // perspective divide
    vec3 projCoords = fragPosLightSpace.xyz / fragPosLightSpace.w;
    // Transform to [0,1] range
    projCoords = projCoords * 0.5 + 0.5;

    float closestDepth = texture2D(shadowMap, projCoords.xy).r;

    float currentDepth = projCoords.z;

    //float bias = max(0.05 * (1.0 - dot(normal, lightDir)), 0.005);
    float bias = 0.00;
    float shadow = 0.0;
        vec2 texelSize = 1.0 / vec2(2048, 2048);
	for(int x = -1; x <= 1; ++x)
	{
	    for(int y = -1; y <= 1; ++y)
	    {
		float pcfDepth = texture2D(shadowMap, projCoords.xy + vec2(x, y) * texelSize).r;
		shadow += currentDepth - bias > pcfDepth  ? 1.0 : 0.0;
	    }
	}
	shadow /= 9.0;

	// Keep the shadow at 0.0 when outside the far_plane region of the light's frustum.
	if(projCoords.z > 1.0)
	    shadow = 0.0;

	return shadow;
}


// array of offset direction for sampling
vec3 gridSamplingDisk[20] = vec3[]
(
   vec3(1, 1, 1), vec3(1, -1, 1), vec3(-1, -1, 1), vec3(-1, 1, 1),
   vec3(1, 1, -1), vec3(1, -1, -1), vec3(-1, -1, -1), vec3(-1, 1, -1),
   vec3(1, 1, 0), vec3(1, -1, 0), vec3(-1, -1, 0), vec3(-1, 1, 0),
   vec3(1, 0, 1), vec3(-1, 0, 1), vec3(1, 0, -1), vec3(-1, 0, -1),
   vec3(0, 1, 1), vec3(0, -1, 1), vec3(0, -1, -1), vec3(0, 1, -1)
);

float ShadowCalculation(vec3 fragPos, Light light)
{

        vec3 fragToLight = fragPos - light.position;

	float currentDepth = length(fragToLight);

	float shadow = 0.0;
	float bias = 0.0;
	int samples = 20;
	float viewDistance = length(cameraPos - fragPos);
	float far_plane = 142.0f;
	float diskRadius = (1.0 + (viewDistance / far_plane)) / 25.0;

	for(int i = 0; i < samples; ++i)
	    {
	        float closestDepth = texture(shadowMapCube, fragToLight + gridSamplingDisk[i] * diskRadius).r;
		closestDepth *= far_plane;   // Undo mapping [0;1]
		if(currentDepth - bias > closestDepth)
		    shadow += 1.0;
	    }
	    shadow /= float(samples);


	return shadow;
}

float DistributionGGX(vec3 N, vec3 H, float roughness)
{
    float a      = roughness*roughness;
    float a2     = a*a;
    float NdotH  = max(dot(N, H), 0.0);
    float NdotH2 = NdotH*NdotH;

    float nom   = a2;
    float denom = (NdotH2 * (a2 - 1.0) + 1.0);
    denom = PI * denom * denom;

    return nom / denom;
}

float GeometrySchlickGGX(float NdotV, float roughness)
{
    float r = (roughness + 1.0);
    float k = (r*r) / 8.0;

    float nom   = NdotV;
    float denom = NdotV * (1.0 - k) + k;

    return nom / denom;
}
float GeometrySmith(vec3 N, vec3 V, vec3 L, float roughness)
{
    float NdotV = max(dot(N, V), 0.0);
    float NdotL = max(dot(N, L), 0.0);
    float ggx2  = GeometrySchlickGGX(NdotV, roughness);
    float ggx1  = GeometrySchlickGGX(NdotL, roughness);

    return ggx1 * ggx2;
}

vec3 fresnelSchlick(float cosTheta, vec3 F0)
{
    return F0 + (1.0 - F0) * pow(1.0 - cosTheta, 5.0);
}

vec3 fresnelSchlickRoughness(float cosTheta, vec3 F0, float roughness)
{
    return F0 + (max(vec3(1.0 - roughness), F0) - F0) * pow(1.0 - cosTheta, 5.0);
}

void main(void)
{


    vec3 FragPos = texture2D(gPosition, TexCoords).rgb;
    vec4 ND =  texture2D(gNormal, TexCoords);
    vec3 Normal = ND.xyz;
    float Depth = ND.a;
    float AmbientOcclusion = texture2D(ssao, TexCoords).r;

    vec3 Color = texture2D(gColorSpec, TexCoords).rgb;
    float Specular = texture2D(gColorSpec, TexCoords).a;

    vec3 viewDir = normalize(cameraPos - FragPos);


    vec3 scatteredLight = vec3(0.0);
    vec3 reflectedLight = vec3(0.0);
    float shadow = 0.0;
    for(int light =  0; light < MAX_LIGHTS; ++light)
    {
	vec3 halfVector;
	vec3 lightDirection = lights[light].position;
	float attenuation = 1.0;

	if(lights[light].isLocal)
	{
	    lightDirection = lights[light].position - FragPos;
	    float lightDistance = length(lightDirection);
	    lightDirection = lightDirection / lightDistance;

	    attenuation = 1.0 / (lights[light].constantAttenuation
	                             + lights[light].linearAttenuation * lightDistance
	                             + lights[light].quadraticAttenuation * lightDistance
	                             * lightDistance);

	    if(lights[light].isSpot)
	    {

		float spotCos = dot(lightDirection, -lights[light].spotDirection);
		if(spotCos < lights[light].spotCosCutOff)
		    attenuation = 0.0f;
		else
		    attenuation *= pow(spotCos, lights[light].spotExponent);
	    }
	    halfVector = normalize(lightDirection + viewDir);
	}
	else
	{
	    lightDirection = lights[light].position;
	    halfVector = normalize(lightDirection + viewDir);
	    if(lights[light].position.y > 0)
		shadow = ShadowCalculation(lightSpaceMatrix * vec4(FragPos, 1.0), Normal, lightDirection);

	}

	float diffuse = max(0.0, dot(Normal, lightDirection));

	float specular = max(0.0, dot(Normal, halfVector));

	specular = pow(specular, 16) * Specular;


	scatteredLight += (lights[light].ambient) * attenuation +
	                      lights[light].color * diffuse * attenuation * (1- shadow);

	if(normalsOnly)
	{
	    scatteredLight += (lights[light].ambient) * attenuation * vec3(AmbientOcclusion)+
	                          lights[light].color * diffuse * attenuation * (1- shadow);

	}
	reflectedLight += lights[light].color * specular * attenuation * (1 - shadow);



    }

    vec3 rgb = Color.rgb * scatteredLight + reflectedLight;
    // if it's on the skybox render it black for later blend
    if(Depth == 1)
    {
	rgb = vec3(0.0,0.0,0.0);
    }

    if(normalsOnly)
    {
	//rgb = Normal;
    }

    if(scaterredOnly)
    {
	rgb = scatteredLight;
    }

    if(reflectedOnly)
    {
	rgb = reflectedLight;
    }

    if(positionsOnly)
    {
	rgb = FragPos;
    }

    if(colorOnly)
    {
	rgb = Color;
    }

    if(shadowsOnly)
    {
	rgb = vec3(1 - shadow);
    }

    if(depthOnly)
    {
	rgb = vec3(Depth);
    }

    FragColor = vec4(rgb, 1.0);

    float brightness = dot(FragColor.rgb, vec3(0.2126, 0.7152, 0.0722));
    if(brightness > 1.0 && Depth != 1.0)
	BrightColor = vec4(FragColor.rgb, 1.0);
    else
	BrightColor = vec4(0.0, 0.0, 0.0, 1.0);


    if(Depth != 1.0)
    {
	ScatterColor = vec4(0.0, 0.0, 0.0, 1.0);
	if(dot(normalize(cameraDir), normalize(lights[0].position)) < 0.5f)
	    ScatterColor = vec4(1.0, 0.0, 0.0, 1.0);
    }



}

