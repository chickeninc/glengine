#version 450 core
in vec2 TexCoords;
out vec4 color;

uniform sampler2D screenTexture;

const float offset = 1.0 / 300;

void main()
{
    vec2 offsets[9] = vec2[](
            vec2(-offset, offset),  // top-left
            vec2(0.0f,    offset),  // top-center
            vec2(offset,  offset),  // top-right
            vec2(-offset, 0.0f),    // center-left
            vec2(0.0f,    0.0f),    // center-center
            vec2(offset,  0.0f),    // center-right
            vec2(-offset, -offset), // bottom-left
            vec2(0.0f,    -offset), // bottom-center
            vec2(offset,  -offset)  // bottom-right
            );

    vec2 test[25] = vec2[](
            vec2(-2*offset, 2*offset),
            vec2(-offset, 2*offset),
            vec2(0.0f,    2*offset),
            vec2(offset,  2*offset),
            vec2(2*offset, 2*offset),

            vec2(-2*offset,offset),
            vec2(-offset, offset),
            vec2(0.0f,    offset),
            vec2(offset,  offset),
            vec2(2*offset, offset),

            vec2(-2*offset, 0.0f),
            vec2(-offset, 0.0f),
            vec2(0.0f,    0.0f),
            vec2(offset,  0.0f),
            vec2(2*offset,  0.0f),

            vec2(-2*offset, -offset),
            vec2(-offset, -offset),
            vec2(0.0f,    -offset),
            vec2(offset,  -offset),
            vec2(2*offset,  -offset),

            vec2(-2*offset, -2*offset),
            vec2(-offset, -2*offset),
            vec2(0.0f,    -2*offset),
            vec2(offset,  -2*offset),
            vec2(2*offset, -2*offset)
            );

    float kernel[3*3] = float[](
             -2.0, -1.0, +0.0,
             -1.0, +1.0, +1.0,
             +0.0, +1.0, +2.0
             );

    float ktest[5*5] = float[](
              0,  0, -1,  0, 0,
              0, -2,  -2, -1, 0,
             -1, -2, 16, -2, -1,
              0, -1, -2, -1, 0,
              0, 0, -1, 0, 0
             );

    vec3 sampleTex[9];
    for(int i = 0; i < 9; i++)
    {
	sampleTex[i] = vec3(texture2D(screenTexture, TexCoords.st + offsets[i]));
    }

    vec3 sampleKTex[25];
    for(int i = 0; i < 25; i++)
    {
	sampleKTex[i] = vec3(texture2D(screenTexture, TexCoords.st + test[i]));
    }
    vec3 col = vec3(0.0);

    for(int i = 0; i < 9; i++)
	col += sampleTex[i] * kernel[i];

//    for(int i = 0; i < 25; i++)
//	col += sampleKTex[i] * ktest[i];

    color = vec4(col, 1.0);
}
