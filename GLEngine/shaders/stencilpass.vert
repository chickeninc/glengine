#version 450 core


struct Light {
    vec3 position;
    vec3 spotDirection;
    vec3 halfVector;

    vec3 ambient;
    vec3 color;

    bool isLocal;
    bool isSpot;

    float radius;

    float constantAttenuation;
    float linearAttenuation;
    float quadraticAttenuation;

    float spotCosCutOff;
    float spotExponent;
};

uniform mat4 projection;
uniform mat4 view;


uniform Light light;


layout (location = 0) in vec3 position;


void main(void)
{
    vec3 fragPos = (position * light.radius) + light.position;
    gl_Position = projection * view * vec4(fragPos, 1.0);
}
