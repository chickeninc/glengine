#version 450 core

layout (location = 0) in vec3 position;
layout (location = 1) in vec3 color;

uniform mat4 projection;
uniform mat4 view;

out vec4 fragColor;

void main(void)
{
    fragColor = vec4(1.0, 0.0, 0.0, 1.0);
    gl_Position = projection * view * vec4(position, 1.0);
}
