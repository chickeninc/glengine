#version 450 core

const float PI = 3.14159265359;


struct Light {
    vec3 position;
    vec3 spotDirection;
    vec3 halfVector;

    vec3 ambient;
    vec3 color;

    bool isLocal;
    bool isSpot;

    float radius;

    float constantAttenuation;
    float linearAttenuation;
    float quadraticAttenuation;

    float spotCosCutOff;
    float spotExponent;
};

const int MAX_LIGHTS = 5;

layout (location = 0) out vec4 FragColor;
layout (location = 1) out vec4 BrightColor;
layout (location = 2) out vec4 ScatterColor;

in vec2 TexCoords;

layout (binding = 0) uniform sampler2D gPosition;
layout (binding = 1) uniform sampler2D gNormal;
layout (binding = 2) uniform sampler2D gColorSpec;
layout (binding = 3) uniform sampler2D shadowMap;
layout (binding = 4) uniform samplerCube shadowMapCube;
layout (binding = 5) uniform sampler2D ssao;

uniform mat4 lightSpaceMatrix;
uniform mat4 shadowMatrix;

uniform samplerCube uIrradianceMap;
uniform samplerCube uReflectionMap;

uniform float metal = 1.0;
uniform float rough = 0.4;

uniform vec3 cameraPos;
uniform float near;
uniform float far;
uniform float aspectRatio;
uniform float fov;
uniform vec3 cameraDir;
uniform Light[MAX_LIGHTS] lights;

uniform mat4 view;

float saturate(in float value)
{
    return clamp(value, 0.0, 1.0);
}

vec3 saturate(in vec3 vec)
{
    return clamp(vec, 0.0, 1.0);
}

vec4 saturate(in vec4 vec)
{
    return clamp(vec, 0.0, 1.0);
}

float ShadowCalculation(vec4 fragPosLightSpace, vec3 normal, vec3 lightDir)
{
/*
    float cosAlpha =  clamp(dot(normal, lightDir), 0.0f, 1.0f);
    float offsetScaleN = sqrt(1.0f - cosAlpha * cosAlpha);
    float offsetScaleL = offsetScaleN / cosAlpha;
    vec2 texelSize = vec2(offsetScaleN, min(2.0f, offsetScaleL));
*/




    // perspective divide
    vec3 projCoords = fragPosLightSpace.xyz / fragPosLightSpace.w;
    // Transform to [0,1] range
    projCoords = projCoords * 0.5 + 0.5;

    float closestDepth = texture2D(shadowMap, projCoords.xy).r;

    float currentDepth = projCoords.z;

    //float bias = max(0.05 * (1.0 - dot(normal, lightDir)), 0.005);
    float bias = 0.00;
    float shadow = 0.0;
        vec2 texelSize = 1.0 / vec2(2048, 2048);
	for(int x = -1; x <= 1; ++x)
	{
	    for(int y = -1; y <= 1; ++y)
	    {
		float pcfDepth = texture2D(shadowMap, projCoords.xy + vec2(x, y) * texelSize).r;
		shadow += currentDepth - bias > pcfDepth  ? 1.0 : 0.0;
	    }
	}
	shadow /= 9.0;

	// Keep the shadow at 0.0 when outside the far_plane region of the light's frustum.
	if(projCoords.z > 1.0)
	    shadow = 0.0;

	return shadow;
}

vec3 blendMaterial(vec3 Kdiff, vec3 Kspec, vec3 Kbase, float metallic)
{
    float scRange = smoothstep(0.2, 0.45, metallic);
    vec3 dielectric = Kdiff + Kspec;
    vec3 metal = Kspec * Kbase;
    return mix(dielectric, metal, scRange);
}

float DistributionGGX(vec3 N, vec3 H, float roughness)
{
    float a      = roughness*roughness;
    float a2     = a*a;
    float NdotH  = max(dot(N, H), 0.0);
    float NdotH2 = NdotH*NdotH;

    float nom   = a2;
    float denom = (NdotH2 * (a2 - 1.0) + 1.0);
    denom = PI * denom * denom;

    return nom / denom;
}

float Distribution_GGX(vec3 N, vec3 H, float alpha)
{
    float a2 = alpha * alpha;
    float NdotH = max(dot(N,H), 0.0);

    float tmp = (NdotH * a2 - NdotH) * NdotH + 1.0;

    return (a2 / (PI * tmp * tmp));
}

float GeometrySchlickGGX(float NdotV, float roughness)
{
    float r = (roughness + 1.0);
    float k = (r*r) / 8.0;

    float nom   = NdotV;
    float denom = NdotV * (1.0 - k) + k;

    return nom / denom;
}
float GeometrySmith(vec3 N, vec3 V, vec3 L, float roughness)
{
    float NdotV = max(dot(N, V), 0.0);
    float NdotL = max(dot(N, L), 0.0);
    float ggx2  = GeometrySchlickGGX(NdotV, roughness);
    float ggx1  = GeometrySchlickGGX(NdotL, roughness);

    return ggx1 * ggx2;
}

vec3 fresnelSchlick(float cosTheta, vec3 F0)
{
    return F0 + (1.0 - F0) * pow(1.0 - cosTheta, 5.0);
}

vec3 fresnelRoughness(in vec3 f0, in float f90, in float roughness) {
    return f0 + (f90-f0) * pow(1-roughness, 5.0f);
}

vec3 fresnelSchlickRoughness(float cosTheta, vec3 F0, float roughness)
{
    return F0 + (max(vec3(1.0 - roughness), F0) - F0) * pow(1.0 - cosTheta, 5.0);
}

vec3 fresnel_factor(in vec3 f0, in float product)
{
    return mix(f0, vec3(1.0), pow(1.01 - product, 5.0));
}

float Visibility_SmithGGX( float vdotN, float ldotN, float alpha )
{
        // alpha is already roughness^2

        float V1 = ldotN + sqrt( alpha + ( 1.0 - alpha ) * ldotN * ldotN );
	float V2 = vdotN + sqrt( alpha + ( 1.0 - alpha ) * vdotN * vdotN );

	// RB: avoid too bright spots
	return ( 1.0 / max( V1 * V2, 0.15 ) );
}

vec3 environmentBRDF(float g, float NdotV, vec3 F0)
{
    vec4 t = vec4(1.0 / 0.96, 0.475, (0.0275 - 0.25 * 0.04) / 0.96, 0.25);
    t *= vec4(g,g,g,g);
    t += vec4(0.0, 0.0, (0.015 - 0.75 * 0.04) / 0.96, 0.75);
    float a0 = t.x * min(t.y, exp2( -9.28 * NdotV)) + t.z;
    float a1 = t.w;

    return saturate(a0 + F0 * (a1 - a0));
}

vec3 environmentBRDFapprox(float roughness, float NdotV, vec3 specularColor)
{
    const vec4 c0 = vec4(-1, -0.0275, -0.572, 0.022);
    const vec4 c1 = vec4( 1,  0.0425, 1.04, -0.04);

    vec4 r = roughness * c0 + c1;
    float a004 = min(r.x * r.x, exp2(-9.28 * NdotV)) * r.x + r.y;
    vec2 AB = vec2(-1.04, 1.0) * a004 + r.zw;

    return specularColor * AB.x + AB.y;
}

// Hammersley function (return random low discrepency points)
vec2 Hammersley(in uint i, in uint N)
{
    return vec2(
                float(i) / float(N),
                float(bitfieldReverse(i)) * 2.3283064365386963e-10
                );
}

// Random rotation based on current fragment's coordinates
float randAngle()
{
  uint x = uint(gl_FragCoord.x);
  uint y = uint(gl_FragCoord.y);
  return float(30u * x ^ y + 10u * x * y);
}

// skewing the sample point and rotating (E : two values from hammersley)
vec2 DGgxSkew(inout vec2 E, in float roughness)
{
    float a = roughness * roughness;
    E.x = atan(sqrt((a * E.x) / (1.0 - E.x)));
    E.y = PI * PI * E.y + randAngle();
    return E;
}

// turn a skewed sample into a 3D vector (random vector looking in +Z hemisphere)
vec3 MakeSample(vec2 E)
{
  float SineTheta = sin(E.x);

  float x = cos(E.y) * SineTheta;
  float y = sin(E.y) * SineTheta;
  float z = cos(E.x);

  return vec3(x, y, z);
}

float D_GGX(in float roughness, in float NdH)
{
    float m = roughness * roughness;
    float m2 = m * m;
    float d = (NdH * m2 - NdH) * NdH + 1.0;
    return m2 / (PI * d * d);
}

vec3 Albedo(vec3 realAlbedo)
{
    return realAlbedo / PI;
}

vec3 Specular(float NoL, float NoH, float NoV, float VoH, float alpha, vec3 realSpecularColor)
{
    vec3 F = realSpecularColor + (1 - realSpecularColor) * pow((saturate(1 - VoH)), 5);
    float G = GeometrySchlickGGX(NoV, alpha);

    float D = D_GGX(alpha, NoH);


    vec3 DFG = D * F * G;
    return DFG / (4 * (NoL) * (NoV));
}

vec3 computeLighting(vec3 normal, vec3 diffuse, vec3 lightDir, vec3 viewDir, Light light, float roughness, float metallic)
{
    vec3 H = normalize(viewDir + lightDir);


    float NoL = max(dot(normal, lightDir), 0.0);
    float NoH = max(dot(normal, H), 0.0);
    float NoV = max(dot(normal, viewDir), 0.0);
    float VoH = max(dot(viewDir, H), 0.0);
    float alpha = max(1e-4, roughness * roughness);


    vec3 realAlbedo = diffuse - diffuse * metallic;
    vec3 realSpecularColor = mix(vec3(0.03), diffuse, metallic);

    vec3 albedoDiffuse = Albedo(realAlbedo);
    vec3 specular = Specular(NoL, NoH, NoV, VoH, alpha, realSpecularColor);


    vec3 finalColor = light.color * NoL * (albedoDiffuse * (1.0 - specular) + specular);

    return finalColor;
}

void main(void)
{


    vec4 FragPosition = texture2D(gPosition, TexCoords);
    vec3 FragPos = FragPosition.rgb;

    vec4 ND =  texture2D(gNormal, TexCoords);
    vec3 Normal = ND.xyz;
    float Depth = FragPosition.a;
    float AmbientOcclusion = FragPosition.a;


    vec4 colorMet = texture2D(gColorSpec, TexCoords);
    vec3 Color = colorMet.rgb;
    //float Specular = texture2D(gColorSpec, TexCoords).a;

    vec3 viewDir = normalize(cameraPos - FragPos);

    float metallic = metal;
    metallic = colorMet.a;

    float roughness = rough;
    roughness = ND.a;

    roughness *= roughness;
    metallic *= metallic;

    vec3 F0 = vec3(0.04);
    F0 = mix(F0, Color, metallic);
    float f90 = clamp(50 * dot(F0, vec3(0.33)), 0, 1);
    //vec3 F = fresnelSchlickRoughness(max(dot(Normal, viewDir), 0.0), F0, roughness);
    vec3 F = fresnelRoughness(F0, f90, roughness);
    //vec3 F = fresnel_factor(F0, roughness);
    vec3 kS = F;
    vec3 kD = vec3(1.0) - kS;

    kD *= 1.0 - metallic;
    vec3 envBRDF;
    float shadow = 0.0;
    vec3 Lo = vec3(0.0);



    for(int light =  0; light < MAX_LIGHTS; ++light)
    {
	vec3 halfVector;
	vec3 lightDirection = lights[light].position;
	float attenuation = 1.0;

	if(lights[light].isLocal)
	{
	    lightDirection = lights[light].position - FragPos;
	    float lightDistance = length(lightDirection);
	    lightDirection = normalize(lightDirection);

	    attenuation = 1.0 / (lights[light].constantAttenuation
	                             + lights[light].linearAttenuation * lightDistance
	                             + lights[light].quadraticAttenuation * lightDistance);

//	    float lightRadius = 50.0;
//	    attenuation = saturate(pow(1 - pow(lightDistance/lightRadius, 4), 2))
//		    / (pow(lightDistance, 2) + 1);

	    if(lights[light].isSpot)
	    {

		float spotCos = dot(lightDirection, -lights[light].spotDirection);
		if(spotCos < lights[light].spotCosCutOff)
		    attenuation = 0.0f;
		else
		    attenuation *= pow(spotCos, lights[light].spotExponent);
	    }
	    halfVector = normalize(lightDirection + viewDir);
	}
	else
	{
	    lightDirection = lights[light].position;
	    halfVector = normalize(lightDirection + viewDir);
	    if(lights[light].position.y > 0)
		shadow = ShadowCalculation(lightSpaceMatrix * vec4(FragPos, 1.0), Normal, lightDirection);


	}


	float NDF = Distribution_GGX(Normal, halfVector, roughness);
	float G   = GeometrySmith(Normal, viewDir, lightDirection, roughness);
	vec3 nominator    = NDF * G * F;
	float denominator = 4 * max(dot(viewDir, Normal), 0.0) * max(dot(lightDirection, Normal), 0.0) + 0.001;
	vec3 brdf         = nominator / denominator;

	float NdotL = max(dot(Normal, lightDirection), 0.0);

	envBRDF = environmentBRDF(G, max(dot(viewDir, Normal), 0.0), F0);
	vec3 radiance = lights[light].color * attenuation;

	Lo += ((kD * Color / PI) + brdf) * radiance * NdotL;

    }

    vec3 ambient = clamp(vec3(0.03) * Color, 0.0, 1.0);
    vec3 result = (ambient + Lo) * (1 - shadow);

    FragColor = vec4(0.0, 0.0, 0.0, 1.0);

    float brightness = dot(FragColor.rgb, vec3(0.2126, 0.7152, 0.0722));
    if(brightness > 1.0 && Depth != 1.0)
	BrightColor = vec4(FragColor.rgb, 1.0);
    else
	BrightColor = vec4(0.0, 0.0, 0.0, 1.0);


    if(Depth != 1.0)
    {
	ScatterColor = vec4(0.0, 0.0, 0.0, 1.0);
	if(dot(normalize(cameraDir), normalize(lights[0].position)) < 0.5f)
	    ScatterColor = vec4(1.0, 0.0, 0.0, 1.0);
    }



}

