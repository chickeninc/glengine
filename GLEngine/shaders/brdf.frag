
in vec2 TexCoords;

out vec4 color;

void main(void)
{
    vec2 integratedBRDF = IntegrateBRDF(TexCoords.x, TexCoords.y);
    color = vec4(integratedBRDF, 0.0, 1.0);

}
