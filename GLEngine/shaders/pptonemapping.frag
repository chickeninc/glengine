#version 450 core
in vec2 TexCoords;

out vec4 color;

layout (binding = 0) uniform sampler2D screenTexture;
layout (binding = 1) uniform sampler2D bloomTexture;
layout (binding = 2) uniform sampler2D scatterTexture;
layout (binding = 3) uniform sampler2D ssaoTexture;


uniform vec2 resolution = vec2(1792, 1008);
uniform float exposure = 2.0f;
uniform bool bloomOnly = false;
uniform bool ssaoOnly = false;

float gamma = 2.2;

vec3 simpleReinhardToneMapping(vec3 color)
{
        color *= exposure/(1. + color / exposure);
	color = pow(color, vec3(1. / gamma));
	return color;
}

vec3 lumaBasedReinhardToneMapping(vec3 color)
{
        float luma = dot(color, vec3(0.2126, 0.7152, 0.0722));
	float toneMappedLuma = luma / (1. + luma);
	color *= toneMappedLuma / luma;
	color = pow(color, vec3(1. / gamma));
	return color;
}

vec3 whitePreservingLumaBasedReinhardToneMapping(vec3 color)
{
        float white = 2.;
	float luma = dot(color, vec3(0.2126, 0.7152, 0.0722));
	float toneMappedLuma = luma * (1. + luma / (white*white)) / (1. + luma);
	color *= toneMappedLuma / luma;
	color = pow(color, vec3(1. / gamma));
	return color;
}

vec3 RomBinDaHouseToneMapping(vec3 color)
{
    color = exp( -1.0 / ( 2.72*color + 0.15 ) );
        color = pow(color, vec3(1. / gamma));
	return color;
}

vec3 filmicToneMapping(vec3 color)
{
        color = max(vec3(0.), color - vec3(0.004));
	color = (color * (6.2 * color + .5)) / (color * (6.2 * color + 1.7) + 0.06);
	return color;
}

vec3 Uncharted2ToneMapping(vec3 color)
{
        float A = 0.15;
	float B = 0.50;
	float C = 0.10;
	float D = 0.20;
	float E = 0.02;
	float F = 0.30;
	float W = 11.2;
	color *= exposure;
	color = ((color * (A * color + C * B) + D * E) / (color * (A * color + B) + D * F)) - E / F;
	float white = ((W * (A * W + C * B) + D * E) / (W * (A * W + B) + D * F)) - E / F;
	color /= white;
	color = pow(color, vec3(1. / gamma));
	return color;
}

void main(void)
{

    color = texture2D(screenTexture, TexCoords);
    vec3 bloom = texture2D(bloomTexture, TexCoords).rgb;
    vec3 ssao = texture2D(ssaoTexture, TexCoords).rgb;
    vec3 scatter = texture2D(scatterTexture, TexCoords).rgb;

//    color.rgb = Uncharted2ToneMapping(mix(color.rgb, scatter, 0.1f));

    color.rgb = Uncharted2ToneMapping(color.rgb);

    if(bloomOnly)
    {
	color.rgb = Uncharted2ToneMapping(bloom);
    }

    if(ssaoOnly)
    {
	color.rgb = (ssao);
    }

}
