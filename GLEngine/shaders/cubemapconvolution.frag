#version 450 core

in vec3 TexCoords;
out vec4 color;

layout (binding = 0) uniform sampler2D skybox;

const float PI = 3.14159265359f;

const vec2 invAtan = vec2(0.1591f, 0.3183f);
vec2 SampleSphericalMap(vec3 v)
{
    vec2 uv = vec2(atan(v.z, v.x), asin(v.y));
    uv *= invAtan;
    uv += 0.5;
    return uv;
}

void main()
{

    vec2 uv = SampleSphericalMap(normalize(TexCoords));
    color.rgb = texture(skybox, uv).rgb;


    vec3 normal = normalize(TexCoords);
    vec3 irradiance = vec3(0.0);

    vec3 up    = vec3(0.0, 1.0, 0.0);
    vec3 right = cross(up, normal);
    up         = cross(normal, right);

    float sampleDelta = 0.025f;
    float nrSamples = 0.0f;
    for(float phi = 0.0; phi < 2.0 * PI; phi += sampleDelta)
    {
	for(float theta = 0.0; theta < 0.5 * PI; theta += sampleDelta)
	{
	    // spherical to cartesian (in tangent space)
	    vec3 tangentSample = vec3(sin(theta) * cos(phi),  sin(theta) * sin(phi), cos(theta));
	    // tangent space to world
	    vec3 sampleVec = tangentSample.x * right + tangentSample.y * up + tangentSample.z * normal;

	    irradiance += texture(skybox, uv).rgb * cos(theta) * sin(theta);
	    nrSamples++;
	}
    }
    irradiance = PI * irradiance * (1.0 / float(nrSamples));

    color = vec4(irradiance, 1.0);
}

