#version 450 core

layout (location = 0) in vec3 position;

out vec3 TexCoords;

uniform mat4 projection;
uniform mat4 view;

uniform vec3 eyePos;


void main()
{
    mat4 rotView = mat4(mat3(view));
    vec4 pos =  projection * rotView * vec4(position, 1.0);
    // set the translation equal to w so with depth test
    // it's gonna do w/w = 1 so it's displayed on the back
    gl_Position = pos.xyww;

    TexCoords = position;

}
