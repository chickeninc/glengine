#version 450 core

in vec4 fragColor;

out vec4 color;

void main(void)
{
    gl_FragColor = fragColor;
}
