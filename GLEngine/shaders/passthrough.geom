#version 450 core

layout (triangles) in;
layout (line_strip, max_vertices = 26) out;

in vec3 vnormal[];
in vec3 fragPos[];
in vec2 uv[];


const float MAGNITUDE = 0.2f;

uniform mat4 model;
uniform mat4 projection;
uniform mat4 view;
uniform vec3 cameraPos;

uniform bool displayWireframe;
uniform bool displayNormals;

out vec4 gColor;

void GenerateNormal(int index)
{
    mat4  pv = projection * view;
    gColor = vec4(1.0f, 0.0f, 0.0f, 1.0f);
    gl_Position = pv * model * vec4(fragPos[index], 1.0);
    EmitVertex();
    gl_Position = pv * model * (vec4(fragPos[index], 1.0) + vec4(vnormal[index], 0.0f) * MAGNITUDE);
    EmitVertex();
    EndPrimitive();
}

void wireFrame(int index1, int index2)
{
    mat4  pv = projection * view;
    gl_Position = pv * model * vec4(fragPos[index1], 1.0);
    EmitVertex();
    gl_Position = pv * model * vec4(fragPos[index2], 1.0);
    EmitVertex();
    EndPrimitive();
}


void main()
{
    if(displayNormals) {
        GenerateNormal(0);
	GenerateNormal(1);
	GenerateNormal(2);
    }

    if(displayWireframe){
    gColor = vec4(0.0f, 0.0f, 1.0f, 1.0f);
        wireFrame(0, 1);
	wireFrame(1, 2);
	wireFrame(2, 0);
    }

}

