#version 450 core
in vec2 TexCoords;

out vec4 color;


uniform mat4 projection;
uniform mat4 view;
uniform vec3 lightPos;
uniform vec2 resolution;
uniform vec3 camPos;

uniform float exposure = 2.0f;
uniform float decay = 1.0f;
uniform float density = 0.5f;
uniform float weight = 0.5f;
const int NUM_SAMPLES = 50;

layout (binding = 0) uniform sampler2D screenTexture;

vec2 get2dPoint(vec3 point3D, mat4 viewMatrix,
                 mat4 projectionMatrix) {

      mat4 viewProjectionMatrix = projectionMatrix * viewMatrix;
      //transform world to clipping coordinates
      vec4 v = (viewProjectionMatrix * vec4(point3D, 1.0));
      float winX = ( v.x + 1 ) / 2.0;
      //we calculate -point3D.getY() because the screen Y axis is
      //oriented top->down
      float winY = ( 1 - v.y ) / 2.0;
      return vec2(winX, winY);
}

void main(void)
{

    color = texture2D(screenTexture, TexCoords);

    mat4 pv = projection * view;

    vec4 result = pv * vec4(lightPos * 50, 1.0);

    result.rgb = result.rgb / result.w;
    vec3 projCoords = result.rgb * 0.5 + 0.5;


    vec2 deltaTextCoord = vec2(TexCoords - projCoords.xy);
    vec2 textCoo = TexCoords;
    deltaTextCoord *= 1.0 /  float(NUM_SAMPLES) * density;
    float illuminationDecay = 0.5;


    for(int i=0; i < NUM_SAMPLES ; i++)
    {
	     textCoo -= deltaTextCoord;
	     vec4 scatter = texture2D(screenTexture, textCoo );

	     scatter *= illuminationDecay * weight;

	     color += scatter;

	     illuminationDecay *= decay;
     }
     color *= exposure;

}
