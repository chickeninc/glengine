#include "lightwidget.h"

LightWidget::LightWidget(GUI& gui, Rectangle r)
    :Widget(gui, "Lights", r),
      m_lightPickWidget(gui, gui.scene().lights(), Rectangle(Point(50,50),Point(250, 350))),
      m_lightsDropdown(gui, "Lights"),
      m_exposureLabel(gui, "Exposure :"),
      m_exposureEbox(gui, "", "", &gui.scene().exposure()),      
      m_turbidityLabel(gui, "Turbidity :"),
      m_turbidityEbox(gui, "", "", &gui.scene().turbidity()),
      m_lightPosLabel(gui, "Light Position :"),
      m_incrementButtonPosX(gui),
      m_decrementButtonPosX(gui),
      m_incrementButtonPosY(gui),
      m_decrementButtonPosY(gui),
      m_incrementButtonPosZ(gui),
      m_decrementButtonPosZ(gui),
      m_lightPosEboxX(gui, "", "x"),
      m_lightPosEboxY(gui, "", "y"),
      m_lightPosEboxZ(gui, "", "z"),
      m_lightColorLabel(gui, "Light Color :"),
      m_lightColorSliderR(gui, 0.5f, 0.0f, 1.0f),
      m_lightColorSliderG(gui, 0.5f, 0.0f, 1.0f),
      m_lightColorSliderB(gui, 0.5f, 0.0f, 1.0f),
      m_lightColorEboxR(gui, "", "r", &m_lightColorSliderR.getValue()),
      m_lightColorEboxG(gui, "", "g", &m_lightColorSliderG.getValue()),
      m_lightColorEboxB(gui, "", "b", &m_lightColorSliderB.getValue()),
      m_lightRadiusLabel(gui, "Radius :"),
      m_lightRadiusSlider(gui, 0.5f, 0.001f, 100.0f),
      m_lightRadiusEbox(gui, "", "", &m_lightRadiusSlider.getValue()),
      m_lightStrengthLabel(gui, "Strength :"),
      m_lightStrengthSlider(gui, 0.5f, 0.0f, 1000.0f),
      m_lightStrengthEbox(gui, "", "", &m_lightStrengthSlider.getValue())
{



    m_lightPosEboxX.setCanEdit(true);
    m_lightPosEboxY.setCanEdit(true);
    m_lightPosEboxZ.setCanEdit(true);

    m_exposureEbox.setCanEdit(true);
    m_exposureEbox.setEditStep(0.05f);

    m_turbidityEbox.setCanEdit(true);
    m_turbidityEbox.setEditStep(0.05f);

    m_type = Type::LIGHTWIDGET;

    m_lightColorSliderR.setOffset(10, 5);
    m_lightColorSliderG.setOffset(10, 5);
    m_lightColorSliderB.setOffset(10, 5);

    m_lightsDropdown.setChild(&m_lightPickWidget);

    /* For each light in the scene */
    for(int i  = 0; i < gui.scene().lights().size(); i++)
    {
        /* Add Light Label */
        std::shared_ptr<LightLabel> l = std::make_shared<LightLabel>(gui, gui.scene().lights()[i], *this);
        std::static_pointer_cast<Container>(m_lightPickWidget.children()[0])->
                addChild(l);
    }

    /*  Containers */
    for(int i = 0; i < 12; i++)
        m_children.push_back(std::make_shared<Container>(gui));

    /*  Lights List */
    std::static_pointer_cast<Container>(m_children[0])->
            addChild(std::shared_ptr<DropDown>(&m_lightsDropdown));

    /* Exposure Factor */
    std::static_pointer_cast<Container>(m_children[1])->
            addChildren({
                            std::shared_ptr<Label>(&m_exposureLabel),
                            std::shared_ptr<EditBoxNum>(&m_exposureEbox)
                        });

    /* Turbidity Factor */
    std::static_pointer_cast<Container>(m_children[2])->
            addChildren({
                            std::shared_ptr<Label>(&m_turbidityLabel),
                            std::shared_ptr<EditBoxNum>(&m_turbidityEbox)
                        });

    /*  Light Position Label */
    std::static_pointer_cast<Container>(m_children[3])->
            addChild(std::shared_ptr<Label>(&m_lightPosLabel));

    /*  Light Position Values */
    std::static_pointer_cast<Container>(m_children[4])->
            addChildren({
                            std::shared_ptr<EditBoxNum>(&m_lightPosEboxX),
                            std::shared_ptr<EditBoxNum>(&m_lightPosEboxY),
                            std::shared_ptr<EditBoxNum>(&m_lightPosEboxZ)
                        });

    /* Light Position Controls */
    std::static_pointer_cast<Container>(m_children[5])->
            addChildren({
                            std::shared_ptr<IncrementButton>(&m_incrementButtonPosX),
                            std::shared_ptr<DecrementButton>(&m_decrementButtonPosX),
                            std::shared_ptr<IncrementButton>(&m_incrementButtonPosY),
                            std::shared_ptr<DecrementButton>(&m_decrementButtonPosY),
                            std::shared_ptr<IncrementButton>(&m_incrementButtonPosZ),
                            std::shared_ptr<DecrementButton>(&m_decrementButtonPosZ)
                        });

    /*  Light Color Label */
    std::static_pointer_cast<Container>(m_children[6])->
            addChild(std::shared_ptr<Label>(&m_lightColorLabel));

    /*  Light Color R */
    std::static_pointer_cast<Container>(m_children[7])->
            addChildren({
                            std::shared_ptr<Slider>(&m_lightColorSliderR),
                            std::shared_ptr<EditBoxNum>(&m_lightColorEboxR)
                        });

    /*  Light Color G */
    std::static_pointer_cast<Container>(m_children[8])->
            addChildren({
                            std::shared_ptr<Slider>(&m_lightColorSliderG),
                            std::shared_ptr<EditBoxNum>(&m_lightColorEboxG)
                        });

    /*  Light Color B */
    std::static_pointer_cast<Container>(m_children[9])->
            addChildren({
                            std::shared_ptr<Slider>(&m_lightColorSliderB),
                            std::shared_ptr<EditBoxNum>(&m_lightColorEboxB)
                        });

    /*  Light Attenuation Label */
    std::static_pointer_cast<Container>(m_children[10])->
            addChildren({
                            std::shared_ptr<Label>(&m_lightRadiusLabel),
                            std::shared_ptr<Slider>(&m_lightRadiusSlider),
                            std::shared_ptr<EditBoxNum>(&m_lightRadiusEbox)
                        });

    /*  Light Constant Attenuation */
    std::static_pointer_cast<Container>(m_children[11])->
            addChildren({
                            std::shared_ptr<Label>(&m_lightStrengthLabel),
                            std::shared_ptr<Slider>(&m_lightStrengthSlider),
                            std::shared_ptr<EditBoxNum>(&m_lightStrengthEbox)
                        });


    /* Add Widgets to gui's list */
    gui.getWidgets().push_back(m_lightPickWidget);
    gui.getWidgets().push_back(*this);

    /* Add all elements to gui's element map */
    gui.getElements().insert(std::make_pair("lightPickWidget",
                                            std::shared_ptr<LightPickWidget>(&m_lightPickWidget)));

    gui.getElements().insert(std::make_pair("lightDropdown",
                                            std::shared_ptr<DropDown>(&m_lightsDropdown)));

    gui.getElements().insert(std::make_pair("exposureLabel",
                                            std::shared_ptr<Label>(&m_exposureLabel)));
    gui.getElements().insert(std::make_pair("exposureEbox",
                                            std::shared_ptr<EditBoxNum>(&m_exposureEbox)));

    gui.getElements().insert(std::make_pair("turbidityLabel",
                                            std::shared_ptr<Label>(&m_turbidityLabel)));
    gui.getElements().insert(std::make_pair("turbidityEbox",
                                            std::shared_ptr<EditBoxNum>(&m_turbidityEbox)));

    gui.getElements().insert(std::make_pair("lightPosLabel",
                                            std::shared_ptr<Label>(&m_lightPosLabel)));

    gui.getElements().insert(std::make_pair("incrementButtonPosX",
                                            std::shared_ptr<IncrementButton>(&m_incrementButtonPosX)));
    gui.getElements().insert(std::make_pair("decrementButtonPosX",
                                            std::shared_ptr<DecrementButton>(&m_decrementButtonPosX)));
    gui.getElements().insert(std::make_pair("incrementButtonPosY",
                                            std::shared_ptr<IncrementButton>(&m_incrementButtonPosY)));
    gui.getElements().insert(std::make_pair("decrementButtonPosY",
                                            std::shared_ptr<DecrementButton>(&m_decrementButtonPosY)));
    gui.getElements().insert(std::make_pair("incrementButtonPosZ",
                                            std::shared_ptr<IncrementButton>(&m_incrementButtonPosZ)));
    gui.getElements().insert(std::make_pair("decrementButtonPosZ",
                                            std::shared_ptr<DecrementButton>(&m_decrementButtonPosZ)));

    gui.getElements().insert(std::make_pair("lightPosEboxX",
                                            std::shared_ptr<EditBoxNum>(&m_lightPosEboxX)));
    gui.getElements().insert(std::make_pair("lightPosEboxY",
                                            std::shared_ptr<EditBoxNum>(&m_lightPosEboxY)));
    gui.getElements().insert(std::make_pair("lightPosEboxZ",
                                            std::shared_ptr<EditBoxNum>(&m_lightPosEboxZ)));

    gui.getElements().insert(std::make_pair("lightColorLabel",
                                            std::shared_ptr<Label>(&m_lightColorLabel)));
    gui.getElements().insert(std::make_pair("lightColorSliderR",
                                            std::shared_ptr<Slider>(&m_lightColorSliderR)));
    gui.getElements().insert(std::make_pair("lightColorSliderG",
                                            std::shared_ptr<Slider>(&m_lightColorSliderG)));
    gui.getElements().insert(std::make_pair("lightColorSliderB",
                                            std::shared_ptr<Slider>(&m_lightColorSliderB)));
    gui.getElements().insert(std::make_pair("lightColorEboxR",
                                            std::shared_ptr<EditBoxNum>(&m_lightColorEboxR)));
    gui.getElements().insert(std::make_pair("lightColorEboxG",
                                            std::shared_ptr<EditBoxNum>(&m_lightColorEboxG)));
    gui.getElements().insert(std::make_pair("lightColorEboxB",
                                            std::shared_ptr<EditBoxNum>(&m_lightColorEboxB)));


    gui.getElements().insert(std::make_pair("lightRadiusLabel",
                                            std::shared_ptr<Label>(&m_lightRadiusLabel)));
    gui.getElements().insert(std::make_pair("lightRadiusSlider",
                                            std::shared_ptr<Slider>(&m_lightRadiusSlider)));
    gui.getElements().insert(std::make_pair("lightRadiusEbox",
                                            std::shared_ptr<EditBoxNum>(&m_lightRadiusEbox)));


    gui.getElements().insert(std::make_pair("lightStrengthLabel",
                                            std::shared_ptr<Label>(&m_lightStrengthLabel)));
    gui.getElements().insert(std::make_pair("lightStrengthSlider",
                                            std::shared_ptr<Slider>(&m_lightStrengthSlider)));
    gui.getElements().insert(std::make_pair("lightStrengthEbox",
                                            std::shared_ptr<EditBoxNum>(&m_lightStrengthEbox)));


    updateChildren();
}

void LightWidget::setLight(Light* light)
{
    m_light = light;
    if(m_light)
    {
        std::string txt = "Light n°" + std::to_string(m_light->id());
        if(!m_light->isLocal())
            txt += " (Directional)";
        else if(m_light->isSpot())
            txt += " (Spot)";
        else
            txt += " (Point)";

        m_lightsDropdown.setText(txt);
        m_lightPosEboxX.setListenedValue(m_light->position().x);
        m_lightPosEboxY.setListenedValue(m_light->position().y);
        m_lightPosEboxZ.setListenedValue(m_light->position().z);

        if(m_light->isLocal())
        {
            m_incrementButtonPosX.setAffectedValue(&m_light->position().x);
            m_decrementButtonPosX.setAffectedValue(&m_light->position().x);
            m_incrementButtonPosY.setAffectedValue(&m_light->position().y);
            m_decrementButtonPosY.setAffectedValue(&m_light->position().y);
            m_incrementButtonPosZ.setAffectedValue(&m_light->position().z);
            m_decrementButtonPosZ.setAffectedValue(&m_light->position().z);
        }

        m_lightColorSliderR.setPosition(m_light->color().r);
        m_lightColorSliderG.setPosition(m_light->color().g);
        m_lightColorSliderB.setPosition(m_light->color().b);
        m_lightColorSliderR.setAffectedValue(&m_light->color().r);
        m_lightColorSliderG.setAffectedValue(&m_light->color().g);
        m_lightColorSliderB.setAffectedValue(&m_light->color().b);

        m_lightRadiusSlider.setPosition(m_light->radius());
        m_lightRadiusSlider.setAffectedValue(&m_light->radius());

        m_lightStrengthSlider.setPosition(m_light->strength());
        m_lightStrengthSlider.setAffectedValue(&m_light->strength());

        updateChildren();
    }
}

void LightWidget::mouseCallback(int button, int action)
{
    Widget::mouseCallback(button, action);
    if(m_lightsDropdown.bounds().contains(m_mouse.x(), m_mouse.y()))
         m_lightsDropdown.setText("Lights ");
}
