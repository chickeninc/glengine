#ifndef ASSETMANAGER_H
#define ASSETMANAGER_H

#include "texture.h"
#include "cubemaptexture.h"
#include <map>

class AssetManager
{
public:
    AssetManager();
    ~AssetManager();

    inline  std::map<std::string, Texture>& textures()
    {
        return m_textures;
    }

    inline  std::map<std::string, Texture> textures() const
    {
        return m_textures;
    }

    inline CubeMapTexture skyboxTexture() const
    {
        return m_skyboxTexture;
    }

    bool loadTextures();

private:

    std::map<std::string, Texture> m_textures;

    CubeMapTexture m_skyboxTexture;


};

#endif // ASSETMANAGER_H
