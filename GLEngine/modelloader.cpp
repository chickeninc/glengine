#include "modelloader.h"
#include <iostream>
#include <fstream>

#include <assimp/Importer.hpp>
#include <assimp/scene.h>
#include <assimp/postprocess.h>


ModelLoader::ModelLoader()
{

}

bool ModelLoader::Import3DFromFile(const std::string &path, std::map<std::string, Model>& models)
{
    Assimp::Importer importer;
    const aiScene* scene;

    std::ifstream fin(path.c_str());
    if(!fin.fail())
    {
        fin.close();
    }
    else
    {
        std::cout << "Couldnt Open 3D file : " << path.c_str() << std::endl;
        std::cout << "Error : " << importer.GetErrorString();
    }

    scene = importer.ReadFile(path, aiProcessPreset_TargetRealtime_Quality);


    if(!scene)
    {
        std::cout << importer.GetErrorString() << std::endl;
    }

    std::cout << "Successfully imported 3D scene : " << path.c_str() << std::endl;

    if(scene->HasMeshes())
    {
        std::cout << "Scene contains " << scene->mNumMeshes << " meshes" << std::endl;
        for(int i = 0; i < scene->mNumMeshes; i++)
        {
            aiMesh* mesh = scene->mMeshes[i];
            std::cout << "Mesh n° : " << i << " : ";
            std::cout << mesh->mName.C_Str() << std::endl;

            Mesh loadedMesh = Mesh();

            for(int j = 0; j < mesh->mNumFaces; j++)
            {
                const aiFace& face = mesh->mFaces[j];
                loadedMesh.indices.push_back(face.mIndices[0]);
                loadedMesh.indices.push_back(face.mIndices[1]);
                loadedMesh.indices.push_back(face.mIndices[2]);

            }

            for(int j = 0; j < mesh->mNumVertices; j++)
            {

                Vertex v;
                glm::vec3 vector;
                vector.x = mesh->mVertices[j].x;
                vector.y = mesh->mVertices[j].y;
                vector.z = mesh->mVertices[j].z;
                v.position = vector;

                if(mesh->HasNormals())
                {
                    glm::vec3 normal;
                    normal.x = mesh->mNormals[j].x;
                    normal.y = mesh->mNormals[j].y;
                    normal.z = mesh->mNormals[j].z;
                    v.normals =  normal;
                }

                if(mesh->mTextureCoords[0])
                {
                    glm::vec2 uv;
                    uv.x = mesh->mTextureCoords[0][j].x;
                    uv.y = mesh->mTextureCoords[0][j].y;
                    v.uvs = uv;
                }
                else
                {
                    v.uvs = glm::vec2(0.0f, 0.0f);
                }

                if(mesh->HasTangentsAndBitangents())
                {
                    glm::vec3 tangent;
                    tangent.x = mesh->mTangents[j].x;
                    tangent.y = mesh->mTangents[j].y;
                    tangent.z = mesh->mTangents[j].z;
                    v.tangent = tangent;

                    glm::vec3 bitangent;
                    bitangent.x = mesh->mBitangents[j].x;
                    bitangent.y = mesh->mBitangents[j].y;
                    bitangent.z = mesh->mBitangents[j].z;
                    v.bitangent = bitangent;
                }

                loadedMesh.vertices.push_back(v);
            }

            Model model = Model(loadedMesh);
            models.insert(std::make_pair(mesh->mName.C_Str(), model));
        }
    }



   return true;
}
