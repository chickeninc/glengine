#ifndef POINT_H
#define POINT_H


class Point
{

public:

    Point();
    Point(int x, int y);

    inline int x() const
    {
        return m_x;
    }

    inline int y() const
    {
        return m_y;
    }

    inline void setX(const int x)
    {
        m_x = x;
    }

    inline void setY(const int y)
    {
        m_y = y;
    }

    inline void addX(const int x)
    {
        m_x += x;
    }

    inline void addY(const int y)
    {
        m_y += y;
    }

protected:

    int m_x = 0;
    int m_y = 0;
};

#endif // POINT_H
