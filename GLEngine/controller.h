#ifndef CONTROLLER_H
#define CONTROLLER_H

#include "gui.h"
#include "scene.h"
#include "renderer.h"

class Controller
{

public:

    Controller(Scene& scene, GUI& ui, Renderer& renderer);

    void control();

private:
    Renderer& renderer;
    Scene& scene;
    GUI& ui;


};

#endif // CONTROLLER_H
