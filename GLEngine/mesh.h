#ifndef MESH_H
#define MESH_H

#include "vector"
#include <glm/vec2.hpp>
#include <glm/vec3.hpp>
#include <GL/gl.h>
#include "btBulletDynamicsCommon.h"
#include <string>
#include <iostream>
#include <assimp/cimport.h>
#include <assimp/scene.h>
#include <assimp/postprocess.h>

#include "stb_image.h"


struct Vertex {
    glm::vec3 position;
    glm::vec3 normals;
    glm::vec2 uvs;
    glm::vec3 tangent;
    glm::vec3 bitangent;
    //GLuint indice;
};

enum MeshType{
    SKYBOX,
    POSTPROCESSQUAD,
    MESH
};

class Mesh
{

public:

    Mesh();
    Mesh(btCollisionShape* shape);


    int size;
    std::vector<Vertex> vertices;
    std::vector<GLuint> indices;

    std::vector<btVector3> shape;

    bool loadOBJ(const char * path);
    void createPlane(float sideSize);
    void createTerrain();
};


#endif // MESH_H
