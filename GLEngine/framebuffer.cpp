#include "framebuffer.h"
#include <iostream>

FrameBuffer::FrameBuffer()
{

}

FrameBuffer::FrameBuffer(int width, int height)
    :m_width(width),
      m_height(height),
      m_texColorBuffer(width, height)
{
    glGenFramebuffers(1, &m_id);
    glBindFramebuffer(GL_FRAMEBUFFER, m_id);

    attach(m_texColorBuffer);

    glGenRenderbuffers(1, &m_renderBuffer);

    glBindRenderbuffer(GL_RENDERBUFFER, m_renderBuffer);
    glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH24_STENCIL8, m_width, m_height);
    glBindRenderbuffer(GL_RENDERBUFFER, 0);

    glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_STENCIL_ATTACHMENT, GL_RENDERBUFFER, m_renderBuffer);

    if(glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
        std::cout << "ERROR::FRAMEBUFFER:: Framebuffer is not complete!" << std::endl;
    glBindFramebuffer(GL_FRAMEBUFFER, 0);

}

void FrameBuffer::resize(const uint width, const uint height)
{
    m_width = width;
    m_height = height;

    m_texColorBuffer.resize(width, height);

    glBindRenderbuffer(GL_RENDERBUFFER, m_renderBuffer);
    glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH24_STENCIL8, m_width, m_height);
    glBindRenderbuffer(GL_RENDERBUFFER, 0);
}

void FrameBuffer::attach(Texture &texture, GLenum attachementPoint)
{
    texture.Use();
    glFramebufferTexture2D(GL_FRAMEBUFFER, attachementPoint, GL_TEXTURE_2D, texture.ID(), 0);
    glBindTexture(GL_TEXTURE_2D, 0);
}

void FrameBuffer::use()
{
    glBindFramebuffer(GL_FRAMEBUFFER, m_id);
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, m_texColorBuffer.ID());
}

void FrameBuffer::clean()
{
    glDeleteFramebuffers(1, &m_id);
    glDeleteRenderbuffers(1, &m_renderBuffer);
    glDeleteTextures(1, &m_texColorBuffer.ID());
}
