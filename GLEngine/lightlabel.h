#ifndef LIGHTLABEL_H
#define LIGHTLABEL_H

#include "label.h"
#include "lightwidget.h"

class LightLabel : public Label
{

public:

    LightLabel(GUI& gui, Light& light, LightWidget& lightWidget);

    void draw(NVGcontext* vg) override;
    void mouseCallback(GLint button, GLint action) override;

    inline void setIsSelectedLight(const GLboolean b)
    {
        m_isSelectedLight = b;
    }

private:
    LightWidget& m_lightWidget;
    Light& m_light;
    glm::vec3& m_color;

    GLboolean m_isSelectedLight = false;
};

#endif // LIGHTLABEL_H
