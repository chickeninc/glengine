#include "button.h"
#include <iostream>


Button::Button(GUI& gui, std::string text)
    :UIElement(gui),
      m_text(text)
{
    m_type = Type::BUTTON;
}

Button::Button(GUI& gui, int preicon, std::string text)
    :UIElement(gui),
      m_preicon(preicon),
      m_text(text)
{
    m_type = Type::BUTTON;
}

Button::Button(GUI& gui, Rectangle r, std::string text)
    :UIElement(gui, r),
      m_text(text)
{
    m_type = Type::BUTTON;
}

Button::Button(GUI& gui, std::string text, NVGcolor color)
    :UIElement(gui),
      m_text(text),
      m_color(color)
{
    m_type = Type::BUTTON;
}

Button::Button(GUI& gui, Rectangle r, std::string text, NVGcolor color)
    :UIElement(gui, r),
      m_text(text),
      m_color(color)
{
    m_type = Type::BUTTON;
}

Button::Button(GUI& gui, Rectangle r, int preicon, std::string text, NVGcolor color)
    :UIElement(gui, r),
      m_preicon(preicon),
      m_text(text),
      m_color(color)
{
    m_type = Type::BUTTON;
}

void Button::setBounds(Rectangle r)
{
    m_bounds = r;
}

void Button::mouseCallback(int button, int action)
{
    if(action == GLFW_PRESS && button == GLFW_MOUSE_BUTTON_LEFT)
        flip();

    const bool b = m_pressed;
    if(m_affectedBoolean)
        (*m_affectedBoolean) = b;
}

void Button::draw(NVGcontext* vg)
{
    NVGpaint bg;
    char icon[8];
    float cornerRadius = 4.0f;
    float tw = 0, iw = 0;

    float x = m_bounds.x()      + m_offsetX;
    float y = m_bounds.y()      + m_offsetY;
    float w = m_bounds.width()  - (2*m_offsetX);
    float h = m_bounds.height() - (2*m_offsetY);

    if(m_pressed){
        bg = nvgLinearGradient(vg, x,y+h,x,y, nvgRGBA(255,255,255,isBlack(m_color)?16:32), nvgRGBA(0,0,0,isBlack(m_color)?16:32));
    } else {
        bg = nvgLinearGradient(vg, x,y,x,y+h, nvgRGBA(255,255,255,isBlack(m_color)?16:32), nvgRGBA(0,0,0,isBlack(m_color)?16:32));
    }

    nvgBeginPath(vg);
    nvgRoundedRect(vg, x+1,y+1, w-2,h-2, cornerRadius-1);
    if (!isBlack(m_color)) {
        nvgFillColor(vg, m_color);
        nvgFill(vg);
    }
    nvgFillPaint(vg, bg);
    nvgFill(vg);

    nvgBeginPath(vg);
    nvgRoundedRect(vg, x+0.5f,y+0.5f, w-1,h-1, cornerRadius-0.5f);
    nvgStrokeColor(vg, nvgRGBA(0,0,0,48));
    nvgStroke(vg);

    nvgFontSize(vg, 20.0f);
    nvgFontFace(vg, "sans-bold");
    tw = nvgTextBounds(vg, 0,0, m_text.data(), NULL, NULL);
    if (m_preicon != 0) {
        nvgFontSize(vg, h*1.3f);
        nvgFontFace(vg, "icons");
        iw = nvgTextBounds(vg, 0,0, cpToUTF8(m_preicon,icon), NULL, NULL);
        iw += h*0.15f;
    }

    if (m_preicon != 0) {
        nvgFontSize(vg, h*1.3f);
        nvgFontFace(vg, "icons");
        nvgFillColor(vg, nvgRGBA(255,255,255,96));
        nvgTextAlign(vg,NVG_ALIGN_LEFT|NVG_ALIGN_MIDDLE);
        nvgText(vg, x+w*0.5f-tw*0.5f-iw*0.75f, y+h*0.5f, cpToUTF8(m_preicon,icon), NULL);
    }

    nvgFontSize(vg, 20.0f);
    nvgFontFace(vg, "sans-bold");
    nvgTextAlign(vg,NVG_ALIGN_LEFT|NVG_ALIGN_MIDDLE);
    nvgFillColor(vg, nvgRGBA(0,0,0,160));
    nvgText(vg, x+w*0.5f-tw*0.5f+iw*0.25f,y+h*0.5f-1, m_text.data(), NULL);
    nvgFillColor(vg, nvgRGBA(255,255,255,160));
    nvgText(vg, x+w*0.5f-tw*0.5f+iw*0.25f,y+h*0.5f, m_text.data(), NULL);
}

IncrementButton::IncrementButton(GUI &gui)
    :Button(gui, "+")
{

}

IncrementButton::IncrementButton(GUI &gui, const GLfloat f)
    :Button(gui, "+"),
      m_incrementFactor(f)
{

}

void IncrementButton::mouseCallback(int button, int action)
{

    if(action == GLFW_PRESS)
    {
        m_pressed = true;
        if(m_affectedValue)
            (*m_affectedValue) += m_incrementFactor;
    }

    if(action == GLFW_RELEASE)
        m_pressed = false;
}

DecrementButton::DecrementButton(GUI &gui)
    :Button(gui, "-")
{

}

DecrementButton::DecrementButton(GUI &gui, const GLfloat f)
    :Button(gui, "-"),
      m_decrementFactor(f)
{

}

void DecrementButton::mouseCallback(int button, int action)
{

    if(action == GLFW_PRESS)
    {
        m_pressed = true;
        if(m_affectedValue)
            (*m_affectedValue) -= m_decrementFactor;
    }

    if(action == GLFW_RELEASE)
        m_pressed = false;
}
