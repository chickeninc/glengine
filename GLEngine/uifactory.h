#ifndef UIFACTORY_H
#define UIFACTORY_H


#include "gui.h"
#include "widget.h"

class GUI;

class UIFactory
{
public:

    static void createLightWidget(GUI& gui);
    static void createPPWidget(GUI& gui);
    static void createInfoWidget(GUI& gui);
    static void createMenuWidget(GUI& gui);
    static void createConsoleWidget(GUI& gui);
    static void createGUIWidget(GUI& gui);
    static void createModelWidget(GUI& gui);
};




#endif // UIFACTORY_H
