#ifndef BUTTON_H
#define BUTTON_H

#include <string>
#include "uielement.h"
#include <GLFW/glfw3.h>

class Button : public UIElement
{
public:

    Button(GUI& gui, std::string text);
    Button(GUI& gui, int preicon, std::string text);
    Button(GUI& gui, Rectangle r, std::string text);
    Button(GUI& gui, std::string text, NVGcolor color);
    Button(GUI& gui, Rectangle r, std::string text, NVGcolor color);
    Button(GUI& gui, Rectangle r, int preicon, std::string text, NVGcolor color);

    inline int preicon() const
    {
        return m_preicon;
    }

    inline std::string text() const
    {
        return m_text;
    }

    inline NVGcolor color() const
    {
        return m_color;
    }

    inline void press()
    {
        m_pressed = true;
    }

    inline void release()
    {
        m_pressed = false;
    }

    inline void flip()
    {
        m_pressed = !m_pressed;
    }

    inline bool isPressed() const
    {
        return m_pressed;
    }

    inline void setAffectedBoolean(GLboolean* b)
    {
        m_affectedBoolean = b;
    }

    void setBounds(Rectangle r) override;
    void mouseCallback(int button, int action) override;
    void draw(NVGcontext* vg) override;

protected:
    int m_preicon = 0;

    std::string m_text;

    NVGcolor m_color = nvgRGBA(0, 0, 0, 64);

    bool m_pressed = false;

    GLboolean* m_affectedBoolean = nullptr;
};

class IncrementButton : public Button
{
public:
    IncrementButton(GUI& gui);
    IncrementButton(GUI& gui, const GLfloat f);

    void mouseCallback(int button, int action) override;

    inline void setAffectedValue(GLfloat* value)
    {
        m_affectedValue = value;
    }

    inline void setIncrementFactor(const GLfloat f)
    {
        m_incrementFactor = f;
    }

private:
    GLfloat m_incrementFactor = 1.0f;
    GLfloat* m_affectedValue = nullptr;
};

class DecrementButton : public Button
{
public:
    DecrementButton(GUI& gui);
    DecrementButton(GUI& gui, const GLfloat f);

    void mouseCallback(int button, int action) override;

    inline void setAffectedValue(GLfloat* value)
    {
        m_affectedValue = value;
    }

    inline void setDecrementFactor(const GLfloat f)
    {
        m_decrementFactor = f;
    }

private:
    GLfloat m_decrementFactor = 1.0f;
    GLfloat* m_affectedValue = nullptr;
};

#endif // BUTTON_H
