#ifndef CAMERA_H
#define CAMERA_H

#include <GL/gl.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <iostream>
#include "listenable.h"

class Camera
{

public:

    Camera();

    GLvoid update();
    GLvoid update(const GLuint w, const GLuint h);

    GLvoid rotateBy(const GLfloat pitch, const GLfloat yaw);

    inline GLvoid moveForward()
    {
        move(m_front);
    }

    inline GLvoid moveBackward()
    {
        move(-m_front);
    }

    inline GLvoid moveLeft()
    {
        move(-glm::normalize(glm::cross(m_front, m_up)));
    }

    inline GLvoid moveRight()
    {
        move(glm::normalize(glm::cross(m_front, m_up)));
    }

    inline GLvoid moveUp()
    {
        move(m_up);
    }

    inline GLvoid moveDown()
    {
        move(-m_up);
    }

    inline GLvoid move(const glm::vec3 direction)
    {
        m_position += direction * m_velocity;
        update();
    }

    inline GLvoid zoom()
    {
        if(m_fov <= 119)
            m_fov -= 1;
    }

    inline GLvoid unzoom()
    {
        if(m_fov >= 41)
            m_fov += 1;
    }

    inline glm::vec3 getPos() const
    {
        return m_position;
    }

    inline glm::vec3& getPos()
    {
        return m_position;
    }

    inline glm::vec3 getUp() const
    {
        return m_up;
    }

    inline glm::vec3 getRight() const
    {
        return glm::normalize(glm::cross(m_up, m_direction));
    }

    inline glm::vec3 getDir() const
    {
        return m_direction;
    }

    inline glm::vec3& getDir()
    {
        return m_direction;
    }

    inline glm::vec3 getFront() const
    {
        return m_front;
    }

    inline glm::vec3& getFront()
    {
        return m_front;
    }

    inline glm::mat4 getProjectionMatrix() const
    {
        return m_projection;
    }

    inline glm::mat4 getViewMatrix() const
    {
        return m_view;
    }

    inline GLfloat far() const
    {
        return m_far;
    }

    inline GLfloat near() const
    {
        return m_near;
    }

    inline GLfloat fov() const
    {
        return m_fov;
    }

    inline GLfloat* getDirection()
    {
        return glm::value_ptr(m_direction);
    }

    inline GLfloat* getProjection()
    {
        return glm::value_ptr(m_projection);
    }

    inline GLfloat* getView()
    {
        return glm::value_ptr(m_view);
    }

    inline GLfloat* getPosition()
    {
        return glm::value_ptr(m_position);
    }

    inline GLfloat aspectRatio() const
    {
        return m_aspectRatio;
    }

    inline void accelerate()
    {
        if(m_velocity < m_maxVelocity)
            m_velocity += 0.01f;
    }

    inline void decelerate()
    {
        if(m_velocity > m_minVelocity)
            m_velocity -= 0.01f;
    }


private:

    glm::mat4x4 m_projection;
    glm::mat4x4 m_view;

    glm::vec3 m_position = glm::vec3(0.0f, 1.0f, 20.0f);
    glm::vec3 m_target = glm::vec3(0.0f, 0.0f, 0.0f);
    glm::vec3 m_front = glm::vec3(0.0f, 0.0f, -1.0f);
    glm::vec3 m_direction;
    glm::vec3 m_up;

    GLfloat m_yaw   = -90.0f;
    GLfloat m_pitch =   0.0f;

    GLfloat m_acceleration = 0.001f;
    GLfloat m_maxVelocity = 0.3f;
    GLfloat m_minVelocity = 0.1f;
    GLfloat m_velocity = 0.1f;

    GLfloat m_near = 0.1f;
    GLfloat m_far = 1000.0f;
    GLfloat m_fov = 67.0f;
    GLfloat m_aspectRatio = 1.0f;


};

#endif // CAMERA_H
