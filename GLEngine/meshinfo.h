#ifndef MESHINFO_H
#define MESHINFO_H

#include "GL/glew.h"
#include "mesh.h"
#include "btBulletDynamicsCommon.h"



class Mesh;

class MeshInfo{

public:

    MeshInfo();
    MeshInfo(Mesh mesh);
    MeshInfo(MeshType meshType);
    MeshInfo(const btVector3* points);

    void clean();

    inline GLuint VAO() const
    {
        return m_VAO;
    }

    inline GLuint VBO() const
    {
        return m_VBO;
    }

    inline GLuint IBO() const
    {
        return m_IBO;
    }


private:

    GLuint m_VAO = 0;
    GLuint m_VBO = 0;
    GLuint m_IBO = 0;
};


#endif // MESHINFO_H
