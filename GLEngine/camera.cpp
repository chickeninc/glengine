#include "camera.h"


Camera::Camera()
    :m_direction(glm::normalize(m_position - m_target)),
     m_projection(glm::perspective(glm::radians(m_fov), 4.0f / 3.0f, m_near, m_far))
{
    glm::vec3 tmpUp = glm::vec3(0.0f, 1.0f, 0.0f);
    glm::vec3 right = glm::normalize(glm::cross(tmpUp, m_direction));
    m_up = glm::cross(m_direction, right);

    m_view = glm::lookAt(m_position, m_position + m_front, m_up);
    update();
}

GLvoid Camera::update()
{
    m_view = glm::lookAt(m_position, m_position + m_front, m_up);
}

GLvoid Camera::update(const GLuint w, const GLuint h)
{
    int lastW, lastH;
    if(w != lastW || h != lastH)
    {
        m_aspectRatio = (GLfloat) (float) w / (float) h;
        m_projection = glm::perspective(glm::radians(m_fov), m_aspectRatio, m_near, m_far);
    //    m_projection = glm::ortho(-2.0f, 2.0f, -2.0f, 2.0f, 0.1f, 50.0f);

    }
    update();
    lastW = w;
    lastH = h;
}

GLvoid Camera::rotateBy(const GLfloat pitch, const GLfloat yaw)
{

    m_pitch += pitch;
    m_yaw += yaw;


    if (m_pitch > 89.0f)
        m_pitch = 89.0f;
    if (m_pitch < -89.0f)
        m_pitch = -89.0f;


    glm::vec3 front;
    front.x = cos(glm::radians(m_yaw)) * cos(glm::radians(m_pitch));
    front.y = sin(glm::radians(m_pitch));
    front.z = cos(glm::radians(m_pitch)) * sin(glm::radians(m_yaw));

    m_front = glm::normalize(front);

    update();
}
