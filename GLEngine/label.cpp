#include "label.h"

Label::Label(GUI& gui)
    :UIElement(gui),
      m_text("Undefined")
{
    m_type = Type::LABEL;
}

Label::Label(GUI& gui, Rectangle r, std::string text)
    :UIElement(gui, r),
      m_text("Undefined")
{
    m_type = Type::LABEL;
}

Label::Label(GUI& gui, std::string text)
    :UIElement(gui),
      m_text(text)
{
    m_type = Type::LABEL;
}

void Label::draw(NVGcontext* vg)
{
    float x = m_bounds.x()             + m_offsetX;
    float y = m_bounds.y()             + m_offsetY;
    float h = m_bounds.height()        - (2*m_offsetX);
    float w = m_bounds.width()         - (2*m_offsetY);
    NVG_NOTUSED(w);

    nvgFontSize(vg, 18.0f);
    nvgFontFace(vg, "sans");

    nvgFillColor(vg, nvgRGBA(255,255,255,128));

    nvgTextAlign(vg,NVG_ALIGN_LEFT|NVG_ALIGN_MIDDLE);
    nvgText(vg, x, y+h*0.5f, m_text.data(), NULL);
}


