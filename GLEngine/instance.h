#ifndef INSTANCE_H
#define INSTANCE_H

#include "model.h"
#include "renderable.h"

#include <btBulletDynamicsCommon.h>
#include "gameobject.h"

class Instance : public GameObject
{

public:

    Instance();
    Instance(Model* m);
    Instance(Model* m, btTransform t, float mass, btCollisionShape* shape, bool isKinematic = true);

    inline Model* model() const
    {
        return m_model;
    }
/*
    inline Model* model()
    {
        return m_model;
    }
*/

    void render(ShaderProgram program, GLenum mode = GL_PATCHES) override;

private:

    Model* m_model = nullptr;
};

#endif // INSTANCE_H
