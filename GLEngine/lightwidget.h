#ifndef LIGHTWIDGET_H
#define LIGHTWIDGET_H


#include "widget.h"
#include "light.h"
#include "editbox.h"
#include "slider.h"
#include "dropdown.h"
#include "label.h"
#include "gui.h"
#include "lightpickwidget.h"
#include "button.h"

class LightWidget : public Widget
{
public:
    LightWidget(GUI& gui, Rectangle r);

    void setLight(Light* light);

    void mouseCallback(int button, int action) override;

private:
    LightPickWidget m_lightPickWidget;

    DropDown m_lightsDropdown;

    Label m_exposureLabel;
    EditBoxNum m_exposureEbox;


    Label m_turbidityLabel;
    EditBoxNum m_turbidityEbox;

    Label m_lightPosLabel;

    IncrementButton m_incrementButtonPosX;
    DecrementButton m_decrementButtonPosX;
    IncrementButton m_incrementButtonPosY;
    DecrementButton m_decrementButtonPosY;
    IncrementButton m_incrementButtonPosZ;
    DecrementButton m_decrementButtonPosZ;

    EditBoxNum m_lightPosEboxX;
    EditBoxNum m_lightPosEboxY;
    EditBoxNum m_lightPosEboxZ;

    Label m_lightColorLabel;

    Slider m_lightColorSliderR;
    Slider m_lightColorSliderG;
    Slider m_lightColorSliderB;

    EditBoxNum m_lightColorEboxR;
    EditBoxNum m_lightColorEboxG;
    EditBoxNum m_lightColorEboxB;

    Label m_lightRadiusLabel;
    Slider m_lightRadiusSlider;
    EditBoxNum m_lightRadiusEbox;

    Label m_lightStrengthLabel;
    Slider m_lightStrengthSlider;
    EditBoxNum m_lightStrengthEbox;



    Light* m_light = nullptr;

};

#endif // LIGHTWIDGET_H
