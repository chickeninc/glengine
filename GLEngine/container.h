#ifndef CONTAINER_H
#define CONTAINER_H

#include "uielement.h"
#include <memory>


class Container : public UIElement, public std::enable_shared_from_this<Container>
{
public:

    typedef std::vector<std::shared_ptr<UIElement>> Children;
    typedef std::shared_ptr<UIElement> Child;

    Container(GUI& gui);
    Container(GUI& gui, Rectangle r);

    Child insertChild(Child e);
    Child insertChildren(Children e);

    virtual void addChild(Child e);
    virtual void addChildren(Children e);

    virtual void updateChildren();

    void setBounds(Rectangle r) override;
    void translate(int x, int y) override;
    void draw(NVGcontext* vg) override { }

    void setVisible(bool b) override;

    void mouseCallback(int button, int action) override;
    void cursorCallback(double xpos, double ypos) override;
    void scrollCallback(double xoffset, double yoffset) override;

    inline void setVertical(const bool b)
    {
        m_isVertical = b;
    }

    inline Children children() const
    {
        return m_children;
    }

protected:
    bool m_isVertical = false;
    Children m_children;

};

#endif // CONTAINER_H
