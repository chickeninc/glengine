#ifndef MOUSE_H
#define MOUSE_H



class Mouse {


public:
    Mouse();


    inline float& x()
    {
        return m_x;
    }

    inline float& y()
    {
        return m_y;
    }

    inline float& lastX()
    {
        return m_lastX;
    }

    inline float& lastY()
    {
        return m_lastY;
    }

    inline float& deltaX()
    {
        return m_deltaX;
    }

    inline float& deltaY()
    {
        return m_deltaY;
    }

    inline bool& lbDown()
    {
        return m_lbDown;
    }

    inline bool& rbDown()
    {
        return m_rbDown;
    }

    inline bool& lbWasDown()
    {
        return m_lbWasDown;
    }

    inline bool& rbWasDown()
    {
        return m_rbWasDown;
    }

    inline void setlbWasDown(const bool& b)
    {
        m_lbWasDown = b;
    }

    inline void setrbWasDown(const bool& b)
    {
        m_rbWasDown = b;
    }

    inline void setlbDown(const bool& b)
    {
        m_lbDown = b;
    }

    inline void setrbDown(const bool& b)
    {
        m_rbDown = b;
    }

    inline void flipLb()
    {
        m_lbDown = !m_lbDown;
    }

    inline void flipRb()
    {
        m_rbDown = !m_rbDown;
    }

    inline void setDeltaX(const float& deltaX)
    {
        m_deltaX = deltaX;
    }

    inline void setDeltaY(const float& deltaY)
    {
        m_deltaY = deltaY;
    }

    inline void setDelta(const float& deltaX, const float& deltaY)
    {
        m_deltaX = deltaX;
        m_deltaY = deltaY;
    }

    void setX(float x);
    void setY(float y);
    void update(float x, float y);

protected:
    float m_x;
    float m_y;
    float m_deltaX;
    float m_deltaY;
    float m_lastX;
    float m_lastY;
    bool m_lbDown = false;
    bool m_rbDown = false;
    bool m_lbWasDown = false;
    bool m_rbWasDown = false;
};

#endif // MOUSE_H
