#ifndef CHECKBOX_H
#define CHECKBOX_H

#include "uielement.h"
#include <string>

class CheckBox : public UIElement
{
public:
    CheckBox(GUI& gui, std::string text);
    CheckBox(GUI& gui, Rectangle r, std::string text);

    inline std::string text() const
    {
        return m_text;
    }
    inline bool isChecked() const
    {
        return m_checked;
    }

    inline void check()
    {
        m_checked = true;
    }

    inline void uncheck()
    {
        m_checked = false;
    }

    inline void flip()
    {
        m_checked = !m_checked;
    }

    inline Rectangle getBox()
    {
        return Rectangle(m_bounds.x() + 1, m_bounds.y() + (int)(m_bounds.height()*0.5f)-9+1, 18, 18);
    }

    inline void setAffectedBoolean(bool* b)
    {
        m_affectedBoolean = b;
    }

    void setBounds(Rectangle r) override;
    void translate(int x, int y) override;


    void draw(NVGcontext* vg) override;
    void cursorCallback(double xpos, double ypos) override;
    void mouseCallback(int button, int action) override;

private:
    bool* m_affectedBoolean = nullptr;

    bool m_mouseOverBox = false;
    bool m_checked = false;

    std::string m_text = "";
    Rectangle m_box;
};

#endif // CHECKBOX_H
