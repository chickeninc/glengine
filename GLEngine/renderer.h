#ifndef RENDERER_H
#define RENDERER_H

#include "renderable.h"
#include "framebuffer.h"
#include "postprocess.h"
#include "callbacks.h"
#include "rectangle.h"
#include "gbuffer.h"
#include "meshinfo.h"
#include "shader.h"
#include "texture.h"
#include "cubemaptexture.h"
#include "assetmanager.h"
#include "perf.h"
#include <list>
#include "shadowmap.h"
#include "light.h"
#include "shaderprogram.h"

#include "debugger.h"
#include "gldebugdrawer.h"

struct DemoData {
    int fontNormal, fontBold, fontIcons, fontEmoji;
    int images[12];
};

class Scene;
class GUI;
class GLFWwindow;

enum class GeometryMode{NONE, NORMAL, FLAT};


struct Modes{
    PostProcessMode ppMode = PostProcessMode::PASSTHROUGH;
    GeometryMode geomMode = GeometryMode::NONE;
};

struct GLContextInfo
{
    struct {
        GLint Major, Minor;
        std::string Driver;
        std::string ShadingLanguage;
    } Version;
    std::string Vendor;
    std::string Renderer;
    std::list<std::string> SupportedExtensions;
    std::list<std::string> SupportedGLSLVersions;
};

class Renderer : public Callbacks
{

public:

    Renderer(GLFWwindow& window, Scene& scene, GUI& gui, Rectangle& screen, AssetManager& assetManager);
    ~Renderer();

    void render();
    void renderSkybox();
    void renderToGBuffer();
    void renderStencilPass();
    void renderShadows();
    void renderGeometryShader();
    void renderLightsSpheres(Light& light);
    void renderLightQuad(ShaderMode mode);
    void renderLightsGeometry();
    void renderShadowsCube(Light light);
    void renderBloom();
    uint convertEquirectanguralToCube(Texture texture, const int width = 512, const int height = 512);
    uint cubeMapConvolution(Texture texture, const int width = 32, const int height = 32);
    uint prefilterEnvMap(uint texture, const int width = 128, const int height = 128);
    GLContextInfo getContextInfos();

    void renderPostProcess();
    void renderLightScatter();
    void renderPostProcess(Texture& texture, PostProcessMode mode);

    bool loadShaders();

    void framebufferSizeCallback() override;

    void printContextInfo();

    inline std::map<PostProcessMode, ShaderProgram>& ppShaders()
    {
        return m_ppShaders;
    }

    inline std::map<ShaderMode, ShaderProgram>& shaders()
    {
        return m_shaders;
    }

    inline FrameBuffer primaryFBO() const
    {
        return m_primaryFBO;
    }

    inline FrameBuffer secondaryFBO() const
    {
        return m_secondaryFBO;
    }

    inline GBuffer gBuffer() const
    {
        return m_gBuffer;
    }

    inline MeshInfo ppInfo() const
    {
        return m_ppInfo;
    }

    inline bool displayWireFrame() const
    {
        return m_displayWireframe;
    }

    inline bool displayNormals() const
    {
        return m_displayNormals;
    }

    inline bool displayLights() const
    {
        return m_displayLights;
    }

    inline bool& displayWireFrame()
    {
        return m_displayWireframe;
    }

    inline bool& displayNormals()
    {
        return m_displayNormals;
    }

    inline bool& displayLights()
    {
        return m_displayLights;
    }

    inline bool displayDeferredNormalsOnly() const
    {
        return m_displayDeferredNormalsOnly;
    }

    inline bool displayDeferredScaterredOnly() const
    {
        return m_displayDeferredScaterredOnly;
    }

    inline bool displayDeferredReflectedOnly() const
    {
        return m_displayDeferredReflectedOnly;
    }

    inline bool displayDeferredAmbientOnly() const
    {
        return m_displayDeferredAmbientOnly;
    }

    inline bool& displayDeferredNormalsOnly()
    {
        return m_displayDeferredNormalsOnly;
    }

    inline bool& displayDeferredScaterredOnly()
    {
        return m_displayDeferredScaterredOnly;
    }

    inline bool& displayDeferredReflectedOnly()
    {
        return m_displayDeferredReflectedOnly;
    }

    inline bool& displayDeferredPositionOnly()
    {
        return m_displayDeferredPositionsOnly;
    }

    inline bool& displayDeferredColorOnly()
    {
        return m_displayDeferredColorOnly;
    }

    inline bool& displayDeferredDepthOnly()
    {
        return m_displayDeferredDepthOnly;
    }

    inline bool& displayDeferredBloomOnly()
    {
        return m_displayDeferredBloomOnly;
    }

    inline bool& displayDeferredShadowsOnly()
    {
        return m_displayDeferredShadowsOnly;
    }

    inline bool& displayDeferredAmbientOnly()
    {
        return m_displayDeferredAmbientOnly;
    }

    inline bool& gammaCorrectionEnabled()
    {
        return m_gammaCorrection;
    }

    inline bool gammaCorrectionEnabled() const
    {
        return m_gammaCorrection;
    }

    inline void setGammaCorrection(const bool gamma)
    {
        m_gammaCorrection = gamma;
    }

    inline MeshInfo skyboxInfo() const
    {
        return m_skyboxInfo;
    }

    inline Modes modes() const
    {
        return m_modes;
    }

    inline void setPostProcessMode(PostProcessMode ppMode)
    {
        m_modes.ppMode = ppMode;
    }

    inline void updateWindowDimensions(const int width, const int height)
    {
        m_width = width;
        m_height = height;
    }

    inline void updateFrameDimensions(const int fwidth, const int fheight)
    {
        m_fwidth = fwidth;
        m_fheight = fheight;
    }

    inline ShadowMap shadowMap() const
    {
        return m_shadowMap;
    }

    inline ShadowMapCube shadowMapCube() const
    {
        return m_shadowMapCube;
    }


private:

    GLDebugDrawer m_glDebugDrawer;

    bool m_displayNormals = false;
    bool m_displayWireframe = false;
    bool m_displayLights = false;
    bool m_displayDeferredNormalsOnly = false;
    bool m_displayDeferredScaterredOnly = false;
    bool m_displayDeferredReflectedOnly = false;
    bool m_displayDeferredPositionsOnly = false;
    bool m_displayDeferredDepthOnly = false;
    bool m_displayDeferredAmbientOnly = false;
    bool m_displayDeferredBloomOnly = false;
    bool m_displayDeferredColorOnly = false;
    bool m_displayDeferredShadowsOnly = false;
    bool m_gammaCorrection = true;

    Rectangle& m_screen;
    Scene& m_scene;
    GUI& m_gui;
    AssetManager& m_assetManager;

    float m_time;
    float m_previousTime;
    float m_deltaTime;

    Modes m_modes;

    int m_width = 800;
    int m_height = 600;
    int m_fwidth = 800;
    int m_fheight = 600;

    uint m_captureFBO, m_captureRBO;
    uint m_envCubeMap;
    uint m_prefilteredMap;
    uint m_irradianceMap;
    uint m_brdfLUT;

    GLuint m_queries[11];
    GLuint m_queryResults[11];

    bool m_pbr = true;
    GLFWwindow& m_window;
    PerfGraph m_fpsGraph;
    PerfGraph m_cpuGraph;
    GLContextInfo m_glContextInfo;

    MeshInfo m_skyboxInfo;
    MeshInfo m_ppInfo;

    ShaderProgram m_lightScatterShader;

    std::map<ShaderMode, ShaderProgram> m_shaders;
    std::map<PostProcessMode, ShaderProgram> m_ppShaders;
    Texture m_bloomTexture;
    GBuffer m_gBuffer;
    FrameBuffer m_primaryFBO;
    FrameBuffer m_secondaryFBO;
    FrameBuffer m_bloomFBO;
    FrameBuffer m_bloomFBObis;
    FrameBuffer m_scatterFBO;
    FrameBuffer m_renderFBO;
    ShadowMap m_shadowMap;
    ShadowMapCube m_shadowMapCube;

    glm::mat4 m_captureProjection;
    glm::mat4 m_captureViews[6];

};

#endif // RENDERER_H
