#ifndef EDITBOX_H
#define EDITBOX_H

#include "container.h"
#include <string>
#include "slider.h"

class EditBoxBase : public Container
{

public:

    EditBoxBase(GUI& gui);
    EditBoxBase(GUI& gui, Rectangle r);

    void draw(NVGcontext* vg) override;

};

class EditBox : public UIElement
{

public:

    EditBox(GUI& gui, std::string text);
    EditBox(GUI& gui, Rectangle r, std::string text);

    inline std::string text() const
    {
        return m_text;
    }

    void draw(NVGcontext* vg) override;

private:

    std::string m_text;
};

class EditBoxNum : public UIElement
{

public:

    EditBoxNum(GUI& gui, std::string text, std::string units);
    EditBoxNum(GUI& gui, std::string text, std::string units, GLfloat* listened);

    void scrollCallback(double xoffset, double yoffset) override;

    inline std::string text() const
    {
        return m_text;
    }
    inline std::string units() const
    {
        return m_units;
    }

    inline void setText(const std::string text)
    {
        m_text = text;
    }

    inline void setUnits(const std::string units)
    {
        m_units = units;
    }

    inline void setListenedValue(float& listened)
    {
        m_listenedValue = &listened;
    }

    void draw(NVGcontext* vg) override;

    inline void setCanEdit(const bool b)
    {
        m_canEdit = b;
    }

    inline void setEditStep(const float s)
    {
        m_editStep = s;
    }

    void update();

private:
    float m_lowestEditValue;
    float m_highestEditValue;
    bool m_canEdit = false;
    float m_editStep = 1.0f;
    float* m_listenedValue = nullptr;
    std::string m_text;
    std::string m_units;
};
#endif // EDITBOX_H
