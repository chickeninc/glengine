#include "widget.h"
#include <iostream>
#include <math.h>
#include <memory>
#include <GLFW/glfw3.h>

Widget::Widget(GUI& gui)
    : Container(gui),
      m_title("Widget")
{
    m_type = Type::WIDGET;
}

Widget::Widget(GUI& gui, std::string title, Rectangle r)
    : Container(gui, r),
      m_title(title),
      m_savedBounds(r)
{
    m_header = Rectangle(r.x()+1, r.y()+1, r.x2()-1, r.y()+1 + HEADERHEIGHT);
    m_window = Rectangle(r.x(), r.y()+HEADERHEIGHT+1, r.x2(), r.y2()-1);
    m_bounds.description();
    m_header.description();
    m_window.description();
    m_type = Type::WIDGET;
}

GLvoid Widget::translate(int x, int y)
{
    if(m_isLocked)
        return;

    if(m_isExpanded) {
        if(m_bounds.x() + x > 0 && m_bounds.x2() + x < m_screen.width()) {
            m_bounds.translate(x, 0);
            m_header.translate(x, 0);
            m_window.translate(x, 0);

            for (Child c : m_children) {
                c->translate(x, 0);
            }
        }

        if(m_bounds.y() + y > 0 && m_bounds.y2() + y < m_screen.height()) {
            m_bounds.translate(0, y);
            m_header.translate(0, y);
            m_window.translate(0, y);

            for (Child c : m_children) {
                c->translate(0, y);
            }
        }
    } else {
        if(m_header.x() + x > 0 && m_header.x2() + x < m_screen.width()) {
            m_bounds.translate(x, 0);
            m_header.translate(x, 0);
            m_window.translate(x, 0);

            for (Child c : m_children) {
                c->translate(x, 0);
            }
        }

        if(m_header.y() + y > 0 && m_header.y2() + y < m_screen.height()) {
            m_bounds.translate(0, y);
            m_header.translate(0, y);
            m_window.translate(0, y);

            for (Child c : m_children) {
                c->translate(0, y);
            }
        }
    }

}

GLvoid Widget::setVisible(GLboolean b)
{
    m_isVisible = b;
    for(Child c : m_children)
        c->setVisible(b);
}

GLvoid Widget::updateChildren()
{

    int i = 0;
    for(Child c : m_children){
        int height = (m_window.height() / m_children.size());
        int x = m_window.x();
        int y = m_window.y() + (i * height);
        int x2 = m_window.x2();
        int y2 = y + (height);
        Rectangle r(x,y,x2,y2);
        c->setBounds(r);
        i++;
    }
}


GLvoid Widget::draw(NVGcontext* vg)
{
    if(!m_isVisible)
        return;

    float cornerRadius = 5.0f;
    NVGpaint shadowPaint;
    NVGpaint headerPaint;

    nvgSave(vg);
    //	nvgClearState(vg);

    Rectangle r = m_bounds;
    Rectangle r2 = m_header;


    if(m_isExpanded)
    {
        m_bounds.contains(m_mouse.x(), m_mouse.y()) ?
                    nvgFillColor(vg, nvgRGBA(58,60,64,255)) :
                    nvgFillColor(vg, nvgRGBA(58,60,64,223));

    } else nvgFillColor(vg, nvgRGBA(58,60,64,128));

    nvgBeginPath(vg);
    nvgFill(vg);

    // Window


    nvgBeginPath(vg);

    if(m_isExpanded) {
        nvgRoundedRect(vg, r.x(),r.y(), r.width(),r.height(), cornerRadius);
    } else {
        nvgRoundedRect(vg, r2.x(),r2.y(), r2.width(),r2.height(), cornerRadius);
    }

    nvgFill(vg);

    if(m_isExpanded) {
        // Drop shadow
        shadowPaint = nvgBoxGradient(vg, r.x(),r.y()+2, r.width(),r.height(), cornerRadius*2, 10, nvgRGBA(0,0,0,128), nvgRGBA(0,0,0,0));
        nvgBeginPath(vg);
        nvgRect(vg, r.x()-10,r.y()-10, r.width()+20,r.height()+30);
        nvgRoundedRect(vg, r.x(),r.y(), r.width(),r.height(), cornerRadius);
        nvgPathWinding(vg, NVG_HOLE);
        nvgFillPaint(vg, shadowPaint);
        nvgFill(vg);
    }


    // Header
    headerPaint = nvgLinearGradient(vg, r2.x(),r2.y(),r2.x(),r2.y()+15, nvgRGBA(255,255,255,8), nvgRGBA(0,0,0,16));
    nvgBeginPath(vg);
    nvgRoundedRect(vg, r2.x(),r2.y(), r2.width() ,r2.height(), cornerRadius-1);
    nvgFillPaint(vg, headerPaint);
    nvgFill(vg);
    nvgBeginPath(vg);
    nvgMoveTo(vg, r.x()+0.5f, r.y()+0.5f+ r2.height());
    nvgLineTo(vg, r.x()+0.5f+r.width()-1, r.y()+0.5f+ r2.height());
    nvgStrokeColor(vg, nvgRGBA(0,0,0,32));
    nvgStroke(vg);

    nvgFontSize(vg, 18.0f);
    nvgFontFace(vg, "sans-bold");
    nvgTextAlign(vg,NVG_ALIGN_CENTER|NVG_ALIGN_MIDDLE);

    nvgFontBlur(vg,2);
    nvgFillColor(vg, nvgRGBA(0,0,0,128));
    nvgText(vg, r.x()+r.width()/2,r.y()+16+1, m_title.data(), NULL);

    nvgFontBlur(vg,0);
    nvgFillColor(vg, nvgRGBA(220,220,220,160));
    nvgText(vg, r.x()+r.width()/2,r.y()+16, m_title.data(), NULL);

    nvgRestore(vg);
}

GLvoid Widget::setBounds(Rectangle r)
{
    m_savedBounds = m_bounds;
    m_bounds = r;
    Rectangle rect = r;
    rect.setY2(r.y() + HEADERHEIGHT);
    m_window = Rectangle(r.x(), r.y()+HEADERHEIGHT+1, r.x2(), r.y2()-1);
    m_header = rect;
    updateChildren();
}

GLvoid Widget::setHeader(Rectangle r)
{
    float deltaH = m_header.height() - r.height();
    float boundsHeight = m_bounds.height();
    m_header = r;
    m_bounds.setX(r.x());
    m_bounds.setY(r.y());
    m_bounds.setX2(r.x2());
    m_bounds.setY2(r.y() + boundsHeight + deltaH);

    m_window = Rectangle(r.x(), r.y()+HEADERHEIGHT+1, r.x2(), m_bounds.y2()-1);
    updateChildren();
}

GLvoid Widget::expand()
{
    for(Child c : children())
        c->setVisible(true);

    if(m_bounds.x2() > m_screen.width())
        translate(m_screen.width() - m_bounds.x2() -1, 0);
    if(m_bounds.y2() > m_screen.height())
        translate(0, m_screen.height() - m_bounds.y2() -1);
    updateChildren();
    m_isExpanded = true;
}

void Widget::mouseCallback(int button, int action)
{
    if(!m_header.contains(m_mouse.x(), m_mouse.y()))
        Container::mouseCallback(button, action);
    else if (action == GLFW_PRESS && button == GLFW_MOUSE_BUTTON_RIGHT) flipExpand();
}
