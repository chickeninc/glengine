#ifndef VERTEXBUFFER_H
#define VERTEXBUFFER_H

#include "GL/glew.h"

class VertexBuffer
{
public:
    VertexBuffer();

    inline GLuint id() const
    {
        return m_id;
    }

    inline void bind() const
    {
        glBindBuffer(GL_ARRAY_BUFFER, m_id);
    }

private:

    GLuint m_id = 0;
};

#endif // VERTEXBUFFER_H
