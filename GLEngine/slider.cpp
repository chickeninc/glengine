#include "slider.h"

Slider::Slider(GUI& gui, float min, float max)
    :UIElement(gui),
      m_minValue(min),
      m_maxValue(max)
{
    m_type = Type::SLIDER;
    m_maxHeight = 40;
}

Slider::Slider(GUI& gui, float position, float min, float max)
    :UIElement(gui),
      m_position(position),
      m_minValue(min),
      m_maxValue(max)
{
    m_type = Type::SLIDER;
    m_maxHeight = 30;
}

void Slider::cursorCallback(double xpos, double ypos)
{
    if(m_mouse.lbDown() && (m_mouseLock))
    {
        setPosition( (m_mouse.x() - m_bounds.x()) / m_bounds.width());
        if(m_affectedValue)
            *m_affectedValue = m_value;
    }


}

void Slider::draw(NVGcontext* vg)
{
    float pos = m_position;
    Rectangle r = m_bounds;
    float x = r.x()      + m_offsetX;
    float y = r.y()      + m_offsetY;
    float w = r.width()  - (2*m_offsetX);
    float h = r.height() - (2*m_offsetY);
    NVGpaint bg, knob;
    float cy = y+(int)(h*0.5f);
    float maxKnobRadius = 8.0f;
    float kr = ((int)(h*0.25f)) > maxKnobRadius ? maxKnobRadius : (int)(h*0.25f);




    nvgSave(vg);
    //nvgClearState(vg);

    // Slot
    bg = nvgBoxGradient(vg, x,cy-2+1, w,4, 2,2, nvgRGBA(0,0,0,32), nvgRGBA(0,0,0,128));
    nvgBeginPath(vg);
    nvgRoundedRect(vg, x,cy-2, w,4, 2);
    nvgFillPaint(vg, bg);
    nvgFill(vg);

    // Knob Shadow
    bg = nvgRadialGradient(vg, x+(int)(pos*w),cy+1, kr-3,kr+3, nvgRGBA(0,0,0,64), nvgRGBA(0,0,0,0));
    nvgBeginPath(vg);
    nvgRect(vg, x+(int)(pos*w)-kr-5,cy-kr-5,kr*2+5+5,kr*2+5+5+3);
    nvgCircle(vg, x+(int)(pos*w),cy, kr);
    nvgPathWinding(vg, NVG_HOLE);
    nvgFillPaint(vg, bg);
    nvgFill(vg);

    // Knob
    knob = nvgLinearGradient(vg, x,cy-kr,x,cy+kr, nvgRGBA(255,255,255,16), nvgRGBA(0,0,0,16));
    nvgBeginPath(vg);
    nvgCircle(vg, x+(int)(pos*w),cy, kr-1);
    nvgFillColor(vg, nvgRGBA(40,43,48,255));
    nvgFill(vg);
    nvgFillPaint(vg, knob);
    nvgFill(vg);

    nvgBeginPath(vg);
    nvgCircle(vg, x+(int)(pos*w),cy, kr-0.5f);
    nvgStrokeColor(vg, nvgRGBA(0,0,0,92));
    nvgStroke(vg);

    nvgRestore(vg);
}
