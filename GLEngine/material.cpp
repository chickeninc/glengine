#include "material.h"

Material::Material(const float roughness, const float metallic)
    :m_roughness{roughness},
      m_metallic{metallic}
{

}

void Material::addTexture(Texture& tex)
{
    switch (tex.type()) {
    case TextureType::ALBEDO:
        m_albedoMap = &tex;
        break;
    case TextureType::NORMAL:
        m_normalMap = &tex;
        break;
    case TextureType::HEIGHT:
        m_heightMap = &tex;
        break;
    case TextureType::AO:
        m_aoMap = &tex;
        break;
    case TextureType::ROUGHNESS:
        m_roughnessMap = &tex;
        break;
    case TextureType::METALLIC:
        m_metallicMap = &tex;
        break;
    }

    m_textures.push_back(tex);
}
