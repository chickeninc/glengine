#include "light.h"

GLuint Light::s_id = 0;

Light::Light()
{
    m_id = Light::s_id++;

}

Light::Light(glm::vec3 color)
    :m_color(color)
{m_id = Light::s_id++;}

DirectionalLight::DirectionalLight()
    :Light()
{
    m_isLocal = false;
    m_isSpot = false;
}

DirectionalLight::DirectionalLight(glm::vec3 color, glm::vec3 direction, glm::vec3 halfVector)
    :Light(color)
{
    m_isLocal = false;
    m_isSpot = false;
    m_position = direction;
    m_halfVector = halfVector;

    GLfloat near = 0.1f;
    GLfloat far = 142.0f;

    m_lightSpaceMatrix = glm::ortho(-50.0f, 50.0f, -50.0f, 50.0f, near, far)
            * glm::lookAt(m_position * glm::vec3( 50, 50, 50),
                          glm::vec3( 0.0f, 0.0f,  0.0f),
                          glm::vec3( 0.0f, 1.0f,  0.0f));

}

PointLight::PointLight()
    :Light()
{
    m_isLocal = true;
    m_isSpot = false;
}

PointLight::PointLight(glm::vec3 color, glm::vec3 position, GLfloat radius, GLfloat strength)
    :Light(color)
{
    m_isLocal = true;
    m_isSpot = false;
    m_position = position;
    m_radius = radius;
    m_strength = strength;

}

SpotLight::SpotLight()
    :Light()
{
    m_isLocal = true;
    m_isSpot = true;
}

SpotLight::SpotLight(glm::vec3 color, glm::vec3 position, glm::vec3 direction, GLfloat radius, GLfloat strength)
    :Light(color)
{
    m_isLocal = true;
    m_isSpot = true;
    m_position = position;
    m_direction = direction;    
    m_radius = radius;    
    m_strength = strength;
}
