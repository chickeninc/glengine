#include "model.h"
#include <stdio.h>
#include <iostream>
#include <string.h>


Model::Model()
{

}

Model::Model(Mesh mesh)
    :m_mesh(mesh),
      m_info(mesh),
      m_material()
{

}


Model::Model(Mesh mesh, Material mat)
    :m_mesh(mesh),
      m_info(mesh),
      m_material(mat)
{

}


void Model::render(ShaderProgram shader, GLenum mode)
{
    if(m_material.albedoMap())
    {
        glActiveTexture(GL_TEXTURE0);
        m_material.albedoMap()->Use();
        glUniform1i(glGetUniformLocation(shader.program(),"albedoMap"), 0);
    }

    if(m_material.metallicMap())
    {
        glActiveTexture(GL_TEXTURE1);
        m_material.metallicMap()->Use();
        glUniform1i(glGetUniformLocation(shader.program(),"metallicMap"), 1);
    }

    if(m_material.normalMap())
    {
        glActiveTexture(GL_TEXTURE2);
        m_material.normalMap()->Use();
        glUniform1i(glGetUniformLocation(shader.program(),"normalMap"), 2);
    }

    if(m_material.heightMap())
    {
        glActiveTexture(GL_TEXTURE3);
        m_material.heightMap()->Use();
        glUniform1i(glGetUniformLocation(shader.program(),"heightMap"), 3);
    }

    if(m_material.roughnessMap())
    {
        glActiveTexture(GL_TEXTURE4);
        m_material.roughnessMap()->Use();
        glUniform1i(glGetUniformLocation(shader.program(),"roughnessMap"), 4);
    }

    if(m_material.aoMap())
    {
        glActiveTexture(GL_TEXTURE5);
        m_material.aoMap()->Use();
        glUniform1i(glGetUniformLocation(shader.program(),"aoMap"), 5);
    }


    glUniform1f(glGetUniformLocation(shader.program(), "material.roughness"), m_material.roughness());
    glUniform1f(glGetUniformLocation(shader.program(), "material.metallic"), m_material.metallic());
    glBindVertexArray(m_info.VAO());
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_info.IBO());
    // Draw elements Indexed using IBO
    glDrawElements(mode, m_mesh.indices.size(), GL_UNSIGNED_INT, (void*)0);


    glBindVertexArray(0);
}

