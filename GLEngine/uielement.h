#ifndef UIELEMENT_H
#define UIELEMENT_H


#include "rectangle.h"
#include <vector>
#include "mouse.h"
#include "nanovg.h"
#include "callbacks.h"
#include "scene.h"
#include <iostream>
#include <functional>
#include <memory>
#include "listenable.h"



#define ICON_SEARCH 0x1F50D
#define ICON_CIRCLED_CROSS 0x2716
#define ICON_CHEVRON_RIGHT 0xE75E
#define ICON_CHECK 0x2713
#define ICON_LOGIN 0xE740
#define ICON_TRASH 0xE729

enum Type{UIELEMENT, WIDGET, CONTAINER, BUTTON, CHECKBOX, DROPDOWN, EDITBOX,
          EDITBOXNUM, LABEL, PARAGRAPH, SEARCHBOX, SLIDER, COLORWHEEL, EDITBOXBASE,
         MENU, LIGHTLABEL, LIGHTWIDGET};

class GUI;

class UIElement : public Callbacks
{

public:
    UIElement(GUI& gui);
    UIElement(GUI& gui, Rectangle r);

    virtual void translate(const int x, const int y);
    virtual void setBounds(const Rectangle r);
    virtual void draw(NVGcontext* vg) = 0;

    void mouseCallback(int button, int action) override;
    void cursorCallback(double xpos, double ypos) override;
    void keyCallback(int key, int scancode, int action, int mode) override;
    void scrollCallback(double xoffset, double yoffset) override;
    void framebufferSizeCallback() override;

    void description();
    float getRatio();
    void updateMouse(double xpos, double ypos);
    int isBlack(NVGcolor col);
    char* cpToUTF8(int cp, char* str);

    inline void setScreen(Rectangle& screen)
    {
        m_screen = screen;
    }
    inline Rectangle& getScreen() const
    {
        return m_screen;
    }

    inline Type type() const
    {
        return m_type;
    }

    inline float offsetX() const
    {
        return m_offsetX;
    }

    inline float offsetY() const
    {
        return m_offsetY;
    }

    inline void setOffset(const float x, const float y)
    {
        m_offsetX = x;
        m_offsetY = y;
    }

    inline virtual void setVisible(const bool b)
    {
        m_isVisible = b;
    }

    inline bool isVisible() const
    {
        return m_isVisible;
    }

    inline Rectangle& bounds()
    {
        return m_bounds;
    }

    inline void lockMouse()
    {
        m_mouseLock = true;
    }

    inline void unlockMouse()
    {
        m_mouseLock = false;
    }

    inline bool isMouseLocked() const
    {
        return m_mouseLock;
    }

    inline void setRatio(const float ratio)
    {
        m_ratio = ratio;
    }

    inline uint maxHeight() const
    {
        return m_maxHeight;
    }

    inline uint id() const
    {
        return m_id;
    }

    inline void setHasPreferredHeight(const bool b)
    {
        m_hasPreferredHeight = b;
    }

    inline bool hasPreferredHeight() const
    {
        return m_hasPreferredHeight;
    }

    inline float preferredHeight() const
    {
        return m_preferredHeight;
    }

    inline void setPreferredHeight(const float h)
    {
        m_hasPreferredHeight = true;
        m_preferredHeight = h;
    }

protected:

    Type        m_type = Type::UIELEMENT;
    Rectangle   m_bounds;

    Mouse&      m_mouse;
    Rectangle&  m_screen;

    bool        m_hasPreferredHeight = false;
    bool        m_mouseOverThis = false;
    bool        m_mouseLock = false;
    bool        m_isVisible = true;

    float       m_preferredHeight = 30.0f;
    float       m_offsetX = 5.0f;
    float       m_offsetY = 5.0f;
    float       m_ratio = 1.0f;
    uint        m_maxHeight;

private:
    GUI& m_gui;
    uint m_id;
    static uint s_id;
};

#endif // UIELEMENT_H
