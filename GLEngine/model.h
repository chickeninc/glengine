#ifndef MODEL_H
#define MODEL_H

#include <GL/glew.h>

#include <vector>
#include "texture.h"
#include "material.h"
#include <stdio.h>      /* printf, scanf, puts, NULL */
#include "mesh.h"
#include "meshinfo.h"
#include <stdlib.h>     /* srand, rand */
#include <time.h>
#include <glm/vec3.hpp>
#include <glm/vec2.hpp>
#include "renderable.h"
#include "shaderprogram.h"

#include "bullet/examples/ThirdPartyLibs/Wavefront/tiny_obj_loader.h"

#include <btBulletDynamicsCommon.h>
#include "gamecomponent.h"

class Model : public GameComponent
{

public:

    Model();
    Model(Mesh mesh);
    Model(Mesh mesh, Material material);

    inline Mesh mesh() const
    {
        return m_mesh;
    }

    inline MeshInfo meshInfo() const
    {
        return m_info;
    }


    void render(ShaderProgram program, GLenum mode = GL_PATCHES) override;

    inline void setMaterial(const Material mat)
    {
        m_material = mat;
    }

    inline Material material() const
    {
        return m_material;
    }

    inline Material& material()
    {
        return m_material;
    }


private:
    Material m_material;
    Mesh m_mesh;
    MeshInfo m_info;
    MeshInfo m_physicsInfo;

};


#endif // MODEL_H
