#ifndef FRAMEBUFFER_H
#define FRAMEBUFFER_H

#include <GL/glew.h>
#include "texture.h"


class FrameBuffer
{

public:

    FrameBuffer();
    FrameBuffer(int width, int height);

    inline uint width() const
    {
        return m_width;
    }

    inline uint height() const
    {
        return m_height;
    }

    inline Texture texColorBuffer() const
    {
        return m_texColorBuffer;
    }

    inline Texture& texColorBuffer()
    {
        return m_texColorBuffer;
    }

    inline uint id() const
    {
        return m_id;
    }

    virtual void resize(const uint width, const uint height);

    virtual void use();
    virtual void clean();
    virtual void attach(Texture& texture, GLenum attachementPoint = GL_COLOR_ATTACHMENT0);

protected:

    uint m_width;
    uint m_height;
    uint m_id;

    uint m_renderBuffer;
    Texture m_texColorBuffer;
};

#endif // FRAMEBUFFER_H
