#include "searchbox.h"


SearchBox::SearchBox(GUI& gui, Rectangle r, std::string text)
    :UIElement(gui, r),
      m_text(text)
{
    m_type = Type::SEARCHBOX;
}

void SearchBox::draw(NVGcontext* vg)
{
    float x = m_bounds.x()             + m_offsetX;
    float y = m_bounds.y()             + m_offsetY;
    float h = m_bounds.height()        - (2*m_offsetX);
    float w = m_bounds.width()         - (2*m_offsetY);

    NVGpaint bg;
    char icon[8];
    float cornerRadius = h/2-1;

    // Edit
    bg = nvgBoxGradient(vg, x,y+1.5f, w,h, h/2,5, nvgRGBA(0,0,0,16), nvgRGBA(0,0,0,92));
    nvgBeginPath(vg);
    nvgRoundedRect(vg, x,y, w,h, cornerRadius);
    nvgFillPaint(vg, bg);
    nvgFill(vg);

    /*	nvgBeginPath(vg);
                    nvgRoundedRect(vg, x+0.5f,y+0.5f, w-1,h-1, cornerRadius-0.5f);
                    nvgStrokeColor(vg, nvgRGBA(0,0,0,48));
                    nvgStroke(vg);*/

    nvgFontSize(vg, h*1.3f);
    nvgFontFace(vg, "icons");
    nvgFillColor(vg, nvgRGBA(255,255,255,64));
    nvgTextAlign(vg,NVG_ALIGN_CENTER|NVG_ALIGN_MIDDLE);
    nvgText(vg, x+h*0.55f, y+h*0.55f, cpToUTF8(ICON_SEARCH,icon), NULL);

    nvgFontSize(vg, 20.0f);
    nvgFontFace(vg, "sans");
    nvgFillColor(vg, nvgRGBA(255,255,255,32));

    nvgTextAlign(vg,NVG_ALIGN_LEFT|NVG_ALIGN_MIDDLE);
    nvgText(vg, x+h*1.05f,y+h*0.5f, m_text.data(), NULL);

    nvgFontSize(vg, h*1.3f);
    nvgFontFace(vg, "icons");
    nvgFillColor(vg, nvgRGBA(255,255,255,32));
    nvgTextAlign(vg,NVG_ALIGN_CENTER|NVG_ALIGN_MIDDLE);
    nvgText(vg, x+w-h*0.55f, y+h*0.55f, cpToUTF8(ICON_CIRCLED_CROSS,icon), NULL);
}
