#ifndef MODELLOADER_H
#define MODELLOADER_H

#include <string>
#include "model.h"

class ModelLoader
{
public:
    ModelLoader();

    bool Import3DFromFile( const std::string& path, std::map<std::string, Model>& models);

};

#endif // MODELLOADER_H
