#include "paragraph.h"
#include <string.h>
#include <stdio.h>


Paragraph::Paragraph(GUI& gui, std::string text)
    :UIElement(gui),
      m_text(text)
{
    m_type = Type::PARAGRAPH;
}

Paragraph::Paragraph(GUI& gui, Rectangle r, std::string text)
    :UIElement(gui, r),
      m_text(text)
{
    m_type = Type::PARAGRAPH;
}

void Paragraph::addText(std::string text)
{
    text.append("\n");
    text.append(m_text);
    setText(text);
}

void Paragraph::draw(NVGcontext* vg)
{


    NVGtextRow rows[3];
    NVGglyphPosition glyphs[100];
    const char* start;
    const char* end;
    int nrows, i, nglyphs, j, lnum = 0;
    float lineh;
    float caretx, px;
    float bounds[4];
    float a;
    float gx,gy;
    int gutter = 0;

    nvgSave(vg);

    nvgFontSize(vg, 18.0f);
    nvgFontFace(vg, "sans");
    nvgTextAlign(vg, NVG_ALIGN_LEFT|NVG_ALIGN_TOP);
    nvgTextMetrics(vg, NULL, NULL, &lineh);

    float x = m_bounds.x()             + m_offsetX;
    float y = m_bounds.y2()             - m_offsetY - lineh;
    float h = m_bounds.height()        - (2*m_offsetX);
    float w = m_bounds.width()         - (2*m_offsetY);

    // The text break API can be used to fill a large buffer of rows,
    // or to iterate over the text just few lines (or just one) at a time.
    // The "next" variable of the last returned item tells where to continue.
    start = m_text.data();
    end = m_text.data() + strlen(m_text.data());
    float totalHeight = 0;
    float debutHeight = h;
    while ((nrows = nvgTextBreakLines(vg, start, end, w, rows, 3))) {
        for (i = 0; i < nrows; i++) {
            if(totalHeight < debutHeight - lineh){
                NVGtextRow* row = &rows[i];
                int hit = m_mouse.x() > x && m_mouse.x() < (x+w) && m_mouse.y() >= y && m_mouse.y() < (y+lineh);

                nvgBeginPath(vg);
                nvgFillColor(vg, nvgRGBA(255,255,255,hit?64:16));
                nvgRect(vg, x, y, row->width, lineh);
                nvgFill(vg);

                nvgFillColor(vg, nvgRGBA(255,255,255,255));
                nvgText(vg, x, y, row->start, row->end);

                if (hit) {
                    caretx = (m_mouse.x() < x+row->width/2) ? x : x+row->width;
                    px = x;
                    nglyphs = nvgTextGlyphPositions(vg, x, y, row->start, row->end, glyphs, 100);
                    for (j = 0; j < nglyphs; j++) {
                        float x0 = glyphs[j].x;
                        float x1 = (j+1 < nglyphs) ? glyphs[j+1].x : x+row->width;
                        float gx = x0 * 0.3f + x1 * 0.7f;
                        if (m_mouse.x() >= px && m_mouse.x() < gx)
                            caretx = glyphs[j].x;
                        px = gx;
                    }
                    nvgBeginPath(vg);
                    nvgFillColor(vg, nvgRGBA(255,192,0,255));
                    nvgRect(vg, caretx, y, 1, lineh);
                    nvgFill(vg);

                    gutter = lnum+1;
                    gx = x - 10;
                    gy = y + lineh/2;
                }
                lnum++;
                y -= lineh;
                totalHeight +=lineh;
            }
        }
        // Keep going...
        start = rows[nrows-1].next;
    }

    if (gutter) {
        char txt[16];
        snprintf(txt, sizeof(txt), "%d", gutter);
        nvgFontSize(vg, 13.0f);
        nvgTextAlign(vg, NVG_ALIGN_RIGHT|NVG_ALIGN_MIDDLE);

        nvgTextBounds(vg, gx,gy, txt, NULL, bounds);

        nvgBeginPath(vg);
        nvgFillColor(vg, nvgRGBA(255,192,0,255));
        nvgRoundedRect(vg, (int)bounds[0]-4,(int)bounds[1]-2, (int)(bounds[2]-bounds[0])+8, (int)(bounds[3]-bounds[1])+4, ((int)(bounds[3]-bounds[1])+4)/2-1);
        nvgFill(vg);

        nvgFillColor(vg, nvgRGBA(32,32,32,255));
        nvgText(vg, gx,gy, txt, NULL);
    }

    /*
     y += 20.0f;

    nvgFontSize(vg, 13.0f);
    nvgTextAlign(vg, NVG_ALIGN_LEFT|NVG_ALIGN_TOP);
    nvgTextLineHeight(vg, 1.2f);

    nvgTextBoxBounds(vg, x,y, 150, "Hover your mouse over the text to see calculated caret position.", NULL, bounds);

    // Fade the tooltip out when close to it.
    gx = fabsf((mouse.x() - (bounds[0]+bounds[2])*0.5f) / (bounds[0] - bounds[2]));
    gy = fabsf((mouse.y() - (bounds[1]+bounds[3])*0.5f) / (bounds[1] - bounds[3]));
    a = maxf(gx, gy) - 0.5f;
    a = clampf(a, 0, 1);
    nvgGlobalAlpha(vg, a);

    nvgBeginPath(vg);
    nvgFillColor(vg, nvgRGBA(220,220,220,255));
    nvgRoundedRect(vg, bounds[0]-2,bounds[1]-2, (int)(bounds[2]-bounds[0])+4, (int)(bounds[3]-bounds[1])+4, 3);
    px = (int)((bounds[2]+bounds[0])/2);
    nvgMoveTo(vg, px,bounds[1] - 10);
    nvgLineTo(vg, px+7,bounds[1]+1);
    nvgLineTo(vg, px-7,bounds[1]+1);
    nvgFill(vg);

    nvgFillColor(vg, nvgRGBA(0,0,0,220));
    nvgTextBox(vg, x,y, 150, "Hover your mouse over the text to see calculated caret position.", NULL);
*/

    nvgRestore(vg);
}
