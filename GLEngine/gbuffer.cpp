#include "gbuffer.h"
#include <iostream>

GBuffer::GBuffer(int width, int height)
    :m_frameBuffer(width, height),
      m_gPosition(width, height, GL_RGBA16F, GL_RGBA, GL_FLOAT),
      m_gNormal(width, height, GL_RGBA16F, GL_RGBA, GL_FLOAT),
      m_gColorSpec(width, height, GL_RGBA16F, GL_RGBA, GL_FLOAT)
{
    m_frameBuffer.use();
    m_frameBuffer.attach(m_gPosition, GL_COLOR_ATTACHMENT0);
    m_frameBuffer.attach(m_gNormal, GL_COLOR_ATTACHMENT1);
    m_frameBuffer.attach(m_gColorSpec, GL_COLOR_ATTACHMENT2);

    GLuint attachments[3] = {GL_COLOR_ATTACHMENT0, GL_COLOR_ATTACHMENT1,
                             GL_COLOR_ATTACHMENT2};
    glDrawBuffers(3, attachments);

    if(glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
        std::cout << "ERROR::FRAMEBUFFER:: Framebuffer is not complete!" << std::endl;
    glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

void GBuffer::resize(const GLuint width, const GLuint height)
{
    m_width = width;
    m_height = height;

    m_frameBuffer.resize(width, height);

    m_gPosition.Use();
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA16F, m_width, m_height, 0, GL_RGBA, GL_FLOAT, NULL);
    glBindTexture(GL_TEXTURE_2D, 0);

    m_gNormal.Use();
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA16F, width, height, 0, GL_RGBA, GL_FLOAT, NULL);
    glBindTexture(GL_TEXTURE_2D, 0);

    m_gColorSpec.Use();
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA16F, width, height, 0, GL_RGBA, GL_FLOAT, NULL);
    glBindTexture(GL_TEXTURE_2D, 0);
}

void GBuffer::use()
{
    m_frameBuffer.use();
}

void GBuffer::clean()
{
    m_frameBuffer.clean();
    m_gPosition.clean();
    m_gNormal.clean();
    m_gColorSpec.clean();
}
