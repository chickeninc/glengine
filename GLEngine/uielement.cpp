#include "gui.h"
#include "uielement.h"
#include <string>
#include <iostream>
#include <GLFW/glfw3.h>


uint UIElement::s_id = 0;

UIElement::UIElement(GUI& gui)
    :m_gui(gui),
      m_screen(gui.screen()),
      m_mouse(gui.mouse())
{
    m_id = UIElement::s_id++;
}

UIElement::UIElement(GUI& gui, Rectangle r)
    :m_gui(gui),
      m_bounds(r),
      m_screen(gui.screen()),
      m_mouse(gui.mouse())
{
    m_id =UIElement::s_id++;
}

void UIElement::setBounds(Rectangle r)
{
    m_bounds = r;
}

void UIElement::translate(int x, int y)
{
    m_bounds.translate(x,y);
}

void UIElement::description()
{
    std::cout << "UIElement::";

    switch (m_type) {
    case UIELEMENT:
        std::cout << "UIELEMENT\n";
        break;
    case WIDGET:
        std::cout << "WIDGET\n";
        break;
    case CONTAINER:
        std::cout << "CONTAINER\n";
        break;
    case BUTTON:
        std::cout << "BUTTON\n";
        break;
    case CHECKBOX:
        std::cout << "CHECKBOX\n";
        break;
    case DROPDOWN:
        std::cout << "DROPDOWN\n";
        break;
    case EDITBOX:
        std::cout << "EDITBOX\n";
        break;
    case EDITBOXNUM:
        std::cout << "EDITBOXNUM\n";
        break;
    case LABEL:
        std::cout << "LABEL\n";
        break;
    case PARAGRAPH:
        std::cout << "PARAGRAPH\n";
        break;
    case SEARCHBOX:
        std::cout << "SEARCHBOX\n";
        break;
    case SLIDER:
        std::cout << "SLIDER\n";
        break;
    case COLORWHEEL:
        std::cout << "COLORWHEEL\n";
        break;
    case EDITBOXBASE:
        std::cout << "EDITBOXBASE\n";
        break;
    case MENU:
        std::cout << "MENU\n";
        break;
    default:
        std::cout << "UNKNOWN\n";
        break;
    }

    m_bounds.description();
}

float UIElement::getRatio(){
    if(m_ratio == 0)
        return 1.0f;
    return m_ratio;
}

void UIElement::mouseCallback(int button, int action)
{

}

void UIElement::cursorCallback(double xpos, double ypos)
{
    if(m_bounds.contains(xpos, ypos))
        m_mouseOverThis = true;
    else m_mouseOverThis = false;
}

int UIElement::isBlack(NVGcolor col)
{
    if( col.r == 0.0f && col.g == 0.0f && col.b == 0.0f && col.a == 0.0f )
    {
        return 1;
    }
    return 0;
}

char* UIElement::cpToUTF8(int cp, char *str)
{
    int n = 0;
    if (cp < 0x80) n = 1;
    else if (cp < 0x800) n = 2;
    else if (cp < 0x10000) n = 3;
    else if (cp < 0x200000) n = 4;
    else if (cp < 0x4000000) n = 5;
    else if (cp <= 0x7fffffff) n = 6;
    str[n] = '\0';
    switch (n) {
    case 6: str[5] = 0x80 | (cp & 0x3f); cp = cp >> 6; cp |= 0x4000000;
    case 5: str[4] = 0x80 | (cp & 0x3f); cp = cp >> 6; cp |= 0x200000;
    case 4: str[3] = 0x80 | (cp & 0x3f); cp = cp >> 6; cp |= 0x10000;
    case 3: str[2] = 0x80 | (cp & 0x3f); cp = cp >> 6; cp |= 0x800;
    case 2: str[1] = 0x80 | (cp & 0x3f); cp = cp >> 6; cp |= 0xc0;
    case 1: str[0] = cp;
    }
    return str;
}

void UIElement::keyCallback(int key, int scancode, int action, int mode)
{

}

void UIElement::scrollCallback(double xoffset, double yoffset)
{

}

void UIElement::framebufferSizeCallback()
{

}

