#include "gldebugdrawer.h"
#include <vector>

GLDebugDrawer::GLDebugDrawer(Camera &cam)
    :m_camera(cam),
      m_shaderProgram({
                      Shader(("/home/lebasith/Git/cpp/GLEngine/shaders/debug.vert"), GL_VERTEX_SHADER),
                      Shader(("/home/lebasith/Git/cpp/GLEngine/shaders/debug.frag"), GL_FRAGMENT_SHADER)
                      })
{
    setDebugMode(btIDebugDraw::DBG_DrawWireframe | btIDebugDraw::DBG_DrawNormals
                 | btIDebugDraw::DBG_DrawAabb| btIDebugDraw::DBG_DrawContactPoints);
}

void GLDebugDrawer::drawLine(const btVector3 &from, const btVector3 &to, const btVector3 &color)
{
    drawLine(from, to, color, color);
}

void GLDebugDrawer::drawLine(const btVector3 &from, const btVector3 &to, const btVector3 &fromColor, const btVector3 &toColor)
{
    m_shaderProgram.Use();

    std::vector<float> data;
    data.push_back(from.getX());
    data.push_back(from.getY());
    data.push_back(from.getZ());
    data.push_back(fromColor.getX());
    data.push_back(fromColor.getY());
    data.push_back(fromColor.getZ());
    data.push_back(to.getX());
    data.push_back(to.getY());
    data.push_back(to.getZ());
    data.push_back(toColor.getX());
    data.push_back(toColor.getY());
    data.push_back(toColor.getZ());

    GLuint vbo;
    glGenBuffers(1, &vbo);
    glBindBuffer(GL_ARRAY_BUFFER, vbo);
    glBufferData(GL_ARRAY_BUFFER, sizeof(float) * data.size(), data.data(), GL_STATIC_DRAW);
    glUniformMatrix4fv(glGetUniformLocation(m_shaderProgram.program(), "projection"), 1, GL_FALSE, m_camera.getProjection());
    glUniformMatrix4fv(glGetUniformLocation(m_shaderProgram.program(), "view"), 1, GL_FALSE, m_camera.getView());

    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(float), (void*) 0);
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(float), (void*) (3 * sizeof(float)));
    glEnableVertexAttribArray(1);
/*
             glVertexPointer( 3,
                         GL_FLOAT,
                         0,
                         &tmpColor );
*/
             glPointSize( 5.0f );
             glDrawArrays( GL_POINTS, 0, 2 );
             glDrawArrays( GL_LINES, 0, 2 );

    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glUseProgram(0);



}

void GLDebugDrawer::draw3dText(const btVector3 &location, const char *textString)
{

}

void GLDebugDrawer::drawContactPoint(const btVector3 &PointOnB, const btVector3 &normalOnB, btScalar distance, int lifeTime, const btVector3 &color)
{

}


