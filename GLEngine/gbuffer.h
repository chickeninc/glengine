#ifndef GBUFFER_H
#define GBUFFER_H

#include "framebuffer.h"

class GBuffer
{
public:    
    GBuffer(int width, int height);

    void resize(const GLuint width, const GLuint height);
    void use();
    void clean();

    inline Texture gPosition() const
    {
        return m_gPosition;
    }

    inline Texture gNormal() const
    {
        return m_gNormal;
    }

    inline Texture gColorSpec() const
    {
        return m_gColorSpec;
    }

    inline FrameBuffer fbo() const
    {
        return m_frameBuffer;
    }

private:
    FrameBuffer m_frameBuffer;

    GLuint m_width;
    GLuint m_height;

    Texture m_gPosition;
    Texture m_gNormal;
    Texture m_gColorSpec;

};

#endif // GBUFFER_H
