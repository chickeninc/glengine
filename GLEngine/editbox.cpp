#include "editbox.h"


EditBox::EditBox(GUI& gui, std::string text)
    :UIElement(gui),
      m_text(text)
{
    m_type = Type::EDITBOX;

}

EditBox::EditBox(GUI& gui, Rectangle r, std::string text)
    :UIElement(gui, r),
      m_text(text)
{
    m_type = Type::EDITBOX;
}

EditBoxBase::EditBoxBase(GUI& gui)
    :Container(gui)
{
    m_type = Type::EDITBOXBASE;
}

EditBoxBase::EditBoxBase(GUI& gui, Rectangle r)
    :Container(gui, r)
{
    m_type = Type::EDITBOXBASE;
}

void EditBox::draw(NVGcontext *vg)
{
    float x = m_bounds.x()             + m_offsetX;
    float y = m_bounds.y()             + m_offsetY;
    float h = m_bounds.height()        - (2*m_offsetX);
    float w = m_bounds.width()         - (2*m_offsetY);
    NVG_NOTUSED(w);

    NVGpaint bg;
    // Edit
    bg = nvgBoxGradient(vg, x+1,y+1+1.5f, w-2,h-2, 3,4, nvgRGBA(255,255,255,32), nvgRGBA(32,32,32,32));
    nvgBeginPath(vg);
    nvgRoundedRect(vg, x+1,y+1, w-2,h-2, 4-1);
    nvgFillPaint(vg, bg);
    nvgFill(vg);

    nvgBeginPath(vg);
    nvgRoundedRect(vg, x+0.5f,y+0.5f, w-1,h-1, 4-0.5f);
    nvgStrokeColor(vg, nvgRGBA(0,0,0,48));
    nvgStroke(vg);


    nvgFontSize(vg, 20.0f);
    nvgFontFace(vg, "sans");
    nvgFillColor(vg, nvgRGBA(255,255,255,64));
    nvgTextAlign(vg,NVG_ALIGN_LEFT|NVG_ALIGN_MIDDLE);
    nvgText(vg, x+h*0.3f,y+h*0.5f, m_text.data(), NULL);
}

void EditBoxBase::draw(NVGcontext* vg)
{
    float x = m_bounds.x()             + m_offsetX;
    float y = m_bounds.y()             + m_offsetY;
    float h = m_bounds.height() - (2*m_offsetX);
    float w = m_bounds.width()         - (2*m_offsetY);

    NVGpaint bg;
    // Edit
    bg = nvgBoxGradient(vg, x+1,y+1+1.5f, w-2,h-2, 3,4, nvgRGBA(255,255,255,32), nvgRGBA(32,32,32,32));
    nvgBeginPath(vg);
    nvgRoundedRect(vg, x+1,y+1, w-2,h-2, 4-1);
    nvgFillPaint(vg, bg);
    nvgFill(vg);

    nvgBeginPath(vg);
    nvgRoundedRect(vg, x+0.5f,y+0.5f, w-1,h-1, 4-0.5f);
    nvgStrokeColor(vg, nvgRGBA(0,0,0,48));
    nvgStroke(vg);

}

EditBoxNum::EditBoxNum(GUI& gui, std::string text, std::string units)
    :UIElement(gui),
      m_text(text),
      m_units(units)
{
    m_type = Type::EDITBOXNUM;
}

EditBoxNum::EditBoxNum(GUI& gui, std::string text, std::string units, GLfloat* listened)
    :UIElement(gui),
      m_text(text),
      m_units(units),
      m_listenedValue(listened)
{
    m_type = Type::EDITBOXNUM;
}

void EditBoxNum::draw(NVGcontext *vg)
{
    float x = m_bounds.x()             + m_offsetX;
    float y = m_bounds.y()             + m_offsetY;
    float h = m_bounds.height()        - (2*m_offsetX);
    float w = m_bounds.width()         - (2*m_offsetY);

    if(m_listenedValue != nullptr)
        m_text = std::to_string(*m_listenedValue).substr(0, 7);
    else m_text = "NaN";

    NVG_NOTUSED(w);

    NVGpaint bg;
    // Edit
    bg = nvgBoxGradient(vg, x+1,y+1+1.5f, w-2,h-2, 3,4, nvgRGBA(255,255,255,32), nvgRGBA(32,32,32,32));
    nvgBeginPath(vg);
    nvgRoundedRect(vg, x+1,y+1, w-2,h-2, 4-1);
    nvgFillPaint(vg, bg);
    nvgFill(vg);

    nvgBeginPath(vg);
    nvgRoundedRect(vg, x+0.5f,y+0.5f, w-1,h-1, 4-0.5f);
    nvgStrokeColor(vg, nvgRGBA(0,0,0,48));
    nvgStroke(vg);

    float uw;

    uw = nvgTextBounds(vg, 0,0, m_units.data(), NULL, NULL);

    nvgFontSize(vg, 18.0f);
    nvgFontFace(vg, "sans");
    nvgFillColor(vg, nvgRGBA(255,255,255,64));
    nvgTextAlign(vg,NVG_ALIGN_RIGHT|NVG_ALIGN_MIDDLE);
    nvgText(vg, x+w-h*0.3f,y+h*0.5f, m_units.data(), NULL);

    nvgFontSize(vg, 20.0f);
    nvgFontFace(vg, "sans");
    nvgFillColor(vg, nvgRGBA(255,255,255,128));
    nvgTextAlign(vg,NVG_ALIGN_RIGHT|NVG_ALIGN_MIDDLE);
    nvgText(vg, x+w-uw-h*0.5f,y+h*0.5f, m_text.c_str(), NULL);

}

void EditBoxNum::scrollCallback(double xoffset, double yoffset)
{
   if(m_listenedValue && m_canEdit)
   {
       const GLfloat f = m_editStep;
       yoffset > 0 ? (*m_listenedValue)+= f: (*m_listenedValue)-= f;
   }
}
