#ifndef POSTPROCESS_H
#define POSTPROCESS_H

#include <cstdint>

enum class PostProcessMode : std::uint16_t
{
    INVERT      = 1 << 0,
    GRAYSCALE   = 1 << 1,
    KERNEL      = 1 << 2,
    BLUR        = 1 << 3,
    GAUSSIANBLUR= 1 << 4,
    EDGE        = 1 << 5,
    CARTOON     = 1 << 6,
    FXAA        = 1 << 7,
    TONEMAPPING = 1 << 8,
    BLOOMFILTER = 1 << 9,
    PASSTHROUGH = 1 << 10
};

inline PostProcessMode operator ++(PostProcessMode& p, int)
{
    switch (p) {
    case PostProcessMode::INVERT:
        return p = PostProcessMode::GRAYSCALE;
        break;
    case PostProcessMode::GRAYSCALE:
        return p = PostProcessMode::KERNEL;
        break;
    case PostProcessMode::KERNEL:
        return p = PostProcessMode::BLUR;
        break;
    case PostProcessMode::BLUR :
        return p = PostProcessMode::GAUSSIANBLUR;
        break;
    case PostProcessMode::GAUSSIANBLUR :
        return p = PostProcessMode::EDGE;
        break;
    case PostProcessMode::EDGE :
        return p = PostProcessMode::CARTOON;
        break;
    case PostProcessMode::CARTOON :
        return p = PostProcessMode::FXAA;
        break;
    case PostProcessMode::FXAA :
        return p = PostProcessMode::TONEMAPPING;
        break;        
    case PostProcessMode::TONEMAPPING :
        return p = PostProcessMode::BLOOMFILTER;
        break;
    case PostProcessMode::BLOOMFILTER :
        return p = PostProcessMode::PASSTHROUGH;
        break;
    case PostProcessMode::PASSTHROUGH :
        return p = PostProcessMode::INVERT;
        break;
    }
}

inline PostProcessMode operator|(PostProcessMode a, PostProcessMode b)
{
    return static_cast<PostProcessMode>(static_cast<std::uint16_t>(a) | static_cast<std::uint16_t>(b));
}

inline bool operator&(PostProcessMode a, PostProcessMode b)
{
    return static_cast<std::uint16_t>(a) & static_cast<std::uint16_t>(b);
}

#endif // POSTPROCESS_H
