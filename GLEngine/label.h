#ifndef LABEL_H
#define LABEL_H

#include "uielement.h"
#include "widget.h"
#include <string>
#include "light.h"

class LightWidget;

class Label : public UIElement
{

public:

    Label(GUI& gui);
    Label(GUI& gui, std::string text);
    Label(GUI& gui, Rectangle r, std::string text);

    virtual void draw(NVGcontext* vg);

    inline std::string text() const
    {
        return m_text;
    }

protected:

    std::string m_text;
};

#endif // LABEL_H
